#!/bin/bash
while read -r line
do
	java -cp $HOME/bin/lib/StemSearch/StemSearch.jar bgu.bio.tools.PrepareData db.fasta rfam/$line.sto.fasta $line
done < $1

#!/usr/bin/python2
import sys
import os
import math
import random
import string

for i in range(1,len(sys.argv)):
	f = open(sys.argv[i],"r")
	data = ""
	allData = []
	sizes = []
	header = f.readline()
	line = f.readline()
	while line:
		line = line.strip()
		if line.startswith(">"):
			if (header != ""):
				allData.append((header,data))
				sizes.append(len(data))
			data = ""
			header = line
		else:
			data = data + line
		line = f.readline()
	
	if (header != ""):
		allData.append((header,data))
		sizes.append(len(data))
	avg = sum(sizes)*1.0/len(sizes)
	if (len(allData) >= 15 and len(allData) < 500 and avg > 50):
		print sys.argv[i] + " " + str(len(allData)) + " " + str(avg)
	f.close()
	

#!/usr/bin/env python2

import math
import sys
import re
import os
import shlex, subprocess

headerPattern = re.compile(r'#=GF\s+AC\s+(.*)',re.M)
typePattern = re.compile(r'#=GF\s+TP\s+Gene;(.*)',re.M)
f = open(sys.argv[1],"r")
data = ""
line = f.readline()
ignoreList = ["miRNA;","antitoxin;","CRISPR;"]
gotGroups = set([])
def handle(data):
	global headerPattern, typePattern, ignoreList,gotGroups
	m = typePattern.search(data)
	group = ""
	if m:
		group = m.group(1).strip()
	if not group in ignoreList:
		if group != "":
			gotGroups.add(group)
		m = headerPattern.search(data)
		name = ""
		if m:
			name = m.group(1)
		fname = sys.argv[2] + "/" + name + ".sto"
		outputFile = open(fname,"w")
		outputFile.write(data)
		outputFile.close()
		fasta = open(fname+".fasta","w")
		p = subprocess.Popen(['perl','/home/milon/s2f.pl',fname], stdin=subprocess.PIPE,stdout=fasta,
		stderr=subprocess.PIPE)
		out, err = p.communicate()
		os.remove(fname)
		fasta.close()
	
while line:
	line = line.strip()
	data = data + line + "\n"
	if (line == "# STOCKHOLM 1.0"):
		#found new header clear old
		if data != "":
			handle(data)
			data = line + "\n"
		
	line = f.readline()
f.close()

if data != "":
	handle(data)
	
print gotGroups

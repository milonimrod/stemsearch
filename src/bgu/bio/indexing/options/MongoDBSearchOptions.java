package bgu.bio.indexing.options;

import java.util.HashMap;

import org.apache.commons.cli.CommandLine;

public class MongoDBSearchOptions extends MongoDBOptions {
  public static final String MATRIX_DIRECTORY = "/config/matrix/";
  public static final String MATRIX_STRUCTURE = "stemSimilarity-structureMatrix";
  public static final String MATRIX_SEQUENCE = "stemSimilarity-sequenceMatrix";
  public static final String PARTITION_FUNCTION_T = "partitionFunction";
  public static final String RELATIVE_SCORE_T = "relativeScore";

  public MongoDBSearchOptions() {
    super("search");
  }

  public void buildOptions() {
    super.buildOptions();

    options.addOption("pval", "pval-collection", true,
        "Name of collection to be used for pvalue computation");
    options.addOption("rel", true, "Relative score value to be used. Default is 0.6");
    options.addOption("pf", true, "Partition function to be used. Default is 0.0001");

  }

  protected void parse(CommandLine line, HashMap<String, Object> argumentsMap) {
    super.parse(line, argumentsMap);

    argumentsMap.put(MATRIX_STRUCTURE, MATRIX_DIRECTORY + "STEMS-sim-RIBOSUM85-60.matrix");
    argumentsMap.put(MATRIX_SEQUENCE, MATRIX_DIRECTORY + "NUC-sim-RIBOSUM85-60.matrix");

    if (line.hasOption("pval")) {
      argumentsMap.put("pvalCollectionName", line.getOptionValue("pval"));
    }
    if (line.hasOption("pf")) {
      argumentsMap.put(PARTITION_FUNCTION_T, Double.parseDouble(line.getOptionValue("pf")));
    } else {
      argumentsMap.put(PARTITION_FUNCTION_T, 0.0001);
    }

    if (line.hasOption("rel")) {
      argumentsMap.put(RELATIVE_SCORE_T, Double.parseDouble(line.getOptionValue("rel")));
    } else {
      argumentsMap.put(RELATIVE_SCORE_T, 0.6);
    }
  }
}

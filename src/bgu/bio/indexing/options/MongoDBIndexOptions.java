package bgu.bio.indexing.options;

import java.util.HashMap;

import org.apache.commons.cli.CommandLine;

public class MongoDBIndexOptions extends MongoDBOptions {
  public final static String OVERRIDE_DATA = "override";
  public final static String INTERACTIVE = "interactive";

  public MongoDBIndexOptions() {
    super("indexer");
  }

  public void buildOptions() {
    super.buildOptions();
    options.addOption("o", "override", false,
        "Should the indexer override data if collection exist. use with caution.");

    options
        .addOption("i", "interactive", false, "Should the indexer run in an interactive manner.");
  }

  protected void parse(CommandLine line, HashMap<String, Object> argumentsMap) {
    super.parse(line, argumentsMap);

    if (line.hasOption("o")) {
      argumentsMap.put(OVERRIDE_DATA, true);
    } else {
      argumentsMap.put(OVERRIDE_DATA, false);
    }

    if (line.hasOption("i")) {
      argumentsMap.put(INTERACTIVE, true);
    } else {
      argumentsMap.put(INTERACTIVE, false);
    }
  }
}

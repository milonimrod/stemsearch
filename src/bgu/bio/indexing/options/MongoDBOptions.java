package bgu.bio.indexing.options;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.HashMap;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class MongoDBOptions {

  public static final String SERVER_LOCATION = "serverLocation";
  public static final String COLLECTION_NAME = "collectionName";
  public static final String DATABASE_NAME = "databaseName";
  public static final String FILTER_PROP_FILE = "propFile";
  public static final String THREADS = "threads";
  public static final String SINGLE_STRAND = "singleStrand";
  public static final String INTERACTIVE = "interactive";

  protected Options options = new Options();
  protected CommandLineParser parser = new GnuParser();
  protected String name;
  public String[] remaining;


  public MongoDBOptions(String name) {
    this.name = name;
  }

  public void buildOptions() {
    options.addOption("q", "quiet", false, "quiet mode. won't print anything to the screen.");
    options.addOption("v", "version", false, "print version and exit.");
    options.addOption("str", "single", false, "single strand running");
    options.addOption("h", "help", false, "print this help");
    options.addOption("db", true,
        "The name of the database to put the indexed stems (created if not exist)");
    options.addOption("col", true, "The name of the collection to be used.");
    Option o =
        new Option(
            "s",
            "server",
            true,
            "The address and port of the mongoDB server. Address is given in format of <server>:<port>. Default is localhost:27017");
    o.setArgName("address");
    options.addOption(o);
    options.addOption("e", true, "<Optional> - The stem extractor config file");
    options
        .addOption(
            "t",
            "threads",
            true,
            "The amount of threads to be used. The default is 2. On search phase, if a pvalue is calculated than 2 is the minimum.");

  }

  public void loadParams(String[] args, HashMap<String, Object> argumentsMap) {

    CommandLine line;
    try {
      line = parser.parse(options, args, true);
      remaining = line.getArgs();
      parse(line, argumentsMap);
    } catch (ParseException e) {
      e.printStackTrace();
    }

  }

  protected void parse(CommandLine line, HashMap<String, Object> argumentsMap) {
    if (line.hasOption("h")) {

      printHelp();
      System.exit(0);
    }

    if (line.hasOption("v")) {
      String version = getVersion();
      System.out.println(version);
      System.exit(0);
    }
    
    if (line.hasOption("str")) {
      argumentsMap.put(SINGLE_STRAND, true);
    }else{
      argumentsMap.put(SINGLE_STRAND, false);
    }

    if (line.hasOption("e")) {
      try {
        argumentsMap.put(FILTER_PROP_FILE, new FileInputStream(line.getOptionValue("e")));
      } catch (FileNotFoundException e) {
        System.err.println("Config file not found");
        System.exit(1);
      }
    } else {
      argumentsMap.put(FILTER_PROP_FILE,
          this.getClass().getResourceAsStream("/config/indexing.properties"));
    }

    if (line.hasOption("db")) {
      argumentsMap.put(DATABASE_NAME, line.getOptionValue("db"));
    } else {
      System.err.println("Must provide database name to use. (-db option)");
      System.exit(1);
    }

    if (line.hasOption("col")) {
      argumentsMap.put(COLLECTION_NAME, line.getOptionValue("col"));
    } else {
      System.err.println("Must provide database name to use. (-col option)");
      System.exit(1);
    }

    argumentsMap.put(SERVER_LOCATION, "localhost:27017");
    if (line.hasOption("s")) {
      String server = line.getOptionValue("s");
      // add default port if port is not given
      if (server.indexOf(':') < 0) {
        server = server + ":27017";
      }
      argumentsMap.put(SERVER_LOCATION, server);
    }

    if (line.hasOption("q")) {
      // revert out to null
      System.setOut(new PrintStream(new OutputStream() {
        public void write(int b) {
          // DO NOTHING
        }
      }));
    }

    if (line.hasOption("t")) {
      argumentsMap.put(THREADS, Integer.parseInt(line.getOptionValue("t")));
    } else {
      argumentsMap.put(THREADS, 2);
    }

  }

  public void printHelp() {
    HelpFormatter formatter = new HelpFormatter();
    formatter.setWidth(200);
    String version = getVersion();
    formatter.printHelp(name + " [Optionts] input", "", options, "Version " + version);
  }

  public String getVersion() {
    String version = "Not available";
    try {
      InputStream in = this.getClass().getResourceAsStream("/version.txt");
      BufferedReader buf = new BufferedReader(new InputStreamReader(in));
      version = buf.readLine();
      buf.close();
    } catch (IOException ex) {
    }
    return version;
  }
}

package bgu.bio.indexing;

import gnu.trove.list.array.TDoubleArrayList;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import bgu.bio.adt.tuples.IntPair;
import bgu.bio.adt.tuples.Triplet;
import bgu.bio.ds.rna.RNA;

public class BenchmarkResult {
  public static void main(String[] args) throws IOException {
    // read queries
    ArrayList<RNA> members = RNA.loadFromFile(args[0] + "/query.fasta", false);

    File directory = new File(args[0]);
    System.out.println(directory);

    File[] blastOutputs = directory.listFiles(new FilenameFilter() {
      @Override
      public boolean accept(File arg0, String arg1) {
        // return all output files in the directory
        return arg1.startsWith("blast-local") && arg1.endsWith(".txt");
      }
    });

    ArrayList<Triplet<Integer, Integer, String>> trueResults =
        loadRealPositionInformation(args[0] + "/info.txt");

    File[] outputs = directory.listFiles(new FilenameFilter() {
      @Override
      public boolean accept(File arg0, String arg1) {
        // return all output files in the directory
        return arg1.startsWith("output") && arg1.endsWith(".gff3");
      }
    });
    System.out.println(Arrays.toString(outputs));


    int i = 0;
    for (RNA member : members) {
      String summaryFile =
          args[0] + "/summary" + i + "_" + member.getHeader().replace("/", "_") + ".txt";
      BufferedWriter writer = new BufferedWriter(new FileWriter(summaryFile));
      for (File file : outputs) {
        HashMap<String, ArrayList<IntPair>> stemSearchResults = loadGFF3File(file);
        writer.write(file.getName() + "\t");
        print(stemSearchResults, trueResults, member, writer);
        writer.write("\n");
      }
      for (File file : blastOutputs) {
        HashMap<String, ArrayList<IntPair>> blastResults =
            loadBlastPositionInformation(file.getAbsolutePath());
        writer.write(file.getName() + "\t");
        print(blastResults, trueResults, member, writer);
        writer.write("\n");
      }
      writer.close();
      i++;
    }

  }

  /**
   * @param numberOfDocuments
   * @param resVector
   * @param onesUntil
   * @param recall
   * @param precision
   * @return
   */
  private static double calculateAVeP(int numberOfDocuments, int[] resVector, double[] recall,
      double[] precision) {
    final double[] onesUntil = new double[resVector.length];

    for (int i = 0; i < resVector.length; i++) {
      onesUntil[i] = i == 0 ? 0 : onesUntil[i - 1];
      if (resVector[i] == 1) {
        onesUntil[i] += 1;
      }
    }
    for (int i = 0; i < resVector.length; i++) {
      recall[i] = onesUntil[i] / (1.0 * numberOfDocuments);
      precision[i] = (1.0 * onesUntil[i] / (i + 1));
    }
    double aveP = 0;
    for (int i = 0; i < resVector.length; i++) {
      double dRecall = recall[i] - (i == 0 ? 0 : recall[i - 1]);
      if (Math.abs(dRecall) < 0.00001) {
        dRecall = 0;
      }
      aveP += precision[i] * dRecall;
    }
    return aveP / numberOfDocuments;
  }

  public static double print(HashMap<String, ArrayList<IntPair>> batchResults,
      ArrayList<Triplet<Integer, Integer, String>> trueResults, RNA member, BufferedWriter writer)
      throws IOException {
    if (batchResults.containsKey(member.getHeader())) {
      ArrayList<IntPair> resultForMember = batchResults.get(member.getHeader());
      int[] resultsVector = new int[resultForMember.size()];
      int pos = 0;
      for (IntPair res : resultForMember) {
        int val = findInResults(res, trueResults) == null ? 0 : 1;
        resultsVector[pos] = val;
        pos++;
      }

      double[] recall = new double[resultForMember.size()];
      double[] precision = new double[resultForMember.size()];
      calculateAVeP(trueResults.size(), resultsVector, recall, precision);
      double auc = calculateAUC(recall, precision);
      writer.write("" + auc);
      writer.write("\t" + recall[recall.length - 1]);
      writer.write("\t" + precision[precision.length - 1]);
      writer.write("\t" + print(resultsVector));
      writer.write("\t" + print(recall));
      writer.write("\t" + print(precision));

      return auc;
    } else {
      writer.write("\t\t\t\t\t");
      return 0;
    }
  }

  private static String print(int[] precision) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < precision.length; i++) {
      sb.append(" ");
      sb.append(precision[i]);
    }
    return sb.toString().trim();
  }

  private static String print(double[] precision) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < precision.length; i++) {
      sb.append(" ");
      sb.append(precision[i]);
    }
    return sb.toString().trim();
  }

  private static double calculateAUC(double[] recall, double[] precision) {
    double ans = 0;
    TDoubleArrayList x = new TDoubleArrayList();
    TDoubleArrayList y = new TDoubleArrayList();
    x.add(0);
    y.add(1);
    for (int i = 0; i < precision.length; i++) {
      x.add(recall[i]);
      y.add(precision[i]);
    }
    for (int p = 1; p < x.size(); p++) {
      double x1 = x.get(p);
      double x2 = x.get(p - 1);
      double y1 = y.get(p);
      double y2 = y.get(p - 1);
      double diffX = Math.abs(x1 - x2);
      double diffY = Math.abs(y1 - y2);
      double minY = Math.min(y1, y2);

      // rectangle
      ans += diffX * minY;
      // triangle
      ans += diffX * diffY;
    }
    return ans;
  }

  private static HashMap<String, ArrayList<IntPair>> loadGFF3File(File file) throws IOException {
    HashMap<String, ArrayList<IntPair>> tmpMap = new HashMap<String, ArrayList<IntPair>>();
    BufferedReader reader = new BufferedReader(new FileReader(file));
    String line = reader.readLine();
    while (line != null) {
      if (!line.startsWith("#") && !(line.trim().length() == 0)) {
        String[] sp = line.split("\t");
        final String key = sp[0];
        if (!tmpMap.containsKey(key)) {
          tmpMap.put(key, new ArrayList<IntPair>());
        }
        ArrayList<IntPair> lst = tmpMap.get(key);
        lst.add(new IntPair(Integer.parseInt(sp[3]), Integer.parseInt(sp[4])));

      }
      line = reader.readLine();
    }
    reader.close();
    return tmpMap;

  }

  private static Triplet<Integer, Integer, String> findInResults(IntPair sequenceRegionResult,
      ArrayList<Triplet<Integer, Integer, String>> res) {
    Triplet<Integer, Integer, String> ans = null;
    double maxOverlap = 0;
    for (Triplet<Integer, Integer, String> triplet : res) {
      double curOverlap = overlap(triplet, sequenceRegionResult);
      if (curOverlap > maxOverlap) {
        maxOverlap = curOverlap;
        ans = triplet;
      }
    }
    return ans;
  }

  private static HashMap<String, ArrayList<IntPair>> loadBlastPositionInformation(String fileName)
      throws IOException {
    // build a map for the name to index
    HashMap<String, ArrayList<IntPair>> map = new HashMap<String, ArrayList<IntPair>>();

    // read the blast file
    File file = new File(fileName);
    if (!file.exists()) {
      throw new RuntimeException("Can't locate needed file " + file.getAbsolutePath());
    }
    FileReader fileReader = new FileReader(file);
    BufferedReader bReader = new BufferedReader(fileReader);
    String line = bReader.readLine();
    while (line != null) {
      if (!line.startsWith("#")) {
        String[] sp = line.split("\t");
        /*
         * Input looks like this:
         * 
         * >>> CP001472.1/3934139-3934250 gi|225184640|emb|AL009126.3| 100.00 112 0 0 1 112 2305
         * 2416 2e-55 207
         */
        if (!map.containsKey(sp[0])) {
          map.put(sp[0], new ArrayList<IntPair>());
        }
        ArrayList<IntPair> lst = map.get(sp[0]);

        lst.add(new IntPair(Integer.parseInt(sp[8]), Integer.parseInt(sp[9])));
      }
      line = bReader.readLine();
    }

    bReader.close();
    return map;
  }

  /**
   * @param locationsOfTrueResults
   * @throws FileNotFoundException
   * @throws IOException
   */
  private static ArrayList<Triplet<Integer, Integer, String>> loadRealPositionInformation(
      String filename) throws IOException {
    ArrayList<Triplet<Integer, Integer, String>> locationsOfTrueResults =
        new ArrayList<Triplet<Integer, Integer, String>>();
    final File file = new File(filename);
    if (!file.exists()) {
      throw new RuntimeException("Can't locate needed file " + file.getAbsolutePath());
    }
    FileReader fileReader = new FileReader(file);
    BufferedReader bReader = new BufferedReader(fileReader);
    String line1 = bReader.readLine();
    String line2 = bReader.readLine();

    while (line1 != null && line2 != null) {
      String[] sp = line2.split(" ");
      /*
       * Input looks like this:
       * 
       * >>> CP001472.1/3934139-3934250 >>> 2305 2416
       */
      locationsOfTrueResults.add(new Triplet<Integer, Integer, String>(Integer.parseInt(sp[0]
          .trim()), Integer.parseInt(sp[1].trim()), line1.trim()));
      line1 = bReader.readLine();
      line2 = bReader.readLine();
    }

    bReader.close();
    return locationsOfTrueResults;
  }

  private static double overlap(Triplet<Integer, Integer, String> triplet,
      IntPair sequenceRegionResult) {
    int a1 = triplet.getFirst();
    int a2 = triplet.getSecond();
    int b1 = sequenceRegionResult.getFirst();
    int b2 = sequenceRegionResult.getSecond();

    return overlap(a1, a2, b1, b2);
  }

  /**
   * @param a1
   * @param a2
   * @param b1
   * @param b2
   * @return
   */
  private static double overlap(int a1, int a2, int b1, int b2) {
    final float maxOverlap = Math.min(a2 - a1 + 1, b2 - b1 + 1);
    final int l0 = Math.max(a1, b1);
    final int l1 = Math.min(a2, b2);
    final int diff = l1 - l0 + 1;
    if (diff <= 0) {
      return 0;
    }
    return diff / maxOverlap;
  }
}

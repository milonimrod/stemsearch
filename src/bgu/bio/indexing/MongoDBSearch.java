package bgu.bio.indexing;

import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.map.hash.TIntDoubleHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import util.TimeFormat;
import bgu.bio.adt.tuples.IntIntDoubleTriplet;
import bgu.bio.ds.rna.RNA;
import bgu.bio.indexing.options.MongoDBIndexOptions;
import bgu.bio.indexing.options.MongoDBSearchOptions;
import bgu.bio.indexing.workers.SearchWorker;
import bgu.bio.io.file.json.JSONException;
import bgu.bio.io.file.json.JSONObject;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.Strand;
import bgu.bio.stems.indexing.filtering.FilterManager;
import bgu.bio.stems.mongodb.query.MongoAverageStackSizeRangeQuery;
import bgu.bio.stems.mongodb.query.MongoEnergyRangeQuery;
import bgu.bio.stems.mongodb.query.MongoLoopLengthRangeQuery;
import bgu.bio.stems.mongodb.query.MongoPositionQuery;
import bgu.bio.stems.mongodb.query.MongoQuery;
import bgu.bio.stems.mongodb.query.MongoStandardDeviationStackSizeRangeQuery;
import bgu.bio.stems.search.SequenceRegionResult;
import bgu.bio.stems.search.StemData;
import bgu.bio.stems.util.PartitionFunction;
import bgu.bio.utils.stats.Histogram;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ReadPreference;

public class MongoDBSearch {

  private HashMap<String, Object> argumentsMap;
  private ArrayList<RNA> inputRNA;
  private File outputFile;
  private Logger logger;
  private String version;

  public MongoDBSearch() {
    argumentsMap = new HashMap<String, Object>();
    logger = Logger.getLogger(MongoDBSearch.class.getCanonicalName());
  }

  public void loadParams(String[] args) {
    logger.info("Started search with arguments " + Arrays.toString(args));
    MongoDBSearchOptions options = new MongoDBSearchOptions();
    options.buildOptions();
    version = options.getVersion();
    options.loadParams(args, argumentsMap);
    if (options.remaining.length != 2) {
      System.err.println("Must provide input and output files ... "
          + Arrays.toString(options.remaining));
      options.printHelp();
      System.exit(3);
    }
    if (logger.isDebugEnabled()) {
      logger.debug("Started search with map " + argumentsMap);
    }
    String inputData = options.remaining[0];
    File f = new File(inputData);
    // check if file
    if (f.isFile() && f.exists()) {
      // read the content
      inputRNA = RNA.loadFromFile(f, false);

    } else {
      inputRNA = new ArrayList<RNA>();
      inputRNA.add(new RNA(0, "query", inputData));
    }

    outputFile = new File(options.remaining[1]);
  }

  private String splitter(String inputSequence2) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < inputSequence2.length(); i++) {
      sb.append(inputSequence2.charAt(i));
      if ((i + 1) % 70 == 0) {
        sb.append("\n");
      }
    }
    return sb.toString();
  }

  /**
   * @throws IOException
   */
  /**
   * @throws IOException
   */
  public void run() throws IOException {

    long time = System.currentTimeMillis();

    String serverLocation = (String) argumentsMap.get(MongoDBSearchOptions.SERVER_LOCATION);
    String[] split = serverLocation.split(":");
    MongoClient mongoClient = new MongoClient(split[0].trim(), Integer.parseInt(split[1].trim()));
    mongoClient.setReadPreference(ReadPreference.secondaryPreferred());
    String databaseName = (String) argumentsMap.get(MongoDBSearchOptions.DATABASE_NAME);
    DB db = mongoClient.getDB(databaseName);

    System.out.println("Connection took ... " + ((System.currentTimeMillis() - time) / 1000.0)
        + " sec");

    Properties props = new Properties();
    InputStream propFile = (InputStream) argumentsMap.get(MongoDBSearchOptions.FILTER_PROP_FILE);
    props.load(propFile);
    propFile.close();
    time = System.currentTimeMillis();
    System.out.print("Creating filter manager...");
    System.out.flush();
    FilterManager filterManager = new FilterManager(props);
    System.out.println(" done.");
    System.out
        .println("Initiation took " + ((System.currentTimeMillis() - time) / 1000.0) + " sec");

    time = System.currentTimeMillis();
    String collectionName = (String) argumentsMap.get(MongoDBSearchOptions.COLLECTION_NAME);

    DBCollection pvalCollection = null;
    if (argumentsMap.containsKey("pvalCollectionName")) {
      String pvalCollectionName = (String) argumentsMap.get("pvalCollectionName");

      if (!db.collectionExists(pvalCollectionName)) {
        System.err.println("Given pvalue collection database does not exist !");
        System.exit(1);
      }
      pvalCollection = db.getCollection(pvalCollectionName);
    }
    // DBCollection collection = db.getCollection(collectionName);
    final String OUTPUT_TYPE;
    BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFile));
    JSONObject json = null;
    if (outputFile.getName().toLowerCase().endsWith(".json")) {
      logger.info("Using JSON output file");
      OUTPUT_TYPE = "JSON";
      json = new JSONObject();
    } else {
      logger.info("Using GFF3 output file");
      OUTPUT_TYPE = "GFF3";
    }
    if (OUTPUT_TYPE.equals("GFF3")) {
      outputWriter.write("##gff-version 3");
      outputWriter.write(System.lineSeparator());
      outputWriter.write("#StemSearch version " + version);
      outputWriter.write(System.lineSeparator());
    }
    Histogram hist = new Histogram();
    hist.setNumOfBins(30);

    for (RNA rna : inputRNA) {
      ArrayList<String> outputHeaders = new ArrayList<String>();
      System.out.println("Start on " + rna);
      time = System.currentTimeMillis();
      logger.info("Start to run on " + rna.toJSON());
      // set input for the filter
      String inputSequence = splitter(rna.getPrimary());
      filterManager.setInput(new StringReader(">" + rna.getHeader() + "\n" + inputSequence),
          Strand.UNKNOWN);

      TIntObjectHashMap<TIntDoubleHashMap> pfMap = null;

      if (PartitionFunction.canRun()) {
        try {
          ArrayList<IntIntDoubleTriplet> ans =
              PartitionFunction.computeParitionFunction(inputSequence);
          pfMap = PartitionFunction.mapList(ans);

        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      } else {
        logger.error("Can't use Partition Function filtering ... can't locate RNAFold");
        System.err.println("Can't use Partition Function filtering ... can't locate RNAFold");
      }

      ArrayList<AnnotatedSequence> querySequences = new ArrayList<AnnotatedSequence>();

      int countAbove = 0;
      final double PARTITION_FUNCTION_VALUE =
          (double) argumentsMap.get(MongoDBSearchOptions.PARTITION_FUNCTION_T);
      int count = 0;
      while (filterManager.hasNext()) {
        AnnotatedSequence sequence = filterManager.next();
        count++;
        if (pfMap != null) {
          double pfVal = PartitionFunction.assignProbabillity(sequence, pfMap);
          if (pfVal > PARTITION_FUNCTION_VALUE) {
            countAbove++;

            sequence.setPartitionFunction(pfVal);
            querySequences.add(sequence);
          }
        } else {
          countAbove++;
          querySequences.add(sequence);
        }

      }

      Collections.sort(querySequences, new Comparator<AnnotatedSequence>() {

        @Override
        public int compare(AnnotatedSequence o1, AnnotatedSequence o2) {
          if (o1.getPartitionFunction() > o2.getPartitionFunction()) {
            return -1;
          } else if (o1.getPartitionFunction() < o2.getPartitionFunction()) {
            return 1;
          }
          return 0;
        }

      });

      while (querySequences.size() > 100) {
        querySequences.remove(querySequences.size() - 1);
      }
      TDoubleArrayList pfValues = new TDoubleArrayList();
      for (AnnotatedSequence sequence : querySequences) {
        pfValues.add(sequence.getPartitionFunction());
      }
      final String pfString =
          String.format(
              "Found %d query stems. %d of them passed the partition function value of %f.", count,
              countAbove, PARTITION_FUNCTION_VALUE);
      outputHeaders.add(pfString);
      logger.info(rna.getHeader() + " " + pfString);
      hist.setData(pfValues);
      logger.info(rna.getHeader() + " " + Arrays.toString(hist.calcHistogramValues()));
      logger.info(rna.getHeader() + " " + Arrays.toString(hist.calcHistogram()));

      // calculate amount of threads
      int threads = (int) argumentsMap.get(MongoDBSearchOptions.THREADS);
      if (threads == 1 && pvalCollection != null) {
        threads = 2;
      }
      int runThreads = pvalCollection != null ? threads / 2 : threads;
      int pvalThreads = pvalCollection != null ? (threads / 2 + threads % 2) : 0;

      logger.info(String.format(rna.getHeader()
          + " Using total of %d threads. %d for run and %d for stats.", threads, runThreads,
          pvalThreads));

      final double PROPORTION = 0.15;
      int maxWindowSize = (int) (filterManager.getIndexInSequence() * (1 + PROPORTION)) + 1;

      boolean bothStrands = !(boolean) argumentsMap.get(MongoDBIndexOptions.SINGLE_STRAND);
      ArrayList<DBCollection> collections = new ArrayList<DBCollection>();
      ArrayList<DBObject> collectionsInformation = new ArrayList<DBObject>();
      if (bothStrands) {
        collections.add(db.getCollection(collectionName + "_PLUS"));
        collections.add(db.getCollection(collectionName + "_MINUS"));
        collectionsInformation.add(getCollectionInfo(db, collections.get(0)));
        collectionsInformation.add(getCollectionInfo(db, collections.get(1)));
      } else {
        collections.add(db.getCollection(collectionName));
        collectionsInformation.add(getCollectionInfo(db, collections.get(0)));
      }

      // compute the genomic size
      int size = calculateSize(db, collections.get(0));
      int portionSize = size / runThreads;
      // initiate the running threads
      ArrayList<SearchWorker> regularWorkers = new ArrayList<SearchWorker>();
      ArrayList<Thread> workersThreads = new ArrayList<Thread>();
      for (int t = 0; t < runThreads; t++) {
        SearchWorker worker =
            initWorker(collections, collectionsInformation, rna, querySequences, maxWindowSize,
                size, portionSize, t);
        regularWorkers.add(worker);
        workersThreads.add(new Thread(worker, "RegWorker" + t));
      }

      // initiate the p-value threads

      ArrayList<SearchWorker> pvalueWorkers = new ArrayList<SearchWorker>();
      if (pvalCollection != null) {
        size = calculateSize(db, pvalCollection);
        portionSize = size / pvalThreads;
        for (int t = 0; t < pvalThreads; t++) {
          ArrayList<DBCollection> pvalCollections = new ArrayList<DBCollection>();
          pvalCollections.add(pvalCollection);
          ArrayList<DBObject> pvalInformations = new ArrayList<DBObject>();
          pvalInformations.add(getCollectionInfo(db, pvalCollection));
          SearchWorker worker =
              initWorker(pvalCollections, pvalInformations, rna, querySequences, maxWindowSize,
                  size, portionSize, t);
          pvalueWorkers.add(worker);
          workersThreads.add(new Thread(worker, "PvalWorker" + t));
        }
      }

      for (Thread thread : workersThreads) {
        thread.start();
      }

      // wait for the threads
      for (Thread thread : workersThreads) {
        try {
          thread.join();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }

      // collect the results
      HashSet<SequenceRegionResult> regularResults = new HashSet<SequenceRegionResult>();
      for (int i = 0; i < regularWorkers.size(); i++) {
        regularResults.addAll(regularWorkers.get(i).getResults());
      }

      // collect statistics
      boolean stats = false;
      double mu = 0, std = 0;
      double sampleSize = 0;
      if (pvalCollection != null) {
        stats = true;
        ArrayList<SequenceRegionResult> lst = new ArrayList<SequenceRegionResult>();
        HashSet<SequenceRegionResult> statsResults = new HashSet<SequenceRegionResult>();
        for (int i = 0; i < pvalueWorkers.size(); i++) {
          statsResults.addAll(pvalueWorkers.get(i).getResults());
        }
        lst.addAll(statsResults);
        double sum = 0;
        double sumSquare = 0;
        sampleSize = lst.size();

        for (SequenceRegionResult sequenceRegionResult : lst) {
          sum += sequenceRegionResult.getScore();
          sumSquare += sequenceRegionResult.getScore() * sequenceRegionResult.getScore();
        }
        sum = sum / sampleSize;
        sumSquare = sumSquare / sampleSize;
        mu = sum;
        std = Math.sqrt(sumSquare - mu * mu);
        String statsString =
            String.format("Statistics: Found %d results, mean = %f (std = %f)", (int) sampleSize,
                mu, std);
        outputHeaders.add(statsString);
        logger.info(rna.getHeader() + " " + statsString);

      }

      // get and sort the results
      ArrayList<SequenceRegionResult> lst = new ArrayList<SequenceRegionResult>();
      lst.addAll(regularResults);
      Collections.sort(lst);
      Collections.reverse(lst);

      // compute the statistics for all
      if (stats) {
        ArrayList<SequenceRegionResult> prunedByPval = new ArrayList<SequenceRegionResult>();
        for (SequenceRegionResult sequenceRegionResult : lst) {
          sequenceRegionResult.computePvalue(sequenceRegionResult.getScore(), mu, std, sampleSize);
          if (Double.isNaN(sequenceRegionResult.getPValue())
              || sequenceRegionResult.getPValue() <= 0.01) {
            // add to the new list if p-value is better than 0.01
            prunedByPval.add(sequenceRegionResult);
          }
        }
        logger.info(String.format(rna.getHeader() + " p-value trimming passed %d of %d",
            prunedByPval.size(), lst.size()));
        lst = prunedByPval;
        // prune by FDR
        final double alpha = 0.01;
        double fdr, pval;
        logger.info(String.format(rna.getHeader()
            + " Using Alpha = %f for FDR (Benjamini-Hochberg)", alpha));
        int k = 0;
        if (lst.size() > 0) {
          do {
            SequenceRegionResult sp = lst.get(k);
            pval = sp.getPValue();
            fdr = ((k + 1.0) / lst.size()) * alpha;
            k++;
          } while (k < lst.size() && (Double.isNaN(pval) || pval <= fdr));
          k = Math.max(1, k);
        }
        ArrayList<SequenceRegionResult> prunedByFDR = new ArrayList<SequenceRegionResult>();
        final String formatFDR = String.format("FDR passed %d out of %d", k, lst.size());
        logger.info(rna.getHeader() + " " + formatFDR);
        outputHeaders.add(formatFDR);
        for (int i = 0; i < k; i++) {
          prunedByFDR.add(lst.get(i));
        }
        lst = prunedByFDR;
      }
      if (OUTPUT_TYPE.equals("GFF3")) {
        writeResultInGFF3File(lst, rna, outputWriter, outputHeaders);
        outputWriter.flush();
      } else {
        try {
          json.put("jobid", "query");

          int x = 0;
          for (AnnotatedSequence seq : querySequences) {
            json.append("query", StemData.toJSON(new StemData(seq.getStructureStartPosition(), seq
                .getRelevantSequence(), seq.getRelevantStructure(), x, 0)));
            x++;
          }

          for (int i = 0; i < lst.size(); i++) {
            JSONObject result = SequenceRegionResult.toJSON(lst.get(i));

            json.append("results", result);
          }
        } catch (JSONException ex) {
          logger.error("Got error in JSON parsing" + ex.getMessage());
        }
        outputWriter.write(json.toString().replace("]", "]\n").replace("[", "\n["));
      }
      String timeString = TimeFormat.formatMilliseconds(System.currentTimeMillis() - time);
      System.out.println(rna.getHeader() + " Finished in " + timeString);
      logger.info(rna.getHeader() + " Finished in " + timeString);
    }
    outputWriter.close();
    // close the connection
    mongoClient.close();
  }

  private SearchWorker initWorker(ArrayList<DBCollection> collections,
      ArrayList<DBObject> collectionsInformations, RNA rna,
      ArrayList<AnnotatedSequence> querySequences, int maxWindowSize, int size, int portionSize,
      int t) {
    int start = Math.max(0, t * portionSize - maxWindowSize);
    int end = Math.min(size, (t + 1) * portionSize + maxWindowSize);
    double percentage = 0.1;
    ArrayList<MongoQuery> queries = new ArrayList<MongoQuery>();
    queries.add(new MongoEnergyRangeQuery(percentage));
    queries.add(new MongoLoopLengthRangeQuery(percentage));
    queries.add(new MongoAverageStackSizeRangeQuery(percentage));
    queries.add(new MongoStandardDeviationStackSizeRangeQuery(percentage));
    queries.add(new MongoPositionQuery(start, end));

    SearchWorker worker =
        new SearchWorker(collections, collectionsInformations, querySequences, rna, queries,
            argumentsMap, (double) argumentsMap.get(MongoDBSearchOptions.RELATIVE_SCORE_T));
    return worker;
  }

  private int calculateSize(DB db, DBCollection collection) {
    DBCollection infoCollection = db.getCollection("info");
    DBObject cur =
        infoCollection.findOne(new BasicDBObject().append("collection", collection.getName()));
    int length = (Integer) cur.get("length");
    return length;
  }

  private DBObject getCollectionInfo(DB db, DBCollection collection) {
    DBCollection infoCollection = db.getCollection("info");
    DBObject cur =
        infoCollection.findOne(new BasicDBObject().append("collection", collection.getName()));
    return cur;
  }

  private void writeResultInGFF3File(ArrayList<SequenceRegionResult> lst, RNA query, Writer writer,
      List<String> headers) throws IOException {
    writer.write("#" + query);
    writer.write(System.lineSeparator());
    if (headers != null) {
      for (String header : headers) {
        writer.write("#");
        writer.write(header);
        writer.write(System.lineSeparator());
      }
    }

    // <seqname> <source> <feature> <start> <end> <score> <strand> <frame>
    // <annotation>
    for (SequenceRegionResult result : lst) {
      String additional = "pvalue=" + result.getPValue();
      writer.write(String.format("%s\t%s\tregion\t%d\t%d\t%f\t%s\t.\t%s%n", query.getHeader(),
          result.getSequenceId(), result.getPosition(), result.getPosition() + result.getLength(),
          result.getScore(), result.getStrand(), additional));
    }
  }

  public static void main(String[] args) throws IOException {
    MongoDBSearch search = new MongoDBSearch();
    search.loadParams(args);
    search.run();
  }
}

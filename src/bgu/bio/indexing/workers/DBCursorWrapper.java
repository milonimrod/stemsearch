package bgu.bio.indexing.workers;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.mongodb.StemFieldsConstants;

import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class DBCursorWrapper {
  DBCursor cursor;
  DBObject current;
  AnnotatedSequence data;
  int ordinal;
  private int currentPosition;

  public DBCursorWrapper(DBCursor cursor, AnnotatedSequence data, int ordinal) {
    this.cursor = cursor;
    this.data = data;
    this.ordinal = ordinal;
    // load the first
    next();
  }

  public boolean hasNext() {
    return current != null;
  }

  public DBObject peek() {
    return current;
  }

  public int position() {
    return currentPosition;
  }

  public DBObject next() {
    DBObject data = current;
    if (cursor.hasNext()) {
      current = cursor.next();
      currentPosition = ((Integer) current.get(StemFieldsConstants.POSITION_END)).intValue();
    } else {
      current = null;
    }
    return data;
  }

  public String stats() {
    return cursor.explain().toString();
  }

  @Override
  public String toString() {
    return this.stats();
  }
}

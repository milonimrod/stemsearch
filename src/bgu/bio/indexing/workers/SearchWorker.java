package bgu.bio.indexing.workers;

import gnu.trove.list.array.TCharArrayList;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.log4j.Logger;

import bgu.bio.adt.queue.Pool;
import bgu.bio.algorithms.alignment.GlobalSequenceAlignment;
import bgu.bio.algorithms.alignment.stems.GlobalStemSimilarity;
import bgu.bio.algorithms.alignment.stems.StemSimilarity;
import bgu.bio.ds.rna.RNA;
import bgu.bio.ds.rna.StemStructure;
import bgu.bio.indexing.options.MongoDBSearchOptions;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.RNASequenceHuffmanCoding;
import bgu.bio.stems.indexing.RNAStructureHuffmanCoding;
import bgu.bio.stems.indexing.Strand;
import bgu.bio.stems.mongodb.StemFieldsConstants;
import bgu.bio.stems.mongodb.query.MongoQuery;
import bgu.bio.stems.search.RegionScorer;
import bgu.bio.stems.search.Result;
import bgu.bio.stems.search.SequenceRegionResult;
import bgu.bio.stems.search.StemData;
import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.RnaAlphabet;
import bgu.bio.util.alphabet.StemStructureAlphabet;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class SearchWorker implements Runnable {

  private ArrayList<DBCollection> collections;
  private ArrayList<AnnotatedSequence> querySequences;
  private ArrayList<MongoQuery> mongodbQueries;

  private Pool<StemData> stemDataPool;
  private RNA rna;

  private HashSet<SequenceRegionResult> results;
  private RNASequenceHuffmanCoding huffSequence;
  private RNAStructureHuffmanCoding huffStructure;
  private TCharArrayList sequence = new TCharArrayList();
  private TCharArrayList structure = new TCharArrayList();
  private BitSet bitset;
  private StemSimilarity stemSimilarity;
  private final double RELATIVE_SCORE;
  private ArrayList<DBObject> collectionsInfotmation;

  public SearchWorker(ArrayList<DBCollection> collections,
      ArrayList<DBObject> collectionsInfotmation, ArrayList<AnnotatedSequence> querySequences,
      RNA rna, ArrayList<MongoQuery> mongodbQueries, HashMap<String, Object> argumentsMap,
      double relativeScoreT) {
    this.RELATIVE_SCORE = relativeScoreT;
    this.collections = collections;
    this.collectionsInfotmation = collectionsInfotmation;
    this.rna = rna;
    this.querySequences = querySequences;
    this.mongodbQueries = mongodbQueries;

    stemDataPool = new Pool<StemData>(1000, true);
    results = new HashSet<SequenceRegionResult>();

    huffSequence = new RNASequenceHuffmanCoding();
    huffStructure = new RNAStructureHuffmanCoding();
    sequence = new TCharArrayList();
    structure = new TCharArrayList();
    bitset = new BitSet();

    initAligner(argumentsMap);
  }

  private void initAligner(HashMap<String, Object> argumentsMap) {
    ScoringMatrix structureMatrix =
        new ScoringMatrix(this.getClass().getResourceAsStream(
            (String) argumentsMap.get(MongoDBSearchOptions.MATRIX_STRUCTURE)),
            StemStructureAlphabet.getInstance());
    GlobalSequenceAlignment sequenceAlignment =
        new GlobalSequenceAlignment(100, 100, RnaAlphabet.getInstance(), new ScoringMatrix(this
            .getClass().getResourceAsStream(
                (String) argumentsMap.get(MongoDBSearchOptions.MATRIX_SEQUENCE)),
            RnaAlphabet.getInstance()));

    this.stemSimilarity = new GlobalStemSimilarity(structureMatrix, sequenceAlignment);
  }

  public HashSet<SequenceRegionResult> getResults() {
    return results;
  }

  /**
   * @throws IOException
   */
  /**
   * @throws IOException
   */
  public void run() {
    Logger logger = Logger.getLogger(SearchWorker.class.getCanonicalName());
    ArrayList<StemData> queryStemDatas = new ArrayList<StemData>();
    ArrayList<StemStructure> queryStemStructures = new ArrayList<StemStructure>();
    // pass on all the stems and create the query in the mongoDB server
    int ordinal = 0;
    for (AnnotatedSequence annotatedSeq : querySequences) {
      // build search for each stem
      TCharArrayList seq = annotatedSeq.getRelevantSequence();
      TCharArrayList str = annotatedSeq.getRelevantStructure();

      StemStructure sStructure = new StemStructure(null, seq, str);
      queryStemStructures.add(sStructure);
      // build self score
      stemSimilarity.setStem1(sStructure);
      stemSimilarity.setStem2(sStructure);
      stemSimilarity.run();

      queryStemDatas.add(new StemData(annotatedSeq.getStructureStartPosition(), seq, str, ordinal,
          (float) stemSimilarity.getAlignmentScore()));
      ordinal++;
    }

    results.clear();
    int collIndex = 0;
    for (DBCollection collection : collections) {
      // get collection information
      DBObject collectionInfo = collectionsInfotmation.get(collIndex);
      // build cursors for each sequence
      ordinal = 0;
      ArrayList<DBCursorWrapper> cursors = new ArrayList<DBCursorWrapper>();
      // create the projection of fields
      BasicDBObject projection = buildProjection();
      long sum = 0;
      long sumSquare = 0;
      boolean shouldCount = false;
      for (AnnotatedSequence annotatedSeq : querySequences) {
        BasicDBObject obj = new BasicDBObject();
        for (MongoQuery query : mongodbQueries) {
          query.addQuery(annotatedSeq, obj);
        }
        DBCursor cur =
            collection.find(obj, projection).sort(
                new BasicDBObject().append(StemFieldsConstants.POSITION_END, 1));
        cur.batchSize(1024);
        if (shouldCount) {
          final long count = cur.count();

          sum += count;
          sumSquare += count * count;
        }
        cursors.add(new DBCursorWrapper(cur, annotatedSeq, ordinal));
        ordinal++;
      }
      if (shouldCount) {
        final double avg = sum / querySequences.size();
        final double std = Math.sqrt((sumSquare / querySequences.size()) - avg * avg);
        logger.info(String.format(rna.getHeader()
            + " Found an average of %f occurences, with %f std", avg, std));
      }

      final double proporsion = 0.15;
      int maxWindowSize = (int) (rna.getPrimary().length() * (1 + proporsion)) + 1;


      RegionScorer regionScorer = new RegionScorer(queryStemDatas, maxWindowSize, stemDataPool);
      regionScorer.startSequence(Strand.UNKNOWN);
      int min = getMinPosition(cursors);
      while (min >= 0) {
        DBCursorWrapper current = cursors.get(min);
        DBObject obj = current.next();
        StemData data = buildStemData(obj, current.ordinal);
        StemStructure stemStructure = new StemStructure(data.getSequence(), data.getStructure());

        stemSimilarity.setStem1(stemStructure);
        stemSimilarity.setStem2(stemStructure);
        stemSimilarity.run();
        float stemSelfScore = (float) stemSimilarity.getAlignmentScore();

        stemSimilarity.setStem1(queryStemStructures.get(current.ordinal));
        stemSimilarity.setStem2(stemStructure);
        stemSimilarity.run();
        float stemsScore = (float) stemSimilarity.getAlignmentScore();
        data.setScore(stemsScore);
        data.setNormScore((float) (2.0 * stemsScore / (stemSelfScore + queryStemDatas.get(
            current.ordinal).getScore())));

        // only compare if normalized score is above the threshold
        if (data.getNormScore() >= RELATIVE_SCORE) {

          if (!regionScorer.push(data)) {
            // push moved the window
            Result result = regionScorer.getResult();
            if (result != null) {
              // if strand is minus fix the positions
              if ("-".equals(collectionInfo.get("strand"))) {
                final int size = (int) collectionInfo.get("length");
                for (StemData stem : result.list) {
                  stem.setPosition1(size - stem.getPosition1() + 1);
                }
              }
              SequenceRegionResult r =
                  new SequenceRegionResult(collection.getName(),
                      (String) collectionInfo.get("strand"), result.list, stemDataPool);
              results.add(r);
            }
          }
        } else {
          stemDataPool.enqueue(data);
        }
        min = getMinPosition(cursors);
      }

      // close the connections
      for (int i = 0; i < cursors.size(); i++) {
        cursors.get(i).cursor.close();
      }

      regionScorer.endSequence();
      Result result = regionScorer.getResult();
      if (result != null) {
        // if strand is minus fix the positions
        if ("-".equals(collectionInfo.get("strand"))) {
          final int size = (int) collectionInfo.get("length");
          for (StemData stem : result.list) {
            stem.setPosition1(size - stem.getPosition1() + 1);
          }
        }
        SequenceRegionResult r =
            new SequenceRegionResult(collection.getName(), (String) collectionInfo.get("strand"),
                result.list, stemDataPool);
        results.add(r);
      }

      logger.info(rna.getHeader() + " Computed " + regionScorer.getComputedRegions()
          + " regions, with " + results.size() + " results.");

      regionScorer.close();
      collIndex++;
    }
  }

  /**
   * @return
   */
  private BasicDBObject buildProjection() {
    BasicDBObject projection = new BasicDBObject();
    projection.put(StemFieldsConstants.POSITION_START, 1);
    projection.put(StemFieldsConstants.POSITION_END, 1);
    projection.put(StemFieldsConstants.SEQUENCE, 1);
    projection.put(StemFieldsConstants.ENERGY, 1);
    projection.put(StemFieldsConstants.STRUCTURE, 1);
    projection.put(StemFieldsConstants.LOOP_LENGTH, 1);
    projection.put("_id", 0);
    return projection;
  }

  private StemData buildStemData(DBObject obj, int ordinal) {
    huffSequence.decode(new ByteArrayInputStream((byte[]) obj.get(StemFieldsConstants.SEQUENCE)),
        sequence, bitset);

    huffStructure.decode(new ByteArrayInputStream((byte[]) obj.get(StemFieldsConstants.STRUCTURE)),
        structure, bitset);

    StemData ans = stemDataPool.dequeue();
    if (ans == null) {
      ans =
          new StemData((int) obj.get(StemFieldsConstants.POSITION_START), sequence, structure,
              ordinal, 0f);
    } else {
      ans.reuse((int) obj.get(StemFieldsConstants.POSITION_START), sequence, structure, ordinal, 0f);
    }
    ans.setEnergy((double) obj.get(StemFieldsConstants.ENERGY));
    ans.setLoopLength((int) obj.get(StemFieldsConstants.LOOP_LENGTH));
    return ans;
  }

  /**
   * Search the cursor index with the minimal position
   * 
   * @param cursors the list of cursors
   * @return the index for which argmin_x(pos[x])
   */
  private int getMinPosition(ArrayList<DBCursorWrapper> cursors) {
    int minIndex = -1;
    int minPos = Integer.MAX_VALUE;
    for (int i = 0; i < cursors.size(); i++) {
      DBCursorWrapper current = cursors.get(i);
      if (current.hasNext()) {
        int pos = current.position();
        if (pos < minPos) {
          minPos = pos;
          minIndex = i;
        }
      }
    }
    return minIndex;
  }

}

package bgu.bio.indexing.workers;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;

import org.apache.log4j.Logger;

import util.ANSIConstants;
import util.TimeFormat;
import bgu.bio.stems.filereader.FastaFormatReaderGenome;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.AnnotatedSequencePool;
import bgu.bio.stems.indexing.Strand;
import bgu.bio.stems.indexing.filtering.FilterManager;
import bgu.bio.stems.mongodb.encoders.AverageStackLengthEncoder;
import bgu.bio.stems.mongodb.encoders.EnergyEncoder;
import bgu.bio.stems.mongodb.encoders.LoopLengthEncoder;
import bgu.bio.stems.mongodb.encoders.MongoEncoder;
import bgu.bio.stems.mongodb.encoders.StandardDeviationStackLengthEncoder;
import bgu.bio.stems.mongodb.encoders.StemInfoEncoder;
import bgu.bio.util.CharBuffer;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.WriteConcern;

public class IndexingWorker implements Runnable {

  private File inputFile;
  private Properties props;
  private ArrayList<MongoEncoder> extendedEnocders;
  private DBCollection collection;
  private int offset;
  private NumberFormat format;

  private int counter;
  private StringBuilder sb;
  private String name;
  private ArrayBlockingQueue<AnnotatedSequence> queue;

  public IndexingWorker(File inputFile, int offset, Properties props,
      ArrayList<MongoEncoder> extendedEnocders, DBCollection collection) {
    super();
    this.inputFile = inputFile;
    this.props = props;
    this.extendedEnocders = extendedEnocders;
    this.collection = collection;
    this.offset = offset;
    format = new DecimalFormat("##0.0");
    queue = new ArrayBlockingQueue<AnnotatedSequence>(300000);
    this.sb = new StringBuilder();
  }

  @Override
  public void run() {
    Logger logger = Logger.getLogger(IndexingWorker.class.getCanonicalName());
    AnnotatedSequencePool pool = new AnnotatedSequencePool();
    this.name = Thread.currentThread().getName();
    ArrayList<MongoEncoder> encoders = new ArrayList<MongoEncoder>();
    encoders.add(new StemInfoEncoder());
    encoders.add(new EnergyEncoder());
    encoders.add(new LoopLengthEncoder());
    encoders.add(new AverageStackLengthEncoder());
    encoders.add(new StandardDeviationStackLengthEncoder());
    if (extendedEnocders != null && extendedEnocders.size() != 0) {
      encoders.addAll(extendedEnocders);
    }

    final AnnotatedSequence DUMMY_SEQUENCE =
        new AnnotatedSequence("DDD", 1, new CharBuffer("AAAAAUUUUU"), new CharBuffer("((((()))))"));
    IndexHelper helper = new IndexHelper(pool, encoders, DUMMY_SEQUENCE);
    Thread helperThread = new Thread(helper, "Helper" + name);
    helperThread.start();

    counter = 0;

    // read file for number of bases
    final int basesInGenome = calculateGenomicSize(inputFile);
    long time = System.currentTimeMillis();
    FilterManager filterManager = null;
    try {
      filterManager = new FilterManager(props, pool);
      logger.info("Initiation took "
          + TimeFormat.formatMilliseconds(System.currentTimeMillis() - time));
      filterManager.setInput(inputFile, Strand.PLUS);
    } catch (IOException ex) {
      ex.printStackTrace();
    }
    long stems = 0;
    int prevSize = 0;
    while (filterManager.hasNext()) {
      AnnotatedSequence sequence = filterManager.next();
      stems++;
      if (stems % 2000 == 0) {
        prepareLine(basesInGenome, stems, prevSize, sequence);
        prevSize = sb.length();
        // System.out.print(sb);
        // System.out.flush();
      }
      sequence.setLocation(sequence.getLocation() + offset);
      try {
        queue.put(sequence);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      counter++;
    }
    prepareLine(basesInGenome, stems, prevSize, null);
    logger.info("done");
    try {
      queue.put(DUMMY_SEQUENCE);
      logger.info("Waiting for helper");
      helperThread.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

  }

  private void prepareLine(int basesInGenome, long stems, int prevSize, AnnotatedSequence sequence) {
    /*
     * for (int c = 0; c < prevSize; c++) { System.out.print('\b'); } System.out.flush();
     */
    synchronized (sb) {
      final int pos = sequence == null ? basesInGenome : sequence.getStructureEndPosition();
      sb.setLength(0);
      sb.append(ANSIConstants.ANSI_BOLD_ON + name + ANSIConstants.ANSI_BOLD_OFF);
      sb.append(" Indexed ");
      sb.append(pos);
      sb.append(" / ");
      sb.append(basesInGenome);
      sb.append(" - ");
      sb.append(ANSIConstants.ANSI_RED + format.format(100.0 * pos / basesInGenome) + "%"
          + ANSIConstants.ANSI_RESET);
      sb.append(" (");
      sb.append(stems);
      sb.append(" stems)");
    }
  }

  public String getStatus() {
    String ans = null;
    synchronized (sb) {
      ans = sb.toString();
    }
    return ans;
  }

  private int calculateGenomicSize(File file) {
    int basesInGenome = 0;
    FastaFormatReaderGenome reader = new FastaFormatReaderGenome(20000);

    reader.setReader(file, false);

    while (reader.hasNext()) {
      AnnotatedSequence seq = reader.next();
      basesInGenome += seq.getLength();
    }
    reader.close();
    return basesInGenome;
  }

  public int getCounter() {
    return counter;
  }

  private class IndexHelper implements Runnable {
    private AnnotatedSequencePool pool;
    private ArrayList<MongoEncoder> encoders;
    private AnnotatedSequence dummy;

    public IndexHelper(AnnotatedSequencePool pool, ArrayList<MongoEncoder> encoders,
        AnnotatedSequence dummy) {
      this.pool = pool;
      this.encoders = encoders;
      this.dummy = dummy;
    }

    @Override
    public void run() {
      AnnotatedSequence seq = null;
      try {
        seq = queue.take();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      ArrayList<DBObject> buffer = new ArrayList<DBObject>();
      final int BATCH_SIZE = 4096;
      while (seq != dummy) {
        BasicDBObject stem = new BasicDBObject();
        for (int i = 0; i < encoders.size(); i++) {
          encoders.get(i).encode(seq, stem);
        }
        buffer.add(stem);
        if (buffer.size() == BATCH_SIZE) {
          collection.insert(buffer, WriteConcern.UNACKNOWLEDGED);
          buffer.clear();
        }
        // append header
        pool.releaseSequence(seq);
        try {
          seq = queue.take();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
      // clear the rest
      if (buffer.size() > 0) {
        collection.insert(buffer, WriteConcern.UNACKNOWLEDGED);
      }
    }

  }

}

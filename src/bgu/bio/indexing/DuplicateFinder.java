package bgu.bio.indexing;

import java.io.ByteArrayInputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.bson.types.ObjectId;

import bgu.bio.stems.indexing.RNASequenceHuffmanCoding;
import bgu.bio.stems.indexing.RNAStructureHuffmanCoding;

import com.mongodb.BasicDBObject;
import com.mongodb.CommandResult;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class DuplicateFinder {
  public static void main(String[] args) throws NumberFormatException, UnknownHostException {
    String serverLocation = "cbnegev4.cs.bgu.ac.il:27017";
    String[] split = serverLocation.split(":");
    MongoClient mongoClient = new MongoClient(split[0].trim(), Integer.parseInt(split[1].trim()));
    String databaseName = "StemSearch-benchmark";
    DB db = mongoClient.getDB(databaseName);

    DBCollection collection = db.getCollection("Shuffle");
    CommandResult stats = collection.getStats();
    System.out.println(stats.get("count"));

    // add index
    System.out.print("creating index ... ");
    collection.createIndex(new BasicDBObject().append("pos", 1));
    System.out.println("done");

    System.out.print("collecting data ... ");
    DBCursor cur = collection.find().sort(new BasicDBObject().append("pos", 1));
    System.out.println("done");
    ArrayList<DBObject> list = new ArrayList<DBObject>();
    ArrayList<ObjectId> duplicates = new ArrayList<ObjectId>();
    int pos = -1;
    RNASequenceHuffmanCoding sequenceHuffmanCoding = new RNASequenceHuffmanCoding();
    RNAStructureHuffmanCoding structureHuffmanCoding = new RNAStructureHuffmanCoding();
    int count = 0;
    int countMiss = 0;
    while (cur.hasNext()) {
      DBObject obj = cur.next();
      int currentPos = (int) obj.get("pos");
      if (pos != currentPos) {
        // clear the list
        list.clear();
        pos = currentPos;
        // System.out.println(pos);
      }
      // search the list
      for (DBObject lstObj : list) {
        // check if equals
        if (lstObj.get("pos2").equals(obj.get("pos2")) && lstObj.get("ll").equals(obj.get("ll"))) {

          String seq1 =
              sequenceHuffmanCoding.decode(new ByteArrayInputStream((byte[]) lstObj.get("seq")));
          String seq2 =
              sequenceHuffmanCoding.decode(new ByteArrayInputStream((byte[]) obj.get("seq")));
          if (seq1.equals(seq2)) {
            String str1 =
                structureHuffmanCoding.decode(new ByteArrayInputStream((byte[]) lstObj.get("str")));
            String str2 =
                structureHuffmanCoding.decode(new ByteArrayInputStream((byte[]) obj.get("str")));
            if (str1.equals(str2)) {
              duplicates.add((ObjectId) obj.get("_id"));
              System.out.println("found duplicate");
              System.out.println((ObjectId) obj.get("_id"));
              System.out.println("because of " + (ObjectId) lstObj.get("_id"));
              count++;
            } else {
              countMiss++;
            }
          }
        }
      }
      list.add(obj);
    }
    System.out.println(count + " " + countMiss);
    System.out.print("removing index ... ");
    collection.dropIndex(new BasicDBObject().append("pos", 1));
    System.out.println("done");
    mongoClient.close();
  }
}

package bgu.bio.indexing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Properties;

import org.apache.log4j.Logger;

import util.TimeFormat;
import bgu.bio.adt.tuples.Pair;
import bgu.bio.indexing.options.MongoDBIndexOptions;
import bgu.bio.indexing.workers.IndexingWorker;
import bgu.bio.stems.filereader.FastaFormatReaderGenome;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.AnnotatedSequencePool;
import bgu.bio.stems.indexing.PropertiesConstants;
import bgu.bio.stems.mongodb.StemFieldsConstants;
import bgu.bio.stems.mongodb.encoders.MongoEncoder;
import bgu.bio.util.alphabet.RnaAlphabet;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.ReadPreference;

public class MongoDBIndexer {

  private HashMap<String, Object> argumentsMap;
  private Logger logger;
  private File inputFile;

  /**
   * Used only for debug purposes
   */
  private ArrayList<MongoEncoder> extendedEnocders;

  public MongoDBIndexer() {
    extendedEnocders = new ArrayList<MongoEncoder>();
    argumentsMap = new HashMap<String, Object>();
    logger = Logger.getLogger(MongoDBIndexer.class.getCanonicalName());
  }

  public MongoDBIndexer(ArrayList<MongoEncoder> extendedEncoders) {
    this();
    extendedEnocders = extendedEncoders;
  }

  public void loadParams(String[] args) {
    logger.info("Started indexing with arguments " + Arrays.toString(args));
    MongoDBIndexOptions options = new MongoDBIndexOptions();
    options.buildOptions();
    options.loadParams(args, argumentsMap);
    if (options.remaining.length == 0) {
      System.err.println("Must provide input.");
      System.exit(3);
    }
    String inputData = options.remaining[0];
    inputFile = new File(inputData);
    // check if file
    if (!inputFile.isFile() || !inputFile.exists()) {
      System.err.println("Input isn't a file or doesn't exist.");
      System.exit(3);
    }
  }

  public void run() {

    long time = System.currentTimeMillis();
    Properties props = new Properties();
    InputStream propFile = (InputStream) argumentsMap.get(MongoDBIndexOptions.FILTER_PROP_FILE);

    try {
      props.load(propFile);
      propFile.close();
    } catch (IOException ex) {
      ex.printStackTrace();
      System.exit(10);
    }

    time = System.currentTimeMillis();
    String serverLocation = (String) argumentsMap.get(MongoDBIndexOptions.SERVER_LOCATION);
    logger.info("Connecting to MongoDB database at: " + serverLocation);
    System.out.println("Connecting to MongoDB database at: " + serverLocation);
    // connect to the mongoDB
    String[] splitLocation = serverLocation.split(":");
    MongoClient mongoClient = null;
    try {
      mongoClient = new MongoClient(splitLocation[0], Integer.parseInt(splitLocation[1]));
    } catch (IOException ex) {
      ex.printStackTrace();
      System.exit(10);
    }
    String databaseName = (String) argumentsMap.get(MongoDBIndexOptions.DATABASE_NAME);
    mongoClient.setReadPreference(ReadPreference.secondaryPreferred());
    DB db = mongoClient.getDB(databaseName);

    System.out.println("Done");
    String collectionName = (String) argumentsMap.get(MongoDBIndexOptions.COLLECTION_NAME);
    boolean bothStrands = !(boolean) argumentsMap.get(MongoDBIndexOptions.SINGLE_STRAND);
    String[] collectionNames;
    String[] strandName;
    File[] inputFiles;
    if (bothStrands) {
      collectionNames = new String[] {collectionName + "_PLUS", collectionName + "_MINUS"};
      strandName = new String[] {"+", "-"};

      inputFiles = new File[] {inputFile, reverseInput(inputFile)};
    } else {
      collectionNames = new String[] {collectionName};
      strandName = new String[] {"?"};
      inputFiles = new File[] {inputFile};
    }

    for (int iter = 0; iter < collectionNames.length; iter++) {
      String currentCollectionName = collectionNames[iter];
      String currentStrand = strandName[iter];
      File currentFile = inputFiles[iter];

      System.out.println("Collection name " + currentCollectionName);
      if (db.collectionExists(currentCollectionName)) {
        boolean override = (Boolean) argumentsMap.get(MongoDBIndexOptions.OVERRIDE_DATA);
        if (override) {
          // drop the collection if exist
          DBCollection col = db.getCollection(currentCollectionName);
          col.drop();
        } else {
          logger.error("Collection exist, use override option to delete current information");
          System.err.println("Collection exist, use override option to delete current information");
          System.exit(1);
        }
      }
      DBCollection collection = db.getCollection(currentCollectionName);

      time = System.currentTimeMillis();

      final int numberOfThreads = (int) argumentsMap.get(MongoDBIndexOptions.THREADS);
      ArrayList<Pair<File, Integer>> subfiles = new ArrayList<Pair<File, Integer>>();
      splitInput(currentFile,
          2 * Integer.parseInt(props.getProperty(PropertiesConstants.WINDOW_SIZE_STRING)),
          numberOfThreads, subfiles);
      // read file for number of bases
      Pair<Integer, String> data = calculateGenomicData(currentFile);

      ArrayList<Thread> threads = new ArrayList<Thread>();
      ArrayList<IndexingWorker> workers = new ArrayList<IndexingWorker>();
      for (int t = 0; t < numberOfThreads; t++) {
        IndexingWorker worker =
            new IndexingWorker(subfiles.get(t).getFirst(), subfiles.get(t).getSecond(), props,
                extendedEnocders, collection);
        workers.add(worker);
        threads.add(new Thread(worker, "Worker" + t));
      }

      // start the threads
      for (int t = 0; t < numberOfThreads; t++) {
        threads.get(t).start();
      }
      final boolean interactive = (Boolean) argumentsMap.get(MongoDBIndexOptions.INTERACTIVE);
      Reporter reporter = null;
      Thread repoterThread = null;
      if (interactive) {
        reporter = new Reporter(workers);
        repoterThread = new Thread(reporter, "Reporter");
        repoterThread.start();
      }

      // wait for the threads
      for (int t = 0; t < numberOfThreads; t++) {
        try {
          threads.get(t).join();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
      if (interactive) {
        reporter.shouldRun = false;
        try {
          repoterThread.join();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }

      // collect data
      int counter = 0;
      for (int t = 0; t < numberOfThreads; t++) {
        counter += workers.get(t).getCounter();
      }

      // creating the statistics document
      BasicDBObject stats = new BasicDBObject();
      // check if info collection is available
      boolean shouldIndexInfo = false;
      if (db.collectionExists("info")) {
        shouldIndexInfo = true;
      }
      DBCollection statsCollection = db.getCollection("info");
      stats.append("collection", collection.getName());
      stats.append("length", data.getFirst());
      stats.append("stems", counter);
      stats.append("strand", currentStrand);
      stats.append("header", data.getSecond());
      statsCollection.update(new BasicDBObject().append("collection", collection.getName()), stats,
          true, false);
      if (shouldIndexInfo) {
        statsCollection.createIndex(new BasicDBObject().append("collection", 1));
      }
      logger.info("Indexing took "
          + TimeFormat.formatMilliseconds(System.currentTimeMillis() - time));
      System.out.println("Indexing took "
          + TimeFormat.formatMilliseconds(System.currentTimeMillis() - time));
      System.out.println("Number of stems written: " + counter);

      time = System.currentTimeMillis();

      System.out.println();
      // .append("background",true)
      System.out.println("Creating indexes in MongoDB ... ");
      boolean allIndexes = false;
      // creating indexes
      collection.createIndex(new BasicDBObject().append(StemFieldsConstants.POSITION_END, 1),
          new BasicDBObject().append("name", "index" + StemFieldsConstants.POSITION_END));
      logger.info("Created index in mongoDB by ending position");
      if (allIndexes) {
        ArrayList<String[]> perms = new ArrayList<String[]>();
        ArrayList<String> vals = new ArrayList<String>();
        vals.add(StemFieldsConstants.ENERGY);
        vals.add(StemFieldsConstants.LOOP_LENGTH);
        vals.add(StemFieldsConstants.AVG_STACK_SIZE);
        vals.add(StemFieldsConstants.STD_STACK_SIZE);
        vals.add(StemFieldsConstants.POSITION_END);
        permute(vals, 0, perms);
        for (int i = 0; i < perms.size(); i++) {
          BasicDBObject obj = new BasicDBObject();
          String[] perm = perms.get(i);
          System.out.println("Creating index for " + Arrays.toString(perm));
          for (int p = 0; p < perm.length; p++) {
            obj.append(perm[p], 1);
          }
          collection.createIndex(obj);
        }
      } else {
        collection.createIndex(
            new BasicDBObject().append(StemFieldsConstants.ENERGY, 1)
                .append(StemFieldsConstants.LOOP_LENGTH, 1)
                .append(StemFieldsConstants.AVG_STACK_SIZE, 1)
                .append(StemFieldsConstants.STD_STACK_SIZE, 1)
                .append(StemFieldsConstants.POSITION_END, 1),
            new BasicDBObject().append("name", StemFieldsConstants.ENERGY + "_"
                + StemFieldsConstants.LOOP_LENGTH + "_" + StemFieldsConstants.AVG_STACK_SIZE + "_"
                + StemFieldsConstants.STD_STACK_SIZE + "_" + StemFieldsConstants.POSITION_END));
      }

      System.out.println("MongoDB indexing took "
          + TimeFormat.formatMilliseconds(System.currentTimeMillis() - time));
      logger.info("MongoDB indexing took "
          + TimeFormat.formatMilliseconds(System.currentTimeMillis() - time));
    }
    mongoClient.close();
  }

  private void permute(ArrayList<String> arr, int k, ArrayList<String[]> permutations) {
    for (int i = k; i < arr.size(); i++) {
      Collections.swap(arr, i, k);
      permute(arr, k + 1, permutations);
      Collections.swap(arr, k, i);
    }
    if (k == arr.size() - 1) {
      permutations.add(arr.toArray(new String[0]));
    }
  }

  private File reverseInput(File inputFile) {
    try {
      File f = File.createTempFile("StemSearch-reverse-" + inputFile.getName(), ".fasta");
      f.deleteOnExit();
      BufferedReader reader = new BufferedReader(new FileReader(inputFile));
      final String header = reader.readLine();
      String line = reader.readLine();
      StringBuilder sb = new StringBuilder();
      while (line != null) {
        sb.append(line);
        line = reader.readLine();
      }
      reader.close();
      BufferedWriter writer = new BufferedWriter(new FileWriter(f));
      char[] chars = new char[26];
      chars['U' - 'A'] = 'A';
      chars['T' - 'A'] = 'A';
      chars['G' - 'A'] = 'C';
      chars['C' - 'A'] = 'G';
      chars['A' - 'A'] = 'U';
      writer.write(header + "\n");
      int j = 0;
      for (int i = sb.length() - 1; i >= 0; i--) {
        j++;

        final char output = chars[Character.toUpperCase(sb.charAt(i)) - 'A'];
        writer.write(output);
        if ((j + 1) % 50 == 0) {
          writer.write('\n');
        }
      }

      writer.close();
      return f;
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  private void splitInput(File inputFile, int overlapSize, int threads,
      ArrayList<Pair<File, Integer>> subfiles) {
    FastaFormatReaderGenome reader = new FastaFormatReaderGenome(20000);
    RnaAlphabet alphabet = RnaAlphabet.getInstance();
    reader.setReader(inputFile, false);
    StringBuilder sb = new StringBuilder();
    while (reader.hasNext()) {
      AnnotatedSequence seq = reader.next();
      alphabet.convertToRna(seq.getSequence());
      sb.append(seq.getSequence().array());
    }
    reader.close();

    int size = sb.length();
    int fileSize = size / threads;
    for (int i = 0; i < threads; i++) {
      try {
        File f = File.createTempFile("StemSearch-file" + i + "_", ".fasta");
        f.deleteOnExit();
        BufferedWriter writer = new BufferedWriter(new FileWriter(f));
        writer.write(">split" + i + "\n");
        String content =
            sb.substring(i * fileSize, Math.min(sb.length(), (i + 1) * fileSize + overlapSize));
        for (int c = 0; c < content.length(); c++) {
          if ((c + 1) % 50 == 0) {
            writer.write("\n");
          }
          writer.write(content.charAt(c));
        }
        writer.close();
        subfiles.add(new Pair<File, Integer>(f, new Integer(i * fileSize)));
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

  }

  private class Reporter implements Runnable {

    ArrayList<IndexingWorker> workers;
    boolean shouldRun = true;

    public Reporter(ArrayList<IndexingWorker> workers) {
      super();
      this.workers = workers;
    }

    @Override
    public void run() {
      try {
        Thread.sleep(4000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      while (shouldRun) {
        for (int t = 0; t < workers.size(); t++) {
          System.out.println(workers.get(t).getStatus());
        }
        try {
          Thread.sleep(4000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        System.out.print("\033[H\033[2J");
        System.out.flush();
      }
    }

  }

  private Pair<Integer, String> calculateGenomicData(File file) {
    int basesInGenome = 0;
    FastaFormatReaderGenome reader = new FastaFormatReaderGenome(50000);
    String header = null;
    reader.setReader(file, false);
    AnnotatedSequencePool pool = new AnnotatedSequencePool();
    reader.setAnnotatedSequencePool(pool);
    while (reader.hasNext()) {
      AnnotatedSequence seq = reader.next();
      if (header == null) {
        header = seq.getSequenceId();
      }
      basesInGenome += seq.getLength();
      pool.releaseSequence(seq);
    }
    reader.close();
    return new Pair<Integer, String>(basesInGenome, header);
  }

  public static void main(String[] args) {
    MongoDBIndexer indexer = new MongoDBIndexer();
    indexer.loadParams(args);
    indexer.run();
  }
}

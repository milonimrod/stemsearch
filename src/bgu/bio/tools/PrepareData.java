package bgu.bio.tools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;

import bgu.bio.adt.tuples.IntPair;
import bgu.bio.ds.rna.RNA;
import bgu.bio.stems.filereader.FastaFormatReaderGenome;
import bgu.bio.stems.indexing.AnnotatedSequence;

public class PrepareData {
  public static void main(String[] args) throws IOException {

    if (args == null || args.length != 3) {
      System.out.println("USAGE: " + PrepareData.class.getCanonicalName()
          + " <genomic sequence> <family input> <name>");
      System.exit(1);
    }

    // load the genomic sequence file and size
    FastaFormatReaderGenome reader = new FastaFormatReaderGenome(100000);
    reader.setReader(new File(args[0]), false);
    String header = "";
    StringBuilder genomeBuilder = new StringBuilder();
    while (reader.hasNext()) {
      AnnotatedSequence seq = reader.next();
      if (reader.isFirstInSequence()) {
        header = seq.getSequenceId();
      }
      genomeBuilder.append(seq.getSequence().array());
    }
    reader.close();
    String genome = genomeBuilder.toString();

    ArrayList<RNA> list = RNA.loadFromFile(args[1], false);
    int listPos = 0;
    int interval = genome.length() / (list.size() + 1);
    File outputDir = new File(args[2]);
    if (!outputDir.isDirectory()) {
      outputDir.mkdir();
    }
    //copy the members file
    File membersFile = new File(args[1]);
    Files.copy(membersFile.toPath(), new File(args[2] + "/query.fasta").toPath());
    
    FileWriter fileWriter = new FileWriter(args[2] + "/db.fasta");
    BufferedWriter writer = new BufferedWriter(fileWriter);
    writer.write(">" + header + " benchmark of " + args[2] + "\n");
    ArrayList<IntPair> locations = new ArrayList<IntPair>();
    // insert the data
    final int LIMIT_SIZE = 70;
    int relPos = 0;
    int limit = LIMIT_SIZE;
    for (int i = 0; i < genome.length(); i++) {
      if (i != 0 && i % interval == 0 && listPos < list.size()) {
        final RNA rna = list.get(listPos);
        String primary = rna.getPrimary().toLowerCase();
        final int start = relPos + 1;
        for (int j = 0; j < primary.length(); j++) {
          writer.write(primary.charAt(j));
          limit--;
          if (limit == 0) {
            writer.write('\n');
            limit = LIMIT_SIZE;
          }
          relPos++;
        }
        final int end = relPos;
        locations.add(new IntPair(start, end));
        listPos++;
      } else {
        writer.write(genome.charAt(i));
        limit--;
        if (limit == 0) {
          writer.write('\n');
          limit = LIMIT_SIZE;
        }
        relPos++;
      }

    }
    writer.close();
    fileWriter = new FileWriter(args[2] + "/info.txt");
    writer = new BufferedWriter(fileWriter);
    for (int i = 0; i < list.size(); i++) {
      writer.write(list.get(i).getHeader());
      writer.write("\n");
      writer.write(locations.get(i).getFirst() + " " + locations.get(i).getSecond());
      writer.write("\n");
    }

    writer.close();
  }
}

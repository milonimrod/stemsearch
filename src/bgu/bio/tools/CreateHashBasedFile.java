package bgu.bio.tools;

import gnu.trove.list.array.TLongArrayList;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import bgu.bio.stems.hybridization.RnaHybrid;
import bgu.bio.tools.utils.Hashing;

public class CreateHashBasedFile {

  private final char[] letters = new char[] {'A', 'C', 'G', 'U'};
  private final String[] pairs = new String[] {"AU", "UA", "CG", "GC", "GU", "UG"};
  private long served, amount, next, writtenAmount;
  private int[] seq;
  private Object lock;
  private boolean done;
  private RandomAccessFile writer;
  private final BlockingQueue<Long> dataQueue;
  private int amountOfTerminals;

  public CreateHashBasedFile(int size, RandomAccessFile writer, int workers, final long terminal,
      final CountDownLatch latch) {
    served = 0;
    final double pow = Math.pow(letters.length, size * 2 - 2);
    amount = (long) (pow * pairs.length);
    System.out.println("Starting the server pair handler");
    seq = new int[size * 2 - 2];
    Arrays.fill(seq, 0);
    done = false;
    amountOfTerminals = workers;
    this.writer = writer;
    try {
      writer.writeInt(size * 2);
    } catch (IOException e) {
      e.printStackTrace();
    }
    next = 1;
    lock = new Object();
    dataQueue = new ArrayBlockingQueue<Long>(10000000);

    Thread t = new Thread(new Runnable() {

      @Override
      public void run() {
        int pairIndex = 0;
        Hashing hashing = new Hashing();
        StringBuilder sb = new StringBuilder();
        while (!done) {
          sb.setLength(0);
          convertToString(sb, seq, 0, seq.length / 2);
          sb.append(pairs[pairIndex]);
          convertToString(sb, seq, seq.length / 2, seq.length);
          try {
            long hashed = hashing.hashSeq(sb);
            if (hashed > 4294967295l) {
              System.out.println(sb);
            }
            dataQueue.put(hashed);
            served++;
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          pairIndex++;
          if (pairIndex == pairs.length) {
            pairIndex = 0;
            if (!increment(seq)) {
              done = true;
              for (int x = 0; x < amountOfTerminals * 2; x++) {
                try {
                  dataQueue.put(terminal);
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              }
            }
          }

          if ((served * 100 / amount) == next) {
            System.out.println(next + "% " + (served + " of " + amount));
            next++;
          }
          if (served % 1000000 == 0) {
            // System.out.println(served);
          }
        }
        latch.countDown();
      }
    }, "submitter");
    t.setPriority(Thread.MAX_PRIORITY);
    t.start();
  }

  private void convertToString(StringBuilder sb, int[] sequence, int from, int to) {
    for (int i = from; i < to; i++) {
      sb.append(letters[sequence[i]]);
    }
  }

  private boolean increment(int[] sequence) {
    final int BASE = 4;
    for (int i = sequence.length - 1; i >= 0; i--) {
      sequence[i]++;
      if (sequence[i] == BASE) {
        sequence[i] = 0;
      } else {
        return true;
      }
    }
    return false;
  }

  public static void main(String[] args) throws IOException, InterruptedException {
    RandomAccessFile out = new RandomAccessFile(args[1], "rw");

    // hold space for counter
    out.writeLong(0);

    final int TUPLE_SIZE = 8;
    final double ENERGY_TRESHOLD = Double.parseDouble(args[2]);
    final int AMOUNT_OF_WORKERS = Integer.parseInt(args[0]);
    final int TERMINAL = 0;
    final CountDownLatch latch = new CountDownLatch(AMOUNT_OF_WORKERS);
    final CreateHashBasedFile service =
        new CreateHashBasedFile(TUPLE_SIZE, out, AMOUNT_OF_WORKERS, TERMINAL, latch);
    ExecutorService ex = Executors.newFixedThreadPool(AMOUNT_OF_WORKERS);

    for (int i = 0; i < AMOUNT_OF_WORKERS - 1; i++) {
      Runnable jobProcessor = new Runnable() {

        private TLongArrayList queue = new TLongArrayList();

        @Override
        public void run() {
          Hashing hashing = new Hashing();
          RnaHybrid rna = null;
          try {
            rna = new RnaHybrid();
          } catch (IOException e) {
            e.printStackTrace();
          }
          rna.setIgnoreDangling(true);
          long sequenceHashed = -1;
          StringBuilder sb = new StringBuilder();
          char[] seq1 = new char[TUPLE_SIZE];
          char[] seq2 = new char[TUPLE_SIZE];
          try {
            sequenceHashed = service.dataQueue.take();
          } catch (InterruptedException e) {
            e.printStackTrace();
            return;
          }
          while (sequenceHashed != TERMINAL) {
            hashing.unhash(sequenceHashed, TUPLE_SIZE * 2, sb);
            for (int i = 0; i < TUPLE_SIZE; i++) {
              seq1[i] = sb.charAt(i);
              seq2[i] = sb.charAt(TUPLE_SIZE + i);
            }
            rna.setSeq1(seq1);
            rna.setSeq2(seq2);
            double energy = rna.hybridize();

            if (energy <= ENERGY_TRESHOLD) {
              queue.add(sequenceHashed);

              if (queue.size() >= 100000) {
                service.submitQueue(queue);
                queue.resetQuick();
              }
            }

            try {
              sequenceHashed = service.dataQueue.take();
            } catch (InterruptedException e) {
              e.printStackTrace();
              return;
            }
          }
          System.out.println("Runner done. last empty ... ");
          service.submitQueue(queue);
          queue.clear();

          System.out.println("Empty was done");
          latch.countDown();
        }
      };
      ex.execute(jobProcessor);
    }

    latch.await();
    ex.shutdown();

    // write the counter
    out.seek(0);
    out.writeLong(service.writtenAmount);
    out.close();
    System.out.printf("Total amount is %d%n", service.writtenAmount);
  }

  protected void submitQueue(TLongArrayList queue) {
    synchronized (lock) {
      writtenAmount += queue.size();
      try {
        for (int i = 0; i < queue.size(); i++) {
          writer.writeLong(queue.get(i));

        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

  }
}

package bgu.bio.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.PrintStream;

import bgu.bio.stems.filereader.FastaFormatReader;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.util.CharBuffer;
import bgu.bio.util.UShuffle;

public class ShuffleGenome {
  public static String shuffle(String fileName, PrintStream out, int limit,
      final boolean printHeader) {
    FastaFormatReader reader = new FastaFormatReader(500000);
    String ans = "";
    InputStreamReader file;
    try {
      file = new InputStreamReader(new FileInputStream(new File(fileName)));
      reader.setReader(file, false);
      UShuffle shuffler = new UShuffle();
      int current = 0;
      while (reader.hasNext()) {
        AnnotatedSequence seq = reader.next();
        CharBuffer buff = seq.getSequence();
        if (reader.isFirstInSequence()) {
          ans = reader.getSequenceId();
          if (printHeader) {
            out.println(">" + reader.getSequenceId());
          }
        }

        char[] t = new char[buff.length()];
        shuffler.shuffle(buff.array(), t, buff.length(), 2);
        for (int i = 0; i < t.length; i++) {
          out.print(t[i]);
          current++;
          if (limit > 0 && current == limit) {
            out.println();
            current = 0;
          }
        }
      }

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }

    return ans;
  }

  public static void main(String[] args) throws FileNotFoundException {
    if (args.length == 1) {
      ShuffleGenome.shuffle(args[0], System.out, 70, true);
    } else {
      ShuffleGenome.shuffle(args[0], new PrintStream(args[1]), 70, true);
    }
  }
}

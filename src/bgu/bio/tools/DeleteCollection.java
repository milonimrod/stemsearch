package bgu.bio.tools;

import java.net.UnknownHostException;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;

public class DeleteCollection {
  public static void main(String[] args) throws NumberFormatException, UnknownHostException {
    MongoClient mongoClient = null;
    if (args[0].indexOf(':') != -1) {
      String[] split = args[0].split(":");
      mongoClient = new MongoClient(split[0].trim(), Integer.parseInt(split[1].trim()));
    } else {
      mongoClient = new MongoClient(args[0]);
    }
    String databaseName = args[1];
    String collectionName = args[2];
    DB db = mongoClient.getDB(databaseName);
    DBCollection info = db.getCollection("info");
    info.remove(new BasicDBObject().append("collection", collectionName));
    DBCollection col = db.getCollection(collectionName);
    col.drop();
    mongoClient.close();
  }
}

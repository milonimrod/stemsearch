package bgu.bio.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;

import bgu.bio.stems.filereader.FastaFormatReader;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.util.CharBuffer;
import bgu.bio.util.alphabet.RnaAlphabet;

public class GenomicStastistics {
  public static void main(String[] args) {
    FastaFormatReader reader = new FastaFormatReader(10000);


    InputStreamReader file;
    try {
      file = new InputStreamReader(new FileInputStream(new File(args[0])));
      reader.setReader(file, false);
      char previous = 'A';
      char current = 'A';
      RnaAlphabet alphabet = RnaAlphabet.getInstance();
      double[] pairs = new double[16];
      long counter = 0;
      while (reader.hasNext()) {
        AnnotatedSequence seq = reader.next();
        CharBuffer buff = seq.getSequence();
        for (int i = 0; i < buff.length(); i++) {
          current = buff.charAt(i);
          try {
            pairs[alphabet.encode(previous) + 4 * alphabet.encode(current)]++;
          } catch (Exception e) {
            System.out.println(previous + " " + current);
            System.out.println(buff);
          }
          previous = current;
          counter++;
        }
      }
      pairs[alphabet.encode('A') + 4 * alphabet.encode('A')]--;

      char[] chars = new char[] {'A', 'C', 'G', 'U'};

      for (int i = 0; i < pairs.length; i++) {
        pairs[i] /= counter;
      }

      for (int i = 0; i < chars.length; i++) {
        for (int j = 0; j < chars.length; j++) {
          System.out.println(chars[i] + "|" + chars[j] + ": "
              + pairs[alphabet.encode(chars[i]) + 4 * alphabet.encode(chars[j])]);
        }

      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }


  }
}

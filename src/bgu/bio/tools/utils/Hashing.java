package bgu.bio.tools.utils;

import bgu.bio.util.alphabet.RnaAlphabet;

public class Hashing {
  RnaAlphabet alphabet = RnaAlphabet.getInstance();

  public void unhash(long num, int size, StringBuilder sb) {
    sb.setLength(0);
    sb.setLength(size);
    while (size >= 1) {
      sb.setCharAt(size - 1, alphabet.decode((int) (num % 4)));
      num = num / 4l;
      size--;
    }

  }

  public long hashSeq(CharSequence str) {
    long num = 0;
    for (int i = 0; i < str.length(); i++) {
      num = num * 4l + alphabet.encode(str.charAt(i));
    }
    return num;
  }
}

package bgu.bio.stems.hybridization;

import java.io.IOException;

import bgu.bio.adt.list.PooledLinkedList;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.AnnotatedSequencePool;
import bgu.bio.stems.indexing.Strand;
import bgu.bio.stems.indexing.filtering.SequenceFilter;
import bgu.bio.stems.indexing.filtering.SequenceOracle;
import bgu.bio.stems.util.RnaUtils;
import bgu.bio.util.CharBuffer;

/**
 * This class assumes it receives partly aligned stem-loops, and uses energy calculations to:
 * <ol>
 * <li>check whether the candidate structure energy is below a given threshold,
 * <li>set the structure by energy and
 * <li>elongate in both directions (currently not implemented).
 * </ol>
 * 
 * @author sivany
 */
public class HybridizationFilter implements SequenceFilter {

  private RnaHybrid rnaHybrid;
  private PooledLinkedList<AnnotatedSequence> sequencesQueue;
  private int tupleSize;
  private int padding;
  private double finalThreshold;
  private double initThreshold;
  private int extensionSize;
  private double extensionThreshold;
  private char[] leftStruct;
  private char[] rightStruct;
  private char[] seq;
  private char[] structure;
  private static final int WINDOW_MULTIPLICATION_FACTOR = 4;
  private int prefixExtension;
  private long pushed, rejected;

  private AnnotatedSequencePool aSPool;

  public HybridizationFilter(int baseStemLength, double finalThreshold, int windowSize,
      double initThreshold, int extensionSize, double extensionThreshold) throws IOException {

    this(baseStemLength, finalThreshold, windowSize, initThreshold, extensionSize,
        extensionThreshold, 0, -1, -1);
  }

  public HybridizationFilter(int baseStemLength, double finalThreshold, int windowSize,
      double initThreshold, int extensionSize, double extensionThreshold, int prefixExtension)
      throws IOException {

    this(baseStemLength, finalThreshold, windowSize, initThreshold, extensionSize,
        extensionThreshold, prefixExtension, -1, -1);
  }

  public HybridizationFilter(int baseStemLength, double finalThreshold, int windowSize,
      double initThreshold, int extensionSize, double extensionThreshold, int prefixExtension,
      int maxInnerLoop, int maxBuldge) throws IOException {
    if (maxBuldge < 0 || maxInnerLoop < 0) {
      rnaHybrid = new RnaHybrid();
    } else {
      rnaHybrid = new RnaHybrid(maxInnerLoop, maxBuldge);
    }
    rnaHybrid.setIgnoreDangling(true);

    this.prefixExtension = prefixExtension;
    sequencesQueue = new PooledLinkedList<AnnotatedSequence>();
    tupleSize = baseStemLength;
    padding = windowSize - tupleSize;
    this.finalThreshold = finalThreshold;
    this.initThreshold = initThreshold;
    this.extensionSize = extensionSize;
    this.extensionThreshold = extensionThreshold;

    this.leftStruct = new char[windowSize * WINDOW_MULTIPLICATION_FACTOR];
    this.rightStruct = new char[windowSize * WINDOW_MULTIPLICATION_FACTOR];
    this.seq = new char[windowSize * WINDOW_MULTIPLICATION_FACTOR];
    this.structure = new char[windowSize * WINDOW_MULTIPLICATION_FACTOR];
    aSPool = AnnotatedSequencePool.getInstance();
    pushed = 0;
    rejected = 0;
  }

  @Override
  public void setAnnotatedSequencePool(AnnotatedSequencePool pool) {
    aSPool = pool;
  }

  @Override
  public AnnotatedSequence next() {
    if (!sequencesQueue.isEmpty()) {
      return sequencesQueue.removeFirst();
    }
    return null;
  }

  @Override
  public void push(AnnotatedSequence sequence) {
    pushed++;
    if (sequence.getStructureEnd() - sequence.getStructureStart() < 2 * tupleSize) {
      throw new RuntimeException("Seqeuence information is too close");
    }
    // put the tuples into rnaHybrid
    boolean from5to3 = !sequence.getStrand().equals(Strand.MINUS);
    CharBuffer charBuffer = sequence.getSequence();
    int structureStart = sequence.getStructureStart();
    int structureEnd = sequence.getStructureEnd() + 1;
    if (from5to3) {
      // 1------> <------2
      // 5' ===========================================> 3'
      rnaHybrid.setSeq1(charBuffer, structureStart, tupleSize, padding, false);
      rnaHybrid.setSeq2(charBuffer, structureEnd - tupleSize, tupleSize, padding, true);
    } else {
      // 2------> <------1
      // 3' <========================================== 5'
      rnaHybrid.setSeq2(charBuffer, structureStart, tupleSize, padding, false);
      rnaHybrid.setSeq1(charBuffer, structureEnd - tupleSize, tupleSize, padding, true);
    }

    // get best hybridization
    double hybridScore = rnaHybrid.hybridize();
    double currThreshold = initThreshold;
    boolean pass = false;
    int location = sequence.getLocation();
    /*
     * checkPairedEnd is used to check whether it is legal to elongate after energy threshold was
     * not reached. This can be done only once, and then elongation can only be result of energy.
     */

    // elongation loop
    int elongations = 0;
    while (elongations < 10 && hybridScore < currThreshold) {
      elongations++;
      pass = true;

      currThreshold = hybridScore + extensionThreshold;

      int newStart = structureStart - extensionSize;

      if (newStart > 0) {
        int startExtension = extensionSize;
        if (newStart + location < 1) {
          startExtension = structureStart + location - 1;
          newStart = structureStart - startExtension;
        }
        // elongate upstream
        int endBegins = structureEnd;
        int endEnds = endBegins + extensionSize;

        // find best hybridization after elongation
        if (elongate(sequence, from5to3, newStart, startExtension, endBegins)) {
          hybridScore = rnaHybrid.hybridize();
          structureStart = newStart;
          structureEnd = endEnds;
        } else {
          hybridScore = rnaHybrid.hybridize();
          break;
        }
      }
    }

    if (pass) {
      // elongate the stem if possible
      double currentEnergy = Double.POSITIVE_INFINITY;
      while (elongations < 10 && rnaHybrid.pairedEnd() && currentEnergy > hybridScore) {
        elongations++;
        while (rnaHybrid.pairedEnd()) {
          final int newStart = structureStart - extensionSize;
          if (newStart > 0 && newStart + location > 0) {
            // elongate upstream
            int endBegins = structureEnd;
            int endEnds = endBegins + extensionSize;

            // find best hybridization after elongation
            if (elongate(sequence, from5to3, newStart, extensionSize, endBegins)) {
              hybridScore = rnaHybrid.hybridizeLimitBulges(true);
              structureStart = newStart;
              structureEnd = endEnds;
            } else {
              hybridScore = rnaHybrid.hybridizeLimitBulges(true);
              break;
            }
          } else {
            break;
          }
        }
      }

      // get structure from rnaHybrid
      final int leftStructSize = rnaHybrid.getSeq1Structure(leftStruct);
      final int rightStructSize = rnaHybrid.getSeq2Structure(rightStruct);
      int leftDots = 0;
      int rightDots = 0;

      for (int i = rightStructSize - rightDots - 1; i >= 0 && rightStruct[i] == '.'; i--) {
        rightDots++;
      }

      // advance the structure
      for (int i = leftDots; i < leftStructSize && leftStruct[i] == '.'; i++) {
        leftDots++;
      }

      if (from5to3) {
        structureStart += leftDots;
        structureEnd -= rightDots;
      } else {
        structureStart += rightDots;
        structureEnd -= leftDots;
      }

      if (hybridScore > finalThreshold) {
        rejected++;
        return;
      }

      // generate the annotated sequence to return
      setStructureAddToQueue(sequence, from5to3, hybridScore, structureStart, structureEnd,
          leftStructSize, rightStructSize, leftDots, rightDots);
    } else {
      rejected++;
    }
  }

  /**
   * @param sequence
   * @param from5to3
   * @param downExtention
   * @param upExtention
   * @param newStart
   * @param startExtension
   * @param endBegins
   */
  private boolean elongate(AnnotatedSequence sequence, boolean from5to3, int newStart,
      int startExtension, int endBegins) {
    boolean elongated = true;

    // elongate upstream
    if (from5to3) {
      elongated = rnaHybrid.elongateSeq1(sequence.getSequence(), newStart, startExtension, false);
    } else {
      elongated = rnaHybrid.elongateSeq2(sequence.getSequence(), newStart, startExtension, false);
    }

    if (elongated) {
      // elongate downstream
      if (endBegins < sequence.getLength()) {
        if (from5to3) {
          elongated =
              rnaHybrid.elongateSeq2(sequence.getSequence(), endBegins, extensionSize, true);
        } else {
          elongated =
              rnaHybrid.elongateSeq1(sequence.getSequence(), endBegins, extensionSize, true);
        }
      }
    }
    return elongated;
  }

  private int setStructureAddToQueue(AnnotatedSequence sequence, boolean from5to3,
      double hybridScore, int structureStart, int structureEnd, int leftStructSize,
      int rightStructSize, int leftDots, int rightDots) {

    setStructure(sequence, leftStructSize, leftDots, rightStructSize, rightDots, structureEnd,
        structureStart);

    AnnotatedSequence toAdd = null;
    int extend = prefixExtension;
    if (structureStart + sequence.getLocation() <= extend) {
      if (structureStart + sequence.getLocation() < 1) {
        extend = 0;
      } else {
        extend = structureStart + sequence.getLocation() - 1;
      }

    }
    if (from5to3) {
      buildSeq(sequence, structureStart - extend, structureEnd - structureStart + extend, false);
    } else {
      buildSeq(sequence, structureStart, structureEnd - structureStart + extend, true);
    }
    toAdd =
        aSPool.getSequence(sequence.getSequenceId(), sequence.getLocation() + structureStart
            - extend, seq, 0, structureEnd - structureStart + extend, structure, structureEnd
            - structureStart + extend, sequence.getStrand());
    toAdd.setStructureStart(0);
    toAdd.setStructureEnd(structureEnd - structureStart - 1 + extend);
    toAdd.setEnergy(hybridScore);
    sequencesQueue.addLast(toAdd);
    return structureEnd - structureStart + extend;
  }

  private void buildSeq(AnnotatedSequence as, int structureStartIndex, int length, boolean reverse) {
    CharBuffer sequence = as.getSequence();
    for (int i = 0; i < length; i++) {
      seq[i] = sequence.charAt(structureStartIndex + i);
    }
    if (reverse) {
      RnaUtils.reverse(seq, length);
    }
  }

  private void setStructure(AnnotatedSequence sequence, int leftStructSize, int leftDots,
      int rightStructSize, int rightDots, int structureEnd, int structureStart) {

    System.arraycopy(leftStruct, leftDots, structure, 0, leftStructSize - leftDots);
    System.arraycopy(rightStruct, 0, structure, structureEnd - structureStart - rightStructSize
        + rightDots, rightStructSize - rightDots);

    for (int i = leftStructSize - leftDots; i < structureEnd - structureStart - rightStructSize
        + rightDots; i++) {
      structure[i] = '.';
    }

    // move the structure downstream
    int extend = prefixExtension;
    if (structureStart + sequence.getLocation() <= extend) {
      if (structureStart + sequence.getLocation() < 1) {
        extend = 0;
      } else {
        extend = structureStart + sequence.getLocation() - 1;
      }

    }
    if (extend > 0) {
      for (int i = structureEnd - structureStart - 1; i >= 0; i--) {
        structure[i + extend] = structure[i];
        structure[i] = '.';
      }

      // change the remains
      for (int i = structureEnd - structureStart; i < extend; i++) {
        structure[i] = '.';
      }
    }
  }

  @Override
  public boolean hasNext() {
    return !sequencesQueue.isEmpty();
  }

  @Override
  public void clear() {
    sequencesQueue.clear();
  }

  @Override
  public void setSequenceOracle(SequenceOracle sequenceOracle) {
    // do nothing
  }

  @Override
  public boolean hasRemaining() {
    return false;
  }

  @Override
  public AnnotatedSequence nextRemaining() {
    return null;
  }

  public int getWindowSize() {
    return padding + tupleSize;
  }

  @Override
  public boolean hasRemainingSequenceDone() {
    return false;
  }

  @Override
  public String printMessage() {
    return "Number of rejected is " + rejected + " out of " + pushed + " ("
        + ((1.0 * rejected) / pushed) + ")";
  }
}

package bgu.bio.stems.hybridization;

import java.io.IOException;
import java.util.Formatter;

import bgu.bio.stems.energy.EnergyParameters;
import bgu.bio.util.CharBuffer;
import bgu.bio.util.alphabet.RnaAlphabet;

/**
 * This class enables hybridization of two RNA sequences, based on C code of rna_hybrid.
 * <p>
 * In order to perform hybridization, set seq1 and seq2 and call {@link #hybridize()}. The structure
 * found can be obtained in parenthesis notation using {@link #getSeq1Structure()} and
 * {@link #getSeq2Structure()}, where it is assumed that both sequences are from the same molecule,
 * and seq1 is upstream to seq2.
 * 
 * @author sivany
 */
public class RnaHybrid {

  /** RnaAlphabet for various checks */
  private static final RnaAlphabet alphabet = RnaAlphabet.getInstance();

  /** an additional letter for masking out hits */
  private static final char X = (char) (RnaAlphabet.SIZE - 1);

  /** Maximal value to put in energy calculation matrixes */
  private static final double MAX_VALUE = 65000;

  /** Empty padding */
  private short emptyPadding = alphabet.encode(alphabet.nonComplement());

  /** Marks whether to ignore scores of dangling ends */
  private boolean ignoreDangling;

  /**
   * First sequence and length.
   */
  private short[] seq1;
  private int l1;
  private int start1;
  private int end1;
  private boolean elongated1;

  /**
   * Second sequence and length.
   */
  private short[] seq2;
  private int l2;
  private int start2;
  private int end2;
  private boolean elongated2;

  /**
   * Variables used to determine the state of this object - whether the energy scores and path where
   * calculated.
   */
  private boolean energyCalculated;
  private boolean pathCalculated;
  private boolean elongated;

  /**
   * The calculated path.
   */
  private int[] tracePath;

  /**
   * Tables containing energy scores.
   */
  private double[][] unpairedLeftBottom;
  private double[][] unpairedLeftTop;
  private double[][] closed;

  /**
   * Tables containing trace data.
   */
  private int[][] traceULB;
  private int[][] traceULT;
  private int[][] traceClosed;

  /**
   * Trace data is encoded in an integer using the following conventions:<br>
   * The first byte contains the size of horizontal movement. <br>
   * The second byte contains the size of vertical movement. <br>
   * The first bit in the third byte marks that the next value is to be taken from trace_ulb, and
   * the second bit in the third byte marks that the next value is to be taken from trace_ult. If
   * these two bytes are 0, the next value is to be taken from closed.
   */
  private static final int TRACE_NONE = 0;
  private static final int TRACE_VERTICAL = 1;
  private static final int TRACE_VERTICAL_MASK = 0xFF;
  private static final int TRACE_HORIZONTAL = 1 << 8;
  private static final int TRACE_HORIZONTAL_MASK = 0xFF00;
  private static final int TRACE_DIAGONAL = TRACE_VERTICAL | TRACE_HORIZONTAL;
  private static final int TRACE_UNPAIRED_LEFT_BOT = 1 << 16;
  private static final int TRACE_UNPAIRED_LEFT_TOP = 1 << 17;
  private static final int TRACE_UNPAIRED = TRACE_UNPAIRED_LEFT_BOT | TRACE_UNPAIRED_LEFT_TOP;

  /**
   * Maximal size of loops to try, directly influence performance - larger values will result in
   * longer iterations.
   */
  private int internalLoopUpperLimit = 6;
  private int bulgeLoopUpperLimit = 6;

  /**
   * Energy and base pairing data.
   */
  private EnergyParameters energy;
  // the following arrays are used to avoid creating new array in each call to
  // Matrix.get(int...)
  private int[] array2 = new int[2];
  private int[] array3 = new int[3];
  private int[] array4 = new int[4];
  private int[] array6 = new int[6];
  private int[] array7 = new int[7];
  private int[] array8 = new int[8];

  public RnaHybrid(int internalLoopUpperLimit, int buldgeLoopUpperLimit) throws IOException {
    this();
    this.internalLoopUpperLimit = internalLoopUpperLimit;
    this.bulgeLoopUpperLimit = buldgeLoopUpperLimit;
  }

  /**
   * Constructor, loads energy parameters.
   * 
   * @throws IOException
   */
  public RnaHybrid() throws IOException {
    energy = EnergyParameters.getInstance();

    energyCalculated = false;
    pathCalculated = false;
  }

  /**
   * Set the first sequence.
   * 
   * @param seq1 A char array containing the sequence.
   */
  public void setSeq1(char[] seq1) {
    if (this.seq1 == null || this.seq1.length < seq1.length + 1) {
      this.seq1 = new short[seq1.length + 1];
    }
    convertSeq(seq1, this.seq1, false);
    l1 = seq1.length;
    start1 = 0;
    end1 = l1;
    energyCalculated = false;
    pathCalculated = false;
    elongated1 = false;
    elongated = false;
  }

  /**
   * Set the first sequence.
   * 
   * @param charBuffer A {@link CharBuffer} containing the sequence.
   * @param offset Offset in charBuffer of the sequence.
   * @param length Sequence length.
   */
  public void setSeq1(CharBuffer charBuffer, int offset, int length) {
    setSeq1(charBuffer, offset, length, 0, false);
  }

  /**
   * Set the first sequence.
   * 
   * @param charBuffer A {@link CharBuffer} containing the sequence.
   * @param offset Offset in charBuffer of the sequence.
   * @param length Sequence length.
   * @param padding Number of empty bases to leave before the sequence.
   */
  public void setSeq1(CharBuffer charBuffer, int offset, int length, int padding, boolean reverse) {
    if (this.seq1 == null || this.seq1.length < padding + length + 1) {
      this.seq1 = new short[padding + length + 1];
    }
    convertSeq(charBuffer.array(), offset, length, this.seq1, padding, reverse);
    l1 = padding + length;
    start1 = padding;
    end1 = l1;
    energyCalculated = false;
    pathCalculated = false;
    elongated1 = false;
    elongated = false;
  }

  /**
   * Try to elongate seq1 backward.
   * 
   * @param charBuffer The sequence from which to take the additional data.
   * @param offset Offset in charBuffer of the sequence.
   * @param length Sequence length.
   * @param reverse Whether to reverse the given sequence or not.
   * @return <code> true </code> if addition succeeded, otherwise <code> false </code>
   */
  public boolean elongateSeq1(CharBuffer charBuffer, int offset, int length, boolean reverse) {
    if (start1 < length) {
      return false;
    }

    convertSeq(charBuffer.array(), offset, length, this.seq1, start1 - length, reverse);

    if (!elongated1) {
      end1 = start1;
      elongated1 = true;
    }
    start1 = start1 - length;
    energyCalculated = false;
    pathCalculated = false;
    elongated = true;

    return true;
  }

  /**
   * Set the second sequence.
   * 
   * @param seq2 A char array containing the sequence.
   */
  public void setSeq2(char[] seq2) {
    if (this.seq2 == null || this.seq2.length < seq2.length + 1) {
      this.seq2 = new short[seq2.length + 1];
    }
    convertSeq(seq2, this.seq2, true);
    l2 = seq2.length;
    start2 = 0;
    end2 = l2;
    energyCalculated = false;
    pathCalculated = false;
    elongated2 = false;
    elongated = false;
  }

  /**
   * Set the second sequence.
   * 
   * @param charBuffer A {@link CharBuffer} containing the sequence.
   * @param offset Offset in charBuffer of the sequence.
   * @param length Sequence length.
   */
  public void setSeq2(CharBuffer charBuffer, int offset, int length) {
    setSeq2(charBuffer, offset, length, 0, true);
  }

  /**
   * Set the second sequence.
   * 
   * @param charBuffer A {@link CharBuffer} containing the sequence.
   * @param offset Offset in charBuffer of the sequence.
   * @param length Sequence length.
   * @param padding Number of empty bases to leave before the sequence.
   */
  public void setSeq2(CharBuffer charBuffer, int offset, int length, int padding, boolean reverse) {
    if (this.seq2 == null || this.seq2.length < padding + length + 1) {
      this.seq2 = new short[padding + length + 1];
    }
    convertSeq(charBuffer.array(), offset, length, this.seq2, padding, reverse);
    l2 = padding + length;
    start2 = padding;
    end2 = l2;
    energyCalculated = false;
    pathCalculated = false;
    elongated2 = false;
    elongated = false;
  }

  /**
   * Try to elongate seq2 forward.
   * 
   * @param charBuffer The sequence from which to take the additional data.
   * @param offset Offset in charBuffer of the sequence.
   * @param length Sequence length.
   * @param reverse Whether to reverse the given sequence or not.
   * @return <code> true </code> if addition succeeded, otherwise <code> false </code>
   */
  public boolean elongateSeq2(CharBuffer charBuffer, int offset, int length, boolean reverse) {
    if (start2 < length) {
      return false;
    }

    convertSeq(charBuffer.array(), offset, length, this.seq2, start2 - length, reverse);
    if (!elongated2) {
      end2 = start2;
      elongated2 = true;
    }
    start2 = start2 - length;
    energyCalculated = false;
    pathCalculated = false;
    elongated = true;

    return true;
  }

  private void convertSeq(char[] src, short[] seq12, boolean reverse) {
    seq12[0] = emptyPadding;
    int add = reverse ? src.length - 1 : 0;
    int mult = reverse ? -1 : 1;
    for (int i = 0; i < src.length; i++) {
      seq12[i + 1] = RnaAlphabet.getInstance().encode(src[add + mult * i]);
    }
  }

  private void convertSeq(char[] src, int offset, int length, short[] dest, int padding,
      boolean reverse) {
    dest[padding] = emptyPadding;
    int add = offset + (reverse ? length - 1 : 0);
    int mult = reverse ? -1 : 1;
    for (int i = 0; i < length; i++) {
      dest[padding + i + 1] = alphabet.encode(src[add + mult * i]);
    }
  }

  /**
   * Perform hybridization of the given sequences.
   * 
   * @return Hybridization score.
   */
  public synchronized double hybridize() {
    if (l1 == 0 || l2 == 0) {
      throw new UnsupportedOperationException("Attempt to hybridize empty sequence.");
    }

    // Set the matrices for a new hybridization
    if (unpairedLeftBottom == null || unpairedLeftBottom.length < l1
        || unpairedLeftBottom[0].length < l2) {
      allocateMatrices();
    }

    fillMatrices();

    // get score
    double score = getScore(0, 0);

    energyCalculated = true;
    elongated1 = false;
    elongated2 = false;

    return score;
  }

  public synchronized double hybridizeLimitBulges(boolean noBulges) {
    if (l1 == 0 || l2 == 0) {
      throw new UnsupportedOperationException("Attempt to hybridize empty sequence.");
    }

    int maxBulge = Math.min(bulgeLoopUpperLimit, Math.min(l1, l2) / 2);
    if (noBulges) {
      maxBulge = 0;
    }

    // Set the matrices for a new hybridization
    if (unpairedLeftBottom == null || unpairedLeftBottom.length < l1
        || unpairedLeftBottom[0].length < l2) {
      allocateMatrices();
    }

    final int temp1 = this.bulgeLoopUpperLimit;
    final int temp2 = this.internalLoopUpperLimit;
    this.bulgeLoopUpperLimit = maxBulge;
    this.internalLoopUpperLimit = maxBulge;

    fillMatrices();

    this.bulgeLoopUpperLimit = temp1;
    this.internalLoopUpperLimit = temp2;

    // get score
    double score = getScore(0, 0);

    energyCalculated = true;
    elongated1 = false;
    elongated2 = false;

    return score;
  }

  public final double getScore(int offset1, int offset2) {
    if (ignoreDangling) {
      return calcHybridScoreIgnoreDangling(start1 + offset1, start2 + offset2);
    }
    return calcHybridScore(start1 + offset1, start2 + offset2);
  }

  private void fillMatrices() {
    // main loops
    if (end1 != l1) {
      // update above existing values
      for (int i1 = end1 - 1; i1 >= start1; i1--) {
        for (int i2 = l2 - 1; i2 >= start2; i2--) {
          calcUnpairedLeftBottom(i1, i2);
          calcClosed(i1, i2);
          calcUnpairedLeftTop(i1, i2);
        }
      }
    }
    if (end2 != l2) {
      // update to the left of existing values
      for (int i2 = end2 - 1; i2 >= start2; i2--) {
        for (int i1 = l1 - 1; i1 >= start1; i1--) {
          calcUnpairedLeftBottom(i1, i2);
          calcClosed(i1, i2);
          calcUnpairedLeftTop(i1, i2);
        }
      }
    }
    // update top left rectangle
    for (int i1 = end1 - 1; i1 >= start1; i1--) {
      for (int i2 = end2 - 1; i2 >= start2; i2--) {
        calcUnpairedLeftBottom(i1, i2);
        calcClosed(i1, i2);
        calcUnpairedLeftTop(i1, i2);
      }
    }
  }

  /**
   * Clear any elongation made to the sequences.
   */
  public void clearElongations() {
    if (elongated) {
      start1 = end1;
      start2 = end2;
      energyCalculated = true;
    }
  }

  private void allocateMatrices() {
    this.unpairedLeftBottom = new double[l1][];
    this.unpairedLeftTop = new double[l1][];
    this.closed = new double[l1][];
    this.traceULB = new int[l1][];
    this.traceULT = new int[l1][];
    this.traceClosed = new int[l1][];
    for (int i = 0; i < l1; i++) {
      this.unpairedLeftBottom[i] = new double[l2];
      this.unpairedLeftTop[i] = new double[l2];
      this.closed[i] = new double[l2];
      this.traceULB[i] = new int[l2];
      this.traceULT[i] = new int[l2];
      this.traceClosed[i] = new int[l2];
    }
  }

  /**
   * Get the structure of the first sequence in parenthesis notation, assuming it is upstream of the
   * second sequence.
   */
  public int getSeq1Structure(char[] structure) {
    setTracePath(start1, start2);
    final int len = l1 - start1;
    int i = 0;
    for (int j = 0; i < len && tracePath[j] != -1; j++) {
      int offset = getVerticalOffset(tracePath[j]);
      if (offset > 0) {
        if ((tracePath[j] & TRACE_UNPAIRED) == 0) {
          // path is from closed
          if (getHorizontalOffset(tracePath[j]) > 0) {
            structure[i] = '(';
            i++;
            offset--;
          }
        }
        for (int k = 0; k < offset; k++) {
          structure[i] = '.';
          i++;
        }
      }
    }
    for (; i < len; i++) {
      structure[i] = '.';
    }
    return len;
  }

  /**
   * Get the structure of the first sequence in parenthesis notation, assuming it is upstream of the
   * second sequence.
   */
  public char[] getSeq1Structure() {
    setTracePath(start1, start2);
    char[] structure = new char[l1 - start1];
    int i = 0;
    for (int j = 0; i < structure.length && tracePath[j] != -1; j++) {
      int offset = getVerticalOffset(tracePath[j]);
      if (offset > 0) {
        if ((tracePath[j] & TRACE_UNPAIRED) == 0) {
          // path is from closed
          if (getHorizontalOffset(tracePath[j]) > 0) {
            structure[i] = '(';
            i++;
            offset--;
          }
        }
        for (int k = 0; k < offset; k++) {
          structure[i] = '.';
          i++;
        }
      }
    }
    for (; i < structure.length; i++) {
      structure[i] = '.';
    }
    return structure;
  }

  /**
   * Get the structure of the second sequence in parenthesis notation, assuming it is downstream of
   * the first sequence.
   */
  public int getSeq2Structure(char[] structure) {
    setTracePath(start1, start2);
    final int len = l2 - start2;
    int i = len - 1;
    for (int j = 0; i >= 0 && tracePath[j] != -1; j++) {
      int offset = getHorizontalOffset(tracePath[j]);
      if (offset > 0) {
        if ((tracePath[j] & TRACE_UNPAIRED) == 0) {
          // path is from closed
          if (getVerticalOffset(tracePath[j]) > 0) {
            structure[i] = ')';
            i--;
            offset--;
          }
        }
        for (int k = 0; k < offset; k++) {
          structure[i] = '.';
          i--;
        }
      }
    }
    for (; i >= 0; i--) {
      structure[i] = '.';
    }
    return len;
  }

  /**
   * Get the structure of the second sequence in parenthesis notation, assuming it is downstream of
   * the first sequence.
   */
  public char[] getSeq2Structure() {
    setTracePath(start1, start2);
    char[] structure = new char[l2 - start2];
    int i = structure.length - 1;
    for (int j = 0; i >= 0 && tracePath[j] != -1; j++) {
      int offset = getHorizontalOffset(tracePath[j]);
      if (offset > 0) {
        if ((tracePath[j] & TRACE_UNPAIRED) == 0) {
          // path is from closed
          if (getVerticalOffset(tracePath[j]) > 0) {
            structure[i] = ')';
            i--;
            offset--;
          }
        }
        for (int k = 0; k < offset; k++) {
          structure[i] = '.';
          i--;
        }
      }
    }
    for (; i >= 0; i--) {
      structure[i] = '.';
    }
    return structure;
  }

  /**
   * Checks if one of the ends that can be elongated ends with a paired letter.
   */
  public boolean pairedEnd() {
    setTracePath(start1, start2);
    boolean seq1Paired = true;
    boolean seq2Paired = true;
    for (int j = 0; tracePath[j] != -1; j++) {
      if ((tracePath[j] & TRACE_UNPAIRED) == 0) {
        // path is from closed
        break;
      }
      if (getVerticalOffset(tracePath[j]) > 0) {
        seq1Paired = false;
      }
      if (getHorizontalOffset(tracePath[j]) > 0) {
        seq2Paired = false;
      }
      if (!seq1Paired && !seq2Paired) {
        return false;
      }
    }
    return seq1Paired || seq2Paired;
  }

  private void printTrace(int trace, Formatter formatter) {
    // print the table
    if ((trace & TRACE_UNPAIRED_LEFT_BOT) != 0) {
      formatter.format("b");
    } else if ((trace & TRACE_UNPAIRED_LEFT_TOP) != 0) {
      formatter.format("t");
    } else {
      formatter.format("c");
    }
    // print the offset
    int offset = getVerticalOffset(trace);
    formatter.format("|%02d", offset);
    offset = getHorizontalOffset(trace);
    formatter.format("_%02d", offset);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    printSequences(builder);
    printTables(builder, true);
    if (this.pathCalculated) {
      printPath(builder);
    }
    return builder.toString();
  }

  public void printSequences(StringBuilder builder) {
    builder.append("Sequence1: ");
    for (int i = start1 + 1; i <= start1 + l1; i++) {
      builder.append(RnaAlphabet.getInstance().decode(seq1[i]));
    }
    builder.append("\n");
    builder.append("           ");
    builder.append(getSeq1Structure());
    builder.append("\n");
    builder.append("Sequence2: ");
    for (int i = seq2.length - 1; i >= seq2.length - l2; i--) {
      builder.append(RnaAlphabet.getInstance().decode(seq2[i]));
    }
    builder.append("\n");
    builder.append("           ");
    builder.append(getSeq2Structure());
    builder.append("\n");
  }

  public void printTables(StringBuilder builder, boolean printTrace) {
    Formatter formatter = new Formatter(builder);

    // print score tables
    builder.append("unpairedLeftBottom\n");
    for (int i1 = start1; i1 < l1; i1++) {
      for (int i2 = start2; i2 < l2; i2++) {
        formatter.format("%8.2f ", unpairedLeftBottom[i1][i2]);
      }
      builder.append("\n");
    }

    builder.append("closed\n");
    for (int i1 = start1; i1 < l1; i1++) {
      for (int i2 = start2; i2 < l2; i2++) {
        formatter.format("%8.2f ", closed[i1][i2]);
      }
      builder.append("\n");
    }

    builder.append("unpairedLeftTop\n");
    for (int i1 = start1; i1 < l1; i1++) {
      for (int i2 = start2; i2 < l2; i2++) {
        formatter.format("%8.2f ", unpairedLeftTop[i1][i2]);
      }
      builder.append("\n");
    }

    if (printTrace) {
      // print trace tables
      builder.append("traceULB\n");
      for (int i1 = start1; i1 < l1; i1++) {
        for (int i2 = start2; i2 < l2; i2++) {
          printTrace(traceULB[i1][i2], formatter);
          builder.append(" ");
        }
        builder.append("\n");
      }

      builder.append("traceClosed\n");
      for (int i1 = start1; i1 < l1; i1++) {
        for (int i2 = start2; i2 < l2; i2++) {
          printTrace(traceClosed[i1][i2], formatter);
          builder.append(" ");
        }
        builder.append("\n");
      }

      builder.append("traceULT\n");
      for (int i1 = start1; i1 < l1; i1++) {
        for (int i2 = start2; i2 < l2; i2++) {
          printTrace(traceULT[i1][i2], formatter);
          builder.append(" ");
        }
        builder.append("\n");
      }

    }
  }

  public void printPath(StringBuilder builder) {
    builder.append("path\n");
    Formatter formatter = new Formatter(builder);
    for (int i = 0; i < tracePath.length && tracePath[i] != -1; i++) {
      printTrace(tracePath[i], formatter);
      builder.append(" ");
    }
    builder.append("\n");
  }

  private int setVerticalOffset(int trace, int offset) {
    trace &= ~TRACE_VERTICAL_MASK;
    return trace | offset;
  }

  private int getVerticalOffset(int trace) {
    return trace & TRACE_VERTICAL_MASK;
  }

  private int setHorizontalOffset(int trace, int offset) {
    trace &= ~TRACE_HORIZONTAL_MASK;
    return trace | (offset << 8);
  }

  private int getHorizontalOffset(int trace) {
    return (trace & TRACE_HORIZONTAL_MASK) >> 8;
  }

  /**
   * Calculate score and trace of unpaired left bottom at position (i1, i2).
   */
  private void calcUnpairedLeftBottom(int i1, int i2) {
    double value = MAX_VALUE;
    int trace = TRACE_NONE;

    final int diff1 = l1 - i1;
    final int diff2 = l2 - i2;
    if (diff1 < 1 || diff2 < 1) {
      return;
    }
    final boolean pairing = alphabet.canPair(seq1[i1 + 1], seq2[i2 + 1]);

    /*
     * v1 = ulb <<< (tt(empty, lbase)) ~~~ p unpaired_left_bot
     */
    if (diff2 >= 2) {
      value = unpairedLeftBottom[i1][i2 + 1];
      trace = TRACE_UNPAIRED_LEFT_BOT | TRACE_HORIZONTAL;

    }

    if (pairing) {
      /*
       * v1 = ulb <<< (tt(empty, lbase)) ~~~ p unpaired_left_bot
       */

      /*
       * v2 = eds <<< (tt(lbase, lbase)) ~~~ p closed
       */
      if (diff1 >= 2 && diff2 >= 2) {
        double temp = closed[i1 + 1][i2 + 1];
        array3[0] = seq1[i1 + 1];
        array3[1] = seq1[i1 + 2];
        array3[2] = seq2[i2 + 2];
        temp += energy.get_dl_dangle_dg(array3);
        array3[0] = seq1[i1 + 2];
        array3[1] = seq2[i2 + 2];
        array3[2] = seq2[i2 + 1];
        temp += energy.get_dr_dangle_dg(array3);
        if (temp < value) {
          value = temp;
          trace = TRACE_DIAGONAL;
        }
      }
      /*
       * v2 = eds <<< (tt(lbase, lbase)) ~~~ p closed
       */

      /*
       * v3 = edt <<< (tt(lbase, empty)) ~~~ p closed
       */
      if (diff1 >= 2) {
        double temp = closed[i1 + 1][i2];
        array3[0] = seq1[i1 + 1];
        array3[1] = seq1[i1 + 2];
        array3[2] = seq2[i2 + 1];
        temp += energy.get_dl_dangle_dg(array3);

        if (temp < value) {
          value = temp;
          trace = TRACE_VERTICAL;
        }
      }
      /*
       * v3 = edt <<< (tt(lbase, empty)) ~~~ p closed
       */

      /*
       * v4 = edb <<< (tt(empty, lbase)) ~~~ p closed
       */
      if (diff2 >= 2) {
        double temp = closed[i1][i2 + 1];
        array3[0] = seq1[i1 + 1];
        array3[1] = seq2[i2 + 2];
        array3[2] = seq2[i2 + 1];
        temp += energy.get_dr_dangle_dg(array3);

        if (temp < value) {
          value = temp;
          trace = TRACE_HORIZONTAL;
        }
      }
    }
    /*
     * v4 = edb <<< (tt(empty, lbase)) ~~~ p closed
     */

    /*
     * assign table entry result
     */
    unpairedLeftBottom[i1][i2] = value;
    traceULB[i1][i2] = trace;
  }

  /**
   * Calculate score and trace of closed at position (i1, i2).
   */
  private void calcClosed(int i1, int i2) {
    double value = MAX_VALUE;
    int trace = TRACE_NONE;

    final int diff1 = l1 - i1;
    final int diff2 = l2 - i2;
    if (diff1 < 1 || diff2 < 1) {
      return;
    }

    if (!alphabet.canPair(seq1[i1 + 1], seq2[i2 + 1])) {
      closed[i1][i2] = value;
      traceClosed[i1][i2] = trace;
      return;
    }

    trace = TRACE_DIAGONAL;

    /*
     * v1 = sr <<< (tt(lbase, lbase) `with` (pairingTTcross compl)) ~~~ p closed
     */
    if (diff1 >= 2 && diff2 >= 2) {
      double temp = closed[i1 + 1][i2 + 1];
      array4[0] = seq1[i1 + 1];
      array4[1] = seq1[i1 + 2];
      array4[2] = seq2[i2 + 2];
      array4[3] = seq2[i2 + 1];
      temp += energy.get_stack_dg(array4);

      if (value > temp) {
        value = temp;
        trace = TRACE_DIAGONAL;
      }
    }

    /*
     * v3 = bt <<< (tt(lbase, lbase) `with` (pairingTTcross compl)) ~~~ (tt(region, empty) `with`
     * (sizeTT 1 15 0 0)) ~~~ p closed
     */
    if (diff1 >= 3 && diff2 >= 2) {
      int maxK = Math.min(i1 + bulgeLoopUpperLimit + 1, l1 - 1);
      for (int k = i1 + 2; k <= maxK; k++) {
        if (seq1[k] == X)
          break;
        double temp =
            (closed[k][i2 + 1] + bulgeLoopStacking(k - (i1 + 1), 0, i1 + 1, i2 + 1))
                + energy.bl_ent(k - (i1 + 1));

        if (temp < value) {
          value = temp;
          trace = setVerticalOffset(trace, k - i1);
          trace = setHorizontalOffset(trace, 1);
        }
      }
    }

    /*
     * v5 = bb <<< (tt(lbase, lbase) `with` (pairingTTcross compl)) ~~~ (tt(empty, region) `with`
     * (sizeTT 0 0 1 15)) ~~~ p closed
     */
    if (diff1 >= 2 && diff2 >= 3) {
      int maxK = Math.min(i2 + bulgeLoopUpperLimit + 1, l2 - 1);
      for (int k = i2 + 2; k <= maxK; k++) {
        double temp =
            (closed[i1 + 1][k] + bulgeLoopStacking(0, k - (i2 + 1), i1 + 1, i2 + 1))
                + energy.bl_ent(k - (i2 + 1));

        if (temp < value) {
          value = temp;
          trace = setVerticalOffset(trace, 1);
          trace = setHorizontalOffset(trace, k - i2);
        }
      }
    }

    /*
     * v7 = il <<< (tt(lbase, lbase) `with` (pairingTTcross compl)) ~~~ (tt(region, region) `with`
     * (sizeTT 1 15 1 15)) ~~~ p closed
     */
    if (diff1 >= 3 && diff2 >= 3) {
      double temp = MAX_VALUE;
      /* special internal loops: */
      int maxK1 = Math.min(i1 + Math.min(3, internalLoopUpperLimit + 1), l1 - 1);
      int maxK2 = Math.min(i2 + Math.min(3, internalLoopUpperLimit + 1), l2 - 1);
      boolean breakX = false;
      for (int k1 = i1 + 2; k1 <= maxK1; k1++) {
        if (seq1[k1] == X) {
          breakX = true;
          break;
        }
        for (int k2 = i2 + 2; k2 <= maxK2; k2++) {
          if (alphabet.canPair(seq1[k1 + 1], seq2[k2 + 1])) {
            temp = closed[k1][k2] + internalSpecial(i1 + 1, i2 + 1, k1 - (i1 + 1), k2 - (i2 + 1));

            if (value > temp) {
              value = temp;
              trace = setVerticalOffset(trace, k1 - i1);
              trace = setHorizontalOffset(trace, k2 - i2);
            }
          }
        }
      }
      if (!breakX) {
        /* normal internal loops: */
        maxK1 = Math.min(i1 + 3, l1 - 1);
        maxK2 = Math.min(i2 + internalLoopUpperLimit + 1, l2 - 1);
        for (int k1 = i1 + 2; k1 <= maxK1; k1++) {
          if (seq1[k1] == X) {
            breakX = true;
            break;
          }
          for (int k2 = i2 + 4; k2 <= maxK2; k2++) {
            if (alphabet.canPair(seq1[k1 + 1], seq2[k2 + 1])) {
              temp = closed[k1][k2] + internalLoop(i1 + 1, i2 + 1, i1 + 1, k1, i2 + 1, k2);

              if (temp < MAX_VALUE)
                temp += internalLoopStackOpen(i1 + 1, i2 + 1);
              if (value > temp) {
                value = temp;
                trace = setVerticalOffset(trace, k1 - i1);
                trace = setHorizontalOffset(trace, k2 - i2);
              }
            }
          }
        }
      }
      /* normal internal loops: */
      if (!breakX) {
        maxK1 = Math.min(i1 + internalLoopUpperLimit + 1, l1 - 1);
        maxK2 = Math.min(i2 + 3, l2 - 1);
        for (int k1 = i1 + 4; k1 <= maxK1; k1++) {
          if (seq1[k1] == X) {
            breakX = true;
            break;
          }
          for (int k2 = i2 + 2; k2 <= maxK2; k2++) {
            if (alphabet.canPair(seq1[k1 + 1], seq2[k2 + 1])) {
              temp = closed[k1][k2] + internalLoop(i1 + 1, i2 + 1, i1 + 1, k1, i2 + 1, k2);

              if (temp < MAX_VALUE)
                temp += internalLoopStackOpen(i1 + 1, i2 + 1);
              if (value > temp) {
                value = temp;
                trace = setVerticalOffset(trace, k1 - i1);
                trace = setHorizontalOffset(trace, k2 - i2);
              }
            }
          }
        }
        /* normal internal loops: */
        if (!(maxK1 >= seq1.length) && !breakX) {
          maxK1 = Math.min(i1 + internalLoopUpperLimit + 1, l1 - 1);
          maxK2 = Math.min(i2 + internalLoopUpperLimit + 1, l2 - 1);
          for (int k1 = i1 + 4; k1 <= maxK1; k1++) {
            if (seq1[k1] == X)
              break;
            for (int k2 = i2 + 4; k2 <= maxK2; k2++) {
              if (alphabet.canPair(seq1[k1 + 1], seq2[k2 + 1])) {
                temp = closed[k1][k2] + internalLoop(i1 + 1, i2 + 1, i1 + 1, k1, i2 + 1, k2);

                if (temp < MAX_VALUE)
                  temp += internalLoopStackOpen(i1 + 1, i2 + 1);
                if (value > temp) {
                  value = temp;
                  trace = setVerticalOffset(trace, k1 - i1);
                  trace = setHorizontalOffset(trace, k2 - i2);
                }
              }
            }
          }
        }
      }
    }

    /*
     * v8 = el <<< (tt(lbase, lbase) `with` (pairingTTcross compl)) ~~~ (tt(uregion, uregion))
     */
    if (diff1 == 1 || seq1[i1 + 2] != 'X') {
      double temp = 0;
      if (!ignoreDangling) {
        if ((l1) > (i1 + 1)) {
          array3[0] = seq2[i2 + 1];
          array3[1] = seq1[i1 + 1];
          array3[2] = seq1[i1 + 2];
          temp += energy.get_dr_dangle_dg(array3);
        }
        if (diff2 > 1) {
          array3[0] = seq2[i2 + 2];
          array3[1] = seq2[i2 + 1];
          array3[2] = seq1[i1 + 1];
          temp += energy.get_dl_dangle_dg(array3);
        }
      }

      if (temp < value) {
        value = temp;
        trace = setVerticalOffset(trace, diff1);
        trace = setHorizontalOffset(trace, diff2);
      }
    }

    /*
     * assign table entry result
     */
    closed[i1][i2] = value;
    traceClosed[i1][i2] = trace;
  }

  /**
   * Calculate score and trace of unpaired left top at position (i1, i2).
   */
  private void calcUnpairedLeftTop(int i1, int i2) {
    double value = MAX_VALUE;
    int trace = TRACE_NONE;

    final int diff1 = l1 - i1;
    final int diff2 = l2 - i2;

    if (diff1 < 1 || diff2 < 1) {
      return;
    }

    /*
     * v1 = ult <<< (tt(lbase, empty)) ~~~ p unpaired_left_top
     */
    if (diff1 >= 2 && diff2 >= 1) {
      value = unpairedLeftTop[i1 + 1][i2];
      trace = TRACE_UNPAIRED_LEFT_TOP | TRACE_VERTICAL;
    }

    /*
     * v2 = p unpaired_left_bot
     */
    if (diff1 >= 1 && diff2 >= 1) {
      if (value > unpairedLeftBottom[i1][i2]) {
        value = unpairedLeftBottom[i1][i2];
        trace = TRACE_UNPAIRED_LEFT_BOT;
      }
    }

    /*
     * assign table entry result
     */
    if (diff1 >= 1 && diff2 >= 1) {
      unpairedLeftTop[i1][i2] = value;
      traceULT[i1][i2] = trace;
    }
  }

  /**
   * Calculate hybridization score at position (i1, i2).
   */
  private double calcHybridScore(int i1, int i2) {
    double value = MAX_VALUE;

    final int diff1 = l1 - i1;
    final int diff2 = l2 - i2;

    /*
     * v1 = nil <<< tt(uregion, uregion)
     */
    if ((diff1 >= 0) && (diff2 >= 0)) {
      value = 0;
    }

    if ((diff1 >= 1) && (diff2 >= 1)) {
      /*
       * v2 = p unpaired_left_top
       */
      double temp = unpairedLeftTop[i1][i2];
      if (value > temp) {
        value = temp;
      }
      /*
       * v3 = p closed
       */
      temp = closed[i1][i2];
      if (value > temp) {
        value = temp;
      }
    }

    return (value);
  }

  /**
   * Calculate hybridization score at position (i1, i2), ignoring leading dangling ends.
   */
  private double calcHybridScoreIgnoreDangling(int i1, int i2) {
    double value = MAX_VALUE;
    int trace = 0;

    final int diff1 = l1 - i1;
    final int diff2 = l2 - i2;

    if ((diff1 >= 0) && (diff2 >= 0)) {
      value = 0;
      trace = -1;
    }

    if ((diff1 >= 1) && (diff2 >= 1)) {
      double temp = unpairedLeftTop[i1][i2];
      if (value > temp) {
        value = temp;
        trace = traceULT[i1][i2];
      }
    }

    while (true) {
      if (trace == TRACE_NONE) {
        break;
      }

      // update i1 and i2
      int verticalOffset = 0;
      if ((trace & TRACE_VERTICAL_MASK) != 0) {
        verticalOffset = getVerticalOffset(trace);
      }
      int horizontalOffset = 0;
      if ((trace & TRACE_HORIZONTAL_MASK) != 0) {
        horizontalOffset = getHorizontalOffset(trace);
      }
      if ((trace & TRACE_UNPAIRED) == 0 && verticalOffset > 0 && horizontalOffset > 0) {
        // next trace data taken should be taken from closed
        return closed[i1][i2];
      }
      i1 += verticalOffset;
      i2 += horizontalOffset;
      if (i1 >= l1 || i2 >= l2) {
        break;
      }

      if ((trace & TRACE_UNPAIRED_LEFT_BOT) != 0) {
        // next trace data taken should be taken from left bottom
        trace = traceULB[i1][i2];
      } else if ((trace & TRACE_UNPAIRED_LEFT_TOP) != 0) {
        // next trace data taken should be taken from left top
        trace = traceULT[i1][i2];
      } else {
        trace = traceClosed[i1][i2];
      }
    }

    return value;
  }

  /**
   * Set the trace path at position (i1, i2).
   */
  private void setTracePath(int i1, int i2) {
    if (pathCalculated) {
      return;
    }
    if (!energyCalculated) {
      throw new RuntimeException("Hybridization not calculated, no path available");
    }
    if (tracePath == null || tracePath.length < l1 + l2 + 1) {
      tracePath = new int[l1 + l2 + 1];
    }

    double value = MAX_VALUE;

    int diff1 = l1 - i1;
    int diff2 = l2 - i2;

    if ((diff1 >= 0) && (diff2 >= 0)) {
      value = 0;
      tracePath[0] = -1;
    }

    if ((diff1 >= 1) && (diff2 >= 1)) {
      double temp = unpairedLeftTop[i1][i2];
      if (value > temp) {
        value = temp;
        tracePath[0] = traceULT[i1][i2];
      }
      temp = closed[i1][i2];
      if (value > temp) {
        value = temp;
        tracePath[0] = traceClosed[i1][i2];
      }
    }

    for (int i = 0; i < tracePath.length - 1; i++) {
      if (tracePath[i] == TRACE_NONE) {
        tracePath[i] = -1;
        break;
      }

      // update i1 and i2
      if ((tracePath[i] & TRACE_VERTICAL_MASK) != 0) {
        i1 += getVerticalOffset(tracePath[i]);
      }
      if ((tracePath[i] & TRACE_HORIZONTAL_MASK) != 0) {
        i2 += getHorizontalOffset(tracePath[i]);
      }
      if (i1 >= l1 || i2 >= l2) {
        // gone out of the matrix
        tracePath[i + 1] = -1;
        break;
      }

      if ((tracePath[i] & TRACE_UNPAIRED) == 0) {
        // next trace data taken should be taken from closed
        tracePath[i + 1] = traceClosed[i1][i2];
      } else if ((tracePath[i] & TRACE_UNPAIRED_LEFT_BOT) != 0) {
        // next trace data taken should be taken from left bottom
        tracePath[i + 1] = traceULB[i1][i2];
      } else if ((tracePath[i] & TRACE_UNPAIRED_LEFT_TOP) != 0) {
        // next trace data taken should be taken from left top
        tracePath[i + 1] = traceULT[i1][i2];
      }
    }
    pathCalculated = true;
  }

  /*****************************************************************
   * Energy related functions
   *****************************************************************/

  private double bulgeLoopStacking(int t, int b, int i, int j) {
    if ((t == 0) && (b == 1)) {
      array4[0] = seq1[i];
      array4[1] = seq1[i + 1];
      array4[2] = seq2[j + 2];
      array4[3] = seq2[j];
      return (energy.get_stack_dg(array4));
    } else if ((t == 1) && (b == 0)) {
      array4[0] = seq1[i];
      array4[1] = seq1[i + 2];
      array4[2] = seq2[j + 1];
      array4[3] = seq2[j];
      return (energy.get_stack_dg(array4));
    }
    return (0);
  }

  private double internalLoopStackOpen(int i, int j) {
    array4[0] = seq1[i];
    array4[1] = seq1[i + 1];
    array4[2] = seq2[j + 1];
    array4[3] = seq2[j];
    return energy.get_tstacki_dg(array4);
  }

  private double internalSpecial(int i, int j, int t, int b) {
    if ((t == 1) && (b == 1)) {
      array6[0] = seq1[i];
      array6[1] = seq2[j];
      array6[2] = seq1[i + 1];
      array6[3] = seq2[j + 1];
      array6[4] = seq1[i + 2];
      array6[5] = seq2[j + 2];
      return (energy.get_int11(array6));
    } else if ((t == 1) && (b == 2)) {
      array7[0] = seq1[i];
      array7[1] = seq2[j];
      array7[2] = seq1[i + 1];
      array7[3] = seq2[j + 1];
      array7[4] = seq2[j + 2];
      array7[5] = seq1[i + 2];
      array7[6] = seq2[j + 3];
      return (energy.get_int21(array7));
    } else if ((t == 2) && (b == 1)) {
      array7[0] = seq2[j + 2];
      array7[1] = seq1[i + 3];
      array7[2] = seq2[j + 1];
      array7[3] = seq1[i + 2];
      array7[4] = seq1[i + 1];
      array7[5] = seq2[j];
      array7[6] = seq1[i];
      return (energy.get_int21(array7));
    } else if ((t == 2) && (b == 2)) {
      array8[0] = seq1[i];
      array8[1] = seq2[j];
      array8[2] = seq1[i + 1];
      array8[3] = seq1[i + 2];
      array8[4] = seq2[j + 1];
      array8[5] = seq2[j + 2];
      array8[6] = seq1[i + 3];
      array8[7] = seq2[j + 3];
      return (energy.get_int22(array8));
    }
    throw new Error("Illegal interval values " + t + ", " + b);
  }

  private double internalLoop(int i, int j, int k, int l, int u, int v) {
    array4[0] = seq2[v + 1];
    array4[1] = seq2[v];
    array4[2] = seq1[l];
    array4[3] = seq1[l + 1];
    array2[0] = l - k;
    array2[1] = v - u;
    return (energy.get_tstacki_dg(array4) + energy.il_ent(l - k + v - u) + energy
        .get_il_asym(array2));
  }

  public void setIgnoreDangling(boolean ignoreDangling) {
    this.ignoreDangling = ignoreDangling;
  }

}

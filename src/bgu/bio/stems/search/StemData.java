package bgu.bio.stems.search;

import gnu.trove.list.array.TCharArrayList;

import java.util.ArrayList;
import java.util.List;

import bgu.bio.io.file.json.JSONArray;
import bgu.bio.io.file.json.JSONException;
import bgu.bio.io.file.json.JSONObject;
import bgu.bio.util.CharBuffer;

public class StemData implements Comparable<StemData> {
  private TCharArrayList sequence;
  private TCharArrayList structure;
  private int position1;
  private int len1;
  private int position2;
  private int len2;
  private int loopSize;
  private int ordinal;
  private float score;
  private float normScore;
  private double energy;

  public StemData(int position, TCharArrayList sequence, TCharArrayList structure, int ordinal,
      float score) {

    this.sequence = new TCharArrayList(sequence);
    this.structure = new TCharArrayList(structure);
    this.ordinal = ordinal;
    this.score = score;
    int size = this.structure.size();
    position1 = position;
    len1 = this.structure.lastIndexOf('(') + 1;
    position2 = this.structure.indexOf(len1, ')');
    len2 = size - position2;
    loopSize = size - len1 - len2;
  }

  /**
   * Instantiates a new stem data using a given one.
   * 
   * @param other the other stem data
   * @param charBufferPool the char buffer pool to be used for {@link CharBuffer}
   */
  public StemData(StemData other) {
    this(other.position1, other.sequence, other.structure, other.ordinal, other.score);
    loopSize = other.loopSize;
    normScore = other.normScore;
  }

  /**
   * Reuse the current stem data and copies the given stem data fields.
   * 
   * @param other the other stem data to copy the fields from.
   * @param pool the pool to use for recycling the {@link CharBuffer}
   */
  public void reuse(StemData other) {
    reuse(other.position1, other.sequence, other.structure, other.ordinal, other.score);
    loopSize = other.loopSize;
    normScore = other.normScore;
  }

  public void reuse(int position, TCharArrayList sequence, TCharArrayList structure, int ordinal,
      float score) {

    // copy the buffers
    if (this.sequence == null) {
      this.sequence = new TCharArrayList(sequence);
    } else {
      this.sequence.resetQuick();
      this.copyTo(sequence, this.sequence);
    }
    if (this.structure == null) {
      this.structure = new TCharArrayList(structure);
    } else {
      this.structure.resetQuick();
      this.copyTo(structure, this.structure);
    }

    this.ordinal = ordinal;
    this.score = score;
    int size = structure.size();
    position1 = position;
    len1 = this.structure.lastIndexOf('(') + 1;
    position2 = this.structure.indexOf(len1, ')');
    len2 = size - position2;
    loopSize = size - len1 - len2;
  }

  private void copyTo(TCharArrayList from, TCharArrayList to) {
    to.ensureCapacity(from.size());
    for (int i = 0; i < from.size(); i++) {
      to.set(i, from.getQuick(i));
    }
  }

  public void setPosition1(int value){
    this.position1 = value;
  }
  
  public int getPosition1() {
    return position1;
  }

  public int getLen1() {
    return len1;
  }

  public TCharArrayList getSequence() {
    return this.sequence;
  }

  public TCharArrayList getStructure() {
    return this.structure;
  }

  public String getSequence1() {
    return listToStr(sequence, 0, len1);
  }

  public String getStructure1() {
    return listToStr(structure, 0, len1);
  }

  public int getPosition2() {
    return position1 + position2 + loopSize;
  }

  public int getLen2() {
    return len2;
  }

  public String getSequence2() {
    return listToStr(sequence, position2, position2 + len2);
  }

  public String getStructure2() {
    return listToStr(structure, position2, position2 + len2);
  }

  /**
   * Transform a section of a {@link TCharArrayList} to a {@link String}
   * 
   * @param lst the list to collect the data from
   * @param from the start index
   * @param to the end index
   * @return A {@link String} representing the section of the list
   */
  private String listToStr(TCharArrayList lst, int from, int to) {
    StringBuilder sb = new StringBuilder(to - from + 1);
    for (int i = from; i < to; i++) {
      sb.append(lst.get(i));
    }
    return sb.toString();
  }

  public int getLoopSize() {
    return loopSize;
  }

  public void setOrdinal(int ordinal) {
    this.ordinal = ordinal;
  }

  public int getOrdinal() {
    return ordinal;
  }

  public float getScore() {
    return score;
  }

  public void setNormScore(float normScore) {
    this.normScore = normScore;
  }

  @Override
  public int compareTo(StemData o) {

    int diff = this.position1 - o.position1;

    if (diff != 0) {
      return diff;
    }
    diff = this.len1 - o.len1;
    if (diff != 0) {
      return diff;
    }
    diff = this.position2 - o.position2;
    if (diff != 0) {
      return diff;
    }
    diff = this.len2 - o.len2;
    if (diff != 0) {
      return diff;
    }
    return 0;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append('{');
    builder.append(getPosition1());
    builder.append('-');
    builder.append(getPosition1() + getLen1() - 1);
    builder.append(',');
    builder.append(getSequence1());
    builder.append(',');
    builder.append(getStructure1());
    builder.append(':');
    builder.append(getStructure2());
    builder.append(',');
    builder.append(getSequence2());
    builder.append(',');
    builder.append(getPosition2() + 1);
    builder.append('-');
    builder.append(getPosition2() + getLen2());
    builder.append('}');
    builder.append('?');
    builder.append(ordinal);
    builder.append('?');
    builder.append(score);
    builder.append('?');
    builder.append(normScore);
    return builder.toString();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + len1;
    result = prime * result + len2;
    result = prime * result + ordinal;
    result = prime * result + position1;
    result = prime * result + position2;
    result = prime * result + Float.floatToIntBits(score);
    result = prime * result + ((sequence == null) ? 0 : sequence.hashCode());
    result = prime * result + loopSize;
    result = prime * result + ((structure == null) ? 0 : structure.hashCode());
    return result;
  }

  public String getAlignedStructure() {
    StringBuilder builder = new StringBuilder();
    // add aligned structure
    for (int j = 0; j < position1 - 1; j++) {
      builder.append(' ');
    }
    builder.append(structure.toString());
    return builder.toString();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof StemData)) {
      return false;
    }

    StemData other = (StemData) obj;
    if (len1 != other.len1) {
      return false;
    }
    if (len2 != other.len2) {
      return false;
    }
    if (ordinal != other.ordinal) {
      return false;
    }
    if (position1 != other.position1) {
      return false;
    }
    if (position2 != other.position2) {
      return false;
    }
    if (loopSize != other.loopSize) {
      return false;
    }
    if (sequence == null) {
      if (other.sequence != null) {
        return false;
      }
    } else if (!sequence.equals(other.sequence)) {
      return false;
    }

    if (structure == null) {
      if (other.structure != null) {
        return false;
      }
    } else if (!structure.equals(other.structure)) {
      return false;
    }

    return true;
  }

  public static JSONObject toJSON(StemData stem) throws JSONException {
    JSONObject result = new JSONObject();
    result.put("start1", stem.getPosition1());
    result.put("len1", stem.getLen1());
    result.put("sequence1", stem.getSequence1());
    result.put("Structure1", stem.getStructure1());
    result.put("start2", stem.getPosition2());
    result.put("len2", stem.getLen2());
    result.put("sequence2", stem.getSequence2());
    result.put("Structure2", stem.getStructure2());
    result.put("loopSize", stem.getLoopSize());
    result.put("ordinal", stem.getOrdinal());
    result.put("score", stem.getScore());
    result.put("normScore", stem.normScore);
    result.put("energy", stem.getEnergy());
    result.put("sequence", stem.sequence.toString());
    result.put("Structure", stem.structure.toString());
    return result;
  }

  public static List<StemData> fromJSON(JSONArray jsonArray) throws JSONException {
    List<StemData> list = new ArrayList<StemData>();
    for (int i = 0; i < jsonArray.length(); i++) {
      list.add(StemData.fromJSON(jsonArray.getJSONObject(i)));
    }
    return list;
  }

  public static StemData fromJSON(JSONObject json) throws JSONException {

    StemData stem =
        new StemData(json.getInt("start1"), new TCharArrayList(json.getString("sequence")
            .toCharArray()), new TCharArrayList(json.getString("Structure").toCharArray()),
            json.getInt("ordinal"), (float) json.getDouble("score"));
    stem.loopSize = json.getInt("loopSize");
    stem.setEnergy(json.getDouble("energy"));
    stem.normScore = (float) json.getDouble("normScore");
    return stem;
  }

  /**
   * @return the energy
   */
  public final double getEnergy() {
    return energy;
  }

  /**
   * @param energy the energy to set
   */
  public final void setEnergy(double energy) {
    this.energy = energy;
  }

  public void setScore(float val) {
    this.score = val;
  }

  public void setLoopLength(int size) {
    this.loopSize = size;
  }

  public float getNormScore() {
    return normScore;
  }
}

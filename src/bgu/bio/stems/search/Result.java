package bgu.bio.stems.search;

import java.util.ArrayList;

import bgu.bio.adt.queue.Pool;

public final class Result implements Comparable<Result> {
  float score;
  public ArrayList<StemData> list;
  int position;
  int length;

  public Result(float score, ArrayList<StemData> list) {
    this.list = list;
    this.score = score;
  }

  public Result(Result other, Pool<StemData> pool) {
    this.score = other.score;
    this.position = other.position;
    this.length = other.length;

    this.list = new ArrayList<StemData>();
    for (int i = 0; i < other.list.size(); i++) {
      StemData copy = other.list.get(i);
      StemData ans = pool.dequeue();
      if (ans == null) {
        ans = new StemData(copy);
      } else {
        ans.reuse(copy);
      }
      this.list.add(ans);
    }
  }

  @Override
  public int compareTo(Result other) {
    final float diffScore = score - other.score;
    if (diffScore == 0)
      return position - other.position;

    return diffScore < 0 ? -1 : 1;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + length;
    result = prime * result + ((list == null) ? 0 : list.hashCode());
    result = prime * result + position;
    result = prime * result + Float.floatToIntBits(score);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Result other = (Result) obj;
    if (position != other.position)
      return false;
    if (Float.floatToIntBits(score) != Float.floatToIntBits(other.score))
      return false;

    if (length != other.length)
      return false;
    if (list == null) {
      if (other.list != null)
        return false;
    } else if (!list.equals(other.list))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "pos: " + this.position + " Score: " + score + "\n count = " + list.size() + ": "
        + list.toString() + "\n";
  }

  public int getStart() {
    return this.position;
  }

  public int getEnd() {
    return this.position + this.length;
  }

  public int getLength() {
    return this.length;
  }

  public static int overlap(Result r1, Result r2) {
    final int l0 = Math.max(r1.getStart(), r2.getStart());
    final int l1 = Math.min(r1.getEnd(), r2.getEnd());
    final int diff = l1 - l0 + 1;
    if (diff <= 0) {
      return 0;
    }
    return diff;
  }

  public static float coverage(Result r1, Result r2) {
    final float maxOverlap = Math.min(r1.length, r2.length);
    return overlap(r1, r2) / maxOverlap;
  }

  public float getScore() {
    return score;
  }

}

package bgu.bio.stems.search;

import gnu.trove.list.array.TIntArrayList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.apache.log4j.Logger;

import bgu.bio.adt.graphs.FlexibleUndirectedGraph;
import bgu.bio.adt.list.Link;
import bgu.bio.adt.list.PooledLinkedList;
import bgu.bio.adt.queue.Pool;
import bgu.bio.adt.tuples.IntPair;
import bgu.bio.algorithms.graphs.MaximalWeightedClique;
import bgu.bio.stems.indexing.Strand;

/**
 * A class to get the best accumulated score within a region.
 */
/**
 * @author milon
 * 
 */
public class RegionScorer {

  private static Logger log = Logger.getLogger(RegionScorer.class.getName());

  /** The query {@link StemData} list. */
  private List<StemData> queryStemData;

  /** The max region size. */
  private int maxRegionSize;

  /** Hold the current results. */
  private Result currentResult;

  /** A pool to hold array lists of the results. */
  private Pool<Result> resultsPool;

  private Pool<StemData> stemsPool;

  private double MAX_DISTORTION = 3.0d;

  private int[][] DISTORTION_ARRAYS = { {0, 1, 2, 3, 4, 5, 6, 7}, {4, 5, 6, 7, 0, 1, 2, 3},
      {0, 1, 4, 5, 6, 7, 2, 3}, {4, 5, 0, 1, 2, 3, 6, 7}, {0, 1, 4, 5, 2, 3, 6, 7},
      {4, 5, 0, 1, 6, 7, 2, 3}};

  /**
   * Holds the current stems in the active window
   */
  private PooledLinkedList<StemData> currentStemsInRegion;

  /**
   * Holds information on the new stems that were added from the last {@link #computeWindowScore()}
   */
  private HashSet<StemData> newStemsInRegion;

  /**
   * This matrix will hold the order of the legs of all pairs of query stems. It will help save
   * computations on sorting the array for the query stems
   */
  private int[][][] queryStemsOrder;

  /**
   * This matrix will hold the type of inner distances of pairs of query stems for which distortion
   * is to be checked.
   */
  private int[][] queryDistanceType;

  /**
   * This matrix will hold the inner distances of pairs of query stems for which distortion is to be
   * checked.
   */
  private int[][] queryInnerDistance;

  /**
   * This matrix holds information whether two query stems can connect topologically (<b>true</b>).
   * The case that are not allowed are cases where the legs of the first stem are fully covered by
   * the second stem (position wise).
   */
  private boolean[][] queryStemsAgreement;

  /**
   * Used for the calculation of the maximal clique
   */
  private MaximalWeightedClique maximal;
  /**
   * Counts the amount of pushed stems to the region scorer
   */
  private long pushedStems = 0;

  private Strand currentStrand;

  private final double OVERLAP_LIMIT = 0.1;

  /**
   * Arrays to hold temporary information on the position of the stems for ordering. used in
   * {@link #fillStemOrder(StemData, StemData, int[])}
   */
  private IntPair[] stemArray1, stemArray2;

  /**
   * Holding statistics of stem pairs encountered.
   */
  private Statistics statistics;

  private int computedRegions;

  private int zeroCalculations;

  /**
   * Build a region scorer for a certain query.
   * 
   * @param queryStemDatas The query stems ordered by ordinal id.
   * @param maxRegionSize the max region size
   * @param ordinalToGroupMap mapping from ordinal to group number
   * @param resultsLimit the results limit
   */

  public RegionScorer(List<StemData> queryStemDatas, int maxRegionSize) {
    this(queryStemDatas, maxRegionSize, new Pool<StemData>(10000, true));
  }

  public RegionScorer(List<StemData> queryStemDatas, int maxRegionSize, Pool<StemData> stemPool) {
    this.stemsPool = stemPool;
    this.currentStemsInRegion = new PooledLinkedList<StemData>();
    this.newStemsInRegion = new HashSet<StemData>();
    // queue
    resultsPool = new Pool<Result>();
    currentResult = null;
    this.queryStemData = queryStemDatas;
    this.maxRegionSize = maxRegionSize;

    queryStemsAgreement = new boolean[queryStemDatas.size()][queryStemDatas.size()];
    fillStemAgreement();
    // for the stem ordering
    queryStemsOrder = new int[queryStemDatas.size()][queryStemDatas.size()][8];
    queryDistanceType = new int[queryStemDatas.size()][queryStemDatas.size()];
    queryInnerDistance = new int[queryStemDatas.size()][queryStemDatas.size()];
    stemArray1 = new IntPair[4];
    stemArray2 = new IntPair[4];
    for (int i = 0; i < 4; i++) {
      stemArray1[i] = new IntPair();
      stemArray2[i] = new IntPair();
    }
    fillStemQueryOrder();
    maximal = new MaximalWeightedClique();
    statistics = new Statistics();
  }

  private void fillStemAgreement() {
    for (int i = 0; i < queryStemData.size(); i++) {
      queryStemsAgreement[i][i] = false;
      StemData stem1 = queryStemData.get(i);
      int f1 = stem1.getPosition1() + stem1.getStructure1().indexOf('(');
      int f2 = stem1.getPosition1() + stem1.getLen1();
      int f3 = stem1.getPosition2();
      int f4 = stem1.getPosition2() + stem1.getStructure2().lastIndexOf(')');
      for (int j = i + 1; j < queryStemData.size(); j++) {
        StemData stem2 = queryStemData.get(j);
        // calculate the positions
        int s1 = stem2.getPosition1() + stem2.getStructure1().indexOf('(');
        int s2 = stem2.getPosition1() + stem2.getLen1();
        int s3 = stem2.getPosition2();
        int s4 = stem2.getPosition2() + stem2.getStructure2().lastIndexOf(')');

        // calculate the intersections
        double intersection1 = intersection(f1, f2, s1, s2);
        double intersection2 = intersection(f3, f4, s3, s4);

        // we only disallow the case where both legs overlap and that
        // this overlap is above a threshold
        if (intersection1 > 0 && intersection2 > 0
            && (intersection1 + intersection2) > OVERLAP_LIMIT) {
          queryStemsAgreement[i][j] = false;
          queryStemsAgreement[j][i] = false;
        } else {
          queryStemsAgreement[i][j] = true;
          queryStemsAgreement[j][i] = true;
        }

      }
    }
  }

  private double intersection(int a1, int a2, int b1, int b2) {
    final double maxOverlap = Math.min(a2 - a1 + 1, b2 - b1 + 1);
    final int l0 = Math.max(a1, b1);
    final int l1 = Math.min(a2, b2);
    final int diff = l1 - l0 + 1;
    if (diff <= 0) {
      return 0;
    }
    return diff / maxOverlap;
  }

  private void fillStemQueryOrder() {
    for (int i = 0; i < queryStemData.size(); i++) {
      for (int j = 0; j < queryStemData.size(); j++) {
        if (i != j) {
          StemData stem1 = queryStemData.get(i);
          StemData stem2 = queryStemData.get(j);
          fillStemOrder(stem1, stem2, queryStemsOrder[i][j]);
          queryDistanceType[i][j] = innerDistanceType(queryStemsOrder[i][j], stem1, stem2);
          queryInnerDistance[i][j] = innerDistance(queryDistanceType[i][j], stem1, stem2);
        }
      }
    }
  }

  private int innerDistanceType(int[] stemsOrder, StemData stem1, StemData stem2) {
    // cover all six relevant configurations
    if (Arrays.equals(stemsOrder, DISTORTION_ARRAYS[0])) {
      // { 0, 1, 2, 3 = 4, 5, 6, 7 }
      return 1;
    } else if (Arrays.equals(stemsOrder, DISTORTION_ARRAYS[1])) {
      // { 4, 5, 6, 7 = 0, 1, 2, 3 }
      return 2;
    } else if (Arrays.equals(stemsOrder, DISTORTION_ARRAYS[2])) {
      // { 0, 1 = 4, 5, 6, 7, 2, 3 }
      return 3;
    } else if (Arrays.equals(stemsOrder, DISTORTION_ARRAYS[3])) {
      // { 4, 5 = 0, 1, 2, 3, 6, 7 }
      return 4;
    } else if (Arrays.equals(stemsOrder, DISTORTION_ARRAYS[4])) {
      // { 0, 1, 4, 5 = 2, 3, 6, 7 }
      return 5;
    } else if (Arrays.equals(stemsOrder, DISTORTION_ARRAYS[5])) {
      // { 4, 5, 0, 1 = 6, 7, 2, 3 }
      return 6;
    }
    return 0;
  }

  private int innerDistance(int distanceType, StemData stem1, StemData stem2) {
    switch (distanceType) {
      case 1:
        // { 0, 1, 2, 3 = 4, 5, 6, 7 }
        return stem2.getPosition1() - stem1.getPosition2() - stem1.getLen2() - 1;
      case 2:
        return stem1.getPosition1() - stem2.getPosition2() - stem2.getLen2() - 1;
        // { 4, 5, 6, 7 = 0, 1, 2, 3 }
      case 3:
        // { 0, 1 = 4, 5, 6, 7, 2, 3 }
        return stem2.getPosition1() - stem1.getPosition1() - stem1.getLen1() + 1;
      case 4:
        // { 4, 5 = 0, 1, 2, 3, 6, 7 }
        return stem1.getPosition1() - stem2.getPosition1() - stem2.getLen1();
      case 5:
        // { 0, 1, 4, 5 = 2, 3, 6, 7 }
        return stem1.getPosition2() - stem2.getPosition1() - stem2.getLen1() + 1;
      case 6:
        // { 4, 5, 0, 1 = 6, 7, 2, 3 }
        return stem2.getPosition2() - stem1.getPosition1() - stem1.getLen1() + 1;
    }
    return 0;
  }

  /**
   * Push the next stem to the scorer. Stems must be pushed in increasing end position order.
   * 
   * @param stemData The next stem to process
   * @return If the pushed stem is inside of the current region return {@code true}, otherwise
   *         return {@code false}.
   */

  public boolean push(StemData stemData) {
    pushedStems++;
    final boolean isInSameWindow =
        !(!currentStemsInRegion.isEmpty() && currentStemsInRegion.getFirst().getPosition1()
            + maxRegionSize < (stemData.getPosition2() + stemData.getLen2()));

    if (!isInSameWindow) {
      computeWindowScore();

      // clean the window
      while (!currentStemsInRegion.isEmpty()
          && currentStemsInRegion.getFirst().getPosition1() + maxRegionSize < (stemData
              .getPosition2() + stemData.getLen2())) {
        StemData data = currentStemsInRegion.removeFirst();
        this.stemsPool.enqueue(data);
      }

      // clear the new stems in region list
      newStemsInRegion.clear();
    }

    currentStemsInRegion.addLast(stemData);
    newStemsInRegion.add(stemData);
    return isInSameWindow;
  }

  private void computeWindowScore() {
    FlexibleUndirectedGraph graph = new FlexibleUndirectedGraph();
    ArrayList<StemData> currentList = new ArrayList<StemData>();
    Link<StemData> currentLink = currentStemsInRegion.getFirstLink();
    TIntArrayList mandatory = new TIntArrayList();
    while (currentLink.getNext() != null) {
      currentList.add(currentLink.getData());
      int id = graph.addNode();
      if (newStemsInRegion.contains(currentLink.getData())) {
        mandatory.add(id);
      }
      currentLink = currentLink.getNext();
    }
    mandatory.sort();
    int[] arrQuery;
    int[] arrRegion = new int[8];
    // create edges
    for (int ind1 = 0; ind1 < currentList.size(); ind1++) {
      final StemData r1 = currentList.get(ind1);
      final int ordinal1 = r1.getOrdinal();
      for (int ind2 = ind1 + 1; ind2 < currentList.size(); ind2++) {
        final StemData r2 = currentList.get(ind2);
        final int ordinal2 = r2.getOrdinal();
        // if the stems map to different group ordinal and have the same
        // topology then they have an edge in the graph
        if (ordinal1 != ordinal2 && queryStemsAgreement[r1.getOrdinal()][r2.getOrdinal()]) {

          // check the strand of the current sequence
          boolean strandMinus = currentStrand.equals(Strand.MINUS);
          if (strandMinus) {
            arrQuery = this.queryStemsOrder[r2.getOrdinal()][r1.getOrdinal()];
          } else {
            arrQuery = this.queryStemsOrder[r1.getOrdinal()][r2.getOrdinal()];
          }
          // check topology and distortion
          fillStemOrder(r1, r2, arrRegion);

          statistics.all++;
          if (Arrays.equals(arrQuery, arrRegion) && !isDistorted(r1, r2, strandMinus)) {
            graph.addEdge(ind1, ind2);
          }
        }
      }
    }

    if (graph.getEdgeNum() == 0) {
      zeroCalculations++;
      return;
    }

    double[] weights = new double[currentList.size()];
    for (int i = 0; i < currentList.size(); i++) {
      weights[i] = currentList.get(i).getScore();
    }
    maximal.setGraph(graph);
    maximal.setTimeout(7000);
    computedRegions++;
    TIntArrayList ans = maximal.findMaximumWeightedClique(weights, mandatory);
    if (maximal.timedout()) {
      log.warn("Got time out on graph: [" + graph.stats() + "]");
    }
    /*
     * count++; if (count % 1000 == 0) { System.out.println(maximal.statistics()); }
     */
    float maxValue = 0;
    if (ans.size() > 0) {
      for (int i = 0; i < ans.size(); i++) {
        maxValue += currentList.get(ans.get(i)).getScore();
      }
      createResult(maxValue, ans, currentList);
    }
  }

  private boolean isDistorted(StemData r1, StemData r2, boolean strandMinus) {
    statistics.topology++;
    final int type;
    final double d1;
    if (strandMinus) {
      type = queryDistanceType[r2.getOrdinal()][r1.getOrdinal()];
      d1 = queryInnerDistance[r2.getOrdinal()][r1.getOrdinal()];
    } else {
      type = queryDistanceType[r1.getOrdinal()][r2.getOrdinal()];
      d1 = queryInnerDistance[r1.getOrdinal()][r2.getOrdinal()];
    }
    double d2 = innerDistance(type, r1, r2);
    if (Math.max(d1, d2) <= 10) {
      return false;
    }
    final double ratio;
    if (d1 > d2) {
      ratio = d1 / d2;
    } else {
      ratio = d2 / d1;
    }
    if (ratio > MAX_DISTORTION) {
      // log.info("inner distances: " + d1 + "\t" + d2);
      statistics.distortion++;
      return true;
    }
    return false;
  }

  private void createResult(float score, TIntArrayList path, ArrayList<StemData> dataList) {
    Result res = resultsPool.dequeue();
    if (res == null) {
      res = new Result(score, new ArrayList<StemData>());
    } else {
      // clear the list to the pool
      for (int i = 0; i < res.list.size(); i++) {
        stemsPool.enqueue(res.list.get(i));
      }
      res.list.clear();
      res.score = score;
    }
    ArrayList<StemData> list = res.list;
    for (int i = 0; i < path.size(); i++) {
      StemData copy = dataList.get(path.get(i));
      StemData ans = stemsPool.dequeue();
      if (ans == null) {
        ans = new StemData(copy);
      } else {
        ans.reuse(copy);
      }
      list.add(ans);
    }
    int minPos = Integer.MAX_VALUE;
    int endAll = Integer.MIN_VALUE;

    // go down until the link is null, add a copy to the result list

    for (int i = 0; i < path.size(); i++) {
      StemData current = dataList.get(path.get(i));

      // check the position and end
      if (minPos > current.getPosition1())
        minPos = current.getPosition1();

      final int end = current.getPosition2() + current.getLen2();
      if (endAll < end) {
        endAll = end;
      }
    }
    res.position = minPos;
    res.length = endAll - minPos + 1;

    if (currentResult != null) {
      resultsPool.enqueue(currentResult);
    }
    currentResult = res;
  }

  private void fillStemOrder(StemData data1, StemData data2, int[] arr) {
    stemArray1[0].setFirst(data1.getPosition1());
    stemArray1[0].setSecond(Legs.F1);
    stemArray1[1].setFirst(data1.getPosition1() + data1.getLen1() - 1);
    stemArray1[1].setSecond(Legs.F2);
    stemArray1[2].setFirst(data1.getPosition2() + 1);
    stemArray1[2].setSecond(Legs.F3);
    stemArray1[3].setFirst(data1.getPosition2() + data1.getLen2());
    stemArray1[3].setSecond(Legs.F4);

    stemArray2[0].setFirst(data2.getPosition1());
    stemArray2[0].setSecond(Legs.S1);
    stemArray2[1].setFirst(data2.getPosition1() + data2.getLen1() - 1);
    stemArray2[1].setSecond(Legs.S2);
    stemArray2[2].setFirst(data2.getPosition2() + 1);
    stemArray2[2].setSecond(Legs.S3);
    stemArray2[3].setFirst(data2.getPosition2() + data2.getLen2());
    stemArray2[3].setSecond(Legs.S4);

    merge(stemArray1, stemArray2, arr);
  }

  public void merge(IntPair[] arr1, IntPair[] arr2, int[] answer) {
    int i = 0, j = 0, k = 0;
    int diff;
    while (i < arr1.length && j < arr2.length) {
      diff = arr1[i].getFirst() - arr2[j].getFirst();
      if (diff < 0) {
        answer[k++] = arr1[i++].getSecond();
      } else if (diff > 0) {
        answer[k++] = arr2[j++].getSecond();
      } else {
        if (arr1[i].getSecond() - arr2[j].getSecond() < 0) {
          answer[k++] = arr1[i++].getSecond();
        } else {
          answer[k++] = arr2[j++].getSecond();
        }
      }
    }

    while (i < arr1.length) {
      answer[k++] = arr1[i++].getSecond();
    }

    while (j < arr2.length) {
      answer[k++] = arr2[j++].getSecond();
    }
  }

  /**
   * @return a deep copy of the current result
   */
  public Result getResult() {
    if (currentResult == null) {
      return currentResult;
    }
    return new Result(currentResult, stemsPool);
  }

  /**
   * Clear the region scorer before handling a new sequence.
   * 
   * @param strand The strand of the sequence being scanned.
   */
  public void startSequence(Strand strand) {

    resultsPool.enqueue(currentResult);
    currentResult = null;

    while (!currentStemsInRegion.isEmpty()) {
      StemData data = currentStemsInRegion.removeFirst();
      this.stemsPool.enqueue(data);
    }

    this.currentStrand = strand;
    statistics.clear();
  }

  /**
   * This method should be called when the current sequence is ended. it sets the current best list
   * to be returned by {@link #getStems()}
   */
  public void endSequence() {
    computeWindowScore();
    while (!currentStemsInRegion.isEmpty()) {
      currentStemsInRegion.removeFirst();
    }
    log.info(statistics.toString());
  }

  public void close() {
    log.info("Pushed a total of " + pushedStems + " stems");
    // clear
    resultsPool.enqueue(currentResult);
    currentResult = null;

    // clear the results queue to the pool
    Result res = resultsPool.dequeue();
    while (res != null) {
      for (int i = 0; i < res.list.size(); i++) {
        stemsPool.enqueue(res.list.get(i));
      }
      res.list.clear();
      res = resultsPool.dequeue();
    }

    while (!currentStemsInRegion.isEmpty()) {
      StemData data = currentStemsInRegion.removeFirst();
      this.stemsPool.enqueue(data);
    }
  }

  private class Legs {
    static final int F1 = 0;
    static final int F2 = 1;
    static final int F3 = 2;
    static final int F4 = 3;
    static final int S1 = 4;
    static final int S2 = 5;
    static final int S3 = 6;
    static final int S4 = 7;
  }

  public void enqueueResult(Result result) {
    this.resultsPool.enqueue(result);
  }

  private class Statistics {
    private int all;
    private int topology;
    private int distortion;

    public void clear() {
      all = 0;
      topology = 0;
      distortion = 0;
    }

    @Override
    public String toString() {
      return "Statistics [all=" + all + ", topology=" + topology + ", distortion=" + distortion
          + "]";
    }

  }

  public int getComputedRegions() {
    return this.computedRegions;
  }

  public void getStatistics(StringBuilder sb) {
    sb.append("Pushed " + pushedStems + " stems\n");
    sb.append("Ran " + computedRegions + " region computations\n");
    sb.append("Found " + zeroCalculations + " region where zero calculation needed\n");
    sb.append(statistics.toString() + "\n");
  }
}

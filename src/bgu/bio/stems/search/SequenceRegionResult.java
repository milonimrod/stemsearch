package bgu.bio.stems.search;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import bgu.bio.adt.queue.Pool;
import bgu.bio.io.file.json.JSONArray;
import bgu.bio.io.file.json.JSONException;
import bgu.bio.io.file.json.JSONObject;

public class SequenceRegionResult implements Comparable<SequenceRegionResult> {

  private double score;
  private String sequenceId;
  private String strand;
  private int position;
  private int length;
  private List<StemData> stems;
  private String sequence;
  private double pValue;

  /**
   * Constructor.
   * 
   * @param sequenceId The sequence ID.
   * @param strand The strand.
   * @param stems The stems in the region.
   * @param charBufferPool
   * @throws IOException
   */
  public SequenceRegionResult(String sequenceId, String strand, List<StemData> stems,
      Pool<StemData> pool) {
    this.sequenceId = sequenceId;
    this.strand = strand;

    this.stems = new ArrayList<StemData>();
    score = 0;
    position = Integer.MAX_VALUE;
    int endAll = Integer.MIN_VALUE;

    for (StemData current : stems) {
      StemData ans = pool.dequeue();
      if (ans != null) {
        ans.reuse(current);
      } else {
        ans = new StemData(current);
      }
      this.stems.add(ans);
      score += current.getScore();
      final int start = current.getPosition1();
      if (position > start) {
        position = start;
      }
      final int end = current.getPosition2() + current.getLen2();
      if (endAll < end) {
        endAll = end;
      }
    }
    this.pValue = Double.NaN;

    score = roundScore(score);

    length = endAll - position + 1;
  }

  @Override
  public int compareTo(SequenceRegionResult other) {
    double diff = this.score - other.score;
    if (diff == 0) {
      return this.position - other.position;
    }
    if (diff > 0) {
      return 1;
    }
    return -1;
  }

  public double getScore() {
    return score;
  }

  public void setScore(double score) {
    this.score = roundScore(score);
  }

  public String getSequenceId() {
    return sequenceId;
  }

  public String getStrand() {
    return strand;
  }

  public int getPosition() {
    return position;
  }

  public void setPosition(int newPosition) {
    this.position = newPosition;
  }

  public int getLength() {
    return length;
  }

  public List<StemData> getStems() {
    return stems;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Score: " + score);
    builder.append('\n');
    builder.append("Sequence: " + sequenceId);
    builder.append('\n');
    builder.append("Strand: " + strand);
    builder.append('\n');
    builder.append("Location: [" + position + "," + (position + length - 1) + "]");
    builder.append('\n');
    builder.append("Stems: ");
    builder.append('\n');
    for (StemData stem : stems) {
      builder.append(stem.toString());
      builder.append('\n');
    }

    return builder.toString();
  }

  public String getSequence() {
    return sequence;
  }

  public void setPvalue(double value) {
    this.pValue = value;
  }

  public double getPValue() {
    return this.pValue;
  }

  public boolean equalStems(List<StemData> otherStems) {
    return stems.equals(otherStems);
  }

  public static JSONObject toJSON(SequenceRegionResult sequenceRegionResult) throws JSONException {
    JSONObject result = new JSONObject();
    result.put("score", sequenceRegionResult.getScore());
    result.put("pValue", sequenceRegionResult.getPValue());
    result.put("sequenceId", sequenceRegionResult.getSequenceId());
    result.put("strand", sequenceRegionResult.getStrand());
    result.put("startPosition", sequenceRegionResult.getPosition());
    result.put("sequence", sequenceRegionResult.getSequence());
    result.put("endPosition", sequenceRegionResult.getPosition() + sequenceRegionResult.getLength()
        - 1);

    List<StemData> stems = sequenceRegionResult.getStems();
    for (StemData stem : stems) {
      result.append("stems", StemData.toJSON(stem));
    }
    return result;
  }

  public static SequenceRegionResult fromJSON(JSONObject result) throws JSONException {
    SequenceRegionResult res =
        new SequenceRegionResult(result.getString("sequenceId"), result.getString("strand"),
            StemData.fromJSON(result.getJSONArray("stems")), new Pool<StemData>());

    res.pValue = result.optDouble("pValue");
    res.sequence = result.optString("sequence", null);
    if (res.sequence == null || res.sequence.length() == 0) {
      res.sequence = null;
    }

    return res;
  }

  public void computePvalue(double score, double mu, double std, double sampleSize) {
    double ans = 0;
    if (sampleSize <= 1) {
      this.pValue = Double.NaN;
      return;
    }
    if (score < mu) {
      // we want a directional result. if the score is smaller than the
      // average we want to ignore it ...
      this.pValue = 1;
      return;
    }
    double k = Math.abs(score - mu) / std;
    // ans = (1.0 / Math.sqrt(sampleSize * (sampleSize + 1)))* (((sampleSize
    // - 1) / (k * k)) + 1);
    ans = Math.min(1.0, 1.0 / (k * k * (sampleSize + 1)));
    this.pValue = ans;
  }

  public int getEnd() {
    return position + length - 1;
  }

  public void setSequence(String sequence) {
    this.sequence = sequence;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + length;
    long temp;
    temp = Double.doubleToLongBits(pValue);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    result = prime * result + position;
    temp = Double.doubleToLongBits(score);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    result = prime * result + ((sequence == null) ? 0 : sequence.hashCode());
    result = prime * result + ((sequenceId == null) ? 0 : sequenceId.hashCode());
    result = prime * result + ((stems == null) ? 0 : stems.hashCode());
    result = prime * result + ((strand == null) ? 0 : strand.hashCode());
    return result;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof SequenceRegionResult)) {
      return false;
    }
    SequenceRegionResult other = (SequenceRegionResult) obj;

    if (length != other.length) {
      return false;
    }
    if (Double.doubleToLongBits(pValue) != Double.doubleToLongBits(other.pValue)) {
      return false;
    }
    if (position != other.position) {
      return false;
    }
    if (Double.doubleToLongBits(score) != Double.doubleToLongBits(other.score)) {
      return false;
    }
    if (sequence == null) {
      if (other.sequence != null) {
        return false;
      }
    } else if (!sequence.equals(other.sequence)) {
      return false;
    }
    if (sequenceId == null) {
      if (other.sequenceId != null) {
        return false;
      }
    } else if (!sequenceId.equals(other.sequenceId)) {
      return false;
    }

    if (strand == null) {
      if (other.strand != null) {
        return false;
      }
    } else if (!strand.equals(other.strand)) {
      return false;
    }

    if (stems == null) {
      if (other.stems != null) {
        return false;
      }
    } else if (!stems.equals(other.stems)) {
      return false;
    }

    return true;
  }

  private double roundScore(double d) {
    return Math.round(d * 1000) / 1000.0;
  }

  public static SequenceRegionResult[] fromJSON(JSONArray jsonArray) throws JSONException {
    SequenceRegionResult[] ans = new SequenceRegionResult[jsonArray.length()];
    for (int i = 0; i < ans.length; i++) {
      ans[i] = SequenceRegionResult.fromJSON(jsonArray.getJSONObject(i));
    }
    return ans;
  }
}

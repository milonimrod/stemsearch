package bgu.bio.stems.filereader;

import java.io.BufferedReader;
import java.io.CharArrayReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PushbackReader;

/**
 * this class will read a file in the FASTA format in a multi-thread way. it uses the
 * {@link PushbackReader} to save initiations of objects.
 */
public class FastaReaderCreator {

  /** The maximum size of single FASTA sequence. */
  private int maxFastaSize;

  /** The buffer to contain the FASTA sequence. */
  private char[] buffer;


  /** The line of the next sequence information. */
  private String line;

  private BufferedReader reader;

  private String fileName;

  /**
   * Instantiates a new FASTA reader creator.
   * 
   * @param maxFastaSize the max FASTA size
   */
  public FastaReaderCreator(String fileName, int maxFastaSize) {
    this.maxFastaSize = maxFastaSize;
    this.fileName = fileName;
    reset();
  }

  public void reset() {
    this.buffer = new char[maxFastaSize];
    this.line = "";
    try {
      this.reader = new BufferedReader(new FileReader(fileName));
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }

  /**
   * Creates the reader.
   * 
   * @return the push back reader
   */
  public PushbackReader createReader() {
    return new PushbackReader(new CharArrayReader(new char[maxFastaSize]), maxFastaSize);
  }

  /**
   * Read next.
   * 
   * @param reader the reader
   */
  public synchronized boolean next(PushbackReader fillReader) {
    if (reader == null) {
      return false;
    }

    int bufPos = 0;
    try {
      // run until EOF or new header
      do {
        line = line.trim();
        if (line.length() != 0) {
          if (bufPos != 0 && line.charAt(0) == '>') // not the first line but header
          {
            break;
          }

          for (int i = 0; i < line.length(); i++) {
            buffer[bufPos + i] = line.charAt(i);
          }

          bufPos += line.length();

          // add new line symbol
          buffer[bufPos] = '\n';
          bufPos++;
        }
      } while ((line = reader.readLine()) != null);
      if (line == null) {
        this.reader.close();
        this.reader = null;
        this.line = "";
      }

      fillReader.unread(buffer, 0, bufPos);
    } catch (IOException e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }

  public static void main(String[] args) throws IOException {
    FastaReaderCreator read =
        new FastaReaderCreator("/home/milon/workspace/Work/Sequences/motifs.fasta", 30000);
    PushbackReader push = read.createReader();
    while (read.next(push)) {
      BufferedReader in = new BufferedReader(push);
      String line;
      System.out.println("READ");
      while ((line = in.readLine()) != null) {
        System.out.println("***" + line + "***");
      }
    }
  }
}

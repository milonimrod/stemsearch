package bgu.bio.stems.filereader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.zip.GZIPInputStream;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.AnnotatedSequencePool;
import bgu.bio.stems.indexing.SequenceReader;
import bgu.bio.util.CharBuffer;
import bgu.bio.util.alphabet.RnaAlphabet;

public class FastaFormatReader implements SequenceReader {

  private static final int MAX_LINE_SIZE = 8192;
  private BufferedReader reader;
  private boolean complement;
  private RnaAlphabet rnaAlphabet;

  private boolean changedHeader;
  private boolean isFirstInSequence;
  private boolean readerFinished;

  private String sequenceIdentifier;
  private String oldSequenceIdentifier;

  private int indexInSequence;
  private int oldIndexInSequence;
  private int initIndexInSequence;

  private AnnotatedSequencePool aSPool;
  private AnnotatedSequence toServe;

  private int windowSize;
  private char[] buffer;
  private int currentBufferPosition;
  private int currentBufferSize;
  private boolean closeReader;

  private static final char NON_LEGAL_REPLACEMENT =
      RnaAlphabet.getInstance().letters()[RnaAlphabet.N];

  public FastaFormatReader(int windowSize) {
    this(windowSize, AnnotatedSequencePool.getInstance());
  }

  public FastaFormatReader(int windowSize, AnnotatedSequencePool pool) {
    this.windowSize = windowSize;
    this.aSPool = pool;
    this.buffer = new char[Math.max(windowSize * 2, MAX_LINE_SIZE * 2)];
    this.rnaAlphabet = RnaAlphabet.getInstance();
    this.closeReader = false;
  }

  @Override
  public void setAnnotatedSequencePool(AnnotatedSequencePool pool) {
    aSPool = pool;
  }

  @Override
  public void setReader(Reader reader, boolean complement) {
    if (reader instanceof BufferedReader) {
      this.reader = (BufferedReader) reader;
    } else {
      this.reader = new BufferedReader(reader);
    }
    this.complement = complement;

    this.readerFinished = false;
    this.currentBufferPosition = 0;
    this.currentBufferSize = 0;
    this.indexInSequence = 1;
    this.initIndexInSequence = this.indexInSequence;
    try {
      this.readLine();
    } catch (IOException e) {
      System.err.println(e);
      this.toServe = null;
      return;
    }
    // read the first line
    this.changedHeader = false;
    this.toServe = serve();
  }

  @Override
  public void setReader(AnnotatedSequence sequence, boolean complement) {

    StringBuilder sb = new StringBuilder(sequence.getLength() + sequence.getSequenceId().length());
    sb.append('>');
    sb.append(sequence.getSequenceId());
    sb.append('\n');
    CharBuffer array = sequence.getSequence();
    for (int i = 0; i < array.length(); i++) {
      sb.append(array.charAt(i));
      if (i % 70 == 0)
        sb.append('\n');
    }

    this.reader = new BufferedReader(new StringReader(sb.toString()));

    this.complement = complement;

    this.readerFinished = false;
    this.currentBufferPosition = 0;
    this.currentBufferSize = 0;
    this.indexInSequence = sequence.getLocation();
    this.initIndexInSequence = this.indexInSequence;
    this.closeReader = true;

    try {
      this.readLine();
    } catch (IOException e) {
      System.err.println(e);
      this.toServe = null;
      return;
    }

    // read the first line
    this.changedHeader = false;
    this.toServe = serve();
  }

  private void readLine() throws IOException {
    String line = null;

    line = reader.readLine();
    while (line != null && line.trim().length() == 0) {
      line = reader.readLine();
    }
    if (line == null) { // reached EOF
      line = "";
      this.readerFinished = true;
      if (closeReader) {
        reader.close();
      }
      return;
    }

    if (isHeaderLine(line)) {
      // Change all the given data; like genome type or name
      this.parseHeaderLine(line);
      this.changedHeader = true;
    } else {
      // put the line in the buffer
      currentBufferPosition = 0;
      final int len = line.length();
      for (int i = 0; i < len; i++) {
        buffer[i] = line.charAt(i);
      }
      currentBufferSize = len;
      // fix errors in sequences
      for (int i = 0; i < currentBufferSize; i++) {
        if (!rnaAlphabet.isLegal(buffer[i])) {
          buffer[i] = NON_LEGAL_REPLACEMENT;
        }
      }
      if (complement) {
        for (int i = 0; i < currentBufferSize; i++) {
          buffer[i] = rnaAlphabet.complement(buffer[i]);
        }
      }
    }
  }

  private boolean isHeaderLine(String line) {
    return line.startsWith(">");
  }

  private void parseHeaderLine(String line) {
    this.oldSequenceIdentifier = this.sequenceIdentifier;
    this.sequenceIdentifier = line.substring(1);

    this.oldIndexInSequence = this.indexInSequence;
    this.indexInSequence = this.initIndexInSequence;
  }

  @Override
  public String getSequenceId() {
    if (this.changedHeader) {
      return oldSequenceIdentifier;
    }
    return sequenceIdentifier;
  }

  @Override
  public boolean hasNext() {
    return (this.toServe != null);
  }

  @Override
  public AnnotatedSequence next() {
    AnnotatedSequence next = toServe;
    toServe = serve();
    isFirstInSequence = (next.getLocation() == initIndexInSequence);
    return next;
  }

  private AnnotatedSequence serve() {

    if (this.readerFinished)
      return null;

    int read = 0;

    this.changedHeader = false;
    char[] ansArray = new char[this.windowSize];
    while (!this.readerFinished && read < this.windowSize && !this.changedHeader) {
      int leftInBuffer = this.currentBufferSize - this.currentBufferPosition;
      if (this.windowSize - read <= leftInBuffer) // enough data in the buffer
      {
        // create the Annotated sequence;
        System
            .arraycopy(this.buffer, this.currentBufferPosition, ansArray, read, windowSize - read);
        this.currentBufferPosition += windowSize - read;
        read += windowSize - read;
      } else {
        if (leftInBuffer > 0) {
          System.arraycopy(this.buffer, this.currentBufferPosition, ansArray, read, leftInBuffer);
          read += leftInBuffer;
          currentBufferPosition += leftInBuffer;
        }

        try {
          this.readLine();
        } catch (IOException e) {
          System.err.println(e);
          return null;
        }
      }
    }
    if (read == 0) {
      return this.serve();
    }
    AnnotatedSequence sequence;
    if (this.changedHeader) {
      sequence =
          aSPool.getSequence(oldSequenceIdentifier, oldIndexInSequence, new CharBuffer(ansArray, 0,
              read));
    } else {
      sequence =
          aSPool
              .getSequence(sequenceIdentifier, indexInSequence, new CharBuffer(ansArray, 0, read));
      indexInSequence += read;
    }

    return sequence;
  }

  @Override
  public void clear() {
    // this.currentBufferPosition = 0;
    // this.currentBufferSize = 0;
  }

  @Override
  public boolean isFirstInSequence() {
    return isFirstInSequence;
  }

  @Override
  public int getReadSize() {
    return this.windowSize;
  }

  @Override
  public int getIndexInSequence() {
    return indexInSequence;
  }

  @Override
  public void setReader(File file, boolean complement) {
    try {
      closeReader = true;
      if (file.getAbsolutePath().toLowerCase().endsWith(".gz")) {
        this.setReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(file))),
            complement);
      } else {
        this.setReader(new FileReader(file), complement);
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void close() {
    try {
      reader.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}

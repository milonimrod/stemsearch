package bgu.bio.stems.filereader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.zip.GZIPInputStream;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.AnnotatedSequencePool;
import bgu.bio.stems.indexing.SequenceReader;
import bgu.bio.util.alphabet.RnaAlphabet;

public class FastaFormatReaderGenome implements SequenceReader {

  private int windowSize;
  private AnnotatedSequencePool pool;
  private BufferedReader reader;
  private char[] buff;
  private int buffPosition;
  private StringBuilder headerBuilder;
  private long served;
  private String sequenceId;
  private int initialPosition;
  private boolean complement;
  private int positionInSequence;
  private int valInReader;

  final RnaAlphabet alphabet = RnaAlphabet.getInstance();

  public FastaFormatReaderGenome(int windowSize) {
    this.windowSize = windowSize;
    buff = new char[windowSize];
    headerBuilder = new StringBuilder();
    pool = new AnnotatedSequencePool();
    served = 0;
    sequenceId = "";
  }

  @Override
  public AnnotatedSequence next() {
    AnnotatedSequence ans =
        pool.getSequence(sequenceId, positionInSequence - buffPosition, buff, 0, buffPosition,
            null, 0);
    // read next
    buffPosition = 0;
    served++;
    try {
      readAhead();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return ans;
  }

  @Override
  public boolean hasNext() {
    return buffPosition > 0;
  }

  @Override
  public void clear() {}

  @Override
  public void setAnnotatedSequencePool(AnnotatedSequencePool pool) {
    this.pool = pool;
  }

  @Override
  public void setReader(File file, boolean complement) {
    try {
      if (file.getAbsolutePath().toLowerCase().endsWith(".gz")) {
        this.setReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(file))),
            complement, 1);
      } else {
        this.setReader(new FileReader(file), complement, 1);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void setReader(Reader reader, boolean complement) {
    setReader(reader, complement, 1);
  }

  @Override
  public void setReader(AnnotatedSequence sequence, boolean complement) {
    setReader(new StringReader(">" + sequence.getSequenceId() + "\n"
        + sequence.getSequence().toString()), complement, sequence.getStructureStartPosition());
  }

  private void setReader(Reader reader, boolean complement, int position) {
    this.reader = new BufferedReader(reader);
    this.complement = complement;
    this.initialPosition = position;
    this.positionInSequence = position;
    this.buffPosition = 0;
    valInReader = -2;
    try {
      readHeader();
      readAhead();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void readHeader() throws IOException {
    valInReader = reader.read();
    char c = (char) valInReader;
    if (c == '>') {
      // header line read until the end of the line
      headerBuilder.setLength(0);

      valInReader = reader.read();
      c = (char) valInReader;
      while (c != '\r' && c != '\n') {
        headerBuilder.append(c);
        valInReader = reader.read();
        c = (char) valInReader;
      }
      served = 0;
      sequenceId = headerBuilder.toString();
      positionInSequence = initialPosition;
    } else {
      throw new RuntimeException("Can't find header start");
    }
  }

  private void readAhead() throws IOException {
    if (valInReader == -2) {
      valInReader = reader.read();
    }
    while (valInReader != -1 && buffPosition < windowSize) {
      char c = (char) valInReader;
      if (c == '>') {
        throw new RuntimeException("Can't handle multiple sequences");
      } else if (c != '\r' && c != '\n') {
        if (complement) {
          c = alphabet.complement(c);
        }
        buff[buffPosition] = c;
        buffPosition++;
        positionInSequence++;
      }
      valInReader = reader.read();
    }
  }

  @Override
  public String getSequenceId() {
    return sequenceId;
  }

  @Override
  public boolean isFirstInSequence() {
    return served == 1;
  }

  @Override
  public int getReadSize() {
    return this.windowSize;
  }

  @Override
  public int getIndexInSequence() {
    return positionInSequence - buffPosition;
  }

  public void close() {
    try {
      this.reader.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}

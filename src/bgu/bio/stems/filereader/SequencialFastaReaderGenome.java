package bgu.bio.stems.filereader;

import java.io.File;
import java.io.Reader;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.AnnotatedSequencePool;
import bgu.bio.stems.indexing.SequenceReader;
import bgu.bio.util.CharBuffer;

public class SequencialFastaReaderGenome implements SequenceReader {

  private FastaFormatReader reader;
  private AnnotatedSequence past, present, future;
  private AnnotatedSequencePool pool;
  private long served = 0;
  private CharBuffer buff;
  private boolean futureIsNew;

  public SequencialFastaReaderGenome(int windowSize) {
    this.reader = new FastaFormatReader(windowSize);
    this.past = AnnotatedSequence.terminalAnnotatedSequence(windowSize, 'N');
    this.present = AnnotatedSequence.terminalAnnotatedSequence(windowSize, 'N');
    pool = AnnotatedSequencePool.getInstance();
    buff = new CharBuffer(windowSize * 3);
  }

  @Override
  public AnnotatedSequence next() {
    int offset = 0;
    buff.setChar(offset, past.getSequence().array());
    offset += past.getLength();
    buff.setChar(offset, present.getSequence().array());
    offset += present.getLength();
    if (future != null && !futureIsNew) {
      buff.setChar(offset, future.getSequence().array());
      offset += future.getLength();
    }
    buff.setSize(offset);
    served++;
    AnnotatedSequence ans =
        new AnnotatedSequence(present.getSequenceId(), past.getLocation(), buff, null);
    ans.setStructureStart(past.getLength());
    ans.setStructureEnd(past.getLength() + present.getLength() - 1);
    pool.releaseSequence(past);
    past = present;
    present = future;
    if (reader.hasNext()) {
      future = reader.next();
    } else {
      future = null;
    }
    return ans;
  }

  @Override
  public boolean hasNext() {
    return this.present != null;
  }

  @Override
  public void clear() {

  }

  @Override
  public void setAnnotatedSequencePool(AnnotatedSequencePool pool) {
    reader.setAnnotatedSequencePool(pool);
    this.pool = pool;
  }

  @Override
  public void setReader(File file, boolean complement) {
    reader.setReader(file, complement);
    this.load();
  }

  private void load() {
    served = 0;
    present = reader.next();
    past.setLocation(present.getLocation() - past.getLength());
    if (reader.hasNext()) {
      future = reader.next();
      if (reader.isFirstInSequence()) {
        futureIsNew = true;
      }
    } else {
      future = null;
    }
  }

  @Override
  public void setReader(Reader reader, boolean complement) {
    this.reader.setReader(reader, complement);
    this.load();
  }

  @Override
  public void setReader(AnnotatedSequence sequence, boolean complement) {
    this.reader.setReader(sequence, complement);
    this.load();
  }

  @Override
  public String getSequenceId() {
    return this.present.getSequenceId();
  }

  @Override
  public boolean isFirstInSequence() {
    return served == 1;
  }

  @Override
  public int getReadSize() {
    return reader.getReadSize();
  }

  @Override
  public int getIndexInSequence() {
    return reader.getIndexInSequence();
  }
}

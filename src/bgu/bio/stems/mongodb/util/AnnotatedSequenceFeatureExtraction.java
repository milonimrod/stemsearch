package bgu.bio.stems.mongodb.util;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.util.CharBuffer;

public class AnnotatedSequenceFeatureExtraction {

  public static int getLoopLength(AnnotatedSequence sequence) {
    CharBuffer structure = sequence.getStructure();
    int lastOpen = structure.lastIndexOf('(', structure.length());
    int firstClose = structure.indexOf(')', 0);
    int loopLength = firstClose - lastOpen - 1;
    return loopLength;
  }

  public static double getAverageLength(AnnotatedSequence sequence) {
    CharBuffer structure = sequence.getStructure();
    int firstOpen = structure.indexOf('(');
    if (firstOpen < 0) {
      System.err.println(sequence);
      throw new Error("Structure without open parenthesis given");
    }
    int currOpen = structure.lastIndexOf('(');
    int currClose = structure.indexOf(')');
    if (currClose < 0) {
      throw new Error("Structure without close parenthesis given");
    }
    int lastClose = structure.lastIndexOf(')');
    int stackLength = 0;
    double sumStackLength = 0;
    int stacks = 0;
    while (currOpen >= firstOpen && currClose <= lastClose) {
      int state = 0;
      if (structure.charAt(currOpen) == '(') {
        state += 2;
      }
      if (structure.charAt(currClose) == ')') {
        state += 1;
      }
      if (state == 0) {
        // both not in stack
        if (stackLength > 0) {
          // close current stack
          sumStackLength += stackLength;
          stackLength = 0;
          stacks++;
        }
      } else {
        if (state != 3) {
          if (stackLength > 0) {
            // close current stack
            sumStackLength += stackLength;
            stackLength = 0;
            stacks++;
          }
          if (state == 1) {
            while (structure.charAt(currOpen) != '(') {
              currOpen--;
            }
          } else { // state = 2
            while (structure.charAt(currClose) != ')') {
              currClose++;
            }
          }
        }
        // stacked - elongate current stack
        stackLength++;
      }
      currOpen--;
      currClose++;
    }
    if (stackLength > 0) {
      // close current stack
      // close current stack
      sumStackLength += stackLength;
      stackLength = 0;

      stacks++;
    }
    if (stacks == 1) {
      return sumStackLength;
    }
    return sumStackLength / stacks;
  }

  public static double getSTDLength(AnnotatedSequence sequence) {
    CharBuffer structure = sequence.getStructure();
    int firstOpen = structure.indexOf('(');
    if (firstOpen < 0) {
      System.err.println(sequence);
      throw new Error("Structure without open parenthesis given");
    }
    int currOpen = structure.lastIndexOf('(');
    int currClose = structure.indexOf(')');
    if (currClose < 0) {
      throw new Error("Structure without close parenthesis given");
    }
    int lastClose = structure.lastIndexOf(')');
    int stackLength = 0;
    double sumStackLength = 0;
    double sumSquare = 0;
    int stacks = 0;
    while (currOpen >= firstOpen && currClose <= lastClose) {
      int state = 0;
      if (structure.charAt(currOpen) == '(') {
        state += 2;
      }
      if (structure.charAt(currClose) == ')') {
        state += 1;
      }
      if (state == 0) {
        // both not in stack
        if (stackLength > 0) {
          // close current stack
          sumStackLength += stackLength;
          sumSquare += stackLength * stackLength;
          stackLength = 0;
          stacks++;
        }
      } else {
        if (state != 3) {
          if (stackLength > 0) {
            // close current stack
            sumStackLength += stackLength;
            sumSquare += stackLength * stackLength;
            stackLength = 0;
            stacks++;
          }
          if (state == 1) {
            while (structure.charAt(currOpen) != '(') {
              currOpen--;
            }
          } else { // state = 2
            while (structure.charAt(currClose) != ')') {
              currClose++;
            }
          }
        }
        // stacked - elongate current stack
        stackLength++;
      }
      currOpen--;
      currClose++;
    }
    if (stackLength > 0) {
      // close current stack
      if (stackLength > 0) {
        // close current stack
        sumStackLength += stackLength;

        sumSquare += stackLength * stackLength;
        stackLength = 0;
        stacks++;
      }
    }
    if (stacks == 1) {
      return 0;
    }
    final double avg = sumStackLength / stacks;
    return Math.sqrt(sumSquare / stacks - avg * avg);
  }
}

package bgu.bio.stems.mongodb;

public class StemFieldsConstants {
  public static final String ENERGY = "ene";
  public static final String POSITION_START = "pos";
  public static final String POSITION_END = "pos2";
  public static final String SEQUENCE = "seq";
  public static final String STRUCTURE = "str";
  public static final String LOOP_LENGTH = "ll";
  public static final String AVG_STACK_SIZE = "avgS";
  public static final String STD_STACK_SIZE = "stdS";
}

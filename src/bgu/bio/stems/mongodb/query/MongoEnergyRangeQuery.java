package bgu.bio.stems.mongodb.query;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.mongodb.StemFieldsConstants;

public class MongoEnergyRangeQuery extends MongoPercentageQuery {

  public MongoEnergyRangeQuery(double range) {
    super(StemFieldsConstants.ENERGY, range);
  }

  @Override
  protected double getParam(AnnotatedSequence sequence) {
    return sequence.getEnergy();
  }
}

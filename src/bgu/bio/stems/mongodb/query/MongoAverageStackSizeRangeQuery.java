package bgu.bio.stems.mongodb.query;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.mongodb.StemFieldsConstants;
import bgu.bio.stems.mongodb.util.AnnotatedSequenceFeatureExtraction;

public class MongoAverageStackSizeRangeQuery extends MongoPercentageQuery {

  public MongoAverageStackSizeRangeQuery(double range) {
    super(StemFieldsConstants.AVG_STACK_SIZE, range);
  }

  @Override
  protected double getParam(AnnotatedSequence sequence) {
    return AnnotatedSequenceFeatureExtraction.getAverageLength(sequence);
  }

}

package bgu.bio.stems.mongodb.query;

import bgu.bio.stems.indexing.AnnotatedSequence;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public abstract class MongoPercentageQuery implements MongoQuery {

  private String name;
  private double percentageValue;

  public MongoPercentageQuery(String name, double percentageValue) {
    this.name = name;
    this.percentageValue = percentageValue;
  }

  protected abstract double getParam(AnnotatedSequence sequence);

  @Override
  public DBObject getQuery(AnnotatedSequence sequence) {
    BasicDBObject obj = new BasicDBObject();
    addQuery(sequence, obj);
    return obj;
  }

  @Override
  public void addQuery(AnnotatedSequence sequence, DBObject obj) {
    final double param = getParam(sequence);
    final double val = percentageValue * param;
    final double min = Math.min(param - val, param + val);
    final double max = Math.max(param - val, param + val);

    BasicDBObject objRange = new BasicDBObject().append("$gte", min).append("$lte", max);

    obj.put(name, objRange);
  }
}

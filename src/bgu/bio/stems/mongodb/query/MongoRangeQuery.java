package bgu.bio.stems.mongodb.query;

import bgu.bio.stems.indexing.AnnotatedSequence;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public abstract class MongoRangeQuery implements MongoQuery {

  private String name;
  private double rangeValue;

  public MongoRangeQuery(String name, double rangeValue) {
    this.name = name;
    this.rangeValue = rangeValue;
  }

  protected abstract double getParam(AnnotatedSequence sequence);

  @Override
  public DBObject getQuery(AnnotatedSequence sequence) {
    BasicDBObject obj = new BasicDBObject();
    addQuery(sequence, obj);
    return obj;
  }

  @Override
  public void addQuery(AnnotatedSequence sequence, DBObject obj) {
    double param = getParam(sequence);

    BasicDBObject objRange =
        new BasicDBObject().append("$gte", param - rangeValue).append("$lte", param + rangeValue);

    obj.put(name, objRange);
  }
}

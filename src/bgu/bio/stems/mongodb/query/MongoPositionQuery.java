package bgu.bio.stems.mongodb.query;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.mongodb.StemFieldsConstants;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class MongoPositionQuery implements MongoQuery {

  private int start;
  private int end;

  public MongoPositionQuery(int start, int end) {
    this.start = start;
    this.end = end;
  }

  @Override
  public DBObject getQuery(AnnotatedSequence sequence) {
    BasicDBObject obj = new BasicDBObject();
    addQuery(null, obj);
    return obj;
  }

  @Override
  public void addQuery(AnnotatedSequence sequence, DBObject obj) {
    BasicDBObject objRange = new BasicDBObject().append("$gte", start).append("$lte", end);

    obj.put(StemFieldsConstants.POSITION_END, objRange);
  }
}

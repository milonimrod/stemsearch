package bgu.bio.stems.mongodb.query;

import bgu.bio.stems.indexing.AnnotatedSequence;

import com.mongodb.DBObject;

public interface MongoQuery {
  public DBObject getQuery(AnnotatedSequence sequence);

  public void addQuery(AnnotatedSequence sequence, DBObject obj);
}

package bgu.bio.stems.mongodb.query;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.mongodb.StemFieldsConstants;
import bgu.bio.stems.mongodb.util.AnnotatedSequenceFeatureExtraction;

public class MongoLoopLengthRangeQuery extends MongoPercentageQuery {

  public MongoLoopLengthRangeQuery(double range) {
    super(StemFieldsConstants.LOOP_LENGTH, range);
  }

  @Override
  protected double getParam(AnnotatedSequence sequence) {
    return AnnotatedSequenceFeatureExtraction.getLoopLength(sequence);
  }



}

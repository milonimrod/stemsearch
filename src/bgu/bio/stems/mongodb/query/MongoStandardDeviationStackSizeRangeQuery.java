package bgu.bio.stems.mongodb.query;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.mongodb.StemFieldsConstants;
import bgu.bio.stems.mongodb.util.AnnotatedSequenceFeatureExtraction;

public class MongoStandardDeviationStackSizeRangeQuery extends MongoPercentageQuery {

  public MongoStandardDeviationStackSizeRangeQuery(double range) {
    super(StemFieldsConstants.STD_STACK_SIZE, range);
  }

  @Override
  protected double getParam(AnnotatedSequence sequence) {
    return AnnotatedSequenceFeatureExtraction.getSTDLength(sequence);
  }

}

package bgu.bio.stems.mongodb.encoders;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.mongodb.StemFieldsConstants;
import bgu.bio.stems.mongodb.util.AnnotatedSequenceFeatureExtraction;

import com.mongodb.DBObject;

public class StandardDeviationStackLengthEncoder implements MongoEncoder {

  @Override
  public void encode(AnnotatedSequence sequence, DBObject obj) {
    obj.put(StemFieldsConstants.STD_STACK_SIZE,
        AnnotatedSequenceFeatureExtraction.getSTDLength(sequence));
  }

}

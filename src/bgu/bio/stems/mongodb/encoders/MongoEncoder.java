package bgu.bio.stems.mongodb.encoders;

import bgu.bio.stems.indexing.AnnotatedSequence;

import com.mongodb.DBObject;

public interface MongoEncoder {
  public void encode(AnnotatedSequence sequence, DBObject obj);
}

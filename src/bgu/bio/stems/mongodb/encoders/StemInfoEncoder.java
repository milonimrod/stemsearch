package bgu.bio.stems.mongodb.encoders;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.RNASequenceHuffmanCoding;
import bgu.bio.stems.indexing.RNAStructureHuffmanCoding;
import bgu.bio.stems.mongodb.StemFieldsConstants;
import bgu.bio.util.CharBuffer;

import com.mongodb.DBObject;

public class StemInfoEncoder implements MongoEncoder {
  RNASequenceHuffmanCoding huffmanSequence;
  RNAStructureHuffmanCoding huffmanStructure;

  public StemInfoEncoder() {
    huffmanSequence = new RNASequenceHuffmanCoding();
    huffmanStructure = new RNAStructureHuffmanCoding();
  }

  @Override
  public void encode(AnnotatedSequence sequence, DBObject obj) {
    obj.put(StemFieldsConstants.POSITION_START, sequence.getStructureStartPosition());
    obj.put(StemFieldsConstants.POSITION_END, sequence.getStructureEndPosition());
    encodeStructure(sequence, obj);
    encodeSequence(sequence, obj);
  }

  private void encodeStructure(AnnotatedSequence sequence, DBObject obj) {
    CharBuffer structure = sequence.getStructure();
    int lastOpen = structure.lastIndexOf('(');
    if (lastOpen < 0) {
      throw new IllegalArgumentException("Structure without open parenthesis given");
    }
    int firstClose = structure.indexOf(')');
    if (firstClose < 0) {
      throw new IllegalArgumentException("Structure without close parenthesis given");
    }

    obj.put(
        StemFieldsConstants.STRUCTURE,
        huffmanStructure.encode(structure.substring(0, lastOpen + 1)
            + structure.substring(firstClose, structure.length() - firstClose)));
  }

  private void encodeSequence(AnnotatedSequence sequence, DBObject obj) {
    CharBuffer seq = sequence.getSequence();
    CharBuffer structure = sequence.getStructure();
    int lastOpen = structure.lastIndexOf('(');
    if (lastOpen < 0) {
      throw new IllegalArgumentException("Structure without open parenthesis given");
    }
    int firstClose = structure.indexOf(')');
    if (firstClose < 0) {
      throw new IllegalArgumentException("Structure without close parenthesis given");
    }

    obj.put(
        StemFieldsConstants.SEQUENCE,
        huffmanSequence.encode(seq.substring(0, lastOpen + 1)
            + seq.substring(firstClose, structure.length() - firstClose)));
  }
}

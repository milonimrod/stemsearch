package bgu.bio.stems.mongodb.encoders;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.mongodb.StemFieldsConstants;

import com.mongodb.DBObject;

public class EnergyEncoder implements MongoEncoder {

  @Override
  public void encode(AnnotatedSequence sequence, DBObject obj) {
    obj.put(StemFieldsConstants.ENERGY, sequence.getEnergy());
  }
}

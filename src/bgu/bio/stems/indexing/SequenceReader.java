package bgu.bio.stems.indexing;

import java.io.File;
import java.io.Reader;

public interface SequenceReader extends SequenceProducer {

  /**
   * Set the reader to receive data from.
   * 
   * @param fil The file to use.
   * @param complement if {@code true} converts each letter to the complement.
   */
  public void setReader(File file, boolean complement);

  /**
   * Set the reader to receive data from.
   * 
   * @param reader The reader to use.
   * @param complement if {@code true} converts each letter to the complement.
   */
  public void setReader(Reader reader, boolean complement);

  /**
   * Set the reader to receive data from.
   * 
   * @param sequence The annotated sequence to be used as a reader.
   * @param complement if {@code true} converts each letter to the complement.
   */
  public void setReader(AnnotatedSequence sequence, boolean complement);

  /**
   * Return the identifier String of the current sequence.
   */
  public String getSequenceId();

  /**
   * Return true if the current {@link AnnotatedSequence} is the first one given for the read
   * sequence. Valid only after calling {@link #next()}.
   */
  boolean isFirstInSequence();

  /**
   * Gets the read size.
   * 
   * @return the read size
   */
  public int getReadSize();

  /**
   * Gets the index of the last data read in the sequence.
   * 
   * @return the index of the last data read in the sequence.
   */
  public int getIndexInSequence();
}

package bgu.bio.stems.indexing;

import gnu.trove.map.hash.TObjectIntHashMap;

import java.util.Arrays;

import bgu.bio.adt.list.PooledLinkedList;
import bgu.bio.util.CharBuffer;

public class AnnotatedSequencePool {

  private static AnnotatedSequencePool INSTANCE;
  private Object lock;
  private PooledLinkedList<AnnotatedSequence> list;
  private static int LIMIT = 30000;

  public static final boolean DEBUG = false;
  private TObjectIntHashMap<String> instanceMap;

  public AnnotatedSequencePool() {
    list = new PooledLinkedList<AnnotatedSequence>();
    lock = new Object();

    if (DEBUG) {
      instanceMap = new TObjectIntHashMap<String>();
    }
  }

  public AnnotatedSequencePool(String mBeanName) {
    this();
  }

  public static AnnotatedSequencePool getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new AnnotatedSequencePool("bgu.bio.pools.StaticAnnotatedSequencePool");
    }
    return INSTANCE;
  }

  public void releaseSequence(AnnotatedSequence sequence) {
    synchronized (lock) {
      if (DEBUG) {
        if (sequence.getStackTrace() == null) {
          sequence.setStackTrace("UNKNOWN");
        }
        int val = instanceMap.get(sequence.getStackTrace());
        instanceMap.put(sequence.getStackTrace(), val - 1);
      }

      if (list.size() < LIMIT) {
        list.addLast(sequence);
      }
    }
  }

  public AnnotatedSequence getSequence(String sequenceId, int location, CharBuffer sequence,
      char[] structure, int structureLength) {
    AnnotatedSequence ret = null;
    synchronized (lock) {
      if (list.size() > 0) {
        ret = list.removeFirst();
      }
    }
    if (ret != null) {
      ret.reuseInstance(sequenceId, location, sequence, structure, structureLength);
    } else {
      ret = new AnnotatedSequence(sequenceId, location, sequence, structure, Strand.UNKNOWN);
    }

    if (DEBUG) {
      String key = Arrays.toString(Thread.currentThread().getStackTrace());
      ret.setStackTrace(key);
      synchronized (lock) {
        if (!instanceMap.containsKey(key)) {
          instanceMap.put(key, 1);
        } else {
          int val = instanceMap.get(key);
          instanceMap.put(key, val + 1);
        }
      }
    }

    return ret;
  }

  public AnnotatedSequence getSequence(String sequenceId, int location, char[] sequence,
      int offset, int length, char[] structure, int structureLength) {
    AnnotatedSequence ret = null;
    synchronized (lock) {
      if (list.size() > 0) {
        ret = list.removeFirst();
      }
    }
    if (ret != null) {
      ret.reuseInstance(sequenceId, location, sequence, offset, length, structure, structureLength);
    } else {
      ret =
          new AnnotatedSequence(sequenceId, location, sequence, offset, length, structure,
              structureLength, Strand.UNKNOWN);
    }

    if (DEBUG) {
      String key = Arrays.toString(Thread.currentThread().getStackTrace());
      ret.setStackTrace(key);
      synchronized (lock) {
        if (!instanceMap.containsKey(key)) {
          instanceMap.put(key, 1);
        } else {
          int val = instanceMap.get(key);
          instanceMap.put(key, val + 1);
        }
      }
    }

    return ret;
  }

  public AnnotatedSequence getSequence(String sequenceId, int location, CharBuffer sequence) {
    AnnotatedSequence seq = getSequence(sequenceId, location, sequence, null, 0);
    return seq;
  }

  public AnnotatedSequence getSequence(String sequenceId, int location, char[] sequence,
      int offset, int length, char[] structure, int structureLength, Strand strand) {
    AnnotatedSequence seq =
        getSequence(sequenceId, location, sequence, offset, length, structure, structureLength);
    seq.setStrand(strand);
    return seq;
  }

  public AnnotatedSequence getSequence(AnnotatedSequence existing, boolean cloneSequence) {
    AnnotatedSequence ret = null;
    synchronized (lock) {
      if (list.size() > 0) {
        ret = list.removeFirst();

      }
    }
    if (ret != null) {
      ret.reuseInstance(existing, cloneSequence);
    } else {
      ret = existing.clone(cloneSequence);
    }

    if (DEBUG) {
      String key = Arrays.toString(Thread.currentThread().getStackTrace());
      ret.setStackTrace(key);
      synchronized (lock) {
        if (!instanceMap.containsKey(key)) {
          instanceMap.put(key, 1);
        } else {
          int val = instanceMap.get(key);
          instanceMap.put(key, val + 1);
        }
      }
    }

    return ret;
  }

  public AnnotatedSequence getSequence(String sequenceId, int location, CharBuffer sequence,
      char[] structure, int structureLength, Strand strand) {
    AnnotatedSequence seq = getSequence(sequenceId, location, sequence, structure, structureLength);
    seq.setStrand(strand);
    return seq;
  }

  /**
   * Clear the pool.
   * 
   * @return the int
   */
  public int clear() {
    int counter = 0;
    synchronized (this.lock) {
      while (list.size() > 0) {
        list.removeFirst();
        counter++;
      }
    }
    return counter;
  }
}

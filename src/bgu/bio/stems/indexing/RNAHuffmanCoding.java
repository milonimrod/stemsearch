package bgu.bio.stems.indexing;

import gnu.trove.list.array.TCharArrayList;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.BitSet;
import java.util.HashMap;

import bgu.bio.compression.HuffmanCode;
import bgu.bio.stems.util.ByteUtils;

/**
 * @author milon Compression of RNA based on Huffman Coding {@link HuffmanCode}.
 */
public abstract class RNAHuffmanCoding {
  protected HuffmanCode code;
  protected HashMap<Character, int[]> map;
  protected BitSet bits;

  public RNAHuffmanCoding() {
    code = new HuffmanCode();
    bits = new BitSet();
  }

  /**
   * @param dis receive a data input stream that represent the information on the encoded sequence
   * @return
   * @throws IOException
   */
  public String decode(InputStream dis) {
    TCharArrayList list = new TCharArrayList();
    decode(dis, list, bits);
    return lstToString(list);
  }

  private String lstToString(TCharArrayList lst) {
    StringBuilder sb = new StringBuilder(lst.size());
    for (int i = 0; i < lst.size(); i++) {
      sb.append(lst.get(i));
    }
    return sb.toString();
  }

  public void decode(InputStream inputStream, TCharArrayList list, BitSet bitset) {
    // number of characters in the sequence
    try {
      int amount = ByteUtils.readVInt(inputStream);
      bitset.clear();
      list.resetQuick();
      int numberOfBytes = 0;

      byte[] buffer = new byte[1];
      int numberRead = 0;
      numberRead = inputStream.read(buffer);
      while (numberRead == buffer.length) {
        // save the current byte
        byte b = buffer[0];
        // read next
        // byte is not the last one
        numberOfBytes++;
        int start = numberOfBytes * 8 - 1;
        for (int i = 0; i < 8; i++) {
          bitset.set(start - i, (b & 1) == 1);
          b >>= 1;
        }
        numberRead = inputStream.read(buffer);
      }

      inputStream.close();
      // start walks on the Trie
      int pos = 0;
      for (int i = 0; i < amount; i++) {
        pos = code.readNext(bitset, pos);
        list.add(code.getChar());
      }
    } catch (IOException ex) {
      throw new RuntimeException("Problem", ex);
    }

  }

  /**
   * The binary output is as follows: vint (amount of characters in the sequence) followed by the
   * stream of bytes representing the data
   * 
   * @param map
   * @param sequence
   * @return
   * @throws IOException
   */
  public byte[] encode(String sequence) {
    ByteArrayOutputStream bOut = new ByteArrayOutputStream(1);
    OutputStream out = new DataOutputStream(bOut);
    try {
      ByteUtils.writeVInt(sequence.length(), out);
      byte b = 0;
      int offset = 0;
      for (int i = 0; i < sequence.length(); i++) {
        int[] seq = map.get(sequence.charAt(i));
        for (int p = 0; p < seq.length; p++) {
          if (seq[p] == 1) {
            b |= 1;
          }
          offset++;
          if (offset == 8) {
            offset = 0;
            out.write(b);
            b = 0;
          }
          b <<= 1;
        }
      }
      // byte marker = (byte) offset;
      if (offset != 0) {
        // padd
        while (offset != 7) {
          b <<= 1;
          offset++;
        }
        out.write(b);
      }
      // out.writeByte(marker);
      out.close();
    } catch (IOException ex) {
      throw new RuntimeException("Error", ex);
    }
    return bOut.toByteArray();
  }
}

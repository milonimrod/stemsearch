package bgu.bio.stems.indexing;

public interface SequenceConsumer {
  void push(AnnotatedSequence sequence);
}

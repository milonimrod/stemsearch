package bgu.bio.stems.indexing;

public interface PropertiesConstants {

  public static final String indexingPropertiesFile = "indexing.properties";
  public static final String searchPropertiesFile = "search.properties";
  public static final String randomIndexLocationString = "index.random.suffix";
  public static final String sequenceReaderString = "sequence.reader";
  public static final String indexLocation = "index.location";
  public static final String bothStrands = "both.strands";
  public static final String filtersString = "filters";
  public static final String tokenEncodersString = "indexing.encoders";
  public static final String tokenEncodersPackage = "indexing.encoders.package";
  public static final String searchEncodersString = "search.sequence.encoders";
  public static final String maxStemsInDocumentString = "max.stems.document";
  public static final String indexRAMBufferSizeMB = "index.ram.buffer.size.mb";

  public static final String WINDOW_SIZE_STRING = "??WINDOW_SIZE??";
  public static final String FIELD_NUM_THREADS = "numThreads";
  public static final String FIELD_NUM_INDEXES = "numIndexes";
  public static final String FIELD_NUM_FILES_FACTOR = "numFilesFactor";

}

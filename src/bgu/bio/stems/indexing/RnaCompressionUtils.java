package bgu.bio.stems.indexing;

import bgu.bio.util.CharBuffer;
import bgu.bio.util.alphabet.RnaAlphabet;

public class RnaCompressionUtils {

  private static byte[][][] sequenceChars2Bytes;
  private static char[][][] bytes2SequenceChars;

  static {
    int nChars = 128;
    sequenceChars2Bytes = new byte[nChars][nChars][nChars];
    int nBytes = 125;
    bytes2SequenceChars = new char[3][nBytes][];

    for (int i = 0; i < nChars; i++) {
      for (int j = 0; j < nChars; j++) {
        for (int k = 0; k < nChars; k++) {
          sequenceChars2Bytes[i][j][k] = (byte) -1;
        }
      }
    }

    RnaAlphabet rnaAlphabet = RnaAlphabet.getInstance();
    for (int i = 0; i < RnaAlphabet.SIZE - 1; i++) {
      char upperI = rnaAlphabet.decode(i);
      char lowerI = Character.toLowerCase(upperI);
      bytes2SequenceChars[0][i] = new char[] {upperI};
      sequenceChars2Bytes[0][0][upperI] = (byte) i;
      sequenceChars2Bytes[0][0][lowerI] = (byte) i;
      for (int j = 0; j < RnaAlphabet.SIZE - 1; j++) {
        char upperJ = rnaAlphabet.decode(j);
        char lowerJ = Character.toLowerCase(upperJ);
        byte value = (byte) (i + j * 5);
        sequenceChars2Bytes[0][upperI][upperJ] = value;
        sequenceChars2Bytes[0][lowerI][upperJ] = value;
        sequenceChars2Bytes[0][upperI][lowerJ] = value;
        sequenceChars2Bytes[0][lowerI][lowerJ] = value;
        bytes2SequenceChars[1][value] = new char[] {upperJ, upperI};
        for (int k = 0; k < RnaAlphabet.SIZE - 1; k++) {
          char upperK = rnaAlphabet.decode(k);
          char lowerK = Character.toLowerCase(upperK);
          value = (byte) (i + j * 5 + k * 25);
          sequenceChars2Bytes[upperI][upperJ][upperK] = value;
          sequenceChars2Bytes[lowerI][upperJ][upperK] = value;
          sequenceChars2Bytes[upperI][lowerJ][upperK] = value;
          sequenceChars2Bytes[lowerI][lowerJ][upperK] = value;
          sequenceChars2Bytes[upperI][upperJ][lowerK] = value;
          sequenceChars2Bytes[lowerI][upperJ][lowerK] = value;
          sequenceChars2Bytes[upperI][lowerJ][lowerK] = value;
          sequenceChars2Bytes[lowerI][lowerJ][lowerK] = value;
          bytes2SequenceChars[2][value] = new char[] {upperK, upperJ, upperI};
        }
      }
    }
  }

  public static byte[] compressSequence(CharBuffer sequence) {
    int length = sequence.length();
    byte[] comp = new byte[getCompressedSequenceLength(length)];
    RnaAlphabet rnaAlphabet = RnaAlphabet.getInstance();
    for (int i = 0, j = 0; i < comp.length; i++, j += 3) {
      int i1 = length - j;
      char charAt0 = rnaAlphabet.getLetter(sequence.charAt(j));
      if (i1 == 1) {
        comp[i] = sequenceChars2Bytes[0][0][charAt0];
      } else {
        char charAt1 = rnaAlphabet.getLetter(sequence.charAt(j + 1));
        if (i1 == 2) {
          comp[i] = sequenceChars2Bytes[0][charAt1][charAt0];
        } else {
          char charAt2 = rnaAlphabet.getLetter(sequence.charAt(j + 2));
          comp[i] = sequenceChars2Bytes[charAt2][charAt1][charAt0];
        }
      }
    }
    return comp;
  }

  public static byte[] compressSequence(char[] sequence) {
    int length = sequence.length;
    byte[] comp = new byte[getCompressedSequenceLength(length)];
    RnaAlphabet rnaAlphabet = RnaAlphabet.getInstance();
    for (int i = 0, j = 0; i < comp.length; i++, j += 3) {
      int i1 = length - j;
      char charAt0 = rnaAlphabet.getLetter(sequence[j]);
      if (i1 == 1) {
        comp[i] = sequenceChars2Bytes[0][0][charAt0];
      } else {
        char charAt1 = rnaAlphabet.getLetter(sequence[j + 1]);
        if (i1 == 2) {
          comp[i] = sequenceChars2Bytes[0][charAt1][charAt0];
        } else {
          char charAt2 = rnaAlphabet.getLetter(sequence[j + 2]);
          comp[i] = sequenceChars2Bytes[charAt2][charAt1][charAt0];
        }
      }
    }
    return comp;
  }

  public final static int getCompressedSequenceLength(int length) {
    return (length + 2) / 3;
  }

  public static char[] decompressSequence(byte[] bytes, int offset, int length) {
    char[] chars = new char[length];
    int currByte = 0;
    int currBase = 0;
    for (; length > 0; currByte++, currBase += 3, length -= 3) {
      if (length == 1) {
        System.arraycopy(bytes2SequenceChars[0][bytes[offset + currByte]], 0, chars, currBase, 1);
      } else if (length == 2) {
        System.arraycopy(bytes2SequenceChars[1][bytes[offset + currByte]], 0, chars, currBase, 2);
      } else {
        // length >= 3, more than one byte left to read
        System.arraycopy(bytes2SequenceChars[2][bytes[offset + currByte]], 0, chars, currBase, 3);
      }
    }
    return chars;
  }

  private static byte[][][][] structureChars2Bytes;
  private static char[][][] bytes2StructureChars;
  private static int STRUCTURE_CHAR_OFFSET = 37;

  static {
    char[] letters = new char[] {'.', '(', ')'};
    int nChars = 10;
    structureChars2Bytes = new byte[nChars][nChars][nChars][nChars];
    int nBytes = 81;
    bytes2StructureChars = new char[4][nBytes][];

    for (int i = 0; i < nChars; i++) {
      for (int j = 0; j < nChars; j++) {
        for (int k = 0; k < nChars; k++) {
          for (int l = 0; l < nChars; l++) {
            structureChars2Bytes[i][j][k][l] = (byte) -1;
          }
        }
      }
    }

    for (int i = 0; i < letters.length; i++) {
      int valI = letters[i] - STRUCTURE_CHAR_OFFSET;
      bytes2StructureChars[0][i] = new char[] {letters[i]};
      structureChars2Bytes[0][0][0][valI] = (byte) i;
      for (int j = 0; j < letters.length; j++) {
        byte value = (byte) (i + j * 3);
        int valJ = letters[j] - STRUCTURE_CHAR_OFFSET;
        structureChars2Bytes[0][0][valI][valJ] = value;
        bytes2StructureChars[1][value] = new char[] {letters[j], letters[i]};
        for (int k = 0; k < letters.length; k++) {
          value = (byte) (i + j * 3 + k * 9);
          int valK = letters[k] - STRUCTURE_CHAR_OFFSET;
          structureChars2Bytes[0][valI][valJ][valK] = value;
          bytes2StructureChars[2][value] = new char[] {letters[k], letters[j], letters[i]};
          for (int l = 0; l < letters.length; l++) {
            value = (byte) (i + j * 3 + k * 9 + l * 27);
            int valL = letters[l] - STRUCTURE_CHAR_OFFSET;
            structureChars2Bytes[valI][valJ][valK][valL] = value;
            bytes2StructureChars[3][value] =
                new char[] {letters[l], letters[k], letters[j], letters[i]};
          }
        }
      }
    }
  }

  public static byte[] compressStructure(CharBuffer structure) {
    int length = structure.length();
    byte[] comp = new byte[getCompressedStructureLength(length)];
    for (int i = 0, j = 0; i < comp.length; i++, j += 4) {
      int i1 = length - j;
      int charAt0 = structure.charAt(j) - STRUCTURE_CHAR_OFFSET;
      if (i1 == 1) {
        comp[i] = structureChars2Bytes[0][0][0][charAt0];
      } else {
        int charAt1 = structure.charAt(j + 1) - STRUCTURE_CHAR_OFFSET;
        if (i1 == 2) {
          comp[i] = structureChars2Bytes[0][0][charAt1][charAt0];
        } else {
          int charAt2 = structure.charAt(j + 2) - STRUCTURE_CHAR_OFFSET;
          if (i1 == 3) {
            comp[i] = structureChars2Bytes[0][charAt2][charAt1][charAt0];
          } else {
            int charAt3 = structure.charAt(j + 3) - STRUCTURE_CHAR_OFFSET;
            comp[i] = structureChars2Bytes[charAt3][charAt2][charAt1][charAt0];
          }
        }
      }
    }
    return comp;
  }

  public final static int getCompressedStructureLength(int length) {
    return (length + 3) / 4;
  }

  public static char[] decompressStructure(byte[] bytes, int length) {
    char[] chars = new char[length];
    int currByte = 0;
    int currBase = 0;
    for (; length > 0; currByte++, currBase += 4, length -= 4) {
      if (length == 1) {
        System.arraycopy(bytes2StructureChars[0][bytes[currByte]], 0, chars, currBase, 1);
      } else if (length == 2) {
        System.arraycopy(bytes2StructureChars[1][bytes[currByte]], 0, chars, currBase, 2);
      } else if (length == 3) {
        System.arraycopy(bytes2StructureChars[2][bytes[currByte]], 0, chars, currBase, 3);
      } else {
        // length >= 3, more than one byte left to read
        System.arraycopy(bytes2StructureChars[3][bytes[currByte]], 0, chars, currBase, 4);
      }
    }
    return chars;
  }

}

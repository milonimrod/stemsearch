package bgu.bio.stems.indexing;

import java.util.HashMap;

import bgu.bio.compression.HuffmanCode;

/**
 * @author milon Compression of RNA sequences based on Huffman Coding {@link HuffmanCode}.
 */
public class RNAStructureHuffmanCoding extends RNAHuffmanCoding {
  public RNAStructureHuffmanCoding() {
    super();

    HashMap<Character, Integer> freq = new HashMap<Character, Integer>();
    freq.put('(', 10);
    freq.put(')', 9);
    freq.put('.', 8);
    code.buildTree(freq);
    map = code.toMap();
  }
}

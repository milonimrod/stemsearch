package bgu.bio.stems.indexing;

import java.util.HashMap;

import bgu.bio.compression.HuffmanCode;

/**
 * @author milon Compression of RNA sequences based on Huffman Coding {@link HuffmanCode}.
 */
public class RNASequenceHuffmanCoding extends RNAHuffmanCoding {
  public RNASequenceHuffmanCoding() {
    super();

    HashMap<Character, Integer> freq = new HashMap<Character, Integer>();
    freq.put('A', 10);
    freq.put('C', 9);
    freq.put('G', 8);
    freq.put('U', 7);
    freq.put('N', 1);
    code.buildTree(freq);
    map = code.toMap();
  }
}

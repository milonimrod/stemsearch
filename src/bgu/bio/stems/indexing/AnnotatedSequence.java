package bgu.bio.stems.indexing;

import gnu.trove.list.array.TCharArrayList;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Random;

import bgu.bio.stems.util.ByteUtils;
import bgu.bio.stems.util.RnaUtils;
import bgu.bio.util.CharBuffer;
import bgu.bio.util.alphabet.AlphabetUtils;

/**
 * The Class AnnotatedSequence represent a sequence in the read genome / chromosome.
 * 
 */
/**
 * @author milon
 * 
 */
public class AnnotatedSequence implements Comparable<AnnotatedSequence> {

  /** the Identifier of the sequence. (Organism / Chromosome) */
  private String sequenceId;

  /** The position in the genome of the sequence's first nucleotide. */
  private int location;

  /** The sequence of nucleotides. */
  private CharBuffer sequence;

  /** The offset in the sequence where the structure information start. */
  private int structureStart;

  /** The offset in the sequence where the structure information end. */
  private int structureEnd;

  /** The structure string of this sequence. */
  private CharBuffer structure;

  /** The number of nucleotides in the sequence. */
  private int length;

  /** Indicates whether the strand is known, and which strand (PLUS or MINUS) **/
  private Strand strand;

  /** Keeps the energy of the current sequence **/
  private double energy;


  /**
   * Keeps the partition function value of the current sequence
   */
  private double pf;

  /** The offset in the sequence where the loop information start. */
  private int loopStart;

  /** The offset in the sequence where the loop information end. */
  private int loopEnd;

  private String stackTrace;

  final int UNINIT = -10000;

  /**
   * This Constructor has only package visibility, since there should be no direct construction of
   * {@link AnnotatedSequence}, only through {@link AnnotatedSequencePool}.
   * 
   * @param sequenceId the sequence id
   * @param location the location
   * @param sequence the sequence
   * @param structure the structure
   */
  AnnotatedSequence(String sequenceId, int location, CharBuffer sequence, char[] structure,
      Strand strand) {
    this.sequenceId = sequenceId;
    this.location = location;

    this.sequence = sequence;
    this.length = sequence.length();
    this.strand = strand;

    initStructure(structure, structure == null ? 0 : structure.length);
  }

  /**
   * This Constructor has only package visibility, since there should be no direct construction of
   * {@link AnnotatedSequence}, only through {@link AnnotatedSequencePool}.
   * 
   * @param sequenceId the sequence id
   * @param location the location
   * @param sequence the sequence
   * @param structure the structure
   */
  AnnotatedSequence(String sequenceId, int location, char[] sequence, int offset, int length,
      char[] structure, int structureLength, Strand strand) {
    this.sequenceId = sequenceId;
    this.location = location;

    this.sequence = new CharBuffer(sequence, offset, length);
    this.length = length;
    this.strand = strand;
    this.loopStart = UNINIT;
    this.loopEnd = UNINIT;
    initStructure(structure, structureLength);
  }

  public AnnotatedSequence(String sequenceId, int location, CharBuffer sequence,
      CharBuffer structure) {
    this.sequenceId = sequenceId;
    this.location = location;

    this.sequence = sequence;
    this.length = sequence.length();
    this.strand = Strand.UNKNOWN;
    this.loopStart = UNINIT;
    this.loopEnd = UNINIT;

    if (structure != null) {
      initStructure(structure.array(), structure.length());
    } else {
      this.structure = null;
    }
  }

  public AnnotatedSequence clone(boolean cloneSequence) {
    AnnotatedSequence clone =
        new AnnotatedSequence(this.sequenceId, this.location, cloneSequence ? new CharBuffer(
            this.sequence) : this.sequence,
            (cloneSequence && this.structure != null) ? new CharBuffer(this.structure)
                : this.structure);
    clone.length = this.length;
    clone.structureStart = this.structureStart;
    clone.structureEnd = this.structureEnd;
    clone.strand = this.strand;
    clone.energy = this.energy;
    clone.pf = this.pf;
    clone.stackTrace = this.stackTrace;
    clone.loopStart = this.loopStart;
    clone.loopEnd = this.loopEnd;
    return clone;
  }

  @Override
  public String toString() {
    int position = getStructureStartPosition();
    if (position == -1) {
      position = location;
    }
    StringBuilder builder =
        new StringBuilder("Sequence ID: " + sequenceId + " , starting at " + position
            + " , strand " + strand.toString());
    builder.append('\n');
    if (getStructureStart() < 0) {
      builder.append(sequence.toString());
    } else {
      builder.append(sequence.charsAt(getStructureStart(), getStructureEnd() - getStructureStart()
          + 1));
    }
    builder.append('\n');
    if (structure != null && structure.length() > 0) {
      builder.append(structure.charsAt(0, structure.length()));
      builder.append('\n');
    }
    builder.append("energy: " + energy);
    builder.append('\n');
    builder.append("pf: " + pf);
    builder.append('\n');

    return builder.toString();
  }

  /**
   * This method has only package visibility, since there should be no direct reuse of
   * {@link AnnotatedSequence}, only through {@link AnnotatedSequencePool}.
   * 
   * @param toCopy the annotated sequence to copy
   * @param cloneSequence
   */
  public void reuseInstance(AnnotatedSequence toCopy, boolean cloneSequence) {
    this.sequenceId = toCopy.sequenceId;
    this.location = toCopy.location;

    if (cloneSequence) {
      this.sequence = new CharBuffer(toCopy.sequence);
    } else {
      this.sequence = toCopy.sequence;
    }
    this.structureStart = toCopy.structureStart;
    this.structureEnd = toCopy.structureEnd;
    this.length = toCopy.length;
    this.strand = toCopy.strand;

    this.loopStart = toCopy.loopStart;
    this.loopEnd = toCopy.loopEnd;

    this.structure = new CharBuffer(toCopy.structure);
  }

  /**
   * This method has only package visibility, since there should be no direct reuse of
   * {@link AnnotatedSequence}, only through {@link AnnotatedSequencePool}.
   * 
   * @param sequenceId the sequence id
   * @param location the location
   * @param sequence the sequence
   * @param structure the structure
   */

  public void reuseInstance(String sequenceId, int location, CharBuffer sequence, char[] structure,
      int structureLength) {
    this.sequenceId = sequenceId;
    this.location = location;

    this.sequence = sequence;
    this.length = sequence.length();
    this.strand = Strand.UNKNOWN;

    this.loopStart = UNINIT;
    this.loopEnd = UNINIT;

    initStructure(structure, structureLength);
  }

  public void initStructure(char[] newStructure, int structureLength) {
    if (newStructure == null) {
      this.structure = null;
      this.structureStart = -1;
      this.structureEnd = 0;
    } else {
      this.structureStart = 0;
      this.structureEnd = structureLength - 1;
      if (this.structure == null) {
        this.structure = new CharBuffer(structureLength);
      }
      this.structure.setChar(0, newStructure, 0, structureLength);
    }
  }

  public void reuseInstance(String sequenceId, int location, char[] sequence, int offset,
      int length, char[] structure, int structureLength) {
    this.sequenceId = sequenceId;
    this.location = location;
    if (this.sequence == null) {
      this.sequence = new CharBuffer(length);
    }
    this.sequence.setChar(0, sequence, offset, length);
    this.loopStart = 0;
    this.loopEnd = 0;
    this.length = length;
    this.strand = Strand.UNKNOWN;

    this.loopStart = UNINIT;
    this.loopEnd = UNINIT;

    initStructure(structure, structureLength);
  }

  public static AnnotatedSequence randomAnnotatedSequence(int length, AlphabetUtils alphabet) {
    char[] structure = new char[length];
    char[] letters = alphabet.letters();
    char[] sequence = new char[length];
    Random rand = new Random();
    for (int i = 0; i < length; i++) {
      sequence[i] = letters[rand.nextInt(letters.length - 1)];
    }
    AnnotatedSequence instance =
        new AnnotatedSequence("Random sequence", 1, new CharBuffer(sequence), structure,
            Strand.UNKNOWN);

    return instance;
  }

  public static AnnotatedSequence terminalAnnotatedSequence(int length, char letter) {
    char[] sequence = new char[length];

    for (int i = 0; i < length; i++) {
      sequence[i] = letter;
    }
    AnnotatedSequence instance =
        new AnnotatedSequence("Random sequence", 1, new CharBuffer(sequence), null, Strand.UNKNOWN);

    return instance;
  }

  /**
   * Split sequence to tuples.
   * 
   * @param tupleLength the length of the tuples to
   * 
   * @return An two dimension array in length (|sequence| - tupleLength + 1) and depth
   *         (tupleLength), containing all tuples in the Sequence in length "length"
   */
  public char[][] splitSequenceToTuples(int tupleLength) {

    return RnaUtils.splitRnaToTuples(this.sequence, tupleLength);
  }

  /**
   * Gets the primary identifier for the sequence (the header).
   * 
   * @return the primary id
   */
  public String getSequenceId() {
    return sequenceId;
  }

  /**
   * Sets the primary id.
   * 
   * @param sequenceId the new primary id
   */
  public void setSequenceId(String sequenceId) {
    this.sequenceId = sequenceId;
  }

  /**
   * Gets the location of the sequence in the genomic sequence.
   * 
   * @return the index of the first letter in the sequence
   */
  public int getLocation() {
    return location;
  }

  /**
   * Sets the location.
   * 
   * @param location the new location
   */
  public void setLocation(int location) {
    this.location = location;
  }

  /**
   * Gets the location of this sequence's structure.
   * 
   * @return if this sequence has no structure returns -1, otherwise returns the location of this
   *         sequence's structure.
   */
  public int getStructureStartPosition() {
    if (structureStart < 0) {
      return -1;
    }
    return location + structureStart;
  }

  public int getStructureEndPosition() {
    if (structureEnd < 0) {
      return -1;
    }
    return location + structureEnd;
  }

  /**
   * Gets the sequence.
   * 
   * @return the sequence
   */
  public CharBuffer getSequence() {
    return this.sequence;
  }

  /**
   * Gets the structure.
   * 
   * @return the structure
   */
  public CharBuffer getStructure() {
    return this.structure;
  }

  /**
   * Sets the structure
   * 
   * @param structure the new structure
   */
  public void setStructure(CharBuffer structure) {
    this.structure = structure;
  }

  /**
   * Gets the length.
   * 
   * @return the length
   */
  public int getLength() {
    return this.length;
  }

  /**
   * Sets the length.
   * 
   * @param length the new length
   */
  public void setLength(int length) {
    this.length = length;
  }

  /**
   * Gets the the structure start index.
   * 
   * @return the structure start index
   */
  public int getStructureStart() {
    return structureStart;
  }

  /**
   * Sets the structure end index.
   * 
   * @param value the new structure end index
   */
  public void setStructureEnd(int value) {
    this.structureEnd = value;
  }

  /**
   * Gets the the structure end index.
   * 
   * @return the structure end index
   */
  public int getStructureEnd() {
    return this.structureEnd;
  }

  /**
   * Sets the structure Start index.
   * 
   * @param value the new structure start index
   */
  public void setStructureStart(int value) {
    this.structureStart = value;
  }

  /**
   * Gets the strand.
   * 
   * @return the strand
   */
  public Strand getStrand() {
    return strand;
  }

  /**
   * Sets the strand.
   * 
   * @param strand the new strand
   */
  public void setStrand(Strand strand) {
    this.strand = strand;
  }

  public double getEnergy() {
    return energy;
  }

  public void setEnergy(double energy) {
    this.energy = energy;
  }

  public double getPartitionFunction() {
    return pf;
  }

  public void setPartitionFunction(double pf) {
    this.pf = pf;
  }

  public int getLoopStart() {
    return loopStart;
  }

  public void setLoopStart(int loopStart) {
    this.loopStart = loopStart;
  }

  public int getLoopEnd() {
    return loopEnd;
  }

  public void setLoopEnd(int loopEnd) {
    this.loopEnd = loopEnd;
  }

  public int getLoopEndPosition() {
    return getStructureStartPosition() + loopEnd;
  }

  public int getLoopStartPosition() {
    return getStructureStartPosition() + loopStart;
  }

  public void updateSequence(int offset, char[] input) {
    if (this.sequence.array().length < offset + input.length) {
      char[] newArray = new char[offset + input.length];
      System.arraycopy(this.sequence.array(), 0, newArray, 0, offset);
      System.arraycopy(input, 0, newArray, offset, input.length);
      this.sequence.setArray(newArray);
    } else {
      System.arraycopy(input, 0, this.sequence.array(), offset, input.length);
    }
    this.length = offset + input.length;
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) {
      return false;
    }
    AnnotatedSequence other = (AnnotatedSequence) o;
    if (this.getStructureStartPosition() != other.getStructureStartPosition()) {
      return false;
    }
    if (this.getStructureEndPosition() != other.getStructureEndPosition()) {
      return false;
    }
    if (!this.strand.equals(other.strand)) {
      return false;
    }
    if (!this.sequenceId.equals(other.sequenceId)) {
      return false;
    }
    if (this.structure == null) {
      if (other.structure != null) {
        return false;
      }
    } else if (other.structure == null) {
      if (this.structure != null) {
        return false;
      }
    } else if (!this.structure.equals(other.structure)) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int hashcode = this.getStructureStartPosition();
    hashcode = 31 * hashcode + this.getStructureEndPosition();
    hashcode = 31 * hashcode + this.strand.hashCode();
    hashcode = 31 * hashcode + this.sequenceId.hashCode();
    if (this.structure != null) {
      hashcode = 31 * hashcode + this.structure.hashCode();
    }
    return hashcode;
  }

  @Override
  public int compareTo(AnnotatedSequence o) {
    int n = this.getStructureStartPosition() - o.getStructureStartPosition();
    if (n != 0) {
      return n;
    }
    n = this.getStructureEndPosition() - o.getStructureEndPosition();
    if (n != 0) {
      return n;
    }
    n = this.strand.compareTo(o.strand);
    if (n != 0) {
      return n;
    }
    n = this.sequenceId.compareTo(o.sequenceId);
    if (n != 0) {
      return n;
    }
    if (this.structure == null) {
      if (o.structure == null) {
        return 0;
      }
      return -1;
    } else if (o.structure == null) {
      return 1;
    }
    n = this.structure.compareTo(o.structure);
    if (n != 0) {
      return n;
    }
    return 0;
  }

  public String getStackTrace() {
    return stackTrace;
  }

  public void setStackTrace(String stackTrace) {
    this.stackTrace = stackTrace;
  }

  public static void toOutputStream(AnnotatedSequence as, OutputStream out) throws IOException {
    // strand is the only variable written as byte, so it is written first
    // to serve as end condition when reading
    if (as.strand.equals(Strand.UNKNOWN)) {
      out.write((byte) 0);
    } else if (as.strand.equals(Strand.MINUS)) {
      out.write((byte) 1);
    } else if (as.strand.equals(Strand.PLUS)) {
      out.write((byte) 2);
    } else if (as.strand.equals(Strand.BOTH)) {
      out.write((byte) 3);
    }
    byte[] sequenceIdBytes = as.sequenceId.getBytes("UTF-8");
    ByteUtils.writeVInt(sequenceIdBytes.length, out);
    out.write(sequenceIdBytes);
    ByteUtils.writeVInt(as.location, out);
    ByteUtils.writeLong(Double.doubleToLongBits(as.energy), out);
    ByteUtils.writeLong(Double.doubleToLongBits(as.pf), out);
    int lSeq = as.sequence.length();
    ByteUtils.writeVInt(lSeq, out);
    out.write(RnaCompressionUtils.compressSequence(as.sequence));
    if (as.structure == null) {
      ByteUtils.writeVInt(0, out);
    } else {
      int lStr = as.structure.length();
      ByteUtils.writeVInt(lStr, out);
      out.write(RnaCompressionUtils.compressStructure(as.structure));
    }
  }

  public static AnnotatedSequence fromInputStream(InputStream in) throws IOException {
    byte strandByte = (byte) in.read();
    Strand strand = Strand.UNKNOWN;
    if (strandByte == 0) {
      strand = Strand.UNKNOWN;
    } else if (strandByte == 1) {
      strand = Strand.MINUS;
    } else if (strandByte == 2) {
      strand = Strand.PLUS;
    } else if (strandByte == 3) {
      strand = Strand.BOTH;
    } else {
      return null;
    }
    int lSeqId = ByteUtils.readVInt(in);
    String sequenceId;
    if (lSeqId == 0) {
      sequenceId = "";
    } else {
      byte[] bytes = new byte[lSeqId];
      in.read(bytes, 0, lSeqId);
      sequenceId = new String(bytes, "UTF-8");
    }
    int location = ByteUtils.readVInt(in);
    long tmp = ByteUtils.readLong(in);
    double energy = Double.longBitsToDouble(tmp);
    tmp = ByteUtils.readLong(in);
    double pf = Double.longBitsToDouble(tmp);
    int lSeq = ByteUtils.readVInt(in);
    byte[] seqBytes = new byte[RnaCompressionUtils.getCompressedSequenceLength(lSeq)];
    in.read(seqBytes);
    CharBuffer sequence = new CharBuffer(RnaCompressionUtils.decompressSequence(seqBytes, 0, lSeq));
    int lStr = ByteUtils.readVInt(in);
    CharBuffer structure = null;
    if (lStr > 0) {
      byte[] strBytes = new byte[RnaCompressionUtils.getCompressedStructureLength(lStr)];
      in.read(strBytes);
      structure = new CharBuffer(RnaCompressionUtils.decompressStructure(strBytes, lStr));
    }
    AnnotatedSequence as = new AnnotatedSequence(sequenceId, location, sequence, structure);
    as.strand = strand;
    as.energy = energy;
    as.pf = pf;
    return as;
  }

  /**
   * Write the sequence of this {@link AnnotatedSequence} to a file in fasta format.
   * 
   * @param file The file to write to.
   * @throws IOException
   */
  public void writeSequenceAsFasta(File file) throws IOException {
    Writer writer = new BufferedWriter(new FileWriter(file));
    writer.write('>');
    writer.write(getSequenceId());
    writer.write('\n');
    final int line = 100;
    for (int i = 0; i < length; i += line) {
      int len = Math.min(length - i, line);
      writer.write(sequence.array(), i, len);
      writer.write('\n');
    }
    writer.close();
  }

  public CharBuffer getStructureSequence() {
    return new CharBuffer(this.getSequence().charsAt(this.getStructureStart(),
        this.getStructureEnd() - this.getStructureStart() + 1));
  }

  public TCharArrayList getRelevantSequence() {
    int structureStart = this.getStructureStart();
    int start1 = 0;
    int end1 = this.getStructure().lastIndexOf('(') + structureStart;

    int start2 = this.getStructure().indexOf(')') + structureStart;
    int end2 = this.getStructure().length() + structureStart - 1;
    int actualLength = end1 - start1 + 1 + end2 - start2 + 1;

    return extract(this.getSequence(), start1, end1, start2, end2, actualLength);
  }

  public TCharArrayList getRelevantStructure() {
    int structureStart = this.getStructureStart();
    int start1 = 0;
    int end1 = this.getStructure().lastIndexOf('(') + structureStart;

    int start2 = this.getStructure().indexOf(')') + structureStart;
    int end2 = this.getStructure().length() + structureStart - 1;
    int actualLength = end1 - start1 + 1 + end2 - start2 + 1;

    return extract(this.getStructure(), start1, end1, start2, end2, actualLength);
  }

  private TCharArrayList extract(CharBuffer seq, int start1, int end1, int start2, int end2,
      int actualLength) {
    char[] relevant = new char[actualLength];
    char[] left = seq.charsAt(start1, end1 - start1 + 1);
    System.arraycopy(left, 0, relevant, 0, left.length);
    char[] right = seq.charsAt(start2, end2 - start2 + 1);
    System.arraycopy(right, 0, relevant, left.length, right.length);
    TCharArrayList sequence = new TCharArrayList(relevant);
    return sequence;
  }

}

package bgu.bio.stems.indexing;

import java.io.Serializable;

/**
 * An enum which defines the following possible strand states: unknown, plus and minus.
 * 
 * @author sivany
 */
public enum Strand implements Serializable {
  UNKNOWN, PLUS, MINUS, BOTH;

  public static Strand parseStrand(String str) {
    if ("UNKNOWN".equalsIgnoreCase(str) || "false".equalsIgnoreCase(str)) {
      return UNKNOWN;
    } else if ("PLUS".equalsIgnoreCase(str) || "+".equals(str)) {
      return PLUS;
    } else if ("MINUS".equalsIgnoreCase(str) || "-".equals(str)) {
      return MINUS;
    } else if ("BOTH".equalsIgnoreCase(str) || "true".equalsIgnoreCase(str)) {
      return BOTH;
    }
    throw new IllegalArgumentException("No Strand definition for string " + str);
  }
}

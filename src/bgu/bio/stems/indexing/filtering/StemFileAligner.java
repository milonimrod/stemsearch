package bgu.bio.stems.indexing.filtering;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.Strand;

public class StemFileAligner {

  public static void main(String[] args) throws ParseException {
    Options opt = new Options();
    opt.addOption(OptionBuilder.withArgName("file").hasArg()
        .withDescription("property file location").isRequired().create("p"));
    opt.addOption(OptionBuilder.withArgName("file").hasArg()
        .withDescription("output file location").isRequired().create("o"));
    opt.addOption("v", false, "print the sequence");

    CommandLineParser parser = new PosixParser();
    CommandLine cmd = parser.parse(opt, args);

    String propertiesFile = cmd.getOptionValue("p");

    try {
      long time = System.currentTimeMillis();
      Properties props = new Properties();
      FileInputStream inStream = new FileInputStream(propertiesFile);
      props.load(inStream);
      inStream.close();

      Writer writer = new BufferedWriter(new FileWriter(cmd.getOptionValue("o")));

      System.out.println("Indexing using properties from " + args[0]);

      System.out.print("Creating filter manager...");
      System.out.flush();
      FilterManager filterManager = new FilterManager(props);
      System.out.println(" done.");
      System.out.println("Initiation took " + ((System.currentTimeMillis() - time) / 1000.0)
          + " sec");

      time = System.currentTimeMillis();

      for (int i = 3; i < args.length; i++) {
        int counter = 0;
        boolean first = true;
        filterManager.setInput(new File(args[i]), Strand.UNKNOWN);
        while (filterManager.hasNext()) {
          AnnotatedSequence sequence = filterManager.next();

          if (first) {
            writer.write("sequence ID: ");
            writer.write(sequence.getSequenceId());
            writer.write("\n");
            if (cmd.hasOption("v")) {
              writer.write(fileToString(args[i]));
              writer.write("\n");
            }
            first = false;
          }
          writeStem(writer, sequence, counter);
          counter++;
        }
      }

      writer.close();

      System.out.println();
      System.out
          .println("Indexing took " + ((System.currentTimeMillis() - time) / 1000.0) + " sec");
      // System.out.println("Number of stems written: " + counter);
      System.out.println(filterManager.printMessages());

    } catch (IOException e) {
      e.printStackTrace();
      return;
    }
  }

  public static String fileToString(String fileName) throws IOException {
    StringBuffer fileData = new StringBuffer(1000);
    BufferedReader reader = new BufferedReader(new FileReader(fileName));
    char[] buf = new char[1024];
    int numRead = 0;
    while ((numRead = reader.read(buf)) != -1) {
      String readData = String.valueOf(buf, 0, numRead);
      fileData.append(readData);
      buf = new char[1024];
    }
    reader.close();
    return fileData.toString();
  }

  public static void writeStem(Writer writer, AnnotatedSequence sequence, int index)
      throws IOException {
    StringBuilder builder = new StringBuilder();
    int start = sequence.getStructureStartPosition();

    if (index >= 0) {
      builder.append(index);
      builder.append('\t');
    }

    // add aligned sequence, positions and strand
    for (int j = 0; j < start; j++) {
      builder.append(' ');
    }
    builder.append(sequence.getSequence().charsAt(sequence.getStructureStart(),
        sequence.getStructureEnd() - sequence.getStructureStart()));
    builder.append(' ');
    builder.append(start);
    builder.append('-');
    builder.append(sequence.getStructureEndPosition());
    builder.append(" , STRAND: ");
    builder.append(sequence.getStrand().toString());
    builder.append('\n');

    if (index >= 0) {
      builder.append(index);
      builder.append('\t');
    }

    // add aligned structure and energy
    for (int j = 0; j < start; j++) {
      builder.append(' ');
    }
    builder.append(sequence.getStructure());
    builder.append(' ');
    builder.append(sequence.getEnergy());
    builder.append('\n');

    writer.write(builder.toString());
  }
}

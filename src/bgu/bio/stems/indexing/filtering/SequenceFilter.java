package bgu.bio.stems.indexing.filtering;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.SequenceConsumer;
import bgu.bio.stems.indexing.SequenceProducer;


public interface SequenceFilter extends SequenceConsumer, SequenceProducer {

  void setSequenceOracle(SequenceOracle sequenceOracle);

  public boolean hasRemaining();

  public AnnotatedSequence nextRemaining();

  public boolean hasRemainingSequenceDone();

  public String printMessage();
}

package bgu.bio.stems.indexing.filtering;

import bgu.bio.adt.IntPairQueue;
import bgu.bio.adt.queue.Pool;
import bgu.bio.adt.tuples.IntPair;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.AnnotatedSequencePool;
import bgu.bio.util.alphabet.AlphabetUtils;
import bgu.bio.util.alphabet.RnaAlphabet;

public class MinimalComplementarityFilter implements SequenceFilter {

  private int minimalAmount;
  private int stemLength;

  private AlphabetUtils alphabet;
  private char[] sequence1;
  private char[] sequence2;
  private int lastPos1;
  private int lastPos2;
  private int currentPos1;
  private int currentPos2;
  private IntPairQueue intPairQueue;
  private Pool<IntPair> intPairPool;
  private AnnotatedSequence workingCopy;

  public static final boolean DEBUG = false;

  public MinimalComplementarityFilter(int stemLength, int minimalAmount) {

    this.minimalAmount = minimalAmount;
    this.stemLength = stemLength;

    sequence1 = new char[stemLength];
    sequence2 = new char[stemLength];

    intPairQueue = new IntPairQueue();
    intPairPool = new Pool<IntPair>();

    lastPos1 = -1;
    lastPos2 = -1;

    alphabet = RnaAlphabet.getInstance();
  }

  public boolean checkPair() {
    int counter = 0;
    for (int i = 0; i < stemLength; i++) {
      if (alphabet.canPair(sequence1[i], sequence2[i])) {
        counter++;
      }
    }
    return counter >= this.minimalAmount;
  }

  @Override
  public void push(AnnotatedSequence sequence) {
    this.workingCopy = sequence;
    // copy sequences to arrays only if needed
    currentPos1 = sequence.getStructureStart();
    currentPos2 = sequence.getStructureEnd();

    if (lastPos1 != currentPos1) {
      for (int i = 0; i < stemLength; i++) {
        sequence1[i] = sequence.getSequence().charAt(sequence.getStructureStart() + i);
      }
    }

    if (lastPos2 != currentPos2) {
      for (int i = 0; i < stemLength; i++) {
        sequence2[i] =
            sequence.getSequence().charAt(sequence.getStructureEnd() - stemLength + 1 + i);
      }
    }

    boolean ans = this.checkPair();
    if (ans) {
      IntPair p = this.intPairQueue.size() == 0 ? new IntPair() : this.intPairPool.dequeue();
      p.setFirst(currentPos1);
      p.setSecond(currentPos2);
      this.intPairQueue.push(p);
    }

    lastPos1 = currentPos1;
    lastPos2 = currentPos2;
  }

  @Override
  public AnnotatedSequence next() {
    IntPair pair = this.intPairQueue.poll();

    this.workingCopy.setStructureStart(pair.getFirst());
    this.workingCopy.setStructureEnd(pair.getSecond());

    this.intPairPool.enqueue(pair);

    return this.workingCopy;
  }

  @Override
  public boolean hasNext() {
    return this.intPairQueue.size() != 0;
  }

  @Override
  public void clear() {
    this.lastPos1 = -1;
    this.lastPos2 = -1;
    this.workingCopy = null;
    while (this.intPairQueue.size() != 0) {
      this.intPairPool.enqueue(this.intPairQueue.poll());
    }
  }

  @Override
  public void setAnnotatedSequencePool(AnnotatedSequencePool pool) {}

  @Override
  public void setSequenceOracle(SequenceOracle sequenceOracle) {}

  @Override
  public boolean hasRemaining() {
    return false;
  }

  @Override
  public AnnotatedSequence nextRemaining() {
    return null;
  }

  @Override
  public boolean hasRemainingSequenceDone() {
    return false;
  }

  @Override
  public String printMessage() {
    return "n/a";
  }
}

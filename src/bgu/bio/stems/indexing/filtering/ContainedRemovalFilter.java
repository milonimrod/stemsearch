package bgu.bio.stems.indexing.filtering;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.AnnotatedSequencePool;
import bgu.bio.util.CharBuffer;

/**
 * Removes from an ordered stream of {@link AnnotatedSequence}s with structure the ones contained in
 * one of their predecessors.
 */
public class ContainedRemovalFilter implements SequenceFilter {

  private AnnotatedSequence lastLegal;
  private int start1;
  private int end1;
  private int start2;
  private int end2;
  private boolean lastLegalRead;

  // reporting fields
  private int contained;

  public ContainedRemovalFilter() {
    lastLegal = null;
    lastLegalRead = true;
    contained = 0;
  }

  @Override
  public void setAnnotatedSequencePool(AnnotatedSequencePool pool) {
    // irrelevant
  }

  @Override
  public void setSequenceOracle(SequenceOracle sequenceOracle) {
    // irrelevant
  }

  @Override
  public void push(AnnotatedSequence sequence) {
    int start1New = sequence.getStructureStartPosition();
    CharBuffer structure = sequence.getStructure();
    int end1New = start1New + structure.lastIndexOf('(');
    int start2New = start1New + structure.indexOf(')');
    int end2New = sequence.getStructureEnd();
    if (lastLegal == null || start1New < start1 || end1 < end1New || start2New < start2
        || end2 < end2New) {
      lastLegal = sequence;
      start1 = start1New;
      end1 = end1New;
      start2 = start2New;
      end2 = end2New;
      lastLegalRead = false;
    } else {
      contained++;
    }
  }

  @Override
  public void clear() {
    lastLegal = null;
    lastLegalRead = true;
  }

  @Override
  public boolean hasNext() {
    return !lastLegalRead;
  }

  @Override
  public AnnotatedSequence next() {
    lastLegalRead = true;
    return lastLegal;
  }

  @Override
  public boolean hasRemaining() {
    return hasNext();
  }

  @Override
  public AnnotatedSequence nextRemaining() {
    return next();
  }

  public int getNumContained() {
    return this.contained;
  }

  @Override
  public boolean hasRemainingSequenceDone() {
    return false;
  }

  @Override
  public String printMessage() {
    return "n/a";
  }
}

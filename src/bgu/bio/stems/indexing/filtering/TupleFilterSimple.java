package bgu.bio.stems.indexing.filtering;

import bgu.bio.adt.IntPairQueue;
import bgu.bio.adt.WindowSequenceTable;
import bgu.bio.adt.queue.Pool;
import bgu.bio.adt.tuples.IntPair;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.AnnotatedSequencePool;
import bgu.bio.util.CharBuffer;
import bgu.bio.util.alphabet.RnaAlphabet;

/**
 * The tuple filter is a sequence filter in the pipeline. It uses the {@link WindowSequenceTable} to
 * keep the information on all the current sequences in the window. this version is one-to-many so
 * it is preferred that this filter will be the first in the pipeline.
 */
public class TupleFilterSimple implements SequenceFilter {

  /** The tuple size. the size of each "stem" in the window */
  protected final int tupleSize;

  /** The threshold that determine if two tuples pass the filter. */
  protected final int threshold;

  /**
   * The working copy. the filter using this sequence as the holder of the window
   */
  private AnnotatedSequence workingCopy;

  /** A pool of annotated sequences */
  private AnnotatedSequencePool aSPool;

  /** A queue of pairs saving the start / end position. */
  private IntPairQueue intPairQueue;

  /** The integer pair pool. using for saving new objects creation */
  private Pool<IntPair> intPairPool;

  private RnaAlphabet alphabet;

  private long encountered, passed;

  /**
   * The Constant MIN_LOOP_SIZE represent the size of the minimal loop length allowed.
   */
  private static final int MIN_LOOP_SIZE = 3;

  /**
   * Instantiates a new tuple filter.
   * 
   * @param tupleSize the tuple size
   * @param windowSize the window size
   * @param threshold the threshold
   */
  public TupleFilterSimple(int tupleSize, int threshold) {

    this.intPairPool = new Pool<IntPair>();
    this.intPairQueue = new IntPairQueue(100);
    this.aSPool = AnnotatedSequencePool.getInstance();

    this.tupleSize = tupleSize;
    this.threshold = threshold;

    this.alphabet = RnaAlphabet.getInstance();

    clear();
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.SequenceProducer#hasNext()
   */
  @Override
  public boolean hasNext() {
    if (this.intPairQueue.size() != 0)
      return true;

    return false;
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.SequenceProducer#next()
   */
  @Override
  public AnnotatedSequence next() {
    // return the next answer in the queue
    IntPair pair = this.intPairQueue.poll();

    this.workingCopy.setStructureStart(pair.getFirst());
    this.workingCopy.setStructureEnd(pair.getSecond());

    this.intPairPool.enqueue(pair);
    return this.workingCopy;
  }

  @Override
  public void setAnnotatedSequencePool(AnnotatedSequencePool pool) {
    aSPool = pool;
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.SequenceConsumer#push(indexing.AnnotatedSequence)
   */
  @Override
  public void push(AnnotatedSequence sequence) {
    if (sequence.getLength() < this.tupleSize * 2 + MIN_LOOP_SIZE) {
      return;
    }
    aSPool.releaseSequence(workingCopy);
    workingCopy = sequence;

    // check all the positions between the start and the end of the
    // structure
    int from = sequence.getStructureStart();
    int to = sequence.getStructureEnd();
    final CharBuffer sequenceData = sequence.getSequence();
    for (int start = from; start <= to; start++) {
      for (int end = start + 2 * tupleSize + MIN_LOOP_SIZE; end <= to; end++) {
        this.encountered++;

        check(sequenceData, start, end);
      }
    }
  }

  /**
   * @param sequenceData
   * @param start
   * @param end
   */
  public void check(final CharBuffer sequenceData, int start, int end) {
    if (alphabet.canPair(sequenceData.charAt(start + tupleSize - 1),
        sequenceData.charAt(end - tupleSize + 1))) {
      IntPair ans = this.intPairPool.size() == 0 ? new IntPair() : this.intPairPool.dequeue();
      ans.setFirst(start);
      ans.setSecond(end);
      intPairQueue.push(ans);
      passed++;
    }
  }

  @Override
  public boolean hasRemaining() {
    return false;
  }

  @Override
  public AnnotatedSequence nextRemaining() {
    return null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.SequenceProducer#clear()
   */
  @Override
  public void clear() {
    while (this.intPairQueue.size() != 0) {
      this.intPairPool.enqueue(this.intPairQueue.poll());
    }
    encountered = 0;
    passed = 0;
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.filtering.SequenceFilter#setSequenceOracle(indexing.filtering .SequenceOracle)
   */
  @Override
  public void setSequenceOracle(SequenceOracle sequenceOracle) {}

  @Override
  public boolean hasRemainingSequenceDone() {
    return false;
  }

  @Override
  public String printMessage() {
    return "Passed " + passed + " out of " + encountered + " possible start positions";
  }
}

package bgu.bio.stems.indexing.filtering;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import java.util.zip.GZIPInputStream;

import bgu.bio.io.file.properties.PropertiesUtils;
import bgu.bio.stems.filereader.FastaFormatReader;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.AnnotatedSequencePool;
import bgu.bio.stems.indexing.PropertiesConstants;
import bgu.bio.stems.indexing.SequenceProducer;
import bgu.bio.stems.indexing.SequenceReader;
import bgu.bio.stems.indexing.Strand;

/**
 * 
 * The FilterManager is the basic pipeline of the Stem extraction methods. The basic constructor
 * depends only on the Properties parameter ( {@link #FilterManager(Properties)}). The
 * {@link Properties} variable must contain the following properties: <h2>Sequence Reader</h2><br/>
 * <dl>
 * <dt>sequence.reader=<i>class name</i></dt>
 * <dd>The class name of the sequence reader. All readers must implement the {@link SequenceReader}
 * interface. The default Reader is the {@link FastaFormatReader}.</dd>
 * <dt>sequence.reader.<i>class name</i>.package</dt>
 * <dd>The package of the selected sequence reader.</dd>
 * <dt>sequence.reader.<i>class name</i>.package</dt>
 * <dd>The package of the selected sequence reader.</dd>
 * </dl>
 * The sequence reader must appear according to the {@link PropertiesUtils} Convention.
 * 
 * @author milon
 */
public class FilterManager implements SequenceProducer, Iterator<AnnotatedSequence> {

  public static final FilterManager EMPTY_FILTER_MANAGER = new FilterManager();

  // Fields
  private SequenceOracle sequenceOracle;
  private ArrayList<SequenceFilter> pipeline;
  private int currentFilterIndex;
  private SequenceFilter currentFilter;
  private AnnotatedSequence nextSequence;
  private AnnotatedSequence firstInSequence;
  private Strand strand;

  private AnnotatedSequencePool pool;

  public FilterManager(Properties props) throws IOException {
    this(props, null);
  }

  public FilterManager(Properties props, AnnotatedSequencePool pool) throws IOException {
    this();
    this.pool = pool;

    // set sequence producer
    String sequenceProducerName = props.getProperty(PropertiesConstants.sequenceReaderString);
    if (sequenceProducerName == null) {
      throw new IOException("no sequence producer name in properties file ");
    }
    setSequenceReader((SequenceReader) PropertiesUtils.instantiateFromProps(sequenceProducerName,
        PropertiesConstants.sequenceReaderString, props, SequenceReader.class));

    // add filters
    addFilters(props);
  }

  /**
   * A constructor for creating empty filter manager
   */
  public FilterManager() {
    nextSequence = null;
    pipeline = new ArrayList<SequenceFilter>();
  }

  private void addFilters(Properties props) throws IOException {
    String temp = props.getProperty(PropertiesConstants.filtersString);
    if (temp == null) {
      throw new IOException("no filters list in properties file");
    }
    String[] filterNames = temp.split(",");
    for (String filterName : filterNames) {
      SequenceFilter filter =
          (SequenceFilter) PropertiesUtils.instantiateFromProps(filterName,
              PropertiesConstants.filtersString, props, SequenceFilter.class);
      // filter.setSequenceOracle(sequenceReader);
      putSequenceFilterInPipeline(filter);
    }
    for (SequenceFilter filter : pipeline) {
      filter.setSequenceOracle(sequenceOracle);
    }

  }

  @Override
  public void setAnnotatedSequencePool(AnnotatedSequencePool pool) {
    this.pool = pool;
  }

  public boolean setSequenceReader(SequenceReader producer) {
    sequenceOracle = new SequenceOracle(producer);
    new ArrayList<AnnotatedSequence>().iterator();
    if (this.pool != null) {
      producer.setAnnotatedSequencePool(pool);
    }
    return true;
  }

  public boolean putSequenceFilterInPipeline(SequenceFilter filter) {
    if (filter == null) {
      return false;
    }

    pipeline.add(filter);

    filter.setSequenceOracle(sequenceOracle);

    if (this.pool != null) { // override the default pool if one is given
      filter.setAnnotatedSequencePool(this.pool);
    }

    return true;
  }

  public boolean setInput(File file, Strand strand) throws IOException {
    return setInput(file, strand, false);
  }

  public boolean setInput(File file, Strand strand, boolean complement) throws IOException {
    Reader reader;
    String filename = file.getAbsolutePath();

    if (filename.toLowerCase().endsWith(".gz")) {
      reader = new InputStreamReader(new GZIPInputStream(new FileInputStream(new File(filename))));
    } else {
      reader = new FileReader(filename);
    }
    return setInput(new BufferedReader(reader), strand, complement);
  }

  public boolean setInput(String sequence, Strand strand, boolean complement) {
    // add > only if not given
    String seq = sequence.indexOf('>') != -1 ? sequence : ">\n" + sequence;

    StringReader reader = new StringReader(seq);
    return setInput(reader, strand, complement);
  }

  public boolean setInput(SequenceReader reader, Strand strand) {
    if (pipeline.size() == 0)
      return false;

    sequenceOracle = new SequenceOracle(reader);
    this.strand = strand;

    currentFilterIndex = 0;

    nextSequence = getNext();
    return true;
  }

  public boolean setInput(Reader reader, Strand strand) {
    return setInput(reader, strand, false);
  }

  public boolean setInput(Reader reader, Strand strand, boolean complement) {
    if (pipeline.size() == 0)
      return false;

    sequenceOracle.setReader(reader, complement);
    this.strand = strand;

    currentFilterIndex = 0;

    nextSequence = getNext();
    return true;
  }

  public boolean setInput(AnnotatedSequence sequence, Strand strand, boolean complement) {
    if (pipeline.size() == 0)
      return false;

    sequenceOracle.setReader(sequence, complement);
    this.strand = strand;

    currentFilterIndex = 0;

    nextSequence = getNext();
    return true;
  }

  private AnnotatedSequence getNext() {
    AnnotatedSequence answer = null;
    while (currentFilterIndex < pipeline.size()) {
      currentFilter = pipeline.get(currentFilterIndex);
      if (answer != null) {
        currentFilter.push(answer);
      }
      if (currentFilter.hasNext()) {
        answer = currentFilter.next();
        // advance in pipeline
        currentFilterIndex++;
      } else {
        if (currentFilterIndex > 0) {
          // go back in the pipeline
          currentFilterIndex--;
          answer = null;
        } else {
          // look for remaining sequences in all the filters
          answer = getRemaining(false);
          if (answer == null) {
            // check if has remaining when sequence done
            // need to get a sequence from the reader
            if (firstInSequence != null) {
              answer = firstInSequence;
              firstInSequence = null;
              currentFilterIndex = 0;
              // clear all filters in the pipeline from
              // leftovers
              for (SequenceFilter filter : pipeline) {
                filter.clear();
              }
            } else if (sequenceOracle.hasNext()) {
              firstInSequence = sequenceOracle.next();
              if (strand != null) {
                firstInSequence.setStrand(strand);
              }
              if (sequenceOracle.isFirstInSequence()) {
                answer = getRemaining(true);
                if (answer == null) {
                  answer = firstInSequence;
                  firstInSequence = null;
                  // clear all filters in the pipeline from
                  // leftovers
                  for (SequenceFilter filter : pipeline) {
                    filter.clear();
                  }
                }
              } else {
                answer = firstInSequence;
                firstInSequence = null;
              }
            } else {
              // there are no more sequences in the reader.
              answer = getRemaining(true);
              if (answer == null) {
                return null;
              }
            }
          }
        }
      }
    }
    // got a result from the last filter in pipeline
    currentFilterIndex--;
    return answer;
  }

  public AnnotatedSequence getRemaining(boolean sequenceDone) {
    // got back to first in pipeline
    currentFilterIndex = 0;
    while (currentFilterIndex < pipeline.size()) {
      currentFilter = pipeline.get(currentFilterIndex);
      currentFilterIndex++;
      boolean hasRemaining =
          sequenceDone ? currentFilter.hasRemainingSequenceDone() : currentFilter.hasRemaining();
      if (hasRemaining) {
        // found remaining, push down pipeline
        AnnotatedSequence answer = currentFilter.nextRemaining();
        if (strand != null) {
          answer.setStrand(strand);
        }
        return answer;
      }
    }

    // no remaining
    currentFilterIndex = 0;
    return null;
  }

  @Override
  public AnnotatedSequence next() {
    if (!hasNext()) {
      return null;
    }
    AnnotatedSequence toReturn = nextSequence.clone(false);
    nextSequence = getNext();
    return toReturn;
  }

  @Override
  public boolean hasNext() {
    return (nextSequence != null);
  }

  @Override
  public void clear() {
    throw new UnsupportedOperationException("No meaning of clear in this Class !!!");
  }

  public String getSequenceId() {
    if (nextSequence == null) {
      return null;
    }
    return nextSequence.getSequenceId();
  }

  @Override
  public void remove() {
    throw new UnsupportedOperationException("FilterManager is read only.");
  }

  public int getIndexInSequence() {
    return sequenceOracle.getIndexInSequence();
  }

  public void setStrand(Strand strand) {
    this.strand = strand;
  }

  public String printMessages() {
    StringBuilder sb = new StringBuilder();
    for (SequenceFilter filter : pipeline) {
      sb.append(filter.getClass().getCanonicalName() + ":\n");
      sb.append(filter.printMessage());
      sb.append("\n");
    }
    return sb.toString();
  }
}

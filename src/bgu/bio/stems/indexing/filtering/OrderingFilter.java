package bgu.bio.stems.indexing.filtering;

import java.util.PriorityQueue;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.AnnotatedSequencePool;

/**
 * Orders a stream of {@link AnnotatedSequence}s.
 */
public class OrderingFilter implements SequenceFilter {

  private PriorityQueue<AnnotatedSequence> queue;
  private int minDistance;
  private int maxStartPosition;

  // reporting fields
  private int queueMaxSize;

  public OrderingFilter(int windowSize) {
    queue = new PriorityQueue<AnnotatedSequence>(10);
    minDistance = 4 * windowSize;

    queueMaxSize = 0;
    maxStartPosition = 0;
  }

  @Override
  public void setSequenceOracle(SequenceOracle sequenceOracle) {
    // irrelevant
  }

  @Override
  public void setAnnotatedSequencePool(AnnotatedSequencePool pool) {
    // irrelevant
  }

  @Override
  public void push(AnnotatedSequence sequence) {
    queue.add(sequence);
    if (queueMaxSize < queue.size()) {
      queueMaxSize = queue.size();
    }
    if (maxStartPosition < sequence.getStructureStartPosition()) {
      maxStartPosition = sequence.getStructureStartPosition();
    }
  }

  @Override
  public void clear() {
    queue.clear();
    maxStartPosition = 0;
  }

  @Override
  public boolean hasNext() {
    if (!queue.isEmpty()
        && queue.peek().getStructureStartPosition() + minDistance < maxStartPosition) {
      return true;
    }
    return false;
  }

  @Override
  public AnnotatedSequence next() {
    if (queue.isEmpty()) {
      return null;
    }
    if (queue.peek().getStructureStartPosition() + minDistance < maxStartPosition) {
      return queue.poll();
    }
    return null;
  }

  public int getQueueMaxSize() {
    return queueMaxSize;
  }

  @Override
  public boolean hasRemaining() {
    return hasNext();
  }

  @Override
  public AnnotatedSequence nextRemaining() {
    return next();
  }

  @Override
  public boolean hasRemainingSequenceDone() {
    maxStartPosition = Integer.MAX_VALUE;
    return hasNext();
  }

  @Override
  public String printMessage() {
    return "n/a";
  }

}

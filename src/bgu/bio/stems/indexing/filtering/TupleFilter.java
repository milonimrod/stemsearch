package bgu.bio.stems.indexing.filtering;

import bgu.bio.adt.IntPairQueue;
import bgu.bio.adt.WindowSequenceTable;
import bgu.bio.adt.queue.Pool;
import bgu.bio.adt.tuples.IntPair;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.AnnotatedSequencePool;
import bgu.bio.util.alphabet.AlphabetUtils;
import bgu.bio.util.alphabet.RnaAlphabet;

/**
 * The tuple filter is a sequence filter in the pipeline. It uses the {@link WindowSequenceTable} to
 * keep the information on all the current sequences in the window. this version is one-to-many so
 * it is preferred that this filter will be the first in the pipeline.
 */
public class TupleFilter implements SequenceFilter {

  /** The tuple size. the size of each "stem" in the window */
  protected final int tupleSize;

  /** The threshold that determine if two tuples pass the filter. */
  protected final int threshold;

  /** The size of the window that the filter works in. */
  private final int windowSize;

  /** The alphabet to be used. */
  private AlphabetUtils alphabet;

  /** The buffer next available position. */
  private int bufferPosition;

  /** Holds current sequence buffer used in the Filter. */
  private char[] currentSequenceBuffer;

  /** The working copy. the filter using this sequence as the holder of the window */
  private AnnotatedSequence workingCopy;

  /** A pool of annotated sequences */
  private AnnotatedSequencePool aSPool;

  /** A sequence oracle to get the future sequence */
  private SequenceOracle sequenceOracle;

  /** The bucket table used by the filter. */
  protected WindowSequenceTable table;

  /** A queue of pairs saving the start / end position. */
  private IntPairQueue intPairQueue;

  /** The integer pair pool. using for saving new objects creation */
  protected Pool<IntPair> intPairPool;

  /**
   * The search results field is for saving runtime / heap usage by providing array for the search
   * method inside the BucketTable.
   */
  protected int[] searchResults;

  /** The number of windows in workingCopy */
  private static final int N_WINDOWS = 4;

  /** The Constant MIN_LOOP_SIZE represent the size of the minimal loop length allowed. */
  private static final int MIN_LOOP_SIZE = 1;

  /** Assume first push is terminal sequence. */
  private final boolean assumeFirstIsTerminal;

  /** is first push. */
  private boolean isFirstPush;

  /**
   * @see TupleFilter#TupleFilter(int, int, int, int, int, boolean)
   */
  public TupleFilter(int tupleSize, int seedSize, int windowSize, int threshold, int maxWobbles) {
    this(tupleSize, seedSize, windowSize, threshold, maxWobbles, false);
  }

  /**
   * Instantiates a new tuple filter.
   * 
   * @param tupleSize the tuple size
   * @param seedSize the seed size
   * @param windowSize the window size
   * @param threshold the threshold
   * @param maxWobbles the max wobbles
   * @param assumeFirstIsTerminal assume first push is terminal end sequence
   */
  public TupleFilter(int tupleSize, int seedSize, int windowSize, int threshold, int maxWobbles,
      boolean assumeFirstIsTerminal) {

    this.intPairPool = new Pool<IntPair>();
    this.intPairQueue = new IntPairQueue(windowSize);
    this.aSPool = AnnotatedSequencePool.getInstance();

    this.assumeFirstIsTerminal = assumeFirstIsTerminal;
    this.tupleSize = tupleSize;
    this.threshold = threshold;
    this.windowSize = windowSize;

    this.alphabet = RnaAlphabet.getInstance();
    this.currentSequenceBuffer = new char[windowSize + 2 * tupleSize];

    this.table =
        new WindowSequenceTable(seedSize, windowSize, tupleSize, tupleSize + MIN_LOOP_SIZE,
            maxWobbles);

    this.searchResults = new int[windowSize];
    clear();
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.SequenceProducer#hasNext()
   */
  @Override
  public boolean hasNext() {
    if (this.intPairQueue.size() != 0)
      return true;

    return false;
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.SequenceProducer#next()
   */
  @Override
  public AnnotatedSequence next() {
    // return the next answer in the queue
    IntPair pair = this.intPairQueue.poll();

    this.workingCopy.setStructureStart(pair.getFirst() - this.workingCopy.getLocation());
    this.workingCopy.setStructureEnd(pair.getSecond() - this.workingCopy.getLocation());

    this.intPairPool.enqueue(pair);
    return this.workingCopy;
  }

  @Override
  public void setAnnotatedSequencePool(AnnotatedSequencePool pool) {
    aSPool = pool;
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.SequenceConsumer#push(indexing.AnnotatedSequence)
   */
  @Override
  public void push(AnnotatedSequence sequence) {
    if (sequence.getLength() < this.tupleSize * 2 + MIN_LOOP_SIZE) {
      return;
    }
    char[] tuple = new char[this.tupleSize];
    boolean search = true;
    if (isFirstPush && assumeFirstIsTerminal) {
      search = false;
      // in case the terminal is not in a negative location (NNN...) then just add the positions
      // into the table
      this.workingCopy.setLocation(sequence.getLocation() - (N_WINDOWS - 1) * windowSize);
      this.isFirstPush = false;
      if (sequence.getLocation() < 0) {
        updateWorkingCopy(sequence);
        return;
      }

    }

    // @pre the buffer is filled with the remaining of the last window (the
    // |tuple|-1 last chars)
    // @pre the length of the sequence is equal to the window size

    int position = sequence.getLocation() - this.bufferPosition;

    // insert the sequence into the buffer
    System.arraycopy(sequence.getSequence().array(), 0, this.currentSequenceBuffer,
        this.bufferPosition, sequence.getLength());

    this.bufferPosition += sequence.getLength();
    final int endLoop = this.bufferPosition - this.tupleSize + 1; // run on all the tuple sized
                                                                  // sequences
    int row;

    for (int pos = 0; pos < endLoop; pos++) {
      System.arraycopy(this.currentSequenceBuffer, pos, tuple, 0, tupleSize);

      final int index = pos + position;

      // Inserting the tuple into the bucket table by relative position in the buffer
      row = table.insert(tuple, index);

      if (search) {
        int nFound = searchInTable(row);
        for (int i = 0; i < nFound; i++) {
          // Adding the pair into the answer pool.
          IntPair result = getResult(index, i);
          this.intPairQueue.push(result);
        }
      }
    }

    System.arraycopy(this.currentSequenceBuffer, this.bufferPosition - this.tupleSize + 1,
        this.currentSequenceBuffer, 0, this.tupleSize - 1);

    this.bufferPosition = this.tupleSize - 1;

    this.updateWorkingCopy(sequence);

    this.aSPool.releaseSequence(sequence);
  }

  @Override
  public boolean hasRemaining() {
    return false;
  }

  @Override
  public AnnotatedSequence nextRemaining() {
    return null;
  }

  /**
   * Search in table.
   * 
   * @param row the row
   * 
   * @return the number of results found
   */
  protected int searchInTable(int row) {
    return table.search(this.threshold, row, this.searchResults, null);
  }

  /**
   * Gets the result.
   * 
   * @param position the position
   * @param i the i
   * 
   * @return the result
   */
  protected IntPair getResult(int position, int i) {
    IntPair ans = this.intPairPool.size() == 0 ? new IntPair() : this.intPairPool.dequeue();
    ans.setFirst(this.searchResults[i]);
    ans.setSecond(position + this.tupleSize - 1);
    return ans;
  }

  /**
   * Update working copy.
   * 
   * @param sequence the sequence
   */
  private void updateWorkingCopy(AnnotatedSequence sequence) {
    char[] workingCopySequence = this.workingCopy.getSequence().array();

    // copying the last |windowsSize| chars to the beginning
    System.arraycopy(workingCopySequence, this.windowSize, workingCopySequence, 0, this.windowSize);

    System.arraycopy(workingCopySequence, this.windowSize * 2, workingCopySequence,
        this.windowSize, this.windowSize);

    // copying the sequence data to the middle.
    System.arraycopy(sequence.getSequence().array(), 0, workingCopySequence, this.windowSize * 2,
        sequence.getLength());

    // adding the future sequence if exists
    AnnotatedSequence future = this.sequenceOracle.downStreamSequence();
    if (future != null) {
      System.arraycopy(future.getSequence().array(), 0, workingCopySequence, this.windowSize * 2
          + sequence.getLength(), future.getLength());
    } else {
      // write N's in the end of the sequence
      for (int i = this.windowSize * 2 + sequence.getLength(); i < workingCopySequence.length; i++) {
        workingCopySequence[i] = this.alphabet.nonComplement();
      }
    }

    // moving the offset in the sequence.
    workingCopy.setLocation(this.workingCopy.getLocation() + this.windowSize);

    // updating other fields
    workingCopy.setStrand(sequence.getStrand());
    workingCopy.setSequenceId(sequence.getSequenceId());
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.SequenceProducer#clear()
   */
  @Override
  public void clear() {
    this.bufferPosition = 0;
    this.table.reset();

    // reset the working copy to be N nucleotides
    this.workingCopy =
        AnnotatedSequence.terminalAnnotatedSequence(windowSize * N_WINDOWS,
            alphabet.nonComplement());
    this.workingCopy.setLocation(1 - (N_WINDOWS - 1) * windowSize);
    this.isFirstPush = true;

    while (this.intPairQueue.size() != 0) {
      this.intPairPool.enqueue(this.intPairQueue.poll());
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.filtering.SequenceFilter#setSequenceOracle(indexing.filtering.SequenceOracle)
   */
  @Override
  public void setSequenceOracle(SequenceOracle sequenceOracle) {
    this.sequenceOracle = sequenceOracle;
  }

  @Override
  public boolean hasRemainingSequenceDone() {
    isFirstPush = true;
    return false;
  }

  @Override
  public String printMessage() {
    return "n/a";
  }
}

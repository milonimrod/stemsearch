package bgu.bio.stems.indexing.filtering;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

import org.apache.log4j.Logger;

import bgu.bio.adt.WindowSequenceTable;
import bgu.bio.adt.list.PooledLinkedList;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.AnnotatedSequencePool;
import bgu.bio.util.CharBuffer;
import bgu.bio.util.alphabet.RnaAlphabet;

/**
 * The tuple filter is a sequence filter in the pipeline. It uses the {@link WindowSequenceTable} to
 * keep the information on all the current sequences in the window. this version is one-to-many so
 * it is preferred that this filter will be the first in the pipeline.
 */
public class HashFileBasedFilter implements SequenceFilter {
  private static final int BUFFER_SIZE = 4096;

  /** The tuple size. the size of each "stem" in the window */
  protected final int tupleSize;

  /** A pool of annotated sequences */
  private AnnotatedSequencePool aSPool;

  private RnaAlphabet alphabet;

  private long encountered, passed;

  private PooledLinkedList<AnnotatedSequence> answer;

  private char[] seq;
  private static int[] array;
  private static Object setLoadingLock = new Object();
  private boolean passAll;
  private Logger logger;

  /**
   * The Constant MIN_LOOP_SIZE represent the size of the minimal loop length allowed.
   */
  private static final int MIN_LOOP_SIZE = 1;

  /**
   * Instantiates a new tuple filter.
   * 
   * @param tupleSize the tuple size
   */
  public HashFileBasedFilter(String fileName, int tupleSize) {
    this.logger = Logger.getLogger(HashFileBasedFilter.class);
    this.answer = new PooledLinkedList<AnnotatedSequence>();
    this.aSPool = new AnnotatedSequencePool();

    this.tupleSize = tupleSize;

    this.alphabet = RnaAlphabet.getInstance();

    seq = new char[tupleSize * 2];
    alphabet = RnaAlphabet.getInstance();
    clear();
    if (fileName.contains("$HOME")) {
      if (logger.isDebugEnabled()) {
        logger.debug("Got $HOME in file path, changing it to " + System.getProperty("user.home"));
      }
      fileName = fileName.replace("$HOME", System.getProperty("user.home"));
    }
    try {
      synchronized (setLoadingLock) {
        File f = new File(fileName);
        if (f.exists()) {
          load(fileName);
        } else {
          logger.warn("File " + fileName + " not found. passing all.");
          array = new int[0];
          passAll = true;
        }
      }

    } catch (IOException ex) {
      System.err.println("Could not load file " + fileName);
      ex.printStackTrace();
    }
  }

  private void load(String fileName) throws IOException {
    if (array == null) {
      loadRegular(fileName);
    }
  }

  private void loadRegular(String fileName) throws FileNotFoundException, IOException {
    final BufferedInputStream bis =
        new BufferedInputStream(new FileInputStream(fileName), BUFFER_SIZE);
    DataInputStream dis = new DataInputStream(bis);
    final int amount = (int) dis.readLong();
    array = new int[amount];
    // tmp array
    final byte[] ints = new byte[4 * 512];
    // read the first integer
    array[0] = dis.readInt();
    int read = dis.read(ints);
    int pos = 1;
    int i;
    while (read > 0) {
      for (i = 0; i < read; i = i + 4) {
        array[pos] = array[pos - 1] + byteArrayToInt(ints, i);
        pos++;
      }
      read = dis.read(ints);

    }
    dis.close();
    logger.info("Loaded " + amount + " of hashed tuples");
  }

  private int byteArrayToInt(byte[] b, int pos) {
    int value = 0;
    for (int i = 0; i < 4; i++) {
      int shift = (pos + 4 - 1 - i) * 8;
      value += (b[pos + i] & 0x000000FF) << shift;
    }
    return value;
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.SequenceProducer#hasNext()
   */
  @Override
  public boolean hasNext() {
    if (this.answer.size() != 0)
      return true;

    return false;
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.SequenceProducer#next()
   */
  @Override
  public AnnotatedSequence next() {
    // return the next answer in the queue

    return this.answer.removeFirst();
  }

  @Override
  public void setAnnotatedSequencePool(AnnotatedSequencePool pool) {

  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.SequenceConsumer#push(indexing.AnnotatedSequence)
   */
  @Override
  public void push(AnnotatedSequence sequence) {

    // check all the positions between the start and the end of the
    // structure

    int start = sequence.getStructureStart();
    int end = sequence.getStructureEnd();

    if (sequence.getLength() < this.tupleSize * 2 + MIN_LOOP_SIZE) {
      return;
    }

    check(sequence, start, end);
  }

  /**
   * @param sequenceData
   * @param start
   * @param end
   */
  public void check(AnnotatedSequence sequence, int start, int end) {
    encountered++;
    final CharBuffer sequenceData = sequence.getSequence();
    if (alphabet.canPair(sequenceData.charAt(start + tupleSize - 1),
        sequenceData.charAt(end - tupleSize + 1))) {

      // copy sequences to arrays
      for (int i = 0; i < tupleSize; i++) {
        seq[i + tupleSize] = sequenceData.charAt(end - tupleSize + i + 1);
        seq[i] = sequenceData.charAt(start + i);
      }
      // check if the tuple is in the file
      final int hash = hash(seq);

      if (passAll || Arrays.binarySearch(array, hash) >= 0) {
        AnnotatedSequence ans = aSPool.getSequence(sequence, true);
        ans.setStructureStart(sequence.getStructureStart());
        ans.setStructureEnd(sequence.getStructureEnd());
        answer.addLast(ans);

        passed++;
      }
    }
  }

  @Override
  public boolean hasRemaining() {
    return false;
  }

  @Override
  public AnnotatedSequence nextRemaining() {
    return null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.SequenceProducer#clear()
   */
  @Override
  public void clear() {
    while (this.answer.size() != 0) {
      aSPool.releaseSequence(answer.removeFirst());
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.filtering.SequenceFilter#setSequenceOracle(indexing.filtering .SequenceOracle)
   */
  @Override
  public void setSequenceOracle(SequenceOracle sequenceOracle) {}

  @Override
  public boolean hasRemainingSequenceDone() {
    return false;
  }

  @Override
  public String toString() {
    return HashFileBasedFilter.class.getName();
  }

  @Override
  public String printMessage() {
    return "Passed " + passed + " out of " + encountered + " possible start positions";
  }

  private int hash(char[] arr) {
    int num = 0;
    for (int i = 0; i < arr.length; i++) {
      num = num * 4 + alphabet.encode(arr[i]);
    }
    return num;
  }
}

package bgu.bio.stems.indexing.filtering;

import java.util.Arrays;

import bgu.bio.adt.GeneralAlphabetWithCompTable;
import bgu.bio.adt.IntPairQueue;
import bgu.bio.adt.ScoringMatrixByCompTable;
import bgu.bio.adt.queue.Pool;
import bgu.bio.adt.tuples.IntPair;
import bgu.bio.algorithms.alignment.constrained.re.FullConstrainedGlobalAlignmentEngine;
import bgu.bio.ds.automata.TransitionTable;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.AnnotatedSequencePool;
import bgu.bio.stems.util.RnaCompTable;
import bgu.bio.stems.util.RnaUtils;
import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.AlphabetUtils;

public class StemSeedAlignmentFilter implements SequenceFilter {

  private RnaCompTable compTable;

  private int[] arr1, arr2;
  private int minimalAmount;
  private final int seedSize;
  private int stemLength;
  private char[] s1;

  private char[] s2;


  private FullConstrainedGlobalAlignmentEngine cAlign;
  private final char emptyLetter;
  private AlphabetUtils dynamicAlphabet;
  private int numOfSeeds;
  private int[] countArr1;
  private int[] countArr2;
  private int[] accessedArr1;
  private char[] sequence1;
  private char[] sequence2;
  private int lastPos1;
  private int lastPos2;
  private int currentPos1;
  private int currentPos2;
  private IntPairQueue intPairQueue;
  private Pool<IntPair> intPairPool;
  private AnnotatedSequence workingCopy;


  public static final boolean DEBUG = false;

  public StemSeedAlignmentFilter(int stemLength, int minimalAmount, int seedSize, int maxWobbles) {
    this.seedSize = seedSize;
    this.minimalAmount = minimalAmount;
    this.stemLength = stemLength;

    this.numOfSeeds = (int) Math.pow(RnaUtils.alphabet.size(), seedSize);

    this.compTable = new RnaCompTable(seedSize, maxWobbles, 0, true);

    arr1 = new int[stemLength - seedSize + 1];
    arr2 = new int[stemLength - seedSize + 1];
    sequence1 = new char[stemLength];
    sequence2 = new char[stemLength];

    String regex = "(i|d|s)*(m+((i|d)*s){" + (seedSize - 1) + "})*(i|d|s)*m*";

    emptyLetter = (char) numOfSeeds;
    char[] letters = new char[numOfSeeds + 1]; // for the gap char
    for (int i = 0; i < letters.length; i++) {
      letters[i] = (char) i;
    }

    AlphabetUtils temp =
        new AlphabetUtils(letters, new int[0][0], new int[0][0], new char[0][0], false) {

          @Override
          public char map(String str) {

            return 0;
          }

          @Override
          public short emptyLetterHashed() {
            return this.encode(emptyLetter);
          }

          @Override
          public char emptyLetter() {
            return emptyLetter;
          }
        };
    this.dynamicAlphabet = temp;

    ScoringMatrix matrix =
        new ScoringMatrixByCompTable(compTable, dynamicAlphabet,
            dynamicAlphabet.encode(emptyLetter));

    GeneralAlphabetWithCompTable general;

    general = new GeneralAlphabetWithCompTable(dynamicAlphabet, compTable);


    TransitionTable tr = new TransitionTable(regex, general);
    cAlign = new FullConstrainedGlobalAlignmentEngine(matrix, tr, general);

    // setting the cutoff to be the amount needed.
    cAlign.setCutoff(minimalAmount);

    s1 = new char[stemLength - seedSize + 1];
    s2 = new char[stemLength - seedSize + 1];

    this.countArr1 = new int[numOfSeeds];
    this.countArr2 = new int[numOfSeeds];
    this.accessedArr1 = new int[stemLength - seedSize + 1];

    intPairQueue = new IntPairQueue();
    intPairPool = new Pool<IntPair>();

    lastPos1 = -1;
    lastPos2 = -1;
  }

  public boolean checkPair() {

    if (lastPos1 != currentPos1)
      RnaUtils.getHashedTuples(sequence1, seedSize, this.arr1);

    if (lastPos2 != currentPos2)
      RnaUtils.getHashedTuples(sequence2, seedSize, this.arr2);

    if (checkNumberOfPossibleMatches())
      return false;

    for (int i = 0; i < s1.length; i++) {
      s1[i] = dynamicAlphabet.decode(arr1[i]);
      s2[s1.length - i - 1] = dynamicAlphabet.decode(arr2[i]);
    }

    cAlign.align(s1, s2);

    return cAlign.getOptimalScore() >= this.minimalAmount;
  }

  /**
   * Check number of possible matches.
   * 
   * @return true, if didn't find the minimal amount
   */
  private boolean checkNumberOfPossibleMatches() {
    Arrays.fill(this.countArr1, 0);
    Arrays.fill(this.countArr2, 0);

    int top = 0;
    // put the hashed tuples in the counting array
    for (int i = 0; i < this.arr1.length; i++) {
      this.countArr1[this.arr1[i]]++;
      if (this.countArr1[this.arr1[i]] == 1)// first time accessed then add to the array;
      {
        this.accessedArr1[top] = this.arr1[i];
        top++;
      }
      this.countArr2[this.arr2[i]]++;
    }

    // move on all accessed array
    int total = 0;
    for (int i = 0; i < top; i++) {
      final int cell = this.accessedArr1[i];
      final int[] comps = this.compTable.getHashed(cell);
      for (int c = 0; c < comps.length; c++) {
        if (this.countArr2[comps[c]] > 0) {
          // only if access more then once. add to the counter and reduce the amount
          final int temp = Math.min(this.countArr1[cell], this.countArr2[comps[c]]);

          total += temp;
          if (total >= this.minimalAmount)
            return false;

          this.countArr1[cell] -= temp;
          this.countArr2[comps[c]] -= temp;
        }
        if (this.countArr1[cell] == 0) {
          // break
          break;
        }
      }
    }

    return true;
  }

  @Override
  public void push(AnnotatedSequence sequence) {
    this.workingCopy = sequence;
    // copy sequences to arrays only if needed
    currentPos1 = sequence.getStructureStart();
    currentPos2 = sequence.getStructureEnd();

    if (lastPos1 != currentPos1) {
      for (int i = 0; i < stemLength; i++) {
        sequence1[i] = sequence.getSequence().charAt(sequence.getStructureStart() + i);
      }
    }

    if (lastPos2 != currentPos2) {
      for (int i = 0; i < stemLength; i++) {
        sequence2[i] =
            sequence.getSequence().charAt(sequence.getStructureEnd() - stemLength + 1 + i);
      }
    }

    boolean ans = this.checkPair();
    if (ans) {
      IntPair p = this.intPairQueue.size() == 0 ? new IntPair() : this.intPairPool.dequeue();
      p.setFirst(currentPos1);
      p.setSecond(currentPos2);
      this.intPairQueue.push(p);
    }

    lastPos1 = currentPos1;
    lastPos2 = currentPos2;
  }

  @Override
  public AnnotatedSequence next() {
    IntPair pair = this.intPairQueue.poll();

    this.workingCopy.setStructureStart(pair.getFirst());
    this.workingCopy.setStructureEnd(pair.getSecond());

    this.intPairPool.enqueue(pair);

    return this.workingCopy;
  }

  @Override
  public boolean hasNext() {
    return this.intPairQueue.size() != 0;
  }

  @Override
  public void clear() {
    this.lastPos1 = -1;
    this.lastPos2 = -1;
    this.workingCopy = null;
    while (this.intPairQueue.size() != 0) {
      this.intPairPool.enqueue(this.intPairQueue.poll());
    }
  }

  @Override
  public void setAnnotatedSequencePool(AnnotatedSequencePool pool) {}

  @Override
  public void setSequenceOracle(SequenceOracle sequenceOracle) {}

  @Override
  public boolean hasRemaining() {
    return false;
  }

  @Override
  public AnnotatedSequence nextRemaining() {
    return null;
  }

  @Override
  public boolean hasRemainingSequenceDone() {
    return false;
  }

  @Override
  public String printMessage() {
    return "n/a";
  }
}

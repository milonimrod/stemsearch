package bgu.bio.stems.indexing.filtering;

import bgu.bio.adt.IntPairQueue;
import bgu.bio.adt.WindowSequenceTable;
import bgu.bio.adt.queue.Pool;
import bgu.bio.adt.tuples.IntPair;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.AnnotatedSequencePool;
import bgu.bio.util.alphabet.AlphabetUtils;
import bgu.bio.util.alphabet.RnaAlphabet;

/**
 * The tuple filter is a sequence filter in the pipeline. It uses the {@link WindowSequenceTable} to
 * keep the information on all the current sequences in the window. this version is one-to-many so
 * it is preferred that this filter will be the first in the pipeline.
 */
public class PassAllFilter implements SequenceFilter {

  /** The tuple size. the size of each "stem" in the window */
  protected final int tupleSize;

  /** The size of the window that the filter works in. */
  private final int windowSize;

  /** The alphabet to be used. */
  private AlphabetUtils alphabet;

  /** The buffer next available position. */
  private int bufferPosition;

  /** Holds current sequence buffer used in the Filter. */
  private char[] currentSequenceBuffer;

  /** The working copy. the filter using this sequence as the holder of the window */
  private AnnotatedSequence workingCopy;

  /** A pool of annotated sequences */
  private AnnotatedSequencePool aSPool;

  /** A sequence oracle to get the future sequence */
  private SequenceOracle sequenceOracle;

  /** A queue of pairs saving the start / end position. */
  private IntPairQueue intPairQueue;

  /** The integer pair pool. using for saving new objects creation */
  protected Pool<IntPair> intPairPool;

  /**
   * The search results field is for saving runtime / heap usage by providing array for the search
   * method inside the BucketTable.
   */
  protected int[] searchResults;

  /** The number of windows in workingCopy */
  private static final int N_WINDOWS = 4;

  /** The Constant MIN_LOOP_SIZE represent the size of the minimal loop length allowed. */
  private static final int MIN_LOOP_SIZE = 4;

  /** Assume first push is terminal sequence. */
  private final boolean assumeFirstIsTerminal;

  /** is first push. */
  private boolean isFirstPush;

  /**
   * @see PassAllFilter#TupleFilter(int, int, int, int, int, boolean)
   */
  public PassAllFilter(int tupleSize, int windowSize) {
    this(tupleSize, windowSize, false);
  }

  /**
   * Instantiates a new tuple filter.
   * 
   * @param tupleSize the tuple size
   * @param seedSize the seed size
   * @param windowSize the window size
   * @param threshold the threshold
   * @param maxWobbles the max wobbles
   * @param assumeFirstIsTerminal assume first push is terminal end sequence
   */
  public PassAllFilter(int tupleSize, int windowSize, boolean assumeFirstIsTerminal) {

    this.assumeFirstIsTerminal = assumeFirstIsTerminal;
    this.tupleSize = tupleSize;
    this.windowSize = windowSize;

    this.alphabet = RnaAlphabet.getInstance();
    this.currentSequenceBuffer = new char[windowSize + 2 * tupleSize];


    clear();

    this.aSPool = AnnotatedSequencePool.getInstance();

    this.intPairPool = new Pool<IntPair>();
    this.intPairQueue = new IntPairQueue(windowSize);
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.SequenceProducer#hasNext()
   */
  @Override
  public boolean hasNext() {
    if (this.intPairQueue.size() != 0)
      return true;

    return false;
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.SequenceProducer#next()
   */
  @Override
  public AnnotatedSequence next() {
    // return the next answer in the queue
    IntPair pair = this.intPairQueue.poll();

    this.workingCopy.setStructureStart(pair.getFirst() - this.workingCopy.getLocation() + 1);
    this.workingCopy.setStructureEnd(pair.getSecond() - this.workingCopy.getLocation() + 2);

    this.intPairPool.enqueue(pair);

    return this.workingCopy;
  }

  @Override
  public void setAnnotatedSequencePool(AnnotatedSequencePool pool) {
    aSPool = pool;
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.SequenceConsumer#push(indexing.AnnotatedSequence)
   */
  @Override
  public void push(AnnotatedSequence sequence) {
    boolean search = true;
    if (isFirstPush && assumeFirstIsTerminal) {
      search = false;
      // in case the terminal is not in a negative location (NNN...) then just add the positions
      // into the table
      this.workingCopy.setLocation(sequence.getLocation() - (N_WINDOWS - 1) * windowSize);
      this.isFirstPush = false;
      if (sequence.getLocation() < 0) {
        updateWorkingCopy(sequence);
        return;
      }

    }

    // @pre the buffer is filled with the remaining of the last window (the
    // |tuple|-1 last chars)
    // @pre the length of the sequence is equal to the window size

    int position = sequence.getLocation() - this.bufferPosition;

    // insert the sequence into the buffer
    System.arraycopy(sequence.getSequence().array(), 0, this.currentSequenceBuffer,
        this.bufferPosition, sequence.getLength());

    this.bufferPosition += sequence.getLength();
    final int endLoop = this.bufferPosition - this.tupleSize + 1; // run on all the tuple sized
                                                                  // sequences

    for (int pos = 0; pos < endLoop; pos++) {
      if (search) {
        for (int w = MIN_LOOP_SIZE + 2 * tupleSize - 1; w < this.windowSize; w++) {
          final int start = pos + position;
          IntPair result = getResult(start, start - w);
          if (result != null) {
            this.intPairQueue.push(result);
          }
        }

      }
    }

    System.arraycopy(this.currentSequenceBuffer, this.bufferPosition - this.tupleSize + 1,
        this.currentSequenceBuffer, 0, this.tupleSize - 1);

    this.bufferPosition = this.tupleSize - 1;

    this.updateWorkingCopy(sequence);

    this.aSPool.releaseSequence(sequence);
  }

  @Override
  public boolean hasRemaining() {
    return false;
  }

  @Override
  public AnnotatedSequence nextRemaining() {
    return null;
  }

  /**
   * Gets the result.
   * 
   * @param position the position
   * @param i the i
   * 
   * @return the result
   */
  protected IntPair getResult(int position, int i) {
    if (i < 0)
      return null;
    IntPair ans = this.intPairPool.size() == 0 ? new IntPair() : this.intPairPool.dequeue();
    ans.setFirst(i);
    ans.setSecond(position);
    return ans;
  }

  /**
   * Update working copy.
   * 
   * @param sequence the sequence
   */
  private void updateWorkingCopy(AnnotatedSequence sequence) {
    char[] workingCopySequence = this.workingCopy.getSequence().array();

    // copying the last |windowsSize| chars to the beginning
    System.arraycopy(workingCopySequence, this.windowSize, workingCopySequence, 0, this.windowSize);

    System.arraycopy(workingCopySequence, this.windowSize * 2, workingCopySequence,
        this.windowSize, this.windowSize);

    // copying the sequence data to the middle.
    System.arraycopy(sequence.getSequence().array(), 0, workingCopySequence, this.windowSize * 2,
        sequence.getLength());

    // adding the future sequence if exists
    AnnotatedSequence future = this.sequenceOracle.downStreamSequence();
    if (future != null) {
      System.arraycopy(future.getSequence().array(), 0, workingCopySequence, this.windowSize * 2
          + sequence.getLength(), future.getLength());
    } else {
      // write N's in the end of the sequence
      for (int i = this.windowSize * 2 + sequence.getLength(); i < workingCopySequence.length; i++) {
        workingCopySequence[i] = this.alphabet.nonComplement();
      }
    }

    // moving the offset in the sequence.
    workingCopy.setLocation(this.workingCopy.getLocation() + this.windowSize);

    // updating other fields
    workingCopy.setStrand(sequence.getStrand());
    workingCopy.setSequenceId(sequence.getSequenceId());
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.SequenceProducer#clear()
   */
  @Override
  public void clear() {
    this.bufferPosition = 0;

    // reset the working copy to be N nucleotides
    this.workingCopy =
        AnnotatedSequence.terminalAnnotatedSequence(windowSize * N_WINDOWS,
            alphabet.nonComplement());
    this.workingCopy.setLocation(1 - (N_WINDOWS - 1) * windowSize);
    this.isFirstPush = true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.filtering.SequenceFilter#setSequenceOracle(indexing.filtering.SequenceOracle)
   */
  @Override
  public void setSequenceOracle(SequenceOracle sequenceOracle) {
    this.sequenceOracle = sequenceOracle;
  }


  /**
   * Gets the window size.
   * 
   * @return the window size
   */
  public int getWindowSize() {
    return windowSize;
  }

  @Override
  public boolean hasRemainingSequenceDone() {
    return false;
  }

  @Override
  public String printMessage() {
    return "n/a";
  }
}

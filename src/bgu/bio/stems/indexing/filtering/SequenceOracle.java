package bgu.bio.stems.indexing.filtering;


import java.io.File;
import java.io.Reader;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.AnnotatedSequencePool;
import bgu.bio.stems.indexing.SequenceReader;

/**
 * This class is a sequence reader that allow storage of buffered data. this class works with any
 * {@link SequenceReader}. The oracle reads the data from the Sequence reader and keeps the data in
 * a buffer. It has several methods and interfaces implementations that allow it to be part of the
 * general pipeline. The Oracle saves three windows: last, current and future
 */
public class SequenceOracle implements SequenceReader {

  private int windowSize;
  /**
   * the buffer of characters the Oracle can see. the size is {@link #windowSize}
   */

  private AnnotatedSequence nextSequence;
  private boolean isNextFromNewSequence;

  private boolean isFirst;
  private String sequenceId;

  private SequenceReader sequenceReader;

  public SequenceOracle(SequenceReader reader) {
    sequenceReader = reader;
    windowSize = reader.getReadSize();
  }

  @Override
  public void setAnnotatedSequencePool(AnnotatedSequencePool pool) {
    // Irrelevant
  }

  @Override
  public void setReader(Reader reader, boolean complement) {
    nextSequence = null;
    isNextFromNewSequence = true;

    // Reading the next sequence from the reader
    sequenceReader.setReader(reader, complement);
    if (sequenceReader.hasNext()) {
      nextSequence = sequenceReader.next();
      sequenceId = sequenceReader.getSequenceId();
    }
  }

  @Override
  public void setReader(File file, boolean complement) {
    nextSequence = null;
    isNextFromNewSequence = true;

    // Reading the next sequence from the reader
    sequenceReader.setReader(file, complement);
    if (sequenceReader.hasNext()) {
      AnnotatedSequence read = sequenceReader.next();
      nextSequence = read;
      sequenceId = sequenceReader.getSequenceId();
    }
  }

  @Override
  public void setReader(AnnotatedSequence sequence, boolean complement) {
    nextSequence = null;
    isNextFromNewSequence = true;

    // Reading the next sequence from the reader
    sequenceReader.setReader(sequence, complement);

    nextSequence = sequenceReader.next();
    sequenceId = sequenceReader.getSequenceId();
  }



  /**
   * Return down stream sequence.
   */
  public AnnotatedSequence downStreamSequence() {
    if (isNextFromNewSequence) {
      return null;
    }

    return nextSequence;
  }

  @Override
  public void clear() {
    throw new UnsupportedOperationException("This method has no meening here !!!");
  }

  @Override
  public boolean hasNext() {
    return nextSequence != null;
  }

  /**
   * this method return the next window size sequence held in the buffer (the current sequence)
   * 
   * @see bgu.bio.stems.indexing.SequenceProducer#next()
   */
  @Override
  public AnnotatedSequence next() {
    AnnotatedSequence ans = nextSequence;
    isFirst = isNextFromNewSequence;
    if (isFirst) {
      sequenceId = ans.getSequenceId();
    }
    nextSequence = null;
    /*
     * now reading the next read (the future), there are two options: 1. the read is from the same
     * sequence, hence it is the future read 2. the read is from a new sequence
     */
    if (sequenceReader.hasNext()) {
      nextSequence = sequenceReader.next();
      isNextFromNewSequence =
          sequenceReader.isFirstInSequence() && !nextSequence.getSequenceId().equals(sequenceId);
    }

    return ans;
  }

  @Override
  public int getReadSize() {
    return windowSize;
  }

  @Override
  public boolean isFirstInSequence() {
    return isFirst;
  }

  @Override
  public String getSequenceId() {
    return sequenceId;
  }

  @Override
  public int getIndexInSequence() {
    return sequenceReader.getIndexInSequence();
  }

}

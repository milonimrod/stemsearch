package bgu.bio.stems.indexing.filtering;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.List;
import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import bgu.bio.ds.rna.RNA;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.Strand;

public class StemFileWriter {

  public static void main(String[] args) throws ParseException {
    Options options = new Options();
    CommandLineParser parser = new GnuParser();
    options.addOption("q", "quiet", false, "quiet mode");
    options.addOption("v", "version", false, "print version");
    options.addOption("h", "help", false, "print this help");
    CommandLine line = parser.parse(options, args, true);
    String[] remain = line.getArgs();
    if (line.hasOption("h")) {
      printHelp(options);
      System.exit(0);
    }

    if (line.hasOption("v")) {
      String version = getVersion();
      System.out.println(version);
      System.exit(0);
    }
    if (remain.length != 3) {
      printHelp(options);
      System.exit(1);
    }

    File propertiesFile = new File(remain[0]);
    FileWriter fileWriter = null;
    try {
      long time = System.currentTimeMillis();
      Properties props = new Properties();
      FileInputStream inStream = new FileInputStream(propertiesFile);
      props.load(inStream);
      inStream.close();

      System.out.println("Indexing using properties from " + remain[0]);

      fileWriter = new FileWriter(remain[1]);
      time = System.currentTimeMillis();
      int counter = 0;
      StringBuilder builder = new StringBuilder();
      List<RNA> listOfRNA = RNA.loadFromFile(remain[2], false);
      System.out.println(listOfRNA.size());
      FilterManager filterManager = new FilterManager(props);
      for (int i = 0; i < listOfRNA.size(); i++) {

        filterManager.setInput(new StringReader(">" + listOfRNA.get(i).getHeader() + "\n"
            + listOfRNA.get(i).getPrimary()), Strand.UNKNOWN);
        while (filterManager.hasNext()) {
          AnnotatedSequence sequence = filterManager.next();

          builder.setLength(0); // clear the buffer
          builder.append(">ID: ");
          builder.append(sequence.getSequenceId());
          builder.append(" , START: ");
          builder.append(sequence.getStructureStartPosition());
          builder.append(" , STRAND: ");
          builder.append(sequence.getStrand().toString());
          builder.append(" , ENERGY: ");
          builder.append(sequence.getEnergy());
          builder.append('\n');
          builder.append(sequence.getSequence().charsAt(sequence.getStructureStart(),
              sequence.getStructureEnd() - sequence.getStructureStart() + 1));
          builder.append('\n');
          builder.append(sequence.getStructure());
          builder.append('\n');

          fileWriter.write(builder.toString());
          counter++;
        }
      }

      System.out.println();
      System.out
          .println("Indexing took " + ((System.currentTimeMillis() - time) / 1000.0) + " sec");
      System.out.println("Number of stems written: " + counter);
      // System.out.println(filterManager.printMessages());

    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (fileWriter != null) {
        try {
          fileWriter.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  private static void printHelp(Options options) {
    HelpFormatter formatter = new HelpFormatter();
    formatter.setWidth(200);
    String version = getVersion();
    formatter.printHelp(StemFileWriter.class.getCanonicalName()
        + " [Optionts] config_file output_file input_file", "", options, "Version " + version);
  }

  private static String getVersion() {
    String version = "Not available";
    try {
      InputStream in = StemFileWriter.class.getResourceAsStream("/version.txt");
      BufferedReader buf = new BufferedReader(new InputStreamReader(in));
      version = buf.readLine();
      buf.close();
    } catch (IOException ex) {
    }
    return version;
  }
}

package bgu.bio.stems.indexing.filtering;

import gnu.trove.set.hash.TIntHashSet;

import java.util.PriorityQueue;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.AnnotatedSequencePool;

/**
 * Orders a stream of {@link AnnotatedSequence}s.
 */
public class DuplicateRemovalFilter implements SequenceFilter {

  private TIntHashSet set;
  private PriorityQueue<AnnotatedSequence> queue;
  private int minDistance;
  private int maxStartPosition;
  private AnnotatedSequencePool pool;
  private int duplicates;

  // reporting fields
  private int queueMaxSize;

  public DuplicateRemovalFilter(int windowSize) {
    queue = new PriorityQueue<AnnotatedSequence>(10);
    minDistance = 4 * windowSize;

    queueMaxSize = 0;
    maxStartPosition = 0;
    set = new TIntHashSet();
    pool = AnnotatedSequencePool.getInstance();
  }

  @Override
  public void setSequenceOracle(SequenceOracle sequenceOracle) {
    // irrelevant
  }

  @Override
  public void setAnnotatedSequencePool(AnnotatedSequencePool pool) {
    this.pool = pool;
  }

  @Override
  public void push(AnnotatedSequence sequence) {
    int sequenceHash = sequence.hashCode();
    if (!set.contains(sequenceHash)) {
      set.add(sequenceHash);
      queue.add(sequence);
      if (queueMaxSize < queue.size()) {
        queueMaxSize = queue.size();
      }
      if (maxStartPosition < sequence.getStructureStartPosition()) {
        maxStartPosition = sequence.getStructureStartPosition();
      }
    } else {
      pool.releaseSequence(sequence);
      duplicates++;
    }
  }

  @Override
  public void clear() {
    queue.clear();
    maxStartPosition = 0;
  }

  @Override
  public boolean hasNext() {
    if (!queue.isEmpty()
        && queue.peek().getStructureStartPosition() + minDistance < maxStartPosition) {
      return true;
    }
    return false;
  }

  @Override
  public AnnotatedSequence next() {
    if (queue.isEmpty()) {
      return null;
    }
    if (queue.peek().getStructureStartPosition() + minDistance < maxStartPosition) {
      AnnotatedSequence sequence = queue.poll();
      set.remove(sequence.hashCode());
      return sequence;
    }
    return null;
  }

  public int getQueueMaxSize() {
    return queueMaxSize;
  }

  @Override
  public boolean hasRemaining() {
    return hasNext();
  }

  @Override
  public AnnotatedSequence nextRemaining() {
    return next();
  }

  @Override
  public boolean hasRemainingSequenceDone() {
    maxStartPosition = Integer.MAX_VALUE;
    return hasNext();
  }

  public int getNumDuplicates() {
    return duplicates;
  }

  @Override
  public String printMessage() {
    return "Number of duplicates found is " + getNumDuplicates();
  }

}

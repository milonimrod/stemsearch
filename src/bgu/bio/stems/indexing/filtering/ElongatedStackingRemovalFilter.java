package bgu.bio.stems.indexing.filtering;

import java.util.NoSuchElementException;
import java.util.PriorityQueue;
import java.util.Queue;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.AnnotatedSequencePool;
import bgu.bio.util.CharBuffer;

/**
 * Removes sequences from a stream of {@link AnnotatedSequence}s if they have another sequence with
 * a flanking stack
 */
public class ElongatedStackingRemovalFilter implements SequenceFilter {

  private Queue<AnnotatedSequence> readyList;
  private Queue<AnnotatedSequence> checkList;
  private Queue<AnnotatedSequence> tempCheckList;

  private int minDistance;
  private AnnotatedSequencePool pool;

  public ElongatedStackingRemovalFilter(int windowSize) {
    this.minDistance = 3 * windowSize;
    readyList = new PriorityQueue<AnnotatedSequence>();
    checkList = new PriorityQueue<AnnotatedSequence>();
    tempCheckList = new PriorityQueue<AnnotatedSequence>();
    this.pool = AnnotatedSequencePool.getInstance();
  }

  @Override
  public void setSequenceOracle(SequenceOracle sequenceOracle) {
    // irrelevant
  }

  @Override
  public void setAnnotatedSequencePool(AnnotatedSequencePool pool) {
    this.pool = pool;
  }

  @Override
  public void push(AnnotatedSequence candidate) {
    final int seqLoopStart = candidate.getStructure().lastIndexOf('(');
    final int seqLoopEnd = candidate.getStructure().indexOf(')', seqLoopStart + 1);
    candidate.setLoopStart(seqLoopStart);
    candidate.setLoopEnd(seqLoopEnd);

    boolean shouldBeAdded = true;
    while (shouldBeAdded && !this.checkList.isEmpty()) {
      AnnotatedSequence current = this.checkList.poll();
      if (current.getStructureStartPosition() + minDistance < candidate.getStructureStartPosition()) {
        this.readyList.add(current);
      } else if (candidate.getStructureStartPosition() == current.getStructureStartPosition()) {
        // same location in start but not in end then we don't need to
        // check
        if (candidate.getStructureEndPosition() != current.getStructureEndPosition()) {
          this.tempCheckList.add(current);
        } else {
          // maybe extend to inner loop
          // ...(((.....)))...
          // ...((((...))))...

          // check if one loop is contained by the other
          if (isContainedBy(current, candidate)) {
            // current is contained in sequence.
            boolean elongate = true;
            for (int i = candidate.getLoopStart(); i < current.getLoopStart() && elongate; i++) {
              elongate = current.getStructure().charAt(i) == '(';
            }
            for (int i = current.getLoopEnd() + 1; i <= candidate.getLoopEnd() && elongate; i++) {
              elongate = current.getStructure().charAt(i) == ')';
            }

            if (elongate) {
              shouldBeAdded = false;
              // System.out.println("removed " + candidate +
              // " becaue of " + current);
            }

            this.tempCheckList.add(current);

          } else if (isContainedBy(candidate, current)) {
            // sequence is contained by current
            boolean elongate = true;
            for (int i = current.getLoopStart(); i < candidate.getLoopStart() && elongate; i++) {
              elongate = candidate.getStructure().charAt(i) == '(';
            }
            for (int i = candidate.getLoopEnd() + 1; i <= current.getLoopEnd() && elongate; i++) {
              elongate = candidate.getStructure().charAt(i) == ')';
            }
            if (elongate) {
              this.pool.releaseSequence(current);
            } else {
              this.tempCheckList.add(current);
            }
          } else {
            // do nothing
            this.tempCheckList.add(current);
          }
        }
      } else {
        // check that the loop is the same (location and length)
        if (candidate.getLoopStartPosition() != current.getLoopStartPosition()
            || candidate.getLoopEndPosition() != current.getLoopEndPosition()) {
          // put back in list
          this.tempCheckList.add(current);
        } else {
          // same loop, then check extension
          // check ..(((((...)))))..
          // (((((((...))))))) extensions
          if (extendedBy(candidate, current)) {
            this.tempCheckList.add(current);
            shouldBeAdded = false;
            // System.out.println("removed " + candidate +
            // " becaue of " + current);
          } else if (extendedBy(current, candidate)) {
            this.pool.releaseSequence(current);
          } else {
            this.tempCheckList.add(current);
          }
        }
      }
    }

    if (shouldBeAdded) {
      tempCheckList.add(candidate);
    } else {
      // empty the rest of the checkList
      while (!this.checkList.isEmpty()) {
        AnnotatedSequence current = this.checkList.poll();
        if (current.getStructureStartPosition() + minDistance < candidate
            .getStructureStartPosition()) {
          this.readyList.add(current);
        } else {
          this.tempCheckList.add(current);
        }
      }
      // release the sequence
      pool.releaseSequence(candidate);
    }

    // switch the lists
    Queue<AnnotatedSequence> tempList = this.checkList;
    this.checkList = this.tempCheckList;
    this.tempCheckList = tempList;
  }

  /**
   * Checks if is contained by.
   * 
   * @param as1 the as1
   * @param as2 the as2
   * @return true, if is contained by
   */
  private boolean isContainedBy(AnnotatedSequence as1, AnnotatedSequence as2) {
    // check if loop is contained
    int f1 = as1.getLoopStartPosition();
    int f2 = as1.getLoopEndPosition();
    int s1 = as2.getLoopStartPosition();
    int s2 = as2.getLoopEndPosition();

    if (!(f1 >= s1 && f1 <= s2 && f2 >= s1 && f2 <= s2))
      return false;

    // check that the rest of the structure is the same except for the loop
    // area
    boolean equal = true;
    int limit = as2.getLoopStartPosition() - as1.getStructureStartPosition() + 1;
    // left side
    for (int i = 0; equal && i < limit; i++) {
      equal = as1.getStructure().charAt(i) == as2.getStructure().charAt(i);
    }

    limit = as1.getStructureEndPosition() - as2.getLoopEndPosition();

    // right side
    for (int i = 0; equal && i < limit; i++) {
      equal =
          as1.getStructure().charAt(as1.getStructure().length() - i - 1) == as2.getStructure()
              .charAt(as2.getStructure().length() - i - 1);
    }

    return equal;
  }

  private boolean extendedBy(AnnotatedSequence sequence, AnnotatedSequence current) {

    final CharBuffer seqStructure = sequence.getStructure();
    final CharBuffer curStructure = current.getStructure();

    // can't be extended by shorter structure
    if (seqStructure.length() >= curStructure.length())
      return false;

    // check if the structure of sequence is contained in current
    final int leftDiff = sequence.getStructureStartPosition() - current.getStructureStartPosition();
    final int rightDiff = current.getStructureEndPosition() - sequence.getStructureEndPosition();

    // if the difference is not the same then no need to check
    if (leftDiff != rightDiff)
      return false;

    // left side
    for (int i = 0; i < sequence.getLoopStart(); i++) {
      if (sequence.getStructure().charAt(i) != current.getStructure().charAt(leftDiff + i))
        return false;
    }

    // right side
    for (int i = sequence.getLoopEnd(); i < seqStructure.length(); i++) {
      if (sequence.getStructure().charAt(i) != current.getStructure().charAt(leftDiff + i))
        return false;
    }

    // check edges
    for (int i = 0; i < leftDiff; i++) {
      if (current.getStructure().charAt(i) != '(')
        return false;
      if (current.getStructure().charAt(curStructure.length() - i - 1) != ')')
        return false;
    }

    return true;
  }

  @Override
  public void clear() {
    readyList.clear();
    checkList.clear();
    tempCheckList.clear();
  }

  @Override
  public boolean hasNext() {
    return !readyList.isEmpty();
  }

  @Override
  public AnnotatedSequence next() {
    final AnnotatedSequence ans = readyList.poll();
    return ans;
  }

  @Override
  public boolean hasRemaining() {
    return !readyList.isEmpty() || !checkList.isEmpty();
  }

  @Override
  public AnnotatedSequence nextRemaining() {
    AnnotatedSequence ans = null;
    if (!readyList.isEmpty())
      ans = readyList.poll();
    else if (!checkList.isEmpty())
      ans = checkList.poll();

    if (ans == null) {
      throw new NoSuchElementException();
    }

    return ans;

  }

  @Override
  public boolean hasRemainingSequenceDone() {
    return false;
  }

  @Override
  public String printMessage() {
    return "n/a";
  }
}

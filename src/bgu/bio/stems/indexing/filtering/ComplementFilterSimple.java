package bgu.bio.stems.indexing.filtering;

import bgu.bio.adt.WindowSequenceTable;
import bgu.bio.adt.list.PooledLinkedList;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.AnnotatedSequencePool;
import bgu.bio.util.CharBuffer;
import bgu.bio.util.alphabet.RnaAlphabet;

/**
 * The tuple filter is a sequence filter in the pipeline. It uses the {@link WindowSequenceTable} to
 * keep the information on all the current sequences in the window. this version is one-to-many so
 * it is preferred that this filter will be the first in the pipeline.
 */
public class ComplementFilterSimple implements SequenceFilter {

  /** The tuple size. the size of each "stem" in the window */
  protected final int tupleSize;

  /** The threshold that determine if two tuples pass the filter. */
  protected final int threshold;

  /** A pool of annotated sequences */
  private AnnotatedSequencePool aSPool;

  private RnaAlphabet alphabet;

  private long encountered, passed;

  private PooledLinkedList<AnnotatedSequence> answer;

  private int stackSize;

  /**
   * The Constant MIN_LOOP_SIZE represent the size of the minimal loop length allowed.
   */
  private static final int MIN_LOOP_SIZE = 3;

  /**
   * Instantiates a new tuple filter.
   * 
   * @param tupleSize the tuple size
   * @param windowSize the window size
   * @param threshold the threshold
   */
  public ComplementFilterSimple(int tupleSize, int threshold) {
    this(tupleSize, 2, 2);
  }

  public ComplementFilterSimple(int tupleSize, int stackSize, int threshold) {

    this.answer = new PooledLinkedList<AnnotatedSequence>();
    this.aSPool = new AnnotatedSequencePool();

    this.tupleSize = tupleSize;
    this.threshold = threshold;

    this.alphabet = RnaAlphabet.getInstance();

    this.stackSize = stackSize;

    clear();
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.SequenceProducer#hasNext()
   */
  @Override
  public boolean hasNext() {
    if (this.answer.size() != 0)
      return true;

    return false;
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.SequenceProducer#next()
   */
  @Override
  public AnnotatedSequence next() {
    // return the next answer in the queue

    return this.answer.removeFirst();
  }

  @Override
  public void setAnnotatedSequencePool(AnnotatedSequencePool pool) {

  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.SequenceConsumer#push(indexing.AnnotatedSequence)
   */
  @Override
  public void push(AnnotatedSequence sequence) {

    // check all the positions between the start and the end of the
    // structure

    int start = sequence.getStructureStart();
    int end = sequence.getStructureEnd();

    if (sequence.getLength() < this.tupleSize * 2 + MIN_LOOP_SIZE) {
      return;
    }

    check(sequence, start, end);
  }

  /**
   * @param sequenceData
   * @param start
   * @param end
   */
  public void check(AnnotatedSequence sequence, int start, int end) {
    encountered++;
    final CharBuffer sequenceData = sequence.getSequence();
    if (alphabet.canPair(sequenceData.charAt(start + tupleSize - 1),
        sequenceData.charAt(end - tupleSize + 1))) {

      AnnotatedSequence ans = aSPool.getSequence(sequence, true);
      ans.setStructureStart(sequence.getStructureStart());
      ans.setStructureEnd(sequence.getStructureEnd());
      answer.addLast(ans);

      passed++;
    }
    /*
     * else { int amount = 0; int start1 = end - tupleSize + 1;
     * 
     * for (int i = 0; i <= tupleSize - stackSize; i++) { System.arraycopy(sequenceData.array(), i +
     * start1, k1, 0, stackSize); for (int j = tupleSize - stackSize; j >= 0; j--) {
     * System.arraycopy(sequenceData.array(), j + start, k2, 0, stackSize); if
     * (compTable.canPair(k1, k2)) { amount++; if (amount >= threshold) { AnnotatedSequence ans =
     * aSPool.getSequence( sequence, true); ans.setStructureStart(sequence.getStructureStart());
     * ans.setStructureEnd(sequence.getStructureEnd()); answer.addLast(ans); return; } } } } }
     */

  }

  @Override
  public boolean hasRemaining() {
    return false;
  }

  @Override
  public AnnotatedSequence nextRemaining() {
    return null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.SequenceProducer#clear()
   */
  @Override
  public void clear() {
    while (this.answer.size() != 0) {
      aSPool.releaseSequence(answer.removeFirst());
    }
    encountered = 0;
    passed = 0;
  }

  /*
   * (non-Javadoc)
   * 
   * @see indexing.filtering.SequenceFilter#setSequenceOracle(indexing.filtering .SequenceOracle)
   */
  @Override
  public void setSequenceOracle(SequenceOracle sequenceOracle) {}

  @Override
  public boolean hasRemainingSequenceDone() {
    return false;
  }

  @Override
  public String toString() {
    return ComplementFilterSimple.class.getName() + " " + stackSize + " " + stackSize + " " + 1;
  }

  @Override
  public String printMessage() {
    return "Passed " + passed + " out of " + encountered + " possible start positions";
  }
}

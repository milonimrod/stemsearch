package bgu.bio.stems.indexing.filtering;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.AnnotatedSequencePool;
import bgu.bio.util.CharBuffer;

/**
 * Removes from an stream of {@link AnnotatedSequence}s with structure the ones which differ from
 * one of their predecessors only by removal of stacked base pairs. The stream must be ordered by:
 * 1) structure start position, and 2) structure end position.
 */
public class ShorterStackFilter implements SequenceFilter {

  private List<AnnotatedSequence> prev;
  private List<AnnotatedSequence> curr;
  private Queue<AnnotatedSequence> next;

  // reporting fields
  private int contained;

  public ShorterStackFilter() {
    contained = 0;
    prev = new ArrayList<AnnotatedSequence>();
    curr = new ArrayList<AnnotatedSequence>();
    next = new LinkedList<AnnotatedSequence>();
  }

  @Override
  public void setSequenceOracle(SequenceOracle sequenceOracle) {
    // irrelevant
  }

  @Override
  public void setAnnotatedSequencePool(AnnotatedSequencePool pool) {
    // irrelevant
  }

  @Override
  public void push(AnnotatedSequence sequence) {
    if (curr.isEmpty()) {
      if (!contained(sequence)) {
        next.add(sequence);
      }
      curr.add(sequence);
    } else {
      int diff = sequence.getStructureStartPosition() - curr.get(0).getStructureStartPosition();
      if (diff == 0) {
        // no change of position
        if (!contained(sequence)) {
          next.add(sequence);
        }
        curr.add(sequence);
      } else if (diff == 1) {
        // position changed by 1, empty prev and move curr to prev
        prev.clear();
        List<AnnotatedSequence> temp = prev;
        prev = curr;
        curr = temp;
        if (!contained(sequence)) {
          next.add(sequence);
        }
        curr.add(sequence);
      } else {
        // position changed by more than 1, clear both prev and curr
        prev.clear();
        curr.clear();
        curr.add(sequence);
        next.add(sequence);
      }
    }
  }

  private boolean contained(AnnotatedSequence sequence) {
    if (checkList(sequence, prev)) {
      return true;
    }
    if (checkList(sequence, curr)) {
      return true;
    }
    return false;
  }

  private boolean checkList(AnnotatedSequence sequence, List<AnnotatedSequence> list) {
    int startNew = sequence.getStructureStartPosition();
    int endNew = sequence.getStructureEndPosition();
    for (AnnotatedSequence reference : list) {
      int start = reference.getStructureStartPosition();
      int end = reference.getStructureEndPosition();
      if ((startNew - start) == (end - endNew)
          && (shorterStack(sequence, reference, startNew, start, end))) {
        // check if base pairs were added inside
        contained++;
        return true;
      }
    }
    return false;
  }

  private boolean shorterStack(AnnotatedSequence sequence, AnnotatedSequence reference,
      int startNew, int start, int end) {
    CharBuffer structure = sequence.getStructure();
    CharBuffer structRef = reference.getStructure();
    int lenPrev = end - start;
    // check outside
    int i = 0;
    for (; i < startNew - start; i++) {
      if (structRef.charAt(i) != '(' || structRef.charAt(lenPrev - i - 1) != ')') {
        return false;
      }
    }
    // check left side
    int j = 0;
    for (; j <= structure.lastIndexOf('('); j++) {
      if (structRef.charAt(i + j) != structure.charAt(j)) {
        return false;
      }
    }
    for (; i + j <= structRef.lastIndexOf('('); j++) {
      if (structRef.charAt(i + j) != '(' || structure.charAt(j) != '.') {
        return false;
      }
    }
    int k = structure.length();
    for (; k > structure.indexOf(')'); k--) {
      if (structRef.charAt(i + k - 1) != structure.charAt(k - 1)) {
        return false;
      }
    }
    for (; i + k > structRef.indexOf(')'); k--) {
      if (structRef.charAt(i + k - 1) != ')' || structure.charAt(k - 1) != '.') {
        return false;
      }
    }
    return true;
  }

  @Override
  public void clear() {
    prev.clear();
    curr.clear();
    next.clear();
  }

  @Override
  public boolean hasNext() {
    return !next.isEmpty();
  }

  @Override
  public AnnotatedSequence next() {
    return next.poll();
  }

  @Override
  public boolean hasRemaining() {
    return hasNext();
  }

  @Override
  public AnnotatedSequence nextRemaining() {
    return next();
  }

  public int getNumContained() {
    return this.contained;
  }

  @Override
  public boolean hasRemainingSequenceDone() {
    return false;
  }

  @Override
  public String printMessage() {
    return "n/a";
  }
}

package bgu.bio.stems.indexing;

/**
 * The Interface SequenceProducer.
 */
public interface SequenceProducer {

  /**
   * Return the next {@link AnnotatedSequence} from the producer
   * 
   * @return the annotated sequence
   */
  public AnnotatedSequence next();

  /**
   * Checks if the producer has more {@link AnnotatedSequence}.
   * 
   * @return true, if there are more sequences to read
   */
  public boolean hasNext();

  /**
   * Clear the producer buffer.
   */
  public void clear();


  void setAnnotatedSequencePool(AnnotatedSequencePool pool);
}

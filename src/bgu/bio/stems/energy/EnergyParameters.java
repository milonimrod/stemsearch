package bgu.bio.stems.energy;

import java.io.IOException;
import java.io.InputStream;
import java.util.prefs.InvalidPreferencesFormatException;
import java.util.prefs.Preferences;

import bgu.bio.adt.Matrix;
import bgu.bio.adt.Matrix2d;
import bgu.bio.adt.Matrix3d;
import bgu.bio.adt.Matrix4d;
import bgu.bio.util.alphabet.RnaAlphabet;

/**
 * This class holds energy parameters required for hybridization calculations.
 * 
 * @author sivany
 */
public class EnergyParameters {
  private Preferences userPrefs;

  private Matrix2d il_asym;

  private Matrix4d stack_dg;
  private Matrix4d tstacki_dg;

  private double hl_ent[];
  private double bl_ent[];
  private double il_ent[];

  private Matrix3d dr_dangle_dg;
  private Matrix3d dl_dangle_dg;

  private Matrix int11;
  private Matrix int21;
  private Matrix int22;

  private static EnergyParameters instance = null;

  private int maxBulgeLength = 15;

  public static EnergyParameters getInstance() throws IOException {
    if (instance == null)
      instance = new EnergyParameters();
    return instance;
  }

  EnergyParameters() throws IOException {
    // Now try to read preferences from file
    InputStream is = getClass().getResourceAsStream("EnergyParameters.xml");
    try {
      Preferences.importPreferences(is);
    } catch (InvalidPreferencesFormatException e) {
      throw new IOException(e.getMessage());
    }
    is.close();

    userPrefs = Preferences.userRoot();

    init_il_asym();

    init_stack_dg();
    init_tstacki_dg();

    init_hl_ent();
    init_bl_ent();
    init_il_ent();

    init_dr_dangle_dg();
    init_dl_dangle_dg();

    init_int11();
    init_int21();
    init_int22();
  }

  private void init_il_asym() throws IOException {
    il_asym = new Matrix2d(maxBulgeLength + 1, maxBulgeLength + 1);

    for (int i = 1; i <= maxBulgeLength; i++)
      for (int j = 1; j <= maxBulgeLength; j++)
        il_asym.set((float) Math.min(3.0, Math.abs(i - j) * 0.3), i, j);
  }

  private void init_stack_dg() throws IOException {
    stack_dg =
        new Matrix4d(userPrefs.node("stack_dg"), RnaAlphabet.SIZE, RnaAlphabet.SIZE,
            RnaAlphabet.SIZE, RnaAlphabet.SIZE);
  }

  private void init_tstacki_dg() throws IOException {
    tstacki_dg =
        new Matrix4d(userPrefs.node("tstacki_dg"), RnaAlphabet.SIZE, RnaAlphabet.SIZE,
            RnaAlphabet.SIZE, RnaAlphabet.SIZE);
  }

  private void init_hl_ent() {

    hl_ent = new double[31];

    hl_ent[3] = 5.7000;
    hl_ent[4] = 5.6000;
    hl_ent[5] = 5.6000;
    hl_ent[6] = 5.4000;
    hl_ent[7] = 5.9000;
    hl_ent[8] = 5.6000;
    hl_ent[9] = 6.4000;
    hl_ent[10] = 6.5000;
    hl_ent[11] = 6.6000;
    hl_ent[12] = 6.7000;
    hl_ent[13] = 6.7800;
    hl_ent[14] = 6.8600;
    hl_ent[15] = 6.9400;
    hl_ent[16] = 7.0100;
    hl_ent[17] = 7.0700;
    hl_ent[18] = 7.1300;
    hl_ent[19] = 7.1900;
    hl_ent[20] = 7.2500;
    hl_ent[21] = 7.3000;
    hl_ent[22] = 7.3500;
    hl_ent[23] = 7.4000;
    hl_ent[24] = 7.4400;
    hl_ent[25] = 7.4900;
    hl_ent[26] = 7.5300;
    hl_ent[27] = 7.5700;
    hl_ent[28] = 7.6100;
    hl_ent[29] = 7.6500;
    hl_ent[30] = 7.6900;
  }

  private void init_bl_ent() {
    bl_ent = new double[31];

    bl_ent[0] = 0.0;
    bl_ent[1] = 3.8000;
    bl_ent[2] = 2.8000;
    bl_ent[3] = 3.2000;
    bl_ent[4] = 3.6000;
    bl_ent[5] = 4.0000;
    bl_ent[6] = 4.4000;
    bl_ent[7] = 4.5900;
    bl_ent[8] = 4.7000;
    bl_ent[9] = 4.8000;
    bl_ent[10] = 4.9000;
    bl_ent[11] = 5.0000;
    bl_ent[12] = 5.1000;
    bl_ent[13] = 5.1900;
    bl_ent[14] = 5.2700;
    bl_ent[15] = 5.3400;
    bl_ent[16] = 5.4100;
    bl_ent[17] = 5.4800;
    bl_ent[18] = 5.5400;
    bl_ent[19] = 5.6000;
    bl_ent[20] = 5.6500;
    bl_ent[21] = 5.7100;
    bl_ent[22] = 5.7600;
    bl_ent[23] = 5.8000;
    bl_ent[24] = 5.8500;
    bl_ent[25] = 5.8900;
    bl_ent[26] = 5.9400;
    bl_ent[27] = 5.9800;
    bl_ent[28] = 6.0200;
    bl_ent[29] = 6.0500;
    bl_ent[30] = 6.0900;
  }

  private void init_il_ent() {
    il_ent = new double[31];

    il_ent[0] = 0.0;
    il_ent[1] = 0.0;
    il_ent[2] = 4.1000;
    il_ent[3] = 5.1000;
    il_ent[4] = 1.7000;
    il_ent[5] = 1.8000;
    il_ent[6] = 2.0000;
    il_ent[7] = 2.2000;
    il_ent[8] = 2.3000;
    il_ent[9] = 2.4000;
    il_ent[10] = 2.5000;
    il_ent[11] = 2.6000;
    il_ent[12] = 2.7000;
    il_ent[13] = 2.7800;
    il_ent[14] = 2.8600;
    il_ent[15] = 2.9400;
    il_ent[16] = 3.0100;
    il_ent[17] = 3.0700;
    il_ent[18] = 3.1300;
    il_ent[19] = 3.1900;
    il_ent[20] = 3.2500;
    il_ent[21] = 3.3000;
    il_ent[22] = 3.3500;
    il_ent[23] = 3.4000;
    il_ent[24] = 3.4500;
    il_ent[25] = 3.4900;
    il_ent[26] = 3.5300;
    il_ent[27] = 3.5700;
    il_ent[28] = 3.6100;
    il_ent[29] = 3.6500;
    il_ent[30] = 3.6900;
  }

  private void init_dr_dangle_dg() throws IOException {
    dr_dangle_dg =
        new Matrix3d(userPrefs.node("dr_dangle_dg"), RnaAlphabet.SIZE, RnaAlphabet.SIZE,
            RnaAlphabet.SIZE + 1);
  }

  private void init_dl_dangle_dg() throws IOException {
    dl_dangle_dg =
        new Matrix3d(userPrefs.node("dl_dangle_dg"), RnaAlphabet.SIZE + 1, RnaAlphabet.SIZE,
            RnaAlphabet.SIZE);
  }

  private void init_int11() throws IOException {
    int11 =
        new Matrix(userPrefs.node("int11"), RnaAlphabet.SIZE, RnaAlphabet.SIZE, RnaAlphabet.SIZE,
            RnaAlphabet.SIZE, RnaAlphabet.SIZE, RnaAlphabet.SIZE);
  }

  private void init_int21() throws IOException {
    int21 =
        new Matrix(userPrefs.node("int21"), RnaAlphabet.SIZE, RnaAlphabet.SIZE, RnaAlphabet.SIZE,
            RnaAlphabet.SIZE, RnaAlphabet.SIZE, RnaAlphabet.SIZE, RnaAlphabet.SIZE);
  }

  private void init_int22() throws IOException {
    int22 =
        new Matrix(userPrefs.node("int22"), RnaAlphabet.SIZE, RnaAlphabet.SIZE, RnaAlphabet.SIZE,
            RnaAlphabet.SIZE, RnaAlphabet.SIZE, RnaAlphabet.SIZE, RnaAlphabet.SIZE,
            RnaAlphabet.SIZE);
  }

  public double bl_ent(int size) {
    return (size <= 30 ? bl_ent[size] : bl_ent[30] + log_interp(size));
  }

  public double il_ent(int size) {
    return ((size) <= 30 ? il_ent[(size)] : il_ent[30] + log_interp((size)));
  }

  private double log_interp(int size) {
    return (1.079 * Math.log(size / 30.0));
  }

  public double get_dl_dangle_dg(int[] array) {
    return dl_dangle_dg.get(array);
  }

  public double get_dr_dangle_dg(int[] array) {
    return dr_dangle_dg.get(array);
  }

  public double get_stack_dg(int[] array) {
    return stack_dg.get(array);
  }

  public double get_tstacki_dg(int[] array) {
    return tstacki_dg.get(array);
  }

  public double get_int11(int[] array) {
    return int11.get(array);
  }

  public double get_int21(int[] array) {
    return int21.get(array);
  }

  public double get_int22(int[] array) {
    return int22.get(array);
  }

  public double get_il_asym(int[] array) {
    return il_asym.get(array);
  }

  public void setMaxBulgeLength(int maxBulgeLength) throws IOException {
    this.maxBulgeLength = maxBulgeLength;
    init_il_asym();
  }

}

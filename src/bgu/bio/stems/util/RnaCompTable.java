package bgu.bio.stems.util;

import gnu.trove.list.array.TCharArrayList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import bgu.bio.util.alphabet.AlphabetUtils;
import bgu.bio.util.alphabet.RnaAlphabet;

/**
 * A class representing a complementarity table. it can be defined for a single base or a string of
 * bases. One can set the amount of wobbles bases in the answer.
 * 
 * @author milon
 * 
 */
public class RnaCompTable implements Serializable {
  /**
	 * 
	 */
  private static final long serialVersionUID = 7243164560498602305L;

  /**
   * Stores the information
   */
  private ArrayList<ArrayList<char[]>> table;
  /**
   * Stores the information in an hashed way
   */
  private int[][] hashedTable;

  private boolean[][] canPair;

  private int baseSize;
  private int maxWobbles;
  private AlphabetUtils alphabetUtil;

  private int maxWildCards;

  public RnaCompTable(int baseSize) {
    this(baseSize, baseSize, 0);
  }

  public RnaCompTable(int baseSize, boolean reverseComplements) {
    this(baseSize, baseSize, 0, reverseComplements);
  }

  public RnaCompTable(int baseSize, int maxWobbles, int maxWildCards) {
    this(baseSize, maxWobbles, maxWildCards, false);
  }

  public RnaCompTable(int baseSize, int maxWobbles, int maxWildCards, boolean reverseComplements) {
    alphabetUtil = RnaAlphabet.getInstance();
    this.baseSize = baseSize;
    this.maxWobbles = maxWobbles;
    this.maxWildCards = maxWildCards;
    int size = (int) Math.pow(alphabetUtil.size(), baseSize);

    this.table = new ArrayList<ArrayList<char[]>>(size);
    for (int i = 0; i < size; i++) {
      this.table.add(new ArrayList<char[]>());
    }
    this.hashedTable = new int[size][];
    this.canPair = new boolean[size][size];
    this.fill(reverseComplements);
  }

  private String[] getWords(int size) {
    if (size == 0) {
      return new String[] {""};
    }
    String[] recAns = getWords(size - 1);
    String[] ans = new String[recAns.length * alphabetUtil.size()];
    char[] letters = alphabetUtil.letters();
    for (int i = 0; i < recAns.length; i++) {
      for (int j = 0; j < letters.length; j++) {
        ans[i + recAns.length * j] = letters[j] + recAns[i];
      }
    }

    return ans;
  }

  /**
   * @param reverse
   */
  /**
   * @param reverse
   */
  private void fill(boolean reverse) {
    String[] words = getWords(this.baseSize);

    for (int i = 0; i < words.length; i++) {

      int hash = alphabetUtil.hash(words[i]);
      ArrayList<char[]> temp = table.get(hash);
      // only have complement if no empty or gap letter are used
      if (words[i].indexOf(alphabetUtil.emptyLetter()) == -1
          && words[i].indexOf(alphabetUtil.nonComplement()) == -1) {
        ArrayList<char[]> l1 = iterateWord(words[i].toCharArray());
        for (char[] cs : l1) {
          ArrayList<char[]> l2 = iterateWildcards(cs);
          for (char[] cs2 : l2) {
            if (reverse) {
              temp.add(rev(cs2));
            } else {
              temp.add(cs2);
            }
          }
        }
      }

      hashedTable[hash] = new int[temp.size()];
      for (int j = 0; j < temp.size(); j++) {
        final int hash2 = alphabetUtil.hash(temp.get(j));
        hashedTable[hash][j] = hash2;
        canPair[hash][hash2] = true;
      }

    }
  }

  private char[] rev(char[] array) {
    char[] rev = new char[array.length];
    for (int i = 0; i < array.length; i++) {
      rev[rev.length - 1 - i] = array[i];
    }
    return rev;
  }

  private ArrayList<char[]> iterateWord(char[] word) {
    TCharArrayList[] options = new TCharArrayList[word.length];
    int[] amounts = new int[word.length];
    for (int i = 0; i < word.length; i++) {
      options[i] = new TCharArrayList();
      char current = word[i];
      options[i].add(alphabetUtil.complement(current));
      if (alphabetUtil.hasWobble(current)) {
        options[i].add(alphabetUtil.wobble(current));
      }
      options[i].add('N');
      amounts[i] = options[i].size();
    }

    return iterateWords(word, amounts, options);
  }

  private ArrayList<char[]> iterateWildcards(char[] word) {
    TCharArrayList[] options = new TCharArrayList[word.length];
    int[] amounts = new int[word.length];
    for (int i = 0; i < word.length; i++) {
      options[i] = new TCharArrayList();
      char current = word[i];
      if (current == 'N') {
        options[i].add('A');
        options[i].add('C');
        options[i].add('G');
        options[i].add('U');
      } else {
        options[i].add(current);
      }
      amounts[i] = options[i].size();
    }

    return iterateWords(word, amounts, options);
  }

  private ArrayList<char[]> iterateWords(char[] word, int[] amounts, TCharArrayList[] options) {

    ArrayList<char[]> list = new ArrayList<char[]>();

    int[] positions = new int[word.length];
    Arrays.fill(positions, -1);
    int currentClass = 0;
    TCharArrayList clique = new TCharArrayList();
    while (currentClass >= 0) {
      positions[currentClass]++;
      if (positions[currentClass] > 0 && positions[currentClass] <= amounts[currentClass]) {
        clique.removeAt(clique.size() - 1);
      }

      if (positions[currentClass] == amounts[currentClass]) {
        positions[currentClass] = -1;
        currentClass--;
      } else {
        clique.add(options[currentClass].get(positions[currentClass]));
        if (countN(clique) <= maxWildCards && countWobble(clique, word) <= maxWobbles) {

          currentClass++;
          // finish
          if (currentClass == positions.length) {
            list.add(clique.toArray());
            currentClass--;
          }
        }
      }
    }
    return list;
  }

  private int countWobble(TCharArrayList clique, char[] word) {
    int count = 0;
    for (int i = 0; i < clique.size(); i++) {
      char c = clique.get(i);
      if (alphabetUtil.hasWobble(word[i]) && c == alphabetUtil.wobble(word[i])) {
        count++;
      }
    }
    return count;
  }

  private int countN(TCharArrayList clique) {
    int count = 0;
    for (int i = 0; i < clique.size(); i++) {
      if (clique.get(i) == 'N') {
        count++;
      }
    }
    return count;
  }

  public boolean canPair(char[] s1, char[] s2) {
    return canPair[alphabetUtil.hash(s1)][alphabetUtil.hash(s2)];
  }

  public int[] getHashed(String seed) {
    return this.getHashed(seed.toCharArray());
  }

  public int[] getHashed(char[] seed) {
    return this.getHashed(alphabetUtil.hash(seed));
  }

  public int[] getHashed(int hashedSeed) {
    return this.hashedTable[hashedSeed];
  }

  @Override
  public String toString() {
    return RnaCompTable.class.getName() + " " + baseSize + " " + maxWobbles + " " + maxWildCards;
  }

  public static void main(String[] args) {
    RnaCompTable s = new RnaCompTable(3, 1, 1, true);
    ArrayList<char[]> l1 = s.iterateWord("AUA".toCharArray());
    for (char[] cs : l1) {
      System.out.println(Arrays.toString(cs));
    }
    System.out.println("dddddddddddddddddddddddd");
    l1 = s.iterateWildcards("ANA".toCharArray());
    for (char[] cs : l1) {
      System.out.println(Arrays.toString(cs));
    }
    System.out.println(s.canPair("AUU".toCharArray(), "AAA".toCharArray()));
  }
}

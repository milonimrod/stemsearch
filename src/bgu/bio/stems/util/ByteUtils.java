package bgu.bio.stems.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ByteUtils {

  /**
   * The maximal number of bytes used when encoding VInt.
   */
  public static int MAX_VINT_BYTES = 5;

  /**
   * The maximal number of bytes used when encoding VLong.
   */
  public static int MAX_VLONG_BYTES = 9;

  /**
   * Writes an int in a variable-length format. Writes between one and five bytes. Smaller values
   * take fewer bytes. Negative numbers are not supported.
   * 
   * @param i The int to write
   * @return An array with the result
   * 
   * @see #readVInt(byte[])
   */
  public static byte[] writeVInt(int i) {
    int j = 0;
    while ((i & ~0x7F) != 0) {
      i >>>= 7;
      j++;
    }
    byte[] bytes = new byte[j++];
    j = 0;
    while ((i & ~0x7F) != 0) {
      bytes[j] = ((byte) ((i & 0x7f) | 0x80));
      i >>>= 7;
      j++;
    }
    bytes[j] = ((byte) i);
    return bytes;
  }

  /**
   * Writes an int in a variable-length format. Writes between one and five bytes. Smaller values
   * take fewer bytes. Negative numbers are not supported. <br>
   * NOTE: The buffer must be long enough to include the new bytes, this method does not check it.
   * 
   * @param i The int to write
   * @param buffer The buffer to write into
   * @param offset The offset to start writing in buffer
   * @return Number of bytes written
   * 
   * @see #readVInt(byte[], int)
   */
  public static int writeVInt(int i, byte[] buffer, int offset) {
    int j = 0;
    while ((i & ~0x7F) != 0) {
      buffer[offset + j] = ((byte) ((i & 0x7f) | 0x80));
      i >>>= 7;
      j++;
    }
    buffer[offset + j] = ((byte) i);
    return ++j;
  }

  /**
   * Writes an int in a variable-length format to an OutputStream. Writes between one and five
   * bytes. Smaller values take fewer bytes. Negative numbers are not supported.
   * 
   * @param i The int to write
   * @param out The OutputStream to write into
   * @return An array with the result
   * @throws IOException
   * 
   * @see #readVInt(InputStream)
   */
  public static int writeVInt(int i, OutputStream out) throws IOException {
    int j = 0;
    while ((i & ~0x7F) != 0) {
      out.write(((byte) ((i & 0x7f) | 0x80)));
      i >>>= 7;
      j++;
    }
    out.write((byte) i);
    return ++j;
  }

  /**
   * Writes a long in a variable-length format. Writes between one and nine bytes. Smaller values
   * take fewer bytes. Negative numbers are not supported.
   * 
   * @param i The int to write
   * @return An array with the result
   * 
   * @see #readVlong(byte[])
   */
  public static byte[] writeVLong(long i) {
    int j = 0;
    while ((i & ~0x7F) != 0) {
      i >>>= 7;
      j++;
    }
    byte[] bytes = new byte[j++];
    j = 0;
    while ((i & ~0x7F) != 0) {
      bytes[j] = ((byte) ((i & 0x7f) | 0x80));
      i >>>= 7;
      j++;
    }
    bytes[j] = ((byte) i);
    return bytes;
  }

  /**
   * Writes a long in a variable-length format to an OutputStream. Writes between one and nine
   * bytes. Smaller values take fewer bytes. Negative numbers are not supported.
   * 
   * @param i The int to write
   * @param out The OutputStream to write into
   * @return An array with the result
   * @throws IOException
   * 
   * @see #readVInt(byte[])
   */
  public static int writeVLong(long i, OutputStream out) throws IOException {
    int j = 0;
    while ((i & ~0x7F) != 0) {
      out.write(((byte) ((i & 0x7f) | 0x80)));
      i >>>= 7;
      j++;
    }
    out.write((byte) i);
    return ++j;
  }

  public static void writeLong(long data, OutputStream out) throws IOException {
    out.write((byte) ((data >> 56) & 0xff));
    out.write((byte) ((data >> 48) & 0xff));
    out.write((byte) ((data >> 40) & 0xff));
    out.write((byte) ((data >> 32) & 0xff));
    out.write((byte) ((data >> 24) & 0xff));
    out.write((byte) ((data >> 16) & 0xff));
    out.write((byte) ((data >> 8) & 0xff));
    out.write((byte) ((data >> 0) & 0xff));
  }

  /**
   * Reads an int stored in variable-length format. Reads between one and five bytes. Smaller values
   * take fewer bytes. Negative numbers are not supported.
   * 
   * @param bytes The array to read from
   * @return The int read
   * 
   * @see #writeVInt(int)
   */
  public static int readVInt(byte[] bytes) {
    int j = 0;
    byte b = bytes[j++];
    int i = b & 0x7F;
    for (int shift = 7; (b & 0x80) != 0; shift += 7) {
      b = bytes[j++];
      i |= (b & 0x7F) << shift;
    }
    return i;
  }

  /**
   * Reads an int stored in variable-length format from an InputStream. Reads between one and five
   * bytes. Smaller values take fewer bytes. Negative numbers are not supported.
   * 
   * @param in The InputStream to read from.
   * @return The int read
   * @throws IOException
   * 
   * @see #writeVInt(int, OutputStream)
   */
  public static int readVInt(InputStream in) throws IOException {
    byte b = (byte) in.read();
    int i = b & 0x7F;
    for (int shift = 7; (b & 0x80) != 0; shift += 7) {
      b = (byte) in.read();
      i |= (b & 0x7F) << shift;
    }
    return i;
  }

  /**
   * Reads a long stored in variable-length format. Reads between one and nine bytes. Smaller values
   * take fewer bytes. Negative numbers are not supported.
   * 
   * @param bytes The array to read from
   * @return The long read
   * 
   * @see #writeVLong(long)
   */
  public static long readVlong(byte[] bytes) {
    int j = 0;
    byte b = bytes[j++];
    long i = b & 0x7F;
    for (int shift = 7; (b & 0x80) != 0; shift += 7) {
      b = bytes[j++];
      i |= (b & 0x7F) << shift;
    }
    return i;
  }

  /**
   * Reads a long stored in variable-length format from an InputStream. Reads between one and nine
   * bytes. Smaller values take fewer bytes. Negative numbers are not supported.
   * 
   * @param in The InputStream to read from
   * @return The long read
   * @throws IOException
   * 
   * @see #writeVLong(long)
   */
  public static long readVlong(InputStream in) throws IOException {
    byte b = (byte) in.read();
    long i = b & 0x7F;
    for (int shift = 7; (b & 0x80) != 0; shift += 7) {
      b = (byte) in.read();
      i |= (b & 0x7F) << shift;
    }
    return i;
  }

  public static long readLong(InputStream in) throws IOException {
    return (long) (0xff & (byte) in.read()) << 56 | (long) (0xff & (byte) in.read()) << 48
        | (long) (0xff & (byte) in.read()) << 40 | (long) (0xff & (byte) in.read()) << 32
        | (long) (0xff & (byte) in.read()) << 24 | (long) (0xff & (byte) in.read()) << 16
        | (long) (0xff & (byte) in.read()) << 8 | (long) (0xff & (byte) in.read()) << 0;
  }

  /**
   * Reads an int stored in variable-length format. Reads between one and five bytes. Smaller values
   * take fewer bytes. Negative numbers are not supported.
   * 
   * @param bytes The array to read from
   * @param offset The offset in bytes to start reading from
   * @return The int read
   * 
   * @see #writeVInt(int, byte[])
   */
  public static int readVInt(byte[] bytes, int offset) {
    byte b = bytes[offset++];
    int i = b & 0x7F;
    for (int shift = 7; (b & 0x80) != 0; shift += 7) {
      b = bytes[offset++];
      i |= (b & 0x7F) << shift;
    }
    return i;
  }

  /**
   * Reads an int stored in variable-length format. Reads between one and five bytes. Smaller values
   * take fewer bytes. Negative numbers are not supported.
   * 
   * @param bytes The array to read from
   * @param offset The offset in bytes to start reading from
   * @return The int read
   * 
   * @see #writeVInt(int, byte[])
   */
  public static int readVInt(byte[] bytes, VIntOffset offset) {
    byte b = bytes[offset.inc()];
    int i = b & 0x7F;
    for (int shift = 7; (b & 0x80) != 0; shift += 7) {
      b = bytes[offset.inc()];
      i |= (b & 0x7F) << shift;
    }
    return i;
  }

  /**
   * Skip an int stored in variable-length format, and return the number of bytes skipped.
   * 
   * @param bytes The array to read from
   * @param offset The offset in bytes to start reading from
   * @return The number of bytes skipped
   */
  public static int skipVInt(byte[] bytes, int offset) {
    byte b = bytes[offset++];
    int i = 1;
    for (; (b & 0x80) != 0; i++) {
      b = bytes[offset++];
    }
    return i;
  }

}

package bgu.bio.stems.util;

import java.util.Arrays;
import java.util.LinkedList;

import bgu.bio.util.CharBuffer;
import bgu.bio.util.alphabet.AlphabetUtils;
import bgu.bio.util.alphabet.RnaAlphabet;

/**
 * The Class RnaUtils.
 */
public class RnaUtils {

  /** The alphabet. */
  public static AlphabetUtils alphabet = RnaAlphabet.getInstance();

  /**
   * Reverse complement.
   * 
   * @param sequence the buffer
   * @param offset the offset
   * @param length the length
   */
  public void reverseComplement(char[] sequence, int offset, int length) {
    char temp;
    int i = 0;
    for (; i < length / 2; i++) {
      temp = alphabet.complement(sequence[i]);
      sequence[i] = alphabet.complement(sequence[offset + length - i - 1]);
      sequence[offset + length - i - 1] = temp;
    }
    if (i * 2 < length) {
      // middle
      sequence[i] = alphabet.complement(sequence[i]);
    }
  }

  /**
   * Complement.
   * 
   * @param sequence the sequence
   * @param offset the offset
   * @param length the length
   * @return the char[]
   */
  public static char[] complement(char[] sequence, int offset, int length) {
    char[] ans = new char[length];
    for (int i = 0; i < ans.length; i++) {
      ans[i] = alphabet.complement(sequence[offset + i]);
    }
    return ans;
  }

  /**
   * Reverse.
   * 
   * @param sequence the sequence
   */
  public static void reverse(char[] sequence) {
    reverse(sequence, sequence.length);
  }

  /**
   * Reverse a sequence.
   * 
   * @param sequence the sequence
   * @param length the length to reverse
   * @return the char[]
   */
  public static void reverse(char[] sequence, int length) {
    char temp;
    for (int i = 0; i < length / 2; i++) {
      temp = sequence[i];
      sequence[i] = sequence[length - i - 1];
      sequence[length - i - 1] = temp;
    }
  }

  /**
   * Reverse.
   * 
   * @param sequence the sequence
   * @param offset the offset
   * @param length the length
   * @param ans the ans
   */
  public static void reverse(char[] sequence, int offset, int length, char[] ans) {
    for (int i = 0; i < length; i++) {
      ans[i] = sequence[offset + length - 1 - i];
    }
  }

  /**
   * Complement with wobbles.
   * 
   * @param sequence the sequence
   * @param offset the offset
   * @param length the length
   * @return the char[][]
   */
  public static char[][] complementWithWobbles(char[] sequence, int offset, int length) {
    return complementWithWobbles(sequence, offset, length, length);
  }

  /**
   * Complement with wobbles.
   * 
   * @param sequence the sequence
   * @param offset the offset
   * @param length the length
   * @param maxWobbles the max wobbles
   * @return the char[][]
   */
  public static char[][] complementWithWobbles(char[] sequence, int offset, int length,
      int maxWobbles) {
    return complementWithWobbles(sequence, offset, length, maxWobbles, false);
  }

  /**
   * Complement with wobbles reversed.
   * 
   * @param sequence the sequence
   * @param offset the offset
   * @param length the length
   * @param maxWobbles the max wobbles
   * @return the char[][]
   */
  public static char[][] complementWithWobblesReversed(char[] sequence, int offset, int length,
      int maxWobbles) {
    return complementWithWobbles(sequence, offset, length, maxWobbles, true);
  }

  /**
   * Complement with wobbles.
   * 
   * @param sequence the sequence
   * @param offset the offset
   * @param length the length
   * @param maxWobbles the max wobbles
   * @param reverse the reverse
   * @return the char[][]
   */
  public static char[][] complementWithWobbles(char[] sequence, int offset, int length,
      int maxWobbles, boolean reverse) {
    int numOfwobbles = 0;
    boolean[] hasWobble = new boolean[length];

    for (int i = offset; i < length; i++) {
      if (alphabet.hasWobble(sequence[i])) {
        hasWobble[i] = true;
        numOfwobbles++;
      }
    }

    char[][] allComplement = new char[(int) Math.pow(2, numOfwobbles)][length];
    int pos = 0;

    allComplement[0][0] = alphabet.complement(sequence[offset]);
    pos = 1;
    if (hasWobble[0]) {

      allComplement[1][0] = alphabet.wobble(sequence[offset]);
      pos = 2;
    }

    for (int i = offset + 1; i < length; i++) {
      if (!hasWobble[i]) {
        for (int j = 0; j < pos; j++) {
          allComplement[j][i] = alphabet.complement(sequence[i]);
        }
      } else {
        char comp = alphabet.complement(sequence[i]);
        char wobble = alphabet.wobble(sequence[i]);
        int posStop = pos;
        for (int j = 0; j < posStop; j++) {
          System.arraycopy(allComplement[j], 0, allComplement[j + posStop], 0, i);
          allComplement[j][i] = comp;
          allComplement[j + posStop][i] = wobble;
          pos++;
        }
      }
    }

    allComplement = filterWobblesAmount(sequence, allComplement, maxWobbles);

    // reverse all the complements
    if (reverse) {
      for (int i = 0; i < allComplement.length; i++) {
        for (int left = 0, right = allComplement[i].length - 1; left < right; left++, right--) {
          // exchange the first and last
          char temp = allComplement[i][left];
          allComplement[i][left] = allComplement[i][right];
          allComplement[i][right] = temp;
        }
      }
    }

    return allComplement;
  }

  /**
   * Filter wobbles amount.
   * 
   * @param seq the seq
   * @param complements the complements
   * @param maxWobbles the max wobbles
   * @return the char[][]
   */
  private static final char[][] filterWobblesAmount(char[] seq, char[][] complements, int maxWobbles) {
    LinkedList<char[]> list = new LinkedList<char[]>();
    for (int i = 0; i < complements.length; i++) {
      if (countWobbles(seq, complements[i]) <= maxWobbles) {
        list.add(complements[i]);
      }
    }
    return list.toArray(new char[0][]);
  }

  /**
   * Count wobbles.
   * 
   * @param seq the seq
   * @param cs the cs
   * @return the int
   */
  private static final int countWobbles(char[] seq, char[] cs) {
    int counter = 0;
    for (int i = 0; i < seq.length; i++) {

      if (alphabet.hasWobble(seq[i]) && alphabet.wobble(seq[i]) == cs[i]) { // its
        // a
        // wobble
        counter++;
      }
    }
    return counter;
  }

  /**
   * Split rna to tuples.
   * 
   * @param sequence the sequence
   * @param tupleLength the tuple length
   * @return the char[][]
   */
  public static char[][] splitRnaToTuples(char[] sequence, int tupleLength) {

    // if (tupleLength > sequence.length)
    // return null;

    int ansSize = sequence.length - tupleLength + 1;
    char[][] ans = new char[ansSize][tupleLength];
    for (int pos = 0; pos < ansSize; pos++) {
      char[] arr = new char[tupleLength];
      for (int i = 0; i < tupleLength; i++)
        arr[i] = sequence[i + pos];
      ans[pos] = arr;
    }

    return ans;
  }

  /**
   * Split rna to tuples.
   * 
   * @param sequence the sequence
   * @param tupleLength the tuple length
   * @param ans the ans
   */
  public static void splitRnaToTuples(char[] sequence, int tupleLength, char[][] ans) {
    int ansSize = sequence.length - tupleLength + 1;
    for (int pos = 0; pos < ansSize; pos++) {
      for (int i = 0; i < tupleLength; i++)
        ans[pos][i] = sequence[i + pos];
    }
  }

  /**
   * Split rna to tuples.
   * 
   * @param sequence the sequence
   * @param tupleLength the tuple length
   * @return the char[][]
   */
  public static char[][] splitRnaToTuples(CharBuffer sequence, int tupleLength) {

    int ansSize = sequence.length() - tupleLength + 1;
    char[][] ans = new char[ansSize][tupleLength];
    for (int pos = 0; pos < ansSize; pos++) {
      char[] arr = new char[tupleLength];
      for (int i = 0; i < tupleLength; i++)
        arr[i] = sequence.charAt(i + pos);
      ans[pos] = arr;
    }

    return ans;
  }

  /**
   * Split rna to tuples.
   * 
   * @param sequence the sequence
   * @param tupleLength the tuple length
   * @param ans the ans
   */
  public static void splitRnaToTuples(CharBuffer sequence, int tupleLength, char[][] ans) {
    int ansSize = sequence.length() - tupleLength + 1;

    for (int pos = 0; pos < ansSize; pos++) {
      for (int i = 0; i < tupleLength; i++)
        ans[pos][i] = sequence.charAt(i + pos);
    }
  }

  /**
   * Gets the hashed tuples.
   * 
   * @param sequence the sequence
   * @param tupleLength the tuple length
   * @return the hashed tuples
   */
  public static int[] getHashedTuples(char[] sequence, int tupleLength) {
    int[] array = new int[sequence.length - tupleLength + 1];
    getHashedTuples(sequence, tupleLength, array);
    return array;
  }

  /**
   * Gets the hashed tuples.
   * 
   * @param sequence the sequence
   * @param tupleLength the tuple length
   * @param array the array
   * @return the hashed tuples
   */
  public static void getHashedTuples(char[] sequence, int tupleLength, int[] array) {
    int ansSize = sequence.length - tupleLength + 1;
    array[0] = alphabet.hash(sequence, 0, tupleLength);
    for (int i = 1; i < ansSize; i++) {
      array[i] = alphabet.hash(sequence, i, tupleLength, array[i - 1]);
    }
  }

  /**
   * The main method.
   * 
   * @param args the arguments
   */
  public static void main(String[] args) {
    char[] seq = new char[] {'u', 'c', 'g', 'a', 'u'};
    int[] arr = getHashedTuples(seq, 3);
    System.out.println(Arrays.toString(arr));

    System.out.println();
  }
}

package bgu.bio.stems.util;

import gnu.trove.map.hash.TIntDoubleHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Random;

import bgu.bio.adt.tuples.IntIntDoubleTriplet;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.search.StemData;
import bgu.bio.util.CharBuffer;

public class PartitionFunction {
  private static String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

  private static String rand(int length) {
    String ans = "";
    Random r = new Random();
    if (length <= 0) {
      return ans;
    }
    while (length > 0) {
      ans += alphabet.charAt(r.nextInt(alphabet.length()));
      length--;
    }
    return ans;
  }

  public static TIntObjectHashMap<TIntDoubleHashMap> mapList(ArrayList<IntIntDoubleTriplet> list) {
    TIntObjectHashMap<TIntDoubleHashMap> ans = new TIntObjectHashMap<TIntDoubleHashMap>();
    for (IntIntDoubleTriplet triplet : list) {
      if (!ans.containsKey(triplet.getFirst())) {
        ans.put(triplet.getFirst(), new TIntDoubleHashMap());
      }
      ans.get(triplet.getFirst()).put(triplet.getSecond(), triplet.getThird());
    }
    return ans;
  }

  public static double assignProbabillity(AnnotatedSequence seq,
      TIntObjectHashMap<TIntDoubleHashMap> map) {
    double val = 0;
    double amount = 0;
    CharBuffer str = seq.getStructure();
    int posLeft = str.lastIndexOf('(');
    int posRight = str.indexOf(')');

    while (posRight < str.length() && posLeft >= 0) {
      double tmp = 1.0 / 1000000.0;
      final int posL = posLeft + seq.getStructureStartPosition();
      final int posR = posRight + seq.getStructureStartPosition();
      if (map.contains(posL)) {
        final TIntDoubleHashMap innerMap = map.get(posL);

        if (innerMap.contains(posR)) {
          tmp = innerMap.get(posR);
        }
      }
      val += tmp;
      amount++;
      // move to next base pair
      posRight++;
      while (posRight < str.length() && str.charAt(posRight) != ')') {
        posRight++;
      }

      posLeft--;
      while (posLeft >= 0 && str.charAt(posLeft) != '(') {
        posLeft--;
      }
    }
    return val / amount;
  }

  public static double assignProbabillity(StemData stemData,
      TIntObjectHashMap<TIntDoubleHashMap> map) {
    String str1 = stemData.getStructure1();
    String str2 = stemData.getStructure2();

    int posLeft = str1.length() - 1;
    int posRight = 0;
    int offset1 = stemData.getPosition1();
    int offset2 = stemData.getPosition2();
    double val = 0;
    double amount = 0;
    while (posRight < str2.length() && posLeft >= 0) {
      double tmp = 1.0 / 1000000.0;
      if (map.contains(posLeft + offset1)) {
        final TIntDoubleHashMap innerMap = map.get(posLeft + offset1);
        if (innerMap.contains(posRight + offset2)) {
          tmp = innerMap.get(posRight + offset2);
        }
      }
      val += tmp;
      amount++;
      // move to next base pair
      posRight++;
      while (posRight < str2.length() && str2.charAt(posRight) != ')') {
        posRight++;
      }

      posLeft--;
      while (posLeft >= 0 && str1.charAt(posLeft) != '(') {
        posLeft--;
      }
    }
    return val / amount;
  }

  public static boolean canRun() {
    try {
      computeParitionFunction("AAAAAAAAAUUUUUUUUU");
      return true;
    } catch (Throwable e) {
      return false;
    }
  }

  public static ArrayList<IntIntDoubleTriplet> computeParitionFunction(String sequence)
      throws IOException, InterruptedException {
    ArrayList<IntIntDoubleTriplet> ans = new ArrayList<IntIntDoubleTriplet>();
    String[] params = new String[] {"RNAfold", "--noPS", "-p"};
    ProcessBuilder pb = new ProcessBuilder(params);
    pb.redirectErrorStream(true);
    Process p = pb.start();
    OutputStream stdin = p.getOutputStream();

    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stdin));

    String key = rand(10);
    writer.write(">" + key + "\n");
    if (sequence.startsWith(">")) {
      sequence = sequence.substring(sequence.indexOf('\n') + 1);
    }
    writer.write(sequence.trim());
    writer.close();

    String line;
    boolean debug = false;
    if (debug) {

      InputStream stdout = p.getInputStream();
      BufferedReader reader = new BufferedReader((new InputStreamReader(stdout)));
      while ((line = reader.readLine()) != null) {
        System.out.println("Stdout: " + line);
      }

      reader.close();
    }

    p.waitFor();

    File dpFile = new File(key + "_dp.ps");
    BufferedReader dpReader = new BufferedReader(new FileReader(dpFile));
    boolean data = false;
    while ((line = dpReader.readLine()) != null) {
      if (line.startsWith("%data starts here")) {
        data = true;
      } else if (data && line.indexOf("ubox") != -1) {
        String[] sp = line.split(" ");
        double val = Double.parseDouble(sp[2]);
        IntIntDoubleTriplet triplet =
            new IntIntDoubleTriplet(Integer.parseInt(sp[0]), Integer.parseInt(sp[1]), val * val);
        ans.add(triplet);
      }

    }
    dpReader.close();
    dpFile.delete();
    return ans;
  }
}

package bgu.bio.stems.util;

/**
 * A utility class for handling offsets while reading VInts.
 */
public class VIntOffset {

  int value;

  public VIntOffset(int value) {
    this.value = value;
  }

  public int inc() {
    return value++;
  }

  public int value() {
    return value;
  }

  @Override
  public String toString() {
    return "" + value;
  }

}

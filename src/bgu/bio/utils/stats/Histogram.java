package bgu.bio.utils.stats;

import gnu.trove.list.array.TDoubleArrayList;

public class Histogram {

  private double min, max;
  private int numBins;
  private TDoubleArrayList data;
  private double binSize;

  public void setNumOfBins(int value) {
    this.numBins = value;
  }

  public void setData(TDoubleArrayList data) {
    this.data = data;
    double max = data.get(0);
    double min = data.get(0);
    for (int i = 1; i < data.size(); i++) {
      final double d = data.get(i);
      if (d < min) {
        min = d;
      }
      if (d > max) {
        max = d;
      }
    }
    this.max = max;
    this.min = min;
    this.binSize = (max - min) / numBins;
  }

  public double[] calcHistogramValues() {
    double[] values = new double[numBins];
    for (int i = 0; i < values.length; i++) {
      values[i] = min + binSize * i;
    }
    return values;
  }

  public int[] calcHistogram() {
    final int[] result = new int[numBins];

    for (int i = 0; i < data.size(); i++) {
      final double d = data.get(i);
      final int bin = (int) ((d - min) / binSize);
      if (bin < 0) { /* this data is smaller than min */
        result[0] += 1;
      } else if (bin >= numBins) { /* this data point is bigger than max */
        result[result.length - 1] += 1;
      } else {
        result[bin] += 1;
      }
    }
    return result;
  }
}

package bgu.bio.adt;

import java.util.Arrays;

import bgu.bio.adt.tuples.IntPair;
import bgu.bio.algorithms.alignment.constrained.re.FullConstrainedGlobalAlignmentEngine;
import bgu.bio.ds.automata.TransitionTable;
import bgu.bio.stems.util.RnaCompTable;
import bgu.bio.stems.util.RnaUtils;
import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.AlphabetUtils;
import bgu.bio.util.alphabet.RnaAlphabet;

/**
 * The Class BucketTable. the class provide methods to search for complementary seeds in stems. the
 * table first dimension is for the sequence (by running & circular ids) and the second Dimension is
 * for the hashed seeds. Using the {@link Framework#hash(char[])} method
 */
public class WindowSequenceTable {

  /**
   * The identifiers. keeps an identifier (e.g. position) for each row (sequence)
   */
  private int[] identifiers;

  private int[][] tuplesTable;

  private char[][] table;

  private boolean[] isCellActive;
  /**
   * The seeds. the number of columns in the table. each column represent an Hashed value of the
   * seed in length {@link WindowSequenceTable#seedSize}
   */
  private int numOfSeeds;

  /** The seed size. the size of each individual seed (default is 3) */
  private int seedSize;

  /** The buffer zone size. */
  private int bufferZoneSize;

  /** The next sequence position. the next row available in the table */
  private int nextSequencePosition;

  /**
   * The sequence length, equivalent to the maximum number of different seeds occurrences.
   */
  private int sequenceLength;

  /** The sequence amount. number of sequences (rows) in the table */
  private int sequenceAmount;

  // the following fields are for run-time efficiency
  private RnaCompTable compLookUp;
  private AlphabetUtils alphabet;
  private FullConstrainedGlobalAlignmentEngine cAlign;

  private char emptyLetter;

  private AlphabetUtils dynamicAlphabet;

  private int[] helpRevArray;

  private char[] revSequence;

  private final int lengthOfTupledSequence;

  private int[] countArr1, countArr1Original;

  private int[] countArr2;

  private int[] accessedArr1;

  /**
   * Instantiates a new bucket table.
   * 
   * @param seedSize the seed length - size of each seed
   * @param sequenceAmount the sequence amount - number of rows
   * @param sequenceLength the sequence length
   * @param bufferZoneSize the buffer zone size
   * @param maxWobbles the max number allows in each seeds
   */
  public WindowSequenceTable(int seedSize, int sequenceAmount, int sequenceLength,
      int bufferZoneSize, int maxWobbles) {

    this.alphabet = RnaAlphabet.getInstance();

    // create the complementary table with reverse complement
    this.compLookUp = new RnaCompTable(seedSize, maxWobbles, 0, true);

    this.seedSize = seedSize;
    this.numOfSeeds = (int) Math.pow(this.alphabet.size(), seedSize);
    this.sequenceLength = sequenceLength;
    this.sequenceAmount = sequenceAmount;
    this.bufferZoneSize = bufferZoneSize;

    this.nextSequencePosition = 0;

    // this.graph = new BipartiteGraph(this.sequenceLength - seedSize +
    // 1,this.sequenceLength - seedSize + 1);
    this.lengthOfTupledSequence = sequenceLength - seedSize + 1;
    this.table = new char[sequenceAmount][sequenceLength - seedSize + 1];
    this.isCellActive = new boolean[sequenceAmount];
    helpRevArray = new int[this.sequenceLength - this.seedSize + 1];
    this.revSequence = new char[sequenceLength - seedSize + 1];
    identifiers = new int[sequenceAmount];
    this.tuplesTable = new int[sequenceAmount][this.sequenceLength - this.seedSize + 1];

    String regex = "(i|d|s)*(m+((i|d)*s){" + (seedSize - 1) + "})*(i|d|s)*m*";
    emptyLetter = (char) this.numOfSeeds;

    char[] letters = new char[this.numOfSeeds + 1]; // for the empty char
    for (int i = 0; i < letters.length; i++) {
      letters[i] = (char) i;
    }

    dynamicAlphabet =
        new AlphabetUtils(letters, new int[0][0], new int[0][0], new char[0][0], false) {

          @Override
          public char map(String str) {

            return 0;
          }

          @Override
          public short emptyLetterHashed() {
            return this.encode(emptyLetter);
          }

          @Override
          public char emptyLetter() {
            return emptyLetter;
          }
        };

    ScoringMatrix matrix =
        new ScoringMatrixByCompTable(compLookUp, dynamicAlphabet,
            dynamicAlphabet.encode(emptyLetter));
    GeneralAlphabetWithCompTable general =
        new GeneralAlphabetWithCompTable(dynamicAlphabet, compLookUp);
    TransitionTable tr = new TransitionTable(regex, general);
    cAlign = new FullConstrainedGlobalAlignmentEngine(matrix, tr, general);

    this.countArr1 = new int[numOfSeeds];
    this.countArr2 = new int[numOfSeeds];
    this.countArr1Original = new int[numOfSeeds];
    this.accessedArr1 = new int[sequenceLength - seedSize + 1];

  }

  /**
   * Next available row in the table.
   * 
   * @return the next free row.
   */
  public int nextAvailableRow() {
    return this.nextSequencePosition;
  }

  /**
   * Inc next sequence.
   */
  private void incNextSequence() {
    this.nextSequencePosition = (this.nextSequencePosition + 1) % this.sequenceAmount;
  }

  /**
   * Insert.
   * 
   * @param sequence the sequence
   * @return int the row number that the sequence has been entered
   */
  public int insert(char[] sequence, int identifier) {
    // sets the identifier data
    int enteredInRow = this.nextSequencePosition;
    this.identifiers[this.nextSequencePosition] = identifier;

    int[] helpArr = this.tuplesTable[this.nextSequencePosition];
    RnaUtils.getHashedTuples(sequence, this.seedSize, helpArr);

    for (int i = 0; i < helpArr.length; i++) {
      this.table[this.nextSequencePosition][i] = dynamicAlphabet.decode(helpArr[i]);
    }
    this.isCellActive[this.nextSequencePosition] = true;

    this.incNextSequence();
    return enteredInRow;
  }

  private boolean compare(int minCommon, char[] sequence, int row2) {
    if (1 == 1)
      return true;
    cAlign.setCutoff(minCommon);
    cAlign.align(sequence, this.table[row2]);

    double score = cAlign.getOptimalScore();
    score = Double.isInfinite(score) ? 0 : score;

    return score >= minCommon;
  }

  /**
   * Search.
   * 
   * @param minCommon the minimum number of common matches
   * @param sequenceRow the row number in which the sequence exist
   * @param offsets
   * 
   * @return the number of search results found.
   */
  public int search(int minCommon, int sequenceRow, int[] ansArray, IntPair[] offsets) {

    int nFound = 0;
    // calculate the start, end positions
    int currentPosition = (sequenceRow + this.sequenceLength) % this.sequenceAmount;
    int endPosition =
        (this.sequenceAmount + sequenceRow + 1 - this.bufferZoneSize) % this.sequenceAmount;

    // System.out.println("Checking from " + currentPosition + " to " +
    // endPosition);
    // copy data to revSequence for the comparison part
    for (int i = 0; i < this.lengthOfTupledSequence; i++) {
      this.revSequence[i] = this.table[sequenceRow][this.lengthOfTupledSequence - i - 1];
    }

    // copy the reverse tuple array and count
    final int limit = sequenceLength - seedSize + 1;
    for (int i = 0; i < limit; i++) {
      helpRevArray[i] = this.tuplesTable[sequenceRow][limit - i - 1];
    }

    Arrays.fill(this.countArr1, 0);
    int top = 0;
    // put the hashed tuples in the counting array
    for (int i = 0; i < this.helpRevArray.length; i++) {
      this.countArr1[this.helpRevArray[i]]++;
      if (this.countArr1[this.helpRevArray[i]] == 1)// first time accessed
      // then add to the
      // array;
      {
        this.accessedArr1[top] = this.helpRevArray[i];
        top++;
      }
    }
    System.arraycopy(this.countArr1, 0, this.countArr1Original, 0, this.numOfSeeds);

    while (currentPosition != endPosition) {

      System.arraycopy(this.countArr1Original, 0, this.countArr1, 0, this.numOfSeeds);

      if (this.isCellActive[currentPosition]
          && this.checkNumberOfPossibleMatches(minCommon, top, this.tuplesTable[currentPosition])
          && this.compare(minCommon, this.revSequence, currentPosition)) {

        ansArray[nFound] = this.identifiers[currentPosition];
        nFound++;
      }
      currentPosition = (currentPosition + 1) % this.sequenceAmount;
    }
    // System.out.println();
    return nFound;
  }

  public void reset() {
    this.nextSequencePosition = 0;
    Arrays.fill(this.isCellActive, false);

  }

  /**
   * Check number of possible matches.
   * 
   * @param minimalAmount the minimal amount
   * @param top the top
   * @param arr2 the arr2
   * @return {@code true} iff number of possible matches between the lines is above the minimum.
   */
  private boolean checkNumberOfPossibleMatches(int minimalAmount, int top, int[] arr2) {
    Arrays.fill(this.countArr2, 0);

    // put the hashed tuples in the counting array
    for (int i = 0; i < arr2.length; i++) {
      this.countArr2[arr2[i]]++;
    }

    // move on all accessed array
    int total = 0;
    for (int i = 0; i < top; i++) {
      final int cell = this.accessedArr1[i];
      final int[] comps = this.compLookUp.getHashed(cell);
      for (int c = 0; c < comps.length; c++) {
        if (this.countArr2[comps[c]] > 0) {
          // only if access more then once. add to the counter and
          // reduce the amount
          final int temp = Math.min(this.countArr1[cell], this.countArr2[comps[c]]);

          total += temp;
          if (total >= minimalAmount)
            return true;

          this.countArr1[cell] -= temp;
          this.countArr2[comps[c]] -= temp;
        }
        if (this.countArr1[cell] == 0) {
          // break
          break;
        }
      }
    }

    return false;
  }

  public static void main(String[] args) {
    int seed = 3;
    int amount = 90;
    int size = 15;
    int buffer = 4 + size;
    int wobbles = 3;
    WindowSequenceTable w = new WindowSequenceTable(seed, amount, size, buffer, wobbles);
    String str = "AGTAATTTTCGTGCTCTCAACAATTGTCGCCGTCACAGATTGTTGTTCGAGCCGAATCTTACT";
    int pos = 0;
    int[] ans = new int[amount];
    for (int i = 0; i < str.length() - size + 1; i++) {
      System.out.println("Starting check for position " + i + " " + str.substring(i, i + size));
      pos = w.insert(str.substring(i, i + size).toCharArray(), i);
      System.out.println("Search\n-----------");
      int found = w.search(3, pos, ans, null);
      System.out.println("results: " + Arrays.toString(Arrays.copyOfRange(ans, 0, found)));
      System.out.println("------------------");
    }

  }
}

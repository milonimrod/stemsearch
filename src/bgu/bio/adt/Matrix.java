package bgu.bio.adt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import bgu.bio.util.alphabet.RnaAlphabet;


/**
 * A class which enables creating double matrices of high dimension.
 * 
 * @author sivany
 * 
 */
public class Matrix {
  protected final float[] mat;

  private final int[] dims;

  private final int[] mults;

  private static final boolean debug = false;

  /**
   * Construct an empty matrix.
   * 
   * @param dims The matrix dimensions
   * @throws IOException
   */
  public Matrix(int... dims) throws IOException {
    if (dims == null || dims.length < 1) {
      throw new IOException("Cannot create a matrix with no dimensions");
    }
    this.dims = dims;
    this.mults = new int[dims.length];
    for (int i = 0; i < dims.length; i++) {
      this.mults[i] = 1;
      for (int j = i + 1; j < dims.length; j++) {
        this.mults[i] *= dims[j];
      }
    }
    this.mat = new float[this.mults[0] * dims[0]];
  }

  /**
   * Construct with values given in file.
   * 
   * @param file input file
   * @param dims The matrix dimensions
   * @throws IOException
   */
  public Matrix(File file, int... dims) throws IOException {
    this(dims);
    BufferedReader reader = new BufferedReader(new FileReader(file));
    String line;
    int[] coords = new int[dims.length];
    while ((line = reader.readLine()) != null) {
      int index = line.indexOf(" ");
      String num = line.substring(0, index);
      float value = Float.parseFloat(num);
      index++;
      for (int i = 0; i < coords.length; i++) {
        coords[i] = RnaAlphabet.getInstance().encode(line.charAt(index));
        index += 2;
      }
      set(value, coords);
    }
    reader.close();
  }

  /**
   * Construct with values given in preferences node.
   * 
   * @param prefs preferences node
   * @param dims The matrix dimensions
   * @throws IOException
   */
  public Matrix(Preferences prefs, int... dims) throws IOException {
    this(dims);
    // iterate over keys
    int[] coords = new int[dims.length];
    try {
      for (String key : prefs.keys()) {
        float value = prefs.getFloat(key, 0);
        for (int i = 0; i < coords.length; i++) {
          coords[i] = RnaAlphabet.getInstance().encode(key.charAt(i));
        }
        set(value, coords);
      }
    } catch (BackingStoreException e) {
      throw new IOException(e.getMessage());
    }
  }

  /**
   * Set a value at certain coordinates in the matrix.
   */
  public void set(float value, int... coords) {
    this.mat[getLocation(coords)] = value;
  }

  /**
   * Get the value at certain coordinates in the matrix.
   */
  public double get(int... coords) {
    return this.mat[getLocation(coords)];
  }

  /**
   * Since the values are held in a single double array, we need this method which calculates the
   * position in the array corresponding to given coordinates.
   */
  private final int getLocation(int[] coords) {
    if (debug) {
      if (coords.length != this.dims.length) {
        throw new AssertionError("Wrong number of coordinates (" + coords + " instead of "
            + this.dims + ")");
      }
      for (int i = 0; i < coords.length; i++) {
        if (coords[i] < 0 || coords[i] >= this.dims[i]) {
          throw new AssertionError("Illegal " + i + " coordinate (" + coords[i] + ", dimension "
              + this.dims + ")");
        }
      }
    }
    int location = 0;
    for (int i = 0; i < coords.length; i++) {
      location += coords[i] * this.mults[i];
    }
    return location;
  }
}

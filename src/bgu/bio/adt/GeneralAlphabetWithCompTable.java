package bgu.bio.adt;

import bgu.bio.stems.util.RnaCompTable;
import bgu.bio.util.alphabet.AlphabetUtils;
import bgu.bio.util.alphabet.constrain.ConstrainedAlphabet;

public class GeneralAlphabetWithCompTable extends ConstrainedAlphabet {
  RnaCompTable table;

  public GeneralAlphabetWithCompTable(AlphabetUtils stringAlphabet, RnaCompTable table) {
    super(new char[] {'i', 'd', 'm', 's'}, stringAlphabet);
    this.table = table;
    load();
    buildMappingVectors();
  }

  @Override
  public void load() {
    if (this.table == null)
      return;

    for (int i = 0; i < this.stringAlphabet.size() - 1; i++) {
      int[] ans = table.getHashed(i);
      for (int j = 0; j < stringAlphabet.size() - 1; j++) {
        this.mapTable[i][j][this.encode('s')] = true;
        this.mapTable[i][j][this.encode('i')] = false;
        this.mapTable[i][j][this.encode('d')] = false;
      }
      for (int j = 0; j < ans.length; j++) {
        this.mapTable[i][ans[j]][this.encode('m')] = true;
        this.mapTable[i][ans[j]][this.encode('s')] = false;
      }

    }

    // setting the empty letter with any other to indel
    for (int i = 0; i < this.stringAlphabet.size(); i++) {
      this.mapTable[i][this.stringAlphabet.emptyLetterHashed()][this.encode('i')] = true;
      this.mapTable[this.stringAlphabet.emptyLetterHashed()][i][this.encode('d')] = true;
    }

    // setting the empty letter not to match nothing
    this.mapTable[this.stringAlphabet.emptyLetterHashed()][this.stringAlphabet.emptyLetterHashed()][this
        .encode('i')] = false;
    this.mapTable[this.stringAlphabet.emptyLetterHashed()][this.stringAlphabet.emptyLetterHashed()][this
        .encode('d')] = false;
    this.mapTable[this.stringAlphabet.emptyLetterHashed()][this.stringAlphabet.emptyLetterHashed()][this
        .encode('m')] = false;
    this.mapTable[this.stringAlphabet.emptyLetterHashed()][this.stringAlphabet.emptyLetterHashed()][this
        .encode('s')] = false;
  }

  @Override
  public String parse(String input) {
    return input;
  }

  @Override
  public char emptyLetter() {
    return '_';
  }

  @Override
  public short emptyLetterHashed() {
    return encode(emptyLetter());
  }

  @Override
  public char map(String str) {
    return 0;
  }

  @Override
  public String toString() {
    String ans = "";

    return ans;
  }
}

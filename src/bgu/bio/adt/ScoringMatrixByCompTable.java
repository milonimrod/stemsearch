package bgu.bio.adt;

import bgu.bio.stems.util.RnaCompTable;
import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.AlphabetUtils;

public class ScoringMatrixByCompTable extends ScoringMatrix {

  private int empty;

  public ScoringMatrixByCompTable(RnaCompTable table, AlphabetUtils alphabet, int empty) {
    super(alphabet);
    this.empty = empty;
    for (int i = 0; i < alphabet.size() - 2; i++) {
      int[] array = table.getHashed(i);
      for (int idx = 0; idx < array.length; idx++) {
        this.table[i][array[idx]] = 1.0f;
      }
    }
  }

  @Override
  public float score(short c1, short c2) {
    if (c1 == empty || c2 == empty)
      return 0.0f;
    return this.table[c1][c2];
  }
}

package bgu.bio.adt;

import java.util.Arrays;

import bgu.bio.adt.tuples.IntPair;

public class IntPairQueue {

  private IntPair[] array;
  private int size;
  private int capacity;
  private int front;
  private int rear;
  private final static int FACTOR = 2;
  private final static int INIT_CAPACITY = 30;

  public IntPairQueue(int capacity) {
    this.capacity = capacity;
    this.size = 0;
    this.array = new IntPair[capacity];
    this.front = 0;
    this.rear = 0;
  }

  public IntPairQueue() {
    this(INIT_CAPACITY);
  }

  public IntPair head() {
    if (this.size == 0)
      return null;

    return this.array[front];
  }

  public IntPair poll() {

    if (this.size == 0)
      return null;
    IntPair ans = this.array[front];
    this.front = this.increase(front);
    size--;
    return ans;
  }

  public void push(IntPair data) {

    this.array[this.rear] = data;
    size++;
    if (size == capacity)
      this.resize();
    else
      this.rear = this.increase(rear);

  }

  public int size() {
    return this.size;
  }

  private int increase(int var) {
    if (var != this.capacity - 1)
      return var + 1;
    return 0;
  }

  private void resize() {
    // System.out.println("before:" + toString());
    IntPair[] newPool = new IntPair[capacity * FACTOR];
    if (front < rear) {
      System.arraycopy(this.array, this.front, newPool, 0, this.size);
    } else {
      System.arraycopy(this.array, this.front, newPool, 0, this.capacity - this.front);
      System.arraycopy(this.array, 0, newPool, this.capacity - this.front, this.front);
    }
    this.front = 0;
    this.rear = this.capacity;
    this.capacity = capacity * FACTOR;
    this.array = newPool;
    // System.out.println("after:" + toString());
  }

  @Override
  public String toString() {
    return "size: " + this.size + ",capacity: " + this.capacity + ",front: " + this.front
        + ", rear:" + this.rear + "," + Arrays.toString(this.array);
  }
}

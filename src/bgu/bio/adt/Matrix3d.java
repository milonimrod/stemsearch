package bgu.bio.adt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import bgu.bio.util.alphabet.RnaAlphabet;

public class Matrix3d {
  private float[][][] mat;

  /**
   * Construct an empty matrix.
   * 
   * @param dims The matrix dimensions
   * @throws IOException
   */
  public Matrix3d(int... dims) throws IOException {
    if (dims.length != 3) {
      throw new IOException("Wrong number of dimensions, expected 3 but was " + dims.length);
    }
    mat = new float[dims[0]][dims[1]][dims[2]];
  }

  /**
   * Construct with values given in file.
   * 
   * @param file input file
   * @param dims The matrix dimensions
   * @throws IOException
   */
  public Matrix3d(File file, int... dims) throws IOException {
    this(dims);
    BufferedReader reader = new BufferedReader(new FileReader(file));
    String line;
    int[] coords = new int[3];
    while ((line = reader.readLine()) != null) {
      int index = line.indexOf(" ");
      String num = line.substring(0, index);
      float value = Float.parseFloat(num);
      index++;
      for (int i = 0; i < coords.length; i++) {
        coords[i] = RnaAlphabet.getInstance().encode(line.charAt(index));
        index += 2;
      }
      mat[coords[0]][coords[1]][coords[2]] = value;
    }
    reader.close();
  }

  /**
   * Construct with values given in preferences node.
   * 
   * @param prefs preferences node
   * @param dims The matrix dimensions
   * @throws IOException
   */
  public Matrix3d(Preferences prefs, int... dims) throws IOException {
    this(dims);
    // iterate over keys
    int[] coords = new int[3];
    try {
      for (String key : prefs.keys()) {
        float value = prefs.getFloat(key, 0);
        for (int i = 0; i < coords.length; i++) {
          coords[i] = RnaAlphabet.getInstance().encode(key.charAt(i));
        }
        mat[coords[0]][coords[1]][coords[2]] = value;
      }
    } catch (BackingStoreException e) {
      throw new IOException(e.getMessage());
    }
  }

  public void set(float value, int... coords) {
    mat[coords[0]][coords[1]][coords[2]] = value;
  }

  public double get(int... coords) {
    return mat[coords[0]][coords[1]][coords[2]];
  }

}

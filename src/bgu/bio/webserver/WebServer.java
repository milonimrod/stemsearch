package bgu.bio.webserver;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Random;

import org.simpleframework.http.Query;
import org.simpleframework.http.Request;
import org.simpleframework.http.Response;
import org.simpleframework.http.core.Container;
import org.simpleframework.http.core.ContainerServer;
import org.simpleframework.transport.Server;
import org.simpleframework.transport.connect.Connection;
import org.simpleframework.transport.connect.SocketConnection;

import bgu.bio.io.file.json.JSONObject;

public class WebServer implements Container {

  private static final char[] AB = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
  private Random rand = new Random();
  private SimpleDateFormat date = new SimpleDateFormat("yyMMdd");
  private static String HOME_DIR = "/home/studies/projects/negevcb/";
  private static String PROJECT_HOME = HOME_DIR + "freespace/stemSearch/";

  public void handle(Request request, Response response) {
    try {

      long time = System.currentTimeMillis();

      response.setValue("Content-Type", "text/plain");
      response.setValue("Server", "StemSearch/2.0 (SimpleFramework.org)");
      response.setDate("Date", time);
      response.setDate("Last-Modified", time);
      PrintStream body = response.getPrintStream();

      if (request.getAddress().toString().indexOf("favicon.ico") < 0) {
        Query query = request.getQuery();
        System.out.println(request);
        String operation = query.get("operation").toLowerCase();
        if (operation.equals("submit")) {
          double relScore = Double.parseDouble(query.get("relativeScore"));
          String sequence = query.get("sequence");
          final int len = 5;
          StringBuilder sb = new StringBuilder(len);
          for (int i = 0; i < len; i++)
            sb.append(AB[rand.nextInt(AB.length)]);

          String id =
              ("R" + sb.toString() + date.format(Calendar.getInstance().getTime())).toUpperCase();
          JSONObject json = new JSONObject();
          json.put("id", id);
          // allow access to the results file to all users
          Runtime.getRuntime().exec("touch " + PROJECT_HOME + "results/" + id + ".json");
          Runtime.getRuntime().exec("chmod a+r " + PROJECT_HOME + "results/" + id + ".json");
          String values =
              String
                  .format(
                      "-t 5 -s cbnegev4.cs.bgu.ac.il:27019 -db StemSearch -col bacil -pval shuffle -pf %f -rel %f %s %s",
                      0.0001, relScore, sequence, PROJECT_HOME + "results/" + id + ".json");
          String[] params =
              new String[] {"qsub", "-N", id, "-o", PROJECT_HOME + "/logs/" + id + ".out", "-e",
                  PROJECT_HOME + "/logs/" + id + ".err", "-l", "nodes=1:ppn=5", "-l",
                  "walltime=0:15:00", "-F", values, HOME_DIR + "bin/stemsearch_search"};
          System.out.println(Arrays.toString(params));
          ProcessBuilder pb = new ProcessBuilder(params);
          pb.redirectErrorStream(true);
          Process p = pb.start();
          InputStream stdout = p.getInputStream();
          BufferedReader reader = new BufferedReader((new InputStreamReader(stdout)));
          String line = null;
          boolean submitted = false;
          while ((line = reader.readLine()) != null) {
            line = line.trim();
            System.out.println("Stdout: " + line);
            if (line.matches(".*[\\d]\\.cbnegev3.*")) {
              submitted = true;
              json.put("serverID", line);
              System.out.println("%%%%%%%%%%% " + line);
            }
          }
          reader.close();
          p.waitFor();
          if (submitted) {
            System.out.println(json);
            body.println(json.toString());
          } else {
            System.out.println("Error");
            body.println("{\"error\":\"Can't submit to server\"}");
          }
        } else if (operation.equals("status")) {
          String jobId = query.get("id");
          String[] params = new String[] {"qstat", "-u", "negevcb"};
          ProcessBuilder pb = new ProcessBuilder(params);
          pb.redirectErrorStream(true);
          Process p = pb.start();
          InputStream stdout = p.getInputStream();
          BufferedReader reader = new BufferedReader((new InputStreamReader(stdout)));
          String line = null;
          String status = "Error";
          while ((line = reader.readLine()) != null) {
            line = line.trim();
            // System.out.println("Stdout: " + line);
            if (line.indexOf(jobId) >= 0) {
              String[] sp = line.split("\\s+");
              String statusCode = sp[sp.length - 2];
              switch (statusCode) {
                case "C":
                  status = "Done";
                  break;
                case "Q":
                  status = "Pending";
                  break;
                case "R":
                  status = "Running";
                  break;
                default:
                  status = "?";
                  break;
              }
            }
          }
          System.out.println(jobId + " " + status);
          body.println("{\"status\":\"" + status + "\"}");
          reader.close();
          p.waitFor();
        }
      }
      body.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void main(String[] list) throws Exception {
    Container container = new WebServer();
    Server server = new ContainerServer(container);
    Connection connection = new SocketConnection(server);
    SocketAddress address = new InetSocketAddress(8080);

    connection.connect(address);
  }
}

package bgu.bio.stems.reader;

import java.io.File;
import java.io.StringReader;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import bgu.bio.ds.rna.RNA;
import bgu.bio.stems.filereader.FastaFormatReader;
import bgu.bio.stems.filereader.FastaFormatReaderGenome;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.utils.CreateRandomRnaFastaFile;
import bgu.bio.util.alphabet.RnaAlphabet;

public class TestFastaFormatReaderGenome {

  @Test
  public void test1() {
    String[] sequences = new String[1];
    sequences[0] =
        ">random sequence 0 of length 500 and seed 17\nguggacuuauaugcgcugagccucuguaaaucucgauuccccguuuuuugccacagcccu\ncaugcuucgauugauugacuagcgcucugguucucaggcaucgaaugagaguagcuggcg\nuuucaaccuguucgguacgaaggucguacguuuggagggguaaagcugaugcugcggcug\nccgaggguaccuuuacauaugccgaacuuuaggggcgauuuugcagucggucuccuggau\ncguacucuggacauugacuauaugcccgcggaaccuucguuuagggacagcacuccgugg\ncaagguaguucgcuacgacagaccaucuccggcgcauagaaaagguugccuaacgguuuc\nacccguguucaaguccuguguugucacgacguguuucuuacgucacccucccccaguacc\naacucaccaugagacaauaacaaauugauacuuagcucuuuccuuaggcccuaagucccg\ncagcuagaucgucguguugg";
    test1Sequence(sequences, 100);
  }

  @Test
  public void test2() {
    String[] sequences = new String[2];
    sequences[0] =
        ">random sequence 0 of length 500 and seed 17\nguggacuuauaugcgcugagccucuguaaaucucgauuccccguuuuuugccacagcccu\ncaugcuucgauugauugacuagcgcucugguucucaggcaucgaaugagaguagcuggcg\nuuucaaccuguucgguacgaaggucguacguuuggagggguaaagcugaugcugcggcug\nccgaggguaccuuuacauaugccgaacuuuaggggcgauuuugcagucggucuccuggau\ncguacucuggacauugacuauaugcccgcggaaccuucguuuagggacagcacuccgugg\ncaagguaguucgcuacgacagaccaucuccggcgcauagaaaagguugccuaacgguuuc\nacccguguucaaguccuguguugucacgacguguuucuuacgucacccucccccaguacc\naacucaccaugagacaauaacaaauugauacuuagcucuuuccuuaggcccuaagucccg\ncagcuagaucgucguguugg";
    sequences[1] =
        ">random sequence 1 of length 60 and seed 42\ncucauguagccacccgcgcucguaaauucucgacauuccgcaguggcagcccuaucgcca\n";
    test1Sequence(sequences, 50);
  }

  @Test
  public void test3() {
    String[] sequences = new String[3];
    sequences[0] =
        ">random sequence 0 of length 500 and seed 17\nguggacuuauaugcgcugagccucuguaaaucucgauuccccguuuuuugccacagcccu\ncaugcuucgauugauugacuagcgcucugguucucaggcaucgaaugagaguagcuggcg\nuuucaaccuguucgguacgaaggucguacguuuggagggguaaagcugaugcugcggcug\nccgaggguaccuuuacauaugccgaacuuuaggggcgauuuugcagucggucuccuggau\ncguacucuggacauugacuauaugcccgcggaaccuucguuuagggacagcacuccgugg\ncaagguaguucgcuacgacagaccaucuccggcgcauagaaaagguugccuaacgguuuc\nacccguguucaaguccuguguugucacgacguguuucuuacgucacccucccccaguacc\naacucaccaugagacaauaacaaauugauacuuagcucuuuccuuaggcccuaagucccg\ncagcuagaucgucguguugg";
    sequences[1] =
        ">random sequence 1 of length 60 and seed 45\ncugccuuugaggcacaguuucguaggguuucguagcaauggcuuugucugaagagaaggg\n";
    sequences[2] =
        ">random sequence 2 of length 500 and seed -1\ngugcgccguggcugucuuaaucggaaaaggaaaagguacuuacugccuauugaacaagcucaguuggcaggguuugcuggaauuaagaagaaaggauucgagacacgguaaaau\nauccucguuuauccgcgcccgucagaaaaagccucucagcgcucaaguuaaacaacguggauggcgcccaguguca\ngugacggccuuuc\ncaccuuuuacgccccua";
    test1Sequence(sequences, 100);
  }

  @Test
  public void test3Break() {
    String[] sequences = new String[3];
    sequences[0] =
        ">random sequence 0 of length 500 and seed 17\ngu\nggacuuauaugcgcugagc\ncucuguaaaucucgauuccccguuuuuugccacagcccu\ncaugcuucgauugauugacuagcgcucugguucucaggcaucgaaugagaguagcuggcg\nuuucaaccuguucgguacgaaggucguacguuuggagggguaaagcugaugcugcggcug\nccgaggguaccuuuacauaugccgaacuuuaggggcgauuuugcagucggucuccuggau\ncguacucuggacauugacuauaugcccgcggaaccuucguuuagggacagcacuccgugg\ncaagguaguucgcuacgacagaccaucuccggcgcauagaaaagguugccuaacgguuuc\nacccguguucaaguccuguguugucacgacguguuucuuacgucacccucccccaguacc\naacucaccaugagacaauaacaaauugauacuuagcucuuuccuuaggcccuaagucccg\ncagcuagaucgucguguugg";
    sequences[1] =
        ">random sequence 2 of length 500 and seed -1\ngugcgccguggcugucuuaaucggaaaaggaaaagguacuuacugccuauugaacaagcucaguuggcaggguuugcuggaauuaagaagaaaggauucgagacacgguaaaau\nauccucguuuauccgcgcccgucagaaaaagccucucagcgcucaaguuaaacaacguggauggcgcccaguguca\ngugacggccuuuc\ncaccuuuuacgccccua";
    sequences[2] =
        ">random sequence 1 of length 60 and seed 45\ncugccuuugaggcacaguuucguaggguuucguagcaauggcuuugucugaagagaaggg\n";
    test1Sequence(sequences, 10);
  }

  @Test
  public void testRandom() {
    final int repeats = 1000;
    final int amount = 200;
    final int maxLen = 1000;
    Random rand = new Random();
    for (int i = 0; i < repeats; i++) {
      String[] sequences = new String[amount];
      for (int a = 0; a < amount; a++) {
        sequences[a] =
            CreateRandomRnaFastaFile.create(RnaAlphabet.getInstance().letters(),
                rand.nextInt(maxLen) + 10, rand.nextInt());
      }
      test1Sequence(sequences, 87);
    }
  }

  @Test
  public void testWithFile2() {
    final String filename = "test/general/FastaFormatReader/input.fasta";
    testWithFile(filename);
  }

  private void testWithFile(String filename) {
    FastaFormatReader reader = new FastaFormatReader(100);
    reader.setReader(new File(filename), false);
    List<RNA> list = RNA.loadFromFile(filename, false);
    String[] output = new String[list.size()];
    int id = -1;
    while (reader.hasNext()) {
      AnnotatedSequence ans = reader.next();
      if (reader.isFirstInSequence()) {
        id++;
        output[id] = ">" + ans.getSequenceId();
      }
      output[id] += ans.getSequence().toString();
    }

    for (int i = 0; i < list.size(); i++) {
      String expected = ">" + list.get(i).getHeader() + list.get(i).getPrimary();
      if (!expected.equalsIgnoreCase(output[i])) {
        System.out.println(expected);
        System.out.println(output[i]);
        Assert.fail("Got the wrong sequence");
      }
    }
  }

  private void test1Sequence(String[] sequences, int readSize) {
    FastaFormatReaderGenome reader = new FastaFormatReaderGenome(readSize);

    for (int i = 0; i < sequences.length; i++) {
      reader.setReader(new StringReader(sequences[i]), false);
      String output = "";
      while (reader.hasNext()) {
        AnnotatedSequence ans = reader.next();
        if (reader.isFirstInSequence()) {
          output = ">" + ans.getSequenceId();
        }
        output += ans.getSequence().toString();
      }
      Assert.assertEquals("Wrong answer", sequences[i].replace("\n", ""), output);

    }
  }
}

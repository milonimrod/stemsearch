package bgu.bio.stems;

import static org.junit.Assert.fail;
import gnu.trove.list.array.TCharArrayList;

import java.io.ByteArrayInputStream;
import java.util.BitSet;
import java.util.Random;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import bgu.bio.stems.indexing.RNAStructureHuffmanCoding;

public class TestRNAStructureHuffmanCodingReusable {

  RNAStructureHuffmanCoding coding;
  BitSet bitset;
  TCharArrayList lst;

  @Before
  public void setUp() {
    coding = new RNAStructureHuffmanCoding();
    bitset = new BitSet();
    lst = new TCharArrayList();
  }

  @Test
  public void test() {
    check("...(((()))))");
  }

  @Test
  public void testRandom10() {
    runRandom(100000, 10);
  }

  @Test
  public void testRandom20() {
    runRandom(100000, 20);
  }

  @Test
  public void testRandom50() {
    runRandom(100000, 50);
  }

  @Test
  public void testRandom100() {
    runRandom(100000, 100);
  }

  @Test
  public void testRandom1000() {
    runRandom(100000, 1000);
  }

  private void runRandom(int repeats, int maxLength) {
    String alphabet = ".()";
    Random rand = new Random();
    for (int i = 0; i < repeats; i++) {
      int len = rand.nextInt(maxLength) + 2;
      StringBuilder sb = new StringBuilder(len);
      for (int j = 0; j < len; j++) {
        sb.append(alphabet.charAt(rand.nextInt(alphabet.length())));
      }
      check(sb.toString());
    }
  }

  private void check(String str) {
    try {
      byte[] ans = coding.encode(str);
      coding.decode(new ByteArrayInputStream(ans), lst, bitset);
      String got = lstToString(lst);
      Assert.assertEquals(str, got);
    } catch (Exception ex) {
      fail("got exception");
    }

  }

  private String lstToString(TCharArrayList lst) {
    StringBuilder sb = new StringBuilder(lst.size());
    for (int i = 0; i < lst.size(); i++) {
      sb.append(lst.get(i));
    }
    return sb.toString();
  }

}

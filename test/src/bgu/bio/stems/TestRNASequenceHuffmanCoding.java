package bgu.bio.stems;

import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.util.Random;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import bgu.bio.stems.indexing.RNASequenceHuffmanCoding;

public class TestRNASequenceHuffmanCoding {

  RNASequenceHuffmanCoding coding;

  @Before
  public void setUp() {
    coding = new RNASequenceHuffmanCoding();
  }

  @Test
  public void test() {
    check("CACCN");
  }

  @Test
  public void testRandom10() {
    runRandom(100000, 10);
  }

  @Test
  public void testRandom20() {
    runRandom(100000, 20);
  }

  @Test
  public void testRandom50() {
    runRandom(100000, 50);
  }

  @Test
  public void testRandom100() {
    runRandom(100000, 100);
  }

  @Test
  public void testRandom1000() {
    runRandom(100000, 1000);
  }

  private void runRandom(int repeats, int maxLength) {
    String alphabet = "ACGUN";
    Random rand = new Random();
    for (int i = 0; i < repeats; i++) {
      int len = rand.nextInt(maxLength) + 2;
      StringBuilder sb = new StringBuilder(len);
      for (int j = 0; j < len; j++) {
        sb.append(alphabet.charAt(rand.nextInt(alphabet.length())));
      }
      check(sb.toString());
    }
  }

  private void check(String str) {
    try {
      byte[] ans = coding.encode(str);
      String got = coding.decode(new ByteArrayInputStream(ans));
      Assert.assertEquals(str, got);
    } catch (Exception ex) {
      fail("got exception");
    }

  }

}

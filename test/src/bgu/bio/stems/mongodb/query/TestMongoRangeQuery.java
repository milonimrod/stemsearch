package bgu.bio.stems.mongodb.query;

import org.junit.Assert;
import org.junit.Test;

import bgu.bio.stems.indexing.AnnotatedSequence;

import com.mongodb.DBObject;

public class TestMongoRangeQuery {
  final String usedName = "MyName";

  @Test
  public void test() {
    MyMongoRangeQuery queryBuilder = new MyMongoRangeQuery(5);
    queryBuilder.setVal(10);
    DBObject obj = queryBuilder.getQuery(null);

    Assert.assertTrue("Should containg a key named MyName", obj.containsField(usedName));
    System.out.println(obj);
    Assert.assertEquals(((Double) ((DBObject) obj.get(usedName)).get("$gte")).doubleValue(), 5.0,
        0.0001);
    Assert.assertEquals(((Double) ((DBObject) obj.get(usedName)).get("$lte")).doubleValue(), 15.0,
        0.0001);
  }

  private class MyMongoRangeQuery extends MongoRangeQuery {

    public MyMongoRangeQuery(double rangeValue) {
      super(usedName, rangeValue);
    }

    private double x;

    public void setVal(double x) {
      this.x = x;
    }

    @Override
    protected double getParam(AnnotatedSequence sequence) {
      return x;
    }

  }
}

package bgu.bio.stems.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Random;

import bgu.bio.stems.util.RnaUtils;

public class CreateRandomRnaFastaFile {

  public static void create(String filename, char[] letters, int length, long seed)
      throws IOException {
    File file = new File(filename);
    Writer out = new BufferedWriter(new FileWriter(file));

    out.write(create(letters, length, seed));
    out.close();
  }

  public static String create(char[] letters, int length, long seed) {

    StringBuilder sb = new StringBuilder();
    sb.append(">random sequence of length " + length + " and seed " + seed);
    Random rand = new Random(seed);
    for (int i = 0; i < length; i++) {
      if (i % 70 == 0)
        sb.append("\n");
      sb.append(letters[rand.nextInt(letters.length - 2)]);
    }

    return sb.toString();
  }

  public static void main(String[] args) {
    if (args.length < 2) {
      System.err.println("Usage: CreateRandomRnaFastaFile"
          + " output_file num_letters [random_seed]");
      return;
    }
    try {
      long seed = System.currentTimeMillis();
      if (args.length > 2) {
        seed = Long.parseLong(args[2]);
      }
      create(args[0], RnaUtils.alphabet.letters(), Integer.parseInt(args[1]), seed);
    } catch (Exception e) {
      e.printStackTrace();
      return;
    }
  }
}

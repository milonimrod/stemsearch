package bgu.bio.stems.hybridization;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import bgu.bio.stems.hybridization.RnaHybrid;
import bgu.bio.util.CharBuffer;

public class TestRnaHybrid {

  @Test
  public void testHybridization() {
    try {
      // test using CharBuffers and padding
      RnaHybrid rnaHybrid = new RnaHybrid();
      CharBuffer chars1 = new CharBuffer("aaaaaagggggg");
      rnaHybrid.setSeq1(chars1, 2, 8, 3, false);
      CharBuffer chars2 = new CharBuffer("ccccccuuuuuu");
      rnaHybrid.setSeq2(chars2, 2, 8, 5, true);
      double score = rnaHybrid.hybridize();
      // System.err.println(rnaHybrid);
      assertEquals("((((((((", new String(rnaHybrid.getSeq1Structure()));
      assertEquals("))))))))", new String(rnaHybrid.getSeq2Structure()));
      assertEquals(-14.7, score, 0.001);

      // test without CharBuffers
      testSequences(rnaHybrid, "aaaagggg", "ccccuuuu", -14.7, "((((((((", "))))))))");
      testSequences(rnaHybrid, "aaaagggg", "ccccuuuuuuuu", -14.8, "((((((((", "))))))))....");
      testSequences(rnaHybrid, "ccccaaaaggg", "ccccuuuuuuuu", -11.8, "....(((((((", ".)))))))....");
      testSequences(rnaHybrid, "aaaaggggaaaa", "ccccuuuuuuuu", -15.9, "((((((((....",
          "))))))))....");
      testSequences(rnaHybrid, "aaaaaccggggg", "cccccaauuuuu", -15.2, "(((((..(((((",
          ")))))..)))))");
      testSequences(rnaHybrid, "aaaaccgggg", "ccccuuuu", -11.1, "......((((", "))))....");
      testSequences(rnaHybrid, "gacugcaa", "auagcagc", -9.1, "(.((((..", "...)))))");
      testSequences(rnaHybrid, "TGGACTGCGAGTTGCCAC", "CACCCATCGTCCGTA", -11.1,
          "(((((.............", "........)))))..");
      testSequences(rnaHybrid, "TCAAAGAAAGAGACTCCTTG", "GTCACATAGTAGCATTA", -5.0,
          "............(((.....", ".......))).......");
      testSequences(rnaHybrid, "TCTCATGACTAGCGGTCTGCCATTGTGAA", "GGGGTGGCACAAAATGATTGGGTAGC",
          -17.1, ".......(((..((((((((((((.(...", ").)))))))......))))))))...");
      testSequences(rnaHybrid, "GAUCGAGCC", "CGCUCGAUU", -16.2, "((((((((.", ".))))))))");

    } catch (IOException e) {
      e.printStackTrace();
      fail();
    }
  }

  @Test
  public void testHybrizationDiff() {
    try {
      RnaHybrid rnaHybrid = new RnaHybrid();
      rnaHybrid.setIgnoreDangling(true);
      testSequences(rnaHybrid, "UGGGGGGGG", "CCCCCCCCA", -25.2, "(((((((((", ")))))))))");
      testSequences(rnaHybrid, "AGCACGUUACUUGGGGGGGG", "CCCCCCCCAA", -26.1, "..........((((((((((",
          "))))))))))");
    } catch (IOException ex) {
      Assert.fail("IO error");
    }

  }

  private void testSequences(RnaHybrid rnaHybrid, String seq1, String seq2, double expectedScore,
      String expectedStruct1, String expectedStruct2) {
    rnaHybrid.setSeq1(seq1.toCharArray());
    rnaHybrid.setSeq2(seq2.toCharArray());
    double score = rnaHybrid.hybridize();
    // System.err.println(rnaHybrid);
    assertEquals(expectedScore, score, 0.001);
    assertEquals(expectedStruct1, new String(rnaHybrid.getSeq1Structure()));
    assertEquals(expectedStruct2, new String(rnaHybrid.getSeq2Structure()));
  }

  @Test
  public void testDangling() {
    try {
      // test using CharBuffers and padding
      RnaHybrid rnaHybrid = new RnaHybrid();
      rnaHybrid.setIgnoreDangling(true);
      testSequences(rnaHybrid, "TGGAC", "GTCCG", -9.3, "(((((", ")))))");
      testSequences(rnaHybrid, "TGGACTTTTTTTTTTTTT", "CCCCCCCCGTCCG", -9.3, "(((((.............",
          "........)))))");

    } catch (IOException e) {
      e.printStackTrace();
      fail();
    }
  }

  @Test
  public void testProblematicCase() {
    try {
      // test using CharBuffers and padding
      RnaHybrid rnaHybrid = new RnaHybrid();
      rnaHybrid.setIgnoreDangling(true);
      // accuguucgguacgaaggucguacguuuggagggguaaagcugaugcugcggcugccgagggu
      // ((((..((((((.........................................))))))))))
      testSequences(rnaHybrid, "accuguucggua", "ugccgagggu", -16.4, "((((..((((((", "))))))))))");

      rnaHybrid.setSeq1(new CharBuffer("guucggua"), 0, 8, 10, false);
      rnaHybrid.setSeq2(new CharBuffer("ugccgagg"), 0, 8, 10, true);
      rnaHybrid.hybridize();
      rnaHybrid.elongateSeq1(new CharBuffer("accu"), 0, 4, false);
      rnaHybrid.elongateSeq2(new CharBuffer("gu"), 0, 2, true);
      assertEquals("wrong energy", -16.4, rnaHybrid.hybridize(), 0.01);

      // Sequence ID: query1 , starting at 23 , strand UNKNOWN
      // guucgguacgaaggucguacguuuggagggguaaagcugaugcugcggcugccgaggguaccuuuacauaugccgaac
      // ((((((((..............................................................))))))))
      // energy: -14.700000166893005

      testSequences(rnaHybrid, "guucggua", "ugccgaac", -14.7, "((((((((", "))))))))");

      rnaHybrid.setSeq1(new CharBuffer("guucggua"), 0, 8);
      rnaHybrid.setSeq2(new CharBuffer("ugccgaac"), 0, 8);
      assertEquals("wrong energy", -14.7, rnaHybrid.hybridize(), 0.01);

    } catch (IOException e) {
      e.printStackTrace();
      fail();
    }
  }

  @Test
  public void testElongate() {
    try {
      RnaHybrid rnaHybrid = new RnaHybrid();
      rnaHybrid.setSeq1("CUUUUUUU".toCharArray());
      rnaHybrid.setSeq2("AAAAACAAG".toCharArray());
      double score = rnaHybrid.hybridize();
      System.out.println(new String(rnaHybrid.getSeq1Structure())
          + new String(rnaHybrid.getSeq2Structure()));
      System.out.println(score);
    } catch (IOException e) {
      fail("Can't load energy parameters");
    }

  }

}

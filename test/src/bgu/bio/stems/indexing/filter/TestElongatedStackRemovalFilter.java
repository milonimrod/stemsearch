package bgu.bio.stems.indexing.filter;

import java.util.ArrayList;

import junit.framework.Assert;

import org.junit.Test;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.filtering.ElongatedStackingRemovalFilter;
import bgu.bio.util.CharBuffer;
import bgu.bio.util.alphabet.RnaAlphabet;


public class TestElongatedStackRemovalFilter {

  @Test
  public void testSimple() {
    ElongatedStackingRemovalFilter filter = new ElongatedStackingRemovalFilter(100);
    AnnotatedSequence small =
        AnnotatedSequence.terminalAnnotatedSequence(10, RnaAlphabet.getInstance().nonComplement());
    AnnotatedSequence big =
        AnnotatedSequence.terminalAnnotatedSequence(16, RnaAlphabet.getInstance().nonComplement());

    small.setLocation(20);
    small.setStructure(new CharBuffer("((((..))))"));
    small.setStructureStart(0);
    small.setStructureEnd(9);
    small.setLength(10);

    big.setLocation(17);
    big.setStructure(new CharBuffer("(((((((..)))))))"));
    big.setStructureStart(0);
    big.setStructureEnd(15);
    big.setLength(16);

    filter.push(big);
    filter.push(small);

    ArrayList<AnnotatedSequence> list = new ArrayList<AnnotatedSequence>();
    while (filter.hasRemaining()) {
      list.add(filter.nextRemaining());
    }

    if (list.size() != 1 && list.get(0).equals(big))
      Assert.fail("Got wrong seqeunce from the filter. got: " + list.get(0) + " instead of " + big);
  }


  @Test
  public void testSimple2() {
    ElongatedStackingRemovalFilter filter = new ElongatedStackingRemovalFilter(100);
    AnnotatedSequence small =
        AnnotatedSequence.terminalAnnotatedSequence(10, RnaAlphabet.getInstance().nonComplement());
    AnnotatedSequence big =
        AnnotatedSequence.terminalAnnotatedSequence(17, RnaAlphabet.getInstance().nonComplement());

    small.setLocation(20);
    small.setStructure(new CharBuffer("((((..))))"));
    small.setStructureStart(0);
    small.setStructureEnd(9);
    small.setLength(10);

    big.setLocation(17);
    big.setStructure(new CharBuffer("(((((((..))))).))"));
    big.setStructureStart(0);
    big.setStructureEnd(15);
    big.setLength(16);

    filter.push(big);
    filter.push(small);

    ArrayList<AnnotatedSequence> list = new ArrayList<AnnotatedSequence>();
    while (filter.hasRemaining()) {
      list.add(filter.nextRemaining());
    }

    if (list.size() != 2 && list.get(0).equals(big) && list.get(1).equals(small))
      Assert.fail("Got wrong seqeunces from the filter.");
  }

  @Test
  public void testSimple3() {
    ElongatedStackingRemovalFilter filter = new ElongatedStackingRemovalFilter(100);
    AnnotatedSequence small =
        AnnotatedSequence.terminalAnnotatedSequence(10, RnaAlphabet.getInstance().nonComplement());
    AnnotatedSequence big =
        AnnotatedSequence.terminalAnnotatedSequence(17, RnaAlphabet.getInstance().nonComplement());

    small.setLocation(2000);
    small.setStructure(new CharBuffer("((((..))))"));
    small.setStructureStart(0);
    small.setStructureEnd(9);
    small.setLength(10);

    big.setLocation(17);
    big.setStructure(new CharBuffer("(((((((..))))).))"));
    big.setStructureStart(0);
    big.setStructureEnd(15);
    big.setLength(16);

    filter.push(big);
    filter.push(small);

    if (!filter.hasNext() && !filter.next().equals(big))
      Assert.fail("didn't get the sequence form the filter");

    ArrayList<AnnotatedSequence> list = new ArrayList<AnnotatedSequence>();
    while (filter.hasRemaining()) {
      list.add(filter.nextRemaining());
    }

    if (list.size() != 1 && list.get(0).equals(small))
      Assert.fail("Got wrong seqeunces from the filter.");
  }
}

package bgu.bio.stems.indexing.filter;

import java.io.StringReader;

import org.junit.Test;

import bgu.bio.stems.filereader.FastaFormatReader;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.Strand;
import bgu.bio.stems.indexing.filtering.FilterManager;
import bgu.bio.stems.indexing.filtering.PassAllFilter;
import bgu.bio.stems.indexing.filtering.StemSeedAlignmentFilter;


public class TestPassAllFilter {

  @Test
  public void test1() {
    final int windowSize = 30;
    PassAllFilter f = new PassAllFilter(8, windowSize);
    StemSeedAlignmentFilter f2 = new StemSeedAlignmentFilter(8, 2, 3, 2);
    FilterManager fm = new FilterManager();
    FastaFormatReader reader = new FastaFormatReader(windowSize);
    fm.setSequenceReader(reader);
    fm.putSequenceFilterInPipeline(f);
    fm.putSequenceFilterInPipeline(f2);

    fm.setInput(
        new StringReader(
            ">header\nAAUAUAUCACUGCUACGAUCGAUCAGCUAGCUAGCUACACUAGCUAGUUUGAUCGUUAGCUUUGACUGCGCUAGUCGUA"),
        Strand.PLUS);
    while (fm.hasNext()) {

      AnnotatedSequence as = fm.next();
      System.out.println(as);
      int s = as.getStructureStartPosition();
      int e = as.getStructureEndPosition();
      System.out.println(s + " + " + e);
    }
  }

  public static void main(String[] args) {
    TestPassAllFilter test = new TestPassAllFilter();
    test.test1();
  }
}

package bgu.bio.stems.indexing.filter;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;

import bgu.bio.stems.filereader.FastaFormatReader;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.filtering.TupleFilter;
import bgu.bio.util.alphabet.RnaAlphabet;


public class TestTupleFilter {
  public static void main(String[] args) {
    long start = System.currentTimeMillis();
    System.out.println("Started at " + start);
    int tuple = 8;
    int seed = 3;
    int window = 100;
    int minimumEqual = 2;

    TupleFilter filter = new TupleFilter(tuple, seed, window, minimumEqual, seed);
    // randomTest(filter);
    fastaTest(filter, args[0], window);

    System.out.println("FINISHED " + ((System.currentTimeMillis() - start) / 1000.0) + " (sec)");
  }

  public static void fastaTest(TupleFilter filter, String name, int window) {
    FastaFormatReader reader = new FastaFormatReader(window);
    try {
      reader.setReader(new FileReader(name), false);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
      return;
    }

    while (reader.hasNext()) {
      AnnotatedSequence seq = reader.next();
      System.out.println(seq.getLocation() + seq.getSequence().toString());
      filter.push(seq);
      while (filter.hasNext()) {
        AnnotatedSequence fromFilter = filter.next();
        // System.out.print(Arrays.toString(fromFilter.getSequence().array()));
        // System.out.println(" structure start: " + fromFilter.getStructureStart() +
        // " structure end: " + fromFilter.getStructureEnd());
        System.out.println(" structure start: " + fromFilter.getStructureStart()
            + " structure end: " + fromFilter.getStructureEnd());
        // System.out.println(Arrays.toString(fromFilter.getSequence().array()));
      }
      // pool.releaseSequence(seq);
    }
  }

  public static void randomTest(TupleFilter filter) {
    AnnotatedSequence random =
        AnnotatedSequence.randomAnnotatedSequence(40, RnaAlphabet.getInstance());

    char[] copy =
        new char[] {'g', 'g', 'g', 'u', 'u', 'u', 'u', 'g', 'g', 'a', 'c', 'c', 'g', 'c', 'c', 'a',
            'u', 'a', 'a', 'a', 'g', 'c', 'c', 'c', 'g', 'c', 'c', 'g', 'c', 'g', 'a', 'a', 'u',
            'c', 'c', 'u', 'a', 'g', 'c', 'c'};
    char[] arr = random.getSequence().array();

    for (int i = 0; i < arr.length; i++) {
      arr[i] = copy[i];
    }

    System.out.println(Arrays.toString(random.getSequence().array()));
    filter.push(random);

    System.out.println("After push 1");
    while (filter.hasNext()) {
      System.out.println("next");
      AnnotatedSequence seq = filter.next();
      System.out.println(Arrays.toString(seq.getSequence().array()));
      System.out.println(Arrays.toString(seq.getStructure().array()));
    }

    random = AnnotatedSequence.randomAnnotatedSequence(40, RnaAlphabet.getInstance());
    random.setLocation(1);
    copy =
        new char[] {'g', 'c', 'c', 'g', 'u', 'a', 'u', 'g', 'a', 'c', 'a', 'g', 'a', 'g', 'g', 'u',
            'u', 'c', 'u', 'c', 'a', 'c', 'u', 'a', 'a', 'a', 'u', 'c', 'u', 'c', 'u', 'a', 'c',
            'g', 'g', 'c', 'a', 'c', 'u', 'u'};
    arr = random.getSequence().array();

    for (int i = 0; i < arr.length; i++) {
      arr[i] = copy[i];
    }

    System.out.println(Arrays.toString(random.getSequence().array()));
    random.setLocation(1 + random.getLength());
    filter.push(random);

    System.out.println("After push 2");
    while (filter.hasNext()) {
      System.out.print("next: ");
      AnnotatedSequence seq = filter.next();
      System.out.print(Arrays.toString(seq.getSequence().array()));
      System.out.println(" structure start: " + seq.getStructureStart() + " structure end: "
          + seq.getStructureEnd());

    }
    int location = 41;

    int limit = 500;
    for (int i = 0; i < limit; i++) {
      random = AnnotatedSequence.randomAnnotatedSequence(40, RnaAlphabet.getInstance());
      // System.out.println(Arrays.toString(random.getSequence().array()));
      location += 40;
      random.setLocation(location);
      filter.push(random);

      // System.out.println("After push " + (i+3));
      while (filter.hasNext()) {
        // System.out.print("next: ");
        AnnotatedSequence seq = filter.next();
        // System.out.print(Arrays.toString(seq.getSequence().array()));
        if (seq.getStructureEnd() < 0 || seq.getStructureStart() < 0)
          System.out.println(" structure start: " + seq.getStructureStart() + " structure end: "
              + seq.getStructureEnd());
      }
    }
  }
}

package bgu.bio.stems.indexing.filter;

import java.io.IOException;

import junit.framework.Assert;

import org.junit.Test;

import bgu.bio.stems.hybridization.HybridizationFilter;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.Strand;
import bgu.bio.util.CharBuffer;

public class TestHybridizationFilter {

  @Test
  public void test1() {
    try {
      System.out.println("test1");
      HybridizationFilter filter = new HybridizationFilter(8, -6, 100, -6, 5, -3);
      AnnotatedSequence seq =
          new AnnotatedSequence("header", 1, new CharBuffer("UUGGGGGGGGAAAAAACCCCCCCCAA"), null);

      seq.setStructureStart(2);
      seq.setStructureEnd(23);
      seq.setStrand(Strand.PLUS);

      filter.push(seq);
      if (filter.hasNext()) {
        System.out.println(filter.next());
      }
    } catch (IOException e) {
      e.printStackTrace();
      Assert.fail("Got Error");
    }

  }

  @Test
  public void test2() {
    try {
      System.out.println("test2");
      HybridizationFilter filter = new HybridizationFilter(8, -6, 100, -6, 5, -3, 0);
      AnnotatedSequence seq =
          new AnnotatedSequence(
              "header",
              1,
              new CharBuffer(
                  "AGCACGUUACUUGGGGGGGGAAAAAACCCCCCCCAACNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN"),
              null);

      seq.setStructureStart(12);
      seq.setStructureEnd(33);
      seq.setStrand(Strand.PLUS);

      filter.push(seq);
      if (filter.hasNext()) {
        System.out.println(filter.next());
      }
    } catch (IOException e) {
      e.printStackTrace();
      Assert.fail("Got Error");
    }

  }

  @Test
  public void test2Rev() {
    try {
      System.out.println("test2Rev");
      HybridizationFilter filter = new HybridizationFilter(8, -6, 100, -6, 5, -3, 0);
      AnnotatedSequence seq =
          new AnnotatedSequence(
              "header",
              1,
              new CharBuffer(
                  "AGCACGUUACUUGGGGGGGGAAAAAACCCCCCCCAACNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN"),
              null);
      seq.setStrand(Strand.MINUS);

      seq.setStructureStart(12);
      seq.setStructureEnd(33);

      filter.push(seq);
      if (filter.hasNext()) {
        System.out.println(filter.next());
      }
    } catch (IOException e) {
      e.printStackTrace();
      Assert.fail("Got Error");
    }

  }

  @Test
  public void test3() {
    try {
      System.out.println("test real sequence from human genome");
      HybridizationFilter filter = new HybridizationFilter(8, -6, 200, -6, 5, -3, 5);
      AnnotatedSequence seq =
          new AnnotatedSequence(
              "header",
              844401,
              new CharBuffer(
                  "CCCCGGGCGCACCGTTGCTGGTATATGCGGGGGTCGGGGTCAGGCCCCCGGGCGCACCGTTGCTGGTATATGCGGTGGTCGGGGTCAGGCCCCCCGGGCGCACTGTTGCTGGTATATGCGGTGGTCGGGGTCAGGCCCCCGGGCGCACCTTTGCTGGTATATGCGGGGGTCGGGGTCAGGCCCCCGGGCGCACCGTTGCTGGTATATGCGGGGGTCGGGGTCAGGCCCCCGGGCGTTGCTGGTATATGCGGGGGTCGGGGTCAGGCCCCCGGGCGCACCGTTGCTGGTATATGCGGTGGTCGGGGTCAGGCCCCCGGGCGCACCGTTGCTGGTATATGCGGGGGTCGGGGTCAGGCCCCCGGGCGCACCGTTGCTGGTATATGCGGTGGTCGGGGTCAGGCCCCCGGGCGCATCTTTGCTGGTATATGCGGTGGTCGGGGTCAGGCCCCCCGGGCGCACTGTTGCTGGTATATGCGGTGGTCGGGGTCAGGCCCCCGGGCGCACCTTTGCTGGTATATGCGGGGGTCGGGGTCAGGCCCCCGGGCGCACCGTTGCTGGTATATGCGGGGGTCGGGGTCAGGCCCCCGGGCGTTGCTGGTATATGCGGTGGTCGGGGTCAGGCCCCCGGGCGCACCGTTGCTGGTATATGCGGGGGTCGGGGTCAGGCCCCCGGGCGCACCGTTGCTGGTATATGCGGTGGTCGGGGTCAGGCCCCCGGGCGCACCGTTGCTGGTATATGCGGGGGTCGGGGTCAGGCCCCCGGGCGTTGCTGGTATATGCGGGGGTCGGGGTCAGGCCCC"),
              null);
      seq.setStrand(Strand.PLUS);

      seq.setStructureStart(415);
      seq.setStructureEnd(598);

      filter.push(seq);
      while (filter.hasNext()) {
        System.out.println(filter.next());
      }
    } catch (IOException e) {
      e.printStackTrace();
      Assert.fail("Got Error");
    }
  }

  public static void main(String[] args) {
    TestHybridizationFilter test = new TestHybridizationFilter();
    test.test2();
  }
}

package bgu.bio.tools.utils;

import java.util.Random;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestHashing {

  Hashing hashing;
  char[] letters = new char[] {'A', 'C', 'G', 'U'};
  Random rand = new Random();

  @Before
  public void setUp() {
    hashing = new Hashing();

  }

  @Test
  public void test1() {
    check("AUCGGAUC");
  }
  
  @Test
  public void test2() {
    String input = "UUUUUUUUUUUUUUUU";
    long num = hashing.hashSeq(input);
    System.out.println(num);
    StringBuilder sb = new StringBuilder(100);
    hashing.unhash(num, input.length(), sb);
    Assert.assertEquals(input, sb.toString());
  }

  @Test
  public void testRandom() {
    int amount = 10000000;
    int MAX_LENGTH = 30;

    for (int i = 0; i < amount; i++) {
      check(shuffle(rand.nextInt(MAX_LENGTH - 1) + 1));
    }

  }

  private String shuffle(int length) {
    String ans = "";
    for (int i = 0; i < length; i++) {
      ans += letters[rand.nextInt(letters.length)];
    }
    return ans;
  }

  private void check(String input) {
    long num = hashing.hashSeq(input);
    StringBuilder sb = new StringBuilder(100);
    hashing.unhash(num, input.length(), sb);
    Assert.assertEquals(input, sb.toString());
  }

}

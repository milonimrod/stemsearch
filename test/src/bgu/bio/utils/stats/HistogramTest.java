package bgu.bio.utils.stats;

import gnu.trove.list.array.TDoubleArrayList;

import org.junit.Assert;
import org.junit.Test;

public class HistogramTest {

  @Test
  public void test1() {
    Histogram hist = new Histogram();
    TDoubleArrayList data = new TDoubleArrayList();
    hist.setNumOfBins(6);
    data.add(new double[] {1, 2, 2, 3, 3, 3, 3, 4, 4, 5, 6});
    hist.setData(data);
    Assert.assertArrayEquals(new int[] {1, 2, 4, 2, 1, 1}, hist.calcHistogram());
  }

  @Test
  public void test2() {
    Histogram hist = new Histogram();
    TDoubleArrayList data = new TDoubleArrayList();
    hist.setNumOfBins(6);
    data.add(new double[] {0.1, 0.2, 0.2, 0.3, 0.3, 0.3, 0.3, 0.4, 0.4, 0.5, 0.6});
    hist.setData(data);
    Assert.assertArrayEquals(new int[] {1, 2, 4, 2, 1, 1}, hist.calcHistogram());
  }

}

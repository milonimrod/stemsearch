package bgu.bio.indexing;

import java.io.ByteArrayInputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import bgu.bio.adt.tuples.Triplet;
import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.indexing.RNASequenceHuffmanCoding;
import bgu.bio.stems.indexing.RNAStructureHuffmanCoding;
import bgu.bio.stems.mongodb.StemFieldsConstants;
import bgu.bio.stems.mongodb.encoders.MongoEncoder;
import bgu.bio.util.CharBuffer;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class TestMongoDBIndexer {

  private final String path = "test/mongo/indexer/";
  private MongoClient mongoClient;
  private DB db;

  @After
  public void tearDown() {
    // delete the database
    System.out.println("Drop");
    db.dropDatabase();

    mongoClient.close();
  }

  @Before
  public void setUp() {
    try {
      mongoClient = new MongoClient("localhost", 27017);
      db = mongoClient.getDB("testingStemSearch");
    } catch (UnknownHostException e) {
      // e.printStackTrace();
      Assume.assumeNoException("Skipped because can't reach mongoDB server", e);
    }

  }

  @Test
  public void test1() {
    ArrayList<MongoEncoder> list = new ArrayList<MongoEncoder>();
    EncoderForDebug debugEncoder = new EncoderForDebug();
    list.add(debugEncoder);

    MongoDBIndexer indexer = new MongoDBIndexer(list);
    indexer.loadParams(new String[] {"-e", path + "/indexing.properties", "-col", "test1", "-db",
        "testingStemSearch", "-f", path + "test.fasta", "-o"});
    indexer.run();
    // run a search on all the stems to see if they find them all.
    DBCollection col = db.getCollection("test1");
    DBCursor cur = col.find();

    RNASequenceHuffmanCoding huffSequence = new RNASequenceHuffmanCoding();
    RNAStructureHuffmanCoding huffStructure = new RNAStructureHuffmanCoding();
    while (cur.hasNext()) {
      DBObject obj = cur.next();
      String sequence =
          huffSequence.decode(new ByteArrayInputStream((byte[]) obj
              .get(StemFieldsConstants.SEQUENCE)));

      String structure =
          huffStructure.decode(new ByteArrayInputStream((byte[]) obj
              .get(StemFieldsConstants.STRUCTURE)));

      int position = (int) obj.get(StemFieldsConstants.POSITION_START);
      // search if the sequence and structure are in the data list
      check(position, sequence, structure, debugEncoder.data);

    }
    cur.close();

    // check that all the stems are found
  }

  private void check(int position, String sequence, String structure,
      ArrayList<Triplet<Integer, String, String>> data) {
    boolean found = false;
    for (Triplet<Integer, String, String> seq : data) {
      if (seq.getFirst().intValue() == position) {
        if (sequence.equals(seq.getSecond()) && structure.equals(seq.getThird())) {
          found = true;
          break;
        }
      }
    }
    if (!found) {
      Assert.fail("Indexing has damaged the sequence: " + sequence + ":" + structure);
    }

  }

}


class EncoderForDebug implements MongoEncoder {

  ArrayList<Triplet<Integer, String, String>> data =
      new ArrayList<Triplet<Integer, String, String>>();

  @Override
  public void encode(AnnotatedSequence sequence, DBObject obj) {
    CharBuffer seq = sequence.getSequence();
    CharBuffer structure = sequence.getStructure();

    int lastOpen = structure.lastIndexOf('(');
    int firstClose = structure.indexOf(')');
    String seqData =
        seq.substring(0, lastOpen + 1) + seq.substring(firstClose, structure.length() - firstClose);
    String strData =
        structure.substring(0, lastOpen + 1)
            + structure.substring(firstClose, structure.length() - firstClose);
    Triplet<Integer, String, String> t =
        new Triplet<Integer, String, String>(sequence.getStructureStartPosition(), seqData, strData);
    synchronized (data) {
      data.add(t);
    }

  }

}

package bgu.bio.indexing.query;

import org.junit.Assert;
import org.junit.Test;

import bgu.bio.stems.indexing.AnnotatedSequence;
import bgu.bio.stems.mongodb.util.AnnotatedSequenceFeatureExtraction;
import bgu.bio.util.CharBuffer;

public class TestStacking {

  @Test
  public void test() {
    AnnotatedSequence seq =
        new AnnotatedSequence("ggg", 1, new CharBuffer("ACAAUGCUACAUCCCGGGAGCCGAAAGC"),
            new CharBuffer(".....(((...((((:))))......)))"));
    Assert.assertEquals(3.5, AnnotatedSequenceFeatureExtraction.getAverageLength(seq), 0.0001);
    Assert.assertEquals(0.5, AnnotatedSequenceFeatureExtraction.getSTDLength(seq), 0.0001);

    // CAAGAGUGGCAGAGGGU,.....((.((...((((:)))).)).)),GCCCAGCAAC
    AnnotatedSequence seq2 =
        new AnnotatedSequence("ggg", 1, new CharBuffer("CAAGAGUGGCAGAGGGUGCCCAGCAAC"),
            new CharBuffer(".....((.((...(((()))).)).))"));
    Assert.assertEquals(8.0 / 3.0, AnnotatedSequenceFeatureExtraction.getAverageLength(seq2),
        0.0001);
    Assert.assertEquals(0.94280, AnnotatedSequenceFeatureExtraction.getSTDLength(seq2), 0.0001);

  }

  @Test
  public void test2() {
    // ,,
    AnnotatedSequence seq =
        new AnnotatedSequence("ggg", 1, new CharBuffer("AGCCCAGCAACCUAGGUGCU"), new CharBuffer(
            ".....((((.((()))))))"));
    Assert.assertEquals(3.5, AnnotatedSequenceFeatureExtraction.getAverageLength(seq), 0.0001);
    Assert.assertEquals(0.5, AnnotatedSequenceFeatureExtraction.getSTDLength(seq), 0.0001);

    AnnotatedSequence seq2 =
        new AnnotatedSequence("ggg", 1, new CharBuffer("AAACUUUGGCUUUGCAUUGCGUGAGCAUUCCCAA"),
            new CharBuffer(".....((((...(((.(((()))))))...)))),"));
    Assert.assertEquals(3.6666, AnnotatedSequenceFeatureExtraction.getAverageLength(seq2), 0.0001);
    Assert.assertEquals(0.4714, AnnotatedSequenceFeatureExtraction.getSTDLength(seq2), 0.0001);

  }

}

#!/usr/bin/python
#this script get as input a file containning two sequences in 5'to3' seperated by tabs.
#the script then run RNAhybrid file (need to be compiled on the computer) then write the hybridzation in Vienna format (assume that the sequences are connected by loop.

import sys
import os
import subprocess

def printEnd(str,s1,s2,start):
	ind1 = s1.find(s1.strip())
	ind2 = s2.find(s2.strip())
	sInd = min(ind1,ind2)
	eInd = max(len(s1.rstrip()),len(s2.rstrip()))
	trimed1 = s1[sInd:eInd]
	trimed2 = s2[sInd:eInd]
	
	#print "\n***" + trimed1 + "***"
	#print "\n***" + trimed2 + "***"
	
	i = 0
	joined = ""
	while (i < len(trimed1)):
		if (trimed1[i] == ' ' and trimed2[i] != ' '):
			joined = joined + trimed2[i]
		elif (trimed2[i] == ' ' and trimed1[i] != ' '):
			joined = joined + trimed1[i]
		i=i+1

	size =  len(str) - str.find(joined,0) - len(joined)
	#print str,len(str),start,str.find(joined,0),joined,len(joined),size
	for i in range(0,size):
		sys.stdout.write(".")

def trim(s1,s2,c="("):
	ind1 = s1.find(s1.strip())
	ind2 = s2.find(s2.strip())
	sInd = min(ind1,ind2)
	eInd = max(len(s1.rstrip()),len(s2.rstrip()))
	trimed1 = s1[sInd:eInd]
	trimed2 = s2[sInd:eInd]
	
	#print "\n***" + trimed1 + "***"
	#print "\n***" + trimed2 + "***"
	#sys.stdout.write("***")
	i = 0
	while (i < len(trimed1)):
		if (trimed1[i] == ' ' and trimed2[i] != ' '):
			sys.stdout.write(c)
		elif (trimed2[i] == ' ' and trimed1[i] != ' '):
			sys.stdout.write(".")
		i=i+1
	#while (i < len(trimed2)):
	#	if (trimed2[i] == ' '):
	#		sys.stdout.write(".")
	#	else:
	#		sys.stdout.write(c)
	#	i=i+1

of = open(sys.argv[1],'r')

padd = "NNNNNNNNNN"
paddLen = len(padd)
loop = ".........."
loopLen = len(loop)
numOfLine = 0
for line in of:
	#print line
	numOfLine += 1
	sp = line.split()
	first = padd + sp[0] + padd
	second = padd + sp[1] + padd
	pro = subprocess.Popen(["RNAhybrid","-c","-s","3utr_human",first,second],stdout=subprocess.PIPE)
	output = pro.communicate()[0]
	
	#remove this
	#subprocess.call(["RNAhybrid","-s","3utr_human",first,second])
	
	spOut = output.replace("\n","").replace("N"," ").split(":")
	energy = spOut[4]
	pos = int(spOut[6]) - paddLen

	line1 = spOut[7]
	line2 = spOut[8]
	line3 = spOut[9][::-1]
	line4 = spOut[10][::-1]
	
	#print line3
	#print line4

	sys.stdout.write(">test" + str(numOfLine) + ",dG=" + energy + "\n")
	#for i in range(1,pos):
	#	sys.stdout.write("N")

	#print the sequences
	sys.stdout.write(sp[0])
	sys.stdout.write(loop)
	sys.stdout.write(sp[1])
	sys.stdout.write("\n")
	
#	print spOut[6],paddLen

	#print seq1 dot padding
	for i in range(1,pos):
		sys.stdout.write(".")
	
	trim(line1,line2,"(")
	
	#calculate seq2 dot padding
	printEnd(sp[1],line4.lower(),line3.lower(),pos)

	for i in range(0,loopLen):
		sys.stdout.write(".")
	
	trim(line4,line3,")")	
	sys.stdout.write("\n")



#!/bin/bash 

# 1 - the sequence
# 2 - the structure
# 3 - the name
pid=$$
echo "ddddd"
echo -e ">header$pid\n$1\n$2" > temp.fasta$pid
RNAplot -o svg < temp.fasta$pid
./scripts/1.py "header$pid""_ss.svg"
convert -resize 240x240 "header$pid""_ss.svg" "header$pid""_ss.png"
rm -rf temp.fasta$pid "header$pid""_ss.svg"
mv "header$pid""_ss.png" "$3"
#mv "header$pid""_ss.svg" "$3.svg"

#!/usr/bin/python
#this script get as input a file containning two sequences in 5'to3' seperated by tabs.
#the script then run RNAhybrid file (need to be compiled on the computer) then write the hybridzation in Vienna format (assume that the sequences are connected by loop.

import sys
import os

of = open(sys.argv[1],'r')
numOfLine = 0
line = of.readline()
while line:
	#print line
	numOfLine = numOfLine + 1
	sp = line.split(',')
	structure = sp[1]
	sequence = sp[0]
	
	right = structure.find(')')
	left = structure.rfind('(')
	endRight = structure.rfind(')')
	endLeft = structure.find('(')
	tStr = structure[endLeft:left+1]
	tStr = tStr + structure[right:endRight+1]
	
	tSeq = sequence[endLeft:left+1]
	tSeq = tSeq + sequence[right:endRight+1]
	
	print tSeq + "," + tStr + "," + sp[2].strip()
		
	if (numOfLine % 1000 == 0):
		print "progress:" + str(numOfLine)
	line = of.readline()



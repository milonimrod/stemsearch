#! /usr/bin/python
import sys

f = open(sys.argv[1],'r')

lines = f.readlines()
f.close()

x=""
y=""
i=-1
for line in lines:
	i=i+1
	if (line.find("@") != -1):
		sp = line.split("\"",4)
		x=str(sp[1])
		y=str(sp[3])
		lines[i] = "\n"
	else:
		lines[i] = line.replace('>N<',' style=\"fill:gray\">N<')
		
f = open(sys.argv[1],'w')
for line in lines:
	if (line.find(x + "," + y) != -1):
		f.write("\"/> <polyline style=\"stroke: gray; fill: none; stroke-width: 1.5\" id=\"outline\" points=\" \n")
	else:
		f.write(line.replace('polyline style="stroke: black;','polyline style="stroke: gray;').replace('<g style="stroke: black; stroke-width: 1" id="pairs">','<g style="stroke: gray; stroke-width: 1; stroke-dasharray:1 1 1 1;" id="pairs">'))
f.close()

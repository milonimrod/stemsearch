#!/bin/bash
while read -r -a myArray
do
	DIR=${myArray[0]}
	echo "Running on family ${myArray[1]} - ${myArray[0]}"
	
	echo "Checking md5 of input files"
	md5sum "$DIR/db.fasta" "$DIR/indexing.properties" > "/tmp/tmp.$DIR.$$"
	
	if [ ! -f "$DIR/md5" ];
	then
		echo "" > "$DIR/md5"
	fi
	
	if diff "/tmp/tmp.$DIR.$$" "$DIR/md5" >/dev/null ; then
		echo "input files didn't change"
	else		
		echo "creating blast DB"
		makeblastdb -in "$DIR/db.fasta" -dbtype nucl -out $DIR/$DIR.db
		echo "creating StemSearch index"
		stemsearch_index -t 10 -s cbnegev4.cs.bgu.ac.il -col ${myArray[1]} -db StemSearch-benchmark $DIR/db.fasta
		#java -cp $HOME/bin/StructureIndexer.jar bgu.bio.stems.indexing.SequenceIndexer $DIR/indexing.properties $DIR/db.fasta $DIR/index
		if [ $? -eq 0 ]; then
			md5sum "$DIR/db.fasta" "$DIR/indexing.properties" > "$DIR/md5"
		fi
	fi
	rm -rvf "/tmp/tmp.$DIR.$$"
done < data.txt

#!/bin/bash

while read -r -a myArray
do
	DIR=${myArray[0]}
	#makeblastdb -in "$DIR/db.fasta" -dbtype nucl -out "$DIR/$DIR.db"
	#blastn -out "$DIR/blast-local.txt" -outfmt "7" -strand plus -query $DIR/query.fasta -db "$DIR/$DIR.db"
	java -cp $HOME/bin/StructureIndexer.jar bgu.bio.benchmark.search.sequence.SummarizeBenchmarkResults "`pwd`/$DIR"
done < data.txt

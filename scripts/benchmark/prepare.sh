#!/bin/bash

while read -r -a myArray
do
	DIR=${myArray[0]}
	echo "Running on family ${myArray[1]} - ${myArray[0]}"

	echo "" > "$DIR/md5"

	rm -rf "$DIR/indexing.properties"
	rm -rf "$DIR/search.properties"
	ln -s "`pwd`/indexing.properties" "$DIR/"
	ln -s "`pwd`/search.properties" "$DIR/"

	echo "preparing the random data set with implented results"
	java -cp $HOME/bin/StructureIndexer.jar bgu.bio.benchmark.search.sequence.PrepareData db.fasta "$DIR/query.fasta" "$DIR/db.fasta" "$DIR/info.txt" "${myArray[1]} - ${myArray[0]}"
done < data.txt

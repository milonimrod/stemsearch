#!/bin/bash

while read -r -a myArray
do
	DIR=${myArray[0]}
	echo "Running on family ${myArray[1]} - ${myArray[0]}"
	
	#only run on completly indexed directories
	
	if [ ! -f "$DIR/index/write.lock" -a -d "$DIR/index" ];
	then
		MD5FILE="$DIR/md5-search"
		TMP="/tmp/tmp-run.$DIR.$$"
		md5sum $DIR/index/* "$DIR/indexing.properties" > $TMP

		if [ ! -f $MD5FILE ];
		then
			echo "" > $MD5FILE
		fi
		
		if diff $TMP $MD5FILE >/dev/null ; then
			echo "input files didn't change"
		else
			echo "running BLAST search"
			#makeblastdb -in "$DIR/db.fasta" -dbtype nucl -out "$DIR/$DIR.db"
			blastn -out "$DIR/blast-local.txt" -outfmt "7" -strand plus -query $DIR/query.fasta -db "$DIR/$DIR.db"
		
			#cp -f search.properties "$DIR/"
			echo "running StemSearch benchmark"
			#-Xrunhprof:cpu=samples,lineno=y,depth=10,cutoff=0.0025,interval=1
			java -Xmx5g -cp $HOME/bin/StructureIndexer.jar bgu.bio.benchmark.search.sequence.BenchmarkQuery "`pwd`/$DIR"
			#java -Xmx5g -Xrunjdwp:transport=dt_socket,address=8001,server=y,suspend=y -cp exec/StructureIndexer.jar bgu.bio.benchmark.search.sequence.BenchmarkQuery "`pwd`/$DIR"

			if [ $? -eq 0 ]; then
				md5sum $DIR/index/* "$DIR/indexing.properties" > $MD5FILE
			fi
		fi
		rm -rvf $TMP

	else
		echo "Skipping $DIR, no index or write locked"
	fi
done < data.txt

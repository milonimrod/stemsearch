#!/usr/bin/python
#this script get as input a file containning two sequences in 5'to3' seperated by tabs.
#the script then run RNAhybrid file (need to be compiled on the computer) then write the hybridzation in Vienna format (assume that the sequences are connected by loop.

import sys
import os

def getStack(structrue,left,right):
	stack = 0
	while (right + stack < len(structure) and left - stack >= 0 and structure[right+stack] == ')' and structure[left-stack] == '('):
		stack = stack + 1
	return stack
of = open(sys.argv[1],'r')
numOfLine = 0
line = of.readline()
while line:
	#print line
	numOfLine = numOfLine + 1
	sp = line.split(',')
	structure = sp[1]
	energy = float(sp[2])
	if (energy < -6):
		#check if has stack smaller than 2
		right = structure.find(')')
		left = structure.rfind('(')
		limit = 2
		stack = getStack(structure,left,right)
		if (stack < limit):
			right2 = structure.find(')',right+stack+1)
			left2 = structure.rfind('(',0,left-stack-1)
			if (left >= 0 and right >= 0):
				s2 = getStack(structure,left2,right2)
				if (s2 < limit):
					print line
	if (numOfLine % 1000000 == 0):
		print "progress:" + str(numOfLine)
	line = of.readline()



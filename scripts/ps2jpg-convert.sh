#! /bin/bash
#this script convert ps files to jpg images
#assumes that ImageMagick is installed 
for i in `ls -C1 *.ps`
do
	convert -verbose -comment “$i” $i out_$i.jpg
done 

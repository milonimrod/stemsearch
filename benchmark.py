#!/usr/bin/env python2

#import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as ppl
#import prettyplotlib as ppl
import numpy as np
import math
import glob
import sys
import collections
import os.path
import matplotlib.cm as cm

def convert2Numbers(vec,prob=0):
	points = []
	for v in vec:
		if (not v == ""):
			points.append(float(v))
		else:
			points.append(prob)
	return points
def readCSV(filename):
	f = open(filename,"r")
	#read headers
	tmp = {}
	headerArray = f.readline().split("\t")
	i = 0
	for header in headerArray:
		tmp[i] = []
		i = i + 1
	
	#read lines
	line = f.readline()
	while line:
		sp = line.split("\t")
		i = 0
		for data in sp:
			tmp[i].append(data.strip())
			i = i + 1
		line = f.readline()
	
	f.close()
	dict = {}
	i = 0
	for header in headerArray:
		dict[header.strip()] = tmp[i]
		i = i + 1
	del tmp
	return dict
		
filenames = glob.glob(sys.argv[1] + '/output*_summary.csv')
for f in filenames:
	data = readCSV(f)
	sp = f.split("/")
	headersForBoxPlot = sp[len(sp)-2]
	dataYRecall = convert2Numbers(data["Stemsearch - recall"])
	dataXRecall = convert2Numbers(data["BLAST - recall"])

	fig, ax = ppl.subplots(figsize=(12,9), dpi=72, facecolor="white")
	#count 
	data = []
	for x in range(0,len(dataXRecall)):
		data.append((dataXRecall[x],dataYRecall[x]))
	
	count = collections.Counter(data)
	sizes = np.array(count.values())*2
	points = count.keys()
	x, y = zip(*points)
	ax.scatter(x, y,s=sizes, marker='o',c=sizes,linewidths=2)
	ax.set_alpha(0.75)
	ax.set_ylabel('StemSearch')
	ax.set_xlabel('BLAST')
	ax.set_title(headersForBoxPlot)
	x = max(max(dataXRecall),max(dataYRecall))
	
	ax.plot((0,x),(0,x), "r--")
	
	fig.savefig(headersForBoxPlot + config + '-scatter.png', format='PNG')
	fig.clf()
